<?php
/**
 * @var \iSite $this
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

defined('_WPF_') or die();

function createVhosts($site)
{

	//открываем файл vhost-ов
	$vhostfile='/etc/httpd/conf.d/vhosts.conf';
	if(!$vh=file_get_contents($vhostfile))
	{
		return false;
	}

	//проходим все сайты, внесенные в системы
	$query="SELECT * FROM sites";
	if($res=$site->dbquery($query))
	{
		if(is_array($res) && count($res) > 0)
		{
			foreach ($res as $row)
			{
				$ndomains=explode(';',$row['domains']);
				$domains=Array();

				foreach ($ndomains as $domain){
					$domains[]=str_replace(' ','',$domain);
				}

				if(strpos($vh,$domains[0]) === false)
				{
					$aliasstr='';

					if(count($domains) > 1)
					{
						$i=-1;
						foreach ($domains as $domain)
						{
							++$i;
							if($i > 0)
							{
								$aliasstr.=$domain.' ';
							}
						}
					}

					//дописываем виртуальный хост, если нет о нем записи
					$newhost='

<VirtualHost *:80>
    ServerAdmin webmaster@khabkrai.ru
    DocumentRoot /home/habkrai/data/www/html.3/oiv/'.$row['file_folder'].'
    ServerName '.$domains[0].'
    DirectoryIndex index.php
    ServerAlias '.$aliasstr.'
</VirtualHost>

';

					$vh.=$newhost;

				}
			}

			if(file_put_contents($vhostfile, $vh))
			{
				return true;
			}
		}
	}


	return false;
}

function editSettings($site)
{
	//массив доменов сайта
	$domains=explode(';',$site['domains']);

	//массив замен
	$changesArr=Array();

	switch ($site['type_id'])
	{
		case '1':
			$typename='министерства';
			break;

		case '2':
			$typename='района';
			break;

		case '3':
			$typename='поселения';
			break;

		default:
			$typename='';
			break;
	}

	$changesArr['{dbname}']=$site['dbname'];
	$changesArr['{dblogin}']=$site['dblogin'];
	$changesArr['{firstdomain}']=$domains[0];
	$changesArr['{sitename}']=$site['name'];
	$changesArr['{sitetypename}']=$typename;
	$changesArr['{siteemblem}']=$site['emblem'];
	$changesArr['{siteflag}']=$site['flag'];

	//открываем файл настроек
	$settingsfile=WPF_ROOT.'/oiv/'.$site['file_folder'].'/settings.php';
	if(file_exists($settingsfile))
	{
		$sf=file_get_contents($settingsfile);

		foreach ($changesArr as $key=>$value)
		{
			$sf=str_replace($key,$value,$sf);
		}

		file_put_contents($settingsfile,$sf);

		return true;
	}
	else
	{
		return false;
	}
}

function setActive($site)
{
	if($site->values->id != '')
	{
		$query="SELECT * FROM sites WHERE id = ".$site->values->id;
		if($res=$site->dbquery($query)){

			$folder_exist=false;
			$db_exist=false;

			$row=$res[0];
			//проверка наличае директории сайта
			$fullpatch=WPF_ROOT.'/oiv/'.$row['file_folder'];
			$folder_exist=file_exists($fullpatch);

			//проверка наличая БД сайта
			$dbuser=$row['dblogin'];
			$dbname=$row['dbname'];

			$testconnect=pg_connect("host=".$site->settings->DB->host.
				" port=".$site->settings->DB->port.
				" dbname=".$dbname.
				" user=".$dbuser.
				" password=".$site->settings->DB->pass.
				" options='--client_encoding=UTF8'");

			if (is_resource($testconnect))
			{
				$db_exist=true;
				pg_close($testconnect);
			}

			if($folder_exist && $db_exist){
				$newstatus=1;
			}
			elseif($folder_exist)
			{
				$newstatus=3;
			}
			elseif($db_exist)
			{
				$newstatus=4;
			}
			else
			{
				$newstatus=2;
			}

			//смена статуса
			if($newstatus != $row['status_id'])
			{
				$query="UPDATE sites SET status_id = $newstatus WHERE id = ".$site->values->id;
				$res=$site->dbquery($query);
			}
		}
	}
}

/*function fullCopy($sourceDir,$destDir){
	//$sourceDir = 'path/to/source/dir';
	//$destDir   = 'path/to/dest/dir';

	if(!file_exists($sourceDir)){
		return false;
	}

	if (!file_exists($destDir)) {
		mkdir($destDir, 0755, true);
	}

	$dirIterator = new RecursiveDirectoryIterator($sourceDir, RecursiveDirectoryIterator::SKIP_DOTS);
	$iterator    = new RecursiveIteratorIterator($dirIterator, RecursiveIteratorIterator::SELF_FIRST);

	foreach ($iterator as $object) {
		$destPath = $destDir . DIRECTORY_SEPARATOR . $iterator->getSubPathName();
		($object->isDir()) ? mkdir($destPath) : copy($object, $destPath);
	}

	return true;
}*/
function fullCopy($source, $dest, $over=false)
{
	if(!is_dir($dest))
		mkdir($dest);
	if($handle = opendir($source))
	{
		while(false !== ($file = readdir($handle)))
		{
			if($file != '.' && $file != '..')
			{
				$path = $source . '/' . $file;
				if(is_link($path))
				{
					$linkpatch=readlink($path);

					//krumo($linkpatch,$path);

					//$linkpatch=str_replace('../../','/home/habkrai/data/www/html.3/',$linkpatch);

					symlink($linkpatch,$dest . '/' . $file);
				}
				elseif(is_file($path))
				{
					if(!is_file($dest . '/' . $file || $over))
						if(!@copy($path, $dest . '/' . $file))
						{
							echo "('.$path.') Ошибка!!! ";
						}
				}
				elseif(is_dir($path))
				{
					if(!is_dir($dest . '/' . $file))
						mkdir($dest . '/' . $file);
					fullCopy($path, $dest . '/' . $file, $over);
				}
			}
		}
		closedir($handle);
	}
}

if ( ! $this->requireAuthUser()->can('admin.sysoptions')) {
	$this->endRequest(403);
}

$this->data->title = 'Управление сайтами';
$this->data->iH1 = 'Управление сайтами';
$this->data->keywords = 'тут, ключевые, слова';
$this->data->description = 'тут описание раздела';
$this->data->breadcrumbsmore = '';
$this->data->errortext = '';

//Проверяем на существование таблицы сайтов
$query="SELECT count(id) FROM sites";
if($res = $this->dbquery($query))
{

	$query="SELECT emblem FROM sites";
	if(!$res=$this->dbquery($query))
	{
		//разворачиваем доп. настройки в таблице сайтов
		$query="ALTER TABLE sites ADD COLUMN emblem character varying(255)";
		$res=$this->dbquery($query);

		$query="ALTER TABLE sites ADD COLUMN flag character varying(255)";
		$res=$this->dbquery($query);
	}

    $query="SELECT parentid FROM sites";
	if(!$res=$this->dbquery($query))
	{
        $query="ALTER TABLE sites ADD COLUMN parentid integer";
        if($res=$this->dbquery($query))
        {
            $query2="CREATE INDEX sites_parentid_idx
            ON sites
            USING btree
            (parentid)";
        };

        $this->dbquery($query2);
    }

}
else
{

	echo 'Создаём таблицы';

	//если нет, создаем таблицы сайтов портала и их настроек
	//создаём таблицу сайтов
	$query1="CREATE TABLE sites
	(
  id integer NOT NULL,
  name character varying(255),
  dbname character varying(127),
  dblogin character varying(127),
  file_folder character varying(127),
  domains character varying(255),
  type_id integer,
  status_id integer,
  CONSTRAINT sites_pkey PRIMARY KEY (id)
	)";

	$query2="CREATE INDEX sites_type_id_idx
  ON sites
  USING btree
  (type_id)";

	$query2="CREATE INDEX sites_status_id_idx
  ON sites
  USING btree
  (status_id)";

	$query3="CREATE TABLE sites_types
	(
  id integer NOT NULL,
  name character varying(255),
  templates_folder character varying(127),
  CONSTRAINT sites_types_pkey PRIMARY KEY (id)
	)";

	$this->dbquery($query1);
	$this->dbquery($query2);
	$this->dbquery($query3);

}

//получаем действие
$this->getPostValues(Array('action','showType'));

//действия над сайтом
switch ($this->values->action){
	case 'add':
		//добавление сайта
		$this->getPostValues(Array('name','dbname','dblogin','file_folder','domains','type_id','status_id','emblem','flag','parentid'));

		if($this->values->name != ''){

			$mid=-1;
			$query="SELECT max(id) mid FROM sites";
			if($res=$this->dbquery($query)){
				if(is_array($res) && count($res) > 0){
					$mid=$res[0]['mid'];
				}
			}

			++$mid;

			$this->values->status_id=0;

			if($this->values->parentid === '')
			{
                $this->values->parentid='NULL';
            }

			$query="INSERT INTO sites(id, name, dbname, dblogin, file_folder, domains, type_id, status_id, emblem, flag, parentid)
			values($mid,
			'".$this->values->name."',
			'".$this->values->dbname."',
			'".$this->values->dblogin."',
			'".$this->values->file_folder."',
			'".$this->values->domains."',
			".$this->values->type_id.",
			".$this->values->status_id.",
			'".$this->values->emblem."',
			'".$this->values->flag."',
			".$this->values->parentid.")";

			//echo $query;

			if($res=$this->dbquery($query)){
				$this->data->errortext='Запись о сайте добавлена';
			}
			else
			{
				$this->data->errortext='Ошибка при добавлении сайта';
			}
		}

		break;

	case 'edit':

		$this->getPostValues(Array('id', 'type_id', 'parentid'));

		//если передан id сайта
		if($this->values->id != '')
		{

            if($this->values->parentid === '')
            {
                $this->values->parentid='NULL';
            }

			//изменяем тип и родителя сайта
			if($this->values->type_id != '')
			{
				$query="UPDATE sites SET type_id = ".$this->values->type_id.", parentid = ".$this->values->parentid." WHERE id = ".$this->values->id;
				$res=$this->dbquery($query);
			}

		}

		break;
	case 'check':
		//провека сайта на наличае директории и БД

		setActive($this);

		break;
	case 'start':

		$dbcomplite=false;
		$filecomplite=false;

		if($this->values->id != '')
		{
			$query="SELECT * FROM sites WHERE id = ".$this->values->id;
			if($res=$this->dbquery($query))
			{
				$row=$res[0];

				if(in_array($row['status_id'], Array(2,3,4)))
				{

					//определяем шаблоны сайтов и папок в зависимости от типа сайта
					$template="template_".$row['type_id'];

					//krumo($row);

					//разворачиваем файловую структуру
					if(in_array($row['status_id'], Array(2,4)))
					{


						$sourceDir=WPF_ROOT.'/oiv/'.$template;
						$destDir=WPF_ROOT.'/oiv/'.$row['file_folder'];

						//krumo($sourceDir, $destDir);

						if(!file_exists($destDir))
						{
							fullCopy($sourceDir,$destDir);

							//верное создание папки photos
							$filesfolder='/var/www/html/oiv/';//директория для хранения вложений на верном разделе HD
                            $fileoivfolder=$filesfolder.$row['file_folder'];
                            $fileoivfolderphotos=$fileoivfolder.'/photos';
                            $dosymlink=false;//переменная указывающая на необходимость создания символической ссылки на папку вложений

                            //создаем папки
                            if(!file_exists($fileoivfolder))
                            {
                                mkdir($fileoivfolder, 0777);
                            }
                            if(!file_exists($fileoivfolderphotos))
                            {
                                mkdir($fileoivfolderphotos, 0777);
                                chmod($fileoivfolderphotos, 0777);
                            }

                            //проверяем совпадение адреса папки вложений с папкой скриптов ОИВ-а
                            if($fileoivfolder != $sourceDir)
                            {
                                $dosymlink=true;
                            }

                            //создаем символическую ссылку на папку с вложениями
                            if($dosymlink)
                            {
                                symlink($fileoivfolderphotos,$destDir.'/photos');
                            }



							//на папку с фото даём полный доступ
							//chmod($destDir.'/photos', 0777);

							//правим settings.php
							if(editSettings($row))
							{
								//все норм - ставим флаг о готовности файлов
								$filecomplite=true;
							}

						}
					}

					//разворачиваем БД
					if(in_array($row['status_id'], Array(2,3)))
					{
						//создаём пользователя
						$query="CREATE USER $row[dblogin] WITH PASSWORD 'gso12345'";
						if($res=$this->dbquery($query)){
							//создаём копию эталонной БД
							$query="CREATE DATABASE $row[dbname] WITH TEMPLATE $template OWNER $row[dblogin]";
							if($res=$this->dbquery($query))
							{

								//даём права новому пользюку
								$query="GRANT ALL PRIVILEGES ON DATABASE $row[dbname] TO $row[dblogin]";
								$res=$this->dbquery($query);

								//соединяемся в БД и даём права пользюку на все таблицы
								$testconnect=pg_connect("host=".$this->settings->DB->host.
									" port=".$this->settings->DB->port.
									" dbname=".$row['dbname'].
									" user=".$this->settings->DB->login.
									" password=".$this->settings->DB->pass.
									" options='--client_encoding=UTF8'");

								if (is_resource($testconnect))
								{

									$query="GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO $row[dblogin]";

									pg_query($testconnect, $query);

									//$db_exist=true;
									pg_close($testconnect);
								}

								//все норм - ставим флаг о готовности БД
								$dbcomplite=true;
							}
						}


					}

					//формируем новый vhosts.conf
					createVhosts($this);

					//если все норм - меняем статус на активный
					setActive($this);

				}

			}
		}

		break;
	case 'addtype':
		//добавление типа сайтов
		$this->getPostValues(Array('name','templates_folder'));

		if($this->values->name != '')
		{

			$mid=-1;
			$query="SELECT max(id) mid FROM sites_types";
			if($res=$this->dbquery($query)){
				if(is_array($res) && count($res) > 0){
					$mid=$res[0]['mid'];
				}
			}

			++$mid;

			$query="INSERT INTO sites_types(id, name, templates_folder)
				VALUES($mid, '".$this->values->name."', '".$this->values->templates_folder."')";
			if($res=$this->dbquery($query)){
				$this->data->errortext='Запись о типе сайтов добавлена';
			}
			else
			{
				$this->data->errortext='Ошибка при добавлении типа сайта';
			}

		}

		break;
	case 'edittype':

		break;
}

//получаем список сайтов
global $sites, $stypes;
$sites=Array();
$query="SELECT s.*, t.name typename, (select p.name FROM sites p WHERE p.id = s.parentid) parentname FROM sites s, sites_types t WHERE s.type_id = t.id";

if($this->values->showType != '')
{
    $query.=" AND s.type_id = ".(int)$this->values->showType;
}

$query.=" ORDER BY t.name ASC, s.parentid ASC, s.name ASC";

if($res = $this->dbquery($query)){
	if(is_array($res) && count($res) > 0){
		$sites=$res;
	}
}

//получаем список типов сайтов
$stypes=Array();
$query="SELECT * FROM sites_types ORDER BY name ASC";
if($res=$this->dbquery($query)){
	if(is_array($res) && count($res) > 0){
		$stypes=$res;
	}
}