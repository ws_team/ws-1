<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();
?>

<div class="container">

    <?php echo $this->data->breadcrumbs ?>

    <?php if(printVersionIsActive($this)): ?>

        <button class="action-print-btn"
                onclick="window.print();return false;">
            Распечатать
        </button>

    <?php endif; ?>

</div>