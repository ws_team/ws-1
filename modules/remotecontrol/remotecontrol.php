<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

require_once(__DIR__.'/functions.php');

remotecontrol_auth_peer($this);
remotecontrol_process_request($this);
