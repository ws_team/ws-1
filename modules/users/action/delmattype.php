<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$this->GetPostValues(Array('user','type'));
if ($this->values->user != '' && $this->values->type != '') {
    $query = 'DELETE FROM "user_rights" WHERE "goal_id" = $1 AND "type_id" = $2';
    $res = $this->dbquery($query, array($this->values->user, $this->values->type));
}