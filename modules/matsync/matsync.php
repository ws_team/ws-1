<?php
/**
 * @var \iSite $this
 */


defined('_WPF_') or die();

$this->logWrite(LOG_DEBUG, __FILE__, __LINE__);

include(__DIR__.'/functions.php');

//добьявляем переменные
$this->data->errortext = '';
$this->data->html = '';

global $oivsettings;

$this->getLogger()->write(LOG_INFO, __FILE__, '_');

// получаем переменные: <данные синхронизируемого материала>, <код для верификации>, <время для проверки кода верификации>,
// <тип сайта, который передал данные>, <путь до директории сайта (относительно корня файловой системы), который передал данные>
$this->GetPostValues(Array('data','code','time','fromtype','fromroot'));

// создаем верификационный код на основе переданного времени
$normalcode = md5('khbSynchronize343Sdb5'.$this->values->time);

//echo '<p>'.$normalcode.' - '.$this->values->code.'</p>';

// выполняем верикацию запроса на синхронизацию
if ($normalcode != $this->values->code) {
    $this->getLogger()->write(LOG_INFO, __FILE__, 'error($normalcode != $this->values->code)');
    $this->forbidden();
}

// разрешенные ip-адреса, с которых может приходить запрос на синхронизацию
$iparr = Array(
    '127.0.0.1',
    '172.20.252.1',
    '172.20.252.2',
    '172.20.252.3',
    '172.18.209.3',// gateway
    '172.18.209.4',// ?
    '172.18.209.51',// local
    '172.19.101.131',
    '172.19.101.132',
    '172.19.101.133',
);

// проверяем ip адрес, с которого поступил запрос
if ( ! in_array($_SERVER['REMOTE_ADDR'], $iparr)) {
    $this->getLogger()->write(LOG_INFO, __FILE__, 'error. Remote address '.$_SERVER['REMOTE_ADDR'].' does not allowed.');
	echo $_SERVER['REMOTE_ADDR'];

    $this->forbidden();
}

// получаем данные материала
$data = unserialize(urldecode($this->values->data));

// записываем данные переданного материала в объект сайта
foreach($data as $key=>$value)
{
    $this->values->$key = $value;
}

// отдельно записываем id переданного материала - скорее всего, это нахер не нужно
//$this->values->mainid = $data['id'];

// объвляем переменную для хранения id обрабатываемого материала
$id = '';

// если сайт, который отправил материал, не является Хабкраем, то что делаем...не понятно зачем...закомментируем пока что
/*if(isset($this->values->fromtype) && $this->values->fromtype == 'oiv')
{
	$data[id]=$data[id]*-1;
}*/

// проверяем, существует ли уже переданный материал на сайте (т.е. он раньше синхронизировался)
// если существует, то присваиваем id существующего материала
$id = matsync_get_local_id($this, $data);
$is_new = empty($id);

$this->getLogger()->write(
    LOG_INFO,
    __FILE__,
    'foreignid='.$data['id'].
    ';local_id='.$id.
    ';type_id='.$data['type_id'].
    ';typechar="'.$data['typechar'].'"'
);

// записываем id переданного материала в объект сайта
$this->values->id = $id;

// если материал уже существует, то получаем его, если нет, то создаем новый массив,
// в который далее будут записываться переданные данные
if ( ! empty($this->values->id)) {
    $material = MaterialGet($this, $this->values->id);
} else {
    $material = Array();
}

// с помощью переданного символьного идентификатора получаем тип материала
$query = 'SELECT id FROM material_types WHERE id_char = $1';
$res = $this->dbquery($query, array($data['typechar']));

// если запрашиваемого типа материала не существует - завершаем скрипт
if (empty($res) || empty($res[0]['id'])) {
    $this->getLogger()->write(LOG_INFO, __FILE__, 'material type not found');
    $this->endRequest();
}

// сохраняем в объект сайта id типа материала
$this->values->type_id = $res[0]['id'];

// указываем, что авторизован юзер с id = 1
$this->autorize->userid = 1;

// записываем дату и время синхронизации
$this->values->date_event = date('d.m.Y H:i', $this->values->unixtime);

$this->getLogger()->write(LOG_INFO, __FILE__, 'before createMaterial');

// пытаемся создать материал
list($newMaterialId, $this->data->errortext) = createMaterial($this);

// если нам передали новый материал (а не ранее синхронизируемый), то записываем id созданного материала
if ($is_new)
    $id = $this->values->id = $newMaterialId;

$this->getLogger()->write(LOG_INFO, __FILE__, 'after createMaterial');

// если при создании материала произошла ошибка, то завершаем скрипт
if (empty($this->values->id)) {
    $this->getLogger()->write(LOG_ERR, __FILE__, 'Failed to save material');
    $this->internalServerError();
}

$this->getLogger()->write(LOG_ERR, __FILE__, 'Material saved');

// если нам передали новый материал (а не ранее синхронизируемый), то заносим данные в таблицу синхронизации
if ($is_new) {
    $query = 'INSERT INTO material_synch(id, foreignid) VALUES ($1, $2)';
    $this->dbquery($query, array($this->values->id, $data['id']));
}

// получаем материал (созданный или существующий до синхронизации)
$material = MaterialGet($this, $this->values->id);

// синхронизируем файлы
if ( ! empty($data['allfiles'])) {

    if($this->values->fromroot != '')
    {
        matsync_process_files($this, $data, $material, $this->values->fromroot);
    }
	else
	{
		matsync_process_files($this, $data, $material);
	}
}

// синхронизируем дополнительные поля
if (isset($data['params'])) {
    matsync_process_extra_params($this, $material, $data, $oivsettings);
}

// чистим кэш
delMatCache($this, $this->values->id);

$material = MaterialGet($this, $this->values->id);

// вешаем хук для постановки материала на рассылку
$this->callHook('materialedit', array(
    'material' => $material,
    'subscriptionDaily' => isset($data['subscriptionDaily']) ? $data['subscriptionDaily'] : null,
    'subscriptionCancel' => isset($data['subscriptionCancel']) ? $data['subscriptionCancel'] : null,
    'subscriptionImmediately' => isset($data['subscriptionImmediately']) ? $data['subscriptionImmediately'] : null,
));

//выплёвываем ответ
$this->endRequest();
