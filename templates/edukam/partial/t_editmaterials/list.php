<?php
/**
 * @var iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

global
    $ids,
    $idstype,
    $mquery;

$this->data->material_types_childs[$this->values->imenu] = GetTypeChilds($this, $this->values->imenu);

//таблица материалов
$ahref='/?menu='.$this->values->menu.'&amp;imenu='.$this->values->imenu.'&amp;action='.$this->values->action;
$ahref.='&amp;status_id='.$this->values->status_id;
$ahref.='&amp;language_id='.$this->values->language_id;
$ahref.='&amp;search='.urlencode($this->values->search);

$ahref_nopage=$ahref;
$ahref.='&amp;page='.$this->values->page;

//формируем ссылки для сортировки
$id_order='&amp;orderby=id';
$id_char_order='&amp;orderby=id_char';
$name_order='&amp;orderby=name';
$status_id_order='&amp;orderby=status_id';
$date_event_order='&amp;orderby=date_event';
$weight_order = '&orderby=weight';

if($this->values->ordersort == 'DESC')
{
    $this->values->ordersort='ASC';
    $orderchar='&#9660;';
}
else
{
    $this->values->ordersort='DESC';
    $orderchar='&#9650;';
}

$ahref_nopage.='$amp;orderby='.$this->values->orderby;
$ahref_nopage.='$amp;ordersort='.$this->values->ordersort;

//поиск
//селекторы
?>
<a href='/?menu=editmaterials&amp;imenu=<?=$this->values->imenu?>&amp;action=add'>
    <button type="button" class="btn btn-success showtooltip" data-toggle="tooltip" data-placement="right"
            title="добавить материал выбранного типа">
        <span class="glyphicon glyphicon-plus-sign"></span> Добавить материал
    </button>
</a>
<p>&nbsp;</p>
<form method="get" action="/">
    <table class="noborders" width="100%">
        <tr>
            <td>Поиск по названию:</td>
            <td><input name="search" type="text" class="styler" value="<?php echo $this->values->search; ?>" style="width: 300px;"></td>
            <td>статус:</td>
            <td><select name="status_id" class="styler">
                    <option value="">все</option>
                    <option value="2" <?php if($this->values->status_id == '2'){echo 'SELECTED';} ?>>Активный</option>
                    <option value="1" <?php if($this->values->status_id == '1'){echo 'SELECTED';} ?>>Скрытый</option>
                    <option value="3" <?php if($this->values->status_id == '3'){echo 'SELECTED';} ?>>Удаленный</option>
                </select></td>
            <td>язык:</td>
            <td>
                <select name="language_id" class="styler">
                    <option value="">все</option>
                    <option value="1" <?php if($this->values->language_id == '1'){echo 'SELECTED';} ?>>Ру</option>
                    <option value="2" <?php if($this->values->language_id == '2'){echo 'SELECTED';} ?>>En</option>
                </select>
            </td>
            <td><input type="hidden" name="menu" value="editmaterials">
                <input type="hidden" name="action" value="list">
                <input type="hidden" name="imenu" value="<?php echo $this->values->imenu; ?>">
                <input type="submit" class="styler" value="показать">
            </td>
        </tr>
    </table>
</form>
<table width="100%" class="table-bootstrap table table-striped table-hover table-condensed">
    <tr>
        <th>Изобр.</th>
        <th><a href="<?php echo $ahref.$id_order.'&ordersort='.$this->values->ordersort; ?>">№<?php if($this->values->orderby == 'id'){ ?>&nbsp;<?php echo $orderchar; ?><?php } ?></a></th>
        <th><a href="<?php echo $ahref.$id_char_order.'&ordersort='.$this->values->ordersort; ?>">Букв. идентификатор<?php if($this->values->orderby == 'id_char'){ ?>&nbsp;<?php echo $orderchar; ?><?php } ?></a></th>
        <th><a href="<?php echo $ahref.$name_order.'&ordersort='.$this->values->ordersort; ?>">Заголовок<?php if($this->values->orderby == 'name'){ ?>&nbsp;<?php echo $orderchar; ?><?php } ?></a></th>
        <th><a href="<?php echo $ahref.$status_id_order.'&ordersort='.$this->values->ordersort; ?>">Статус<?php if($this->values->orderby == 'status_id'){ ?>&nbsp;<?php echo $orderchar; ?><?php } ?></a></th>
        <th><a href="<?php echo $ahref.$date_event_order.'&ordersort='.$this->values->ordersort; ?>">Дата<?php if($this->values->orderby == 'date_event'){ ?>&nbsp;<?php echo $orderchar; ?><?php } ?></a></th>
        <th>
            <a href="<?= ($ahref.$weight_order) ?>&amp;ordersort=<?= $this->values->ordersort ?>">Вес<?php
                if ($this->values->orderby == 'weight'){ ?>&nbsp;<?php echo $orderchar; ?><?php } ?></a>
        </th>
        <th>Действия</th>
    </tr>
    <?php
    if (is_array($this->data->materials) && !empty($this->data->materials)) {
        foreach ($this->data->materials as $material) {
        ?>
            <tr>
                <td>
                    <?php
                    if (!empty($material['photo'])) {
                        ?>
                        <img src='/photos/<?= $material['photo'] ?>_xy64x64.jpg' border='0'
                             width='42' height='42' class="img-thumbnail"/>
                        <?php
                    }

                    if($material['status_id'] == 2)
                    {
                        $statustext="<span style='color:#00b101;' data-toggle=\"tooltip\" data-placement=\"top\" class=\"glyphicon glyphicon-eye-open\" title='{$material['statusname']}'></span>";
                    }
                    elseif($material['status_id'] == 1)
                    {
                        $statustext= "<span style='color:#d0bd05;' data-toggle=\"tooltip\" data-placement=\"top\" class=\"glyphicon glyphicon-eye-close\" title='{$material['statusname']}'></span>";
                    }
                    else
                    {
                        $statustext= "<span style='color:#b14100;' data-toggle=\"tooltip\" data-placement=\"top\" class=\"glyphicon glyphicon-trash\" title='{$material[statusname]}'></span>";
                    }


                    ?>
                </td>
                <td><?=$material['id']?></td>
                <td><?=$material['id_char']?></td>
                <td><?=$material['name']?></td>
                <td style='vertical-align: middle; text-align: center;' ><?=$statustext?></td>
                <td style='vertical-align: middle; font-size: 10px;' ><?=russian_datetime_from_timestamp($material['unixtime'])?></td>
                <td class="weight-edit">
                    <form action="/" method="post">
                        <input type="hidden" name="menu" value="editmaterials" />
                        <input type="hidden" name="action" value="edit" />
                        <input type="hidden" name="imenu" value="<?=$material['type_id']?>" />
                        <input type="hidden" name="id" value="<?=$material['id']?>" />
                        <input type="hidden" name="redirect_to" value="<?=
                        htmlspecialchars($_SERVER['REQUEST_URI'])?>" />
                        <input type="text" name="extra_number2" value="<?=
                        $material['extra_number2']?>" id="extra_number2" maxlength="2"
                            style="width: 48px;height: 28px;margin: 1px;box-sizing: border-box;line-height: 30px;vertical-align: middle;"/>
                        <button type="submit button" class="btn btn-default showtooltip small" data-toggle="tooltip" data-placement="top" title="изменить вес материала" style="width: 48px;">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                    </form>
                </td>
                <td><div class="btn-group btn-group-xs">
                        <a href="/?menu=editmaterials&amp;imenu=<?=$material['type_id']?>&amp;id=<?=
                        $material['id']?>&amp;action=edit">
                            <button type="button" class="btn btn-default showtooltip small" data-toggle="tooltip" data-placement="top" title="форма изменения материала" style="width: 48px;">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </button>
                        </a>
                        <a href="/?menu=editmaterials&amp;imenu=<?=$material['type_id']?>&amp;id=<?=
                        $material['id']?>&amp;action=edit&amp;clonematerial=<?=$material['id']?>">
                            <button type="button" class="btn btn-warning showtooltip small" data-toggle="tooltip" data-placement="top" title="клонировать материал" style="width: 48px;">
                                <span class="glyphicon glyphicon-share-alt"></span>
                                <span class="glyphicon glyphicon-plus-sign"></span>
                            </button>
                        </a>
                    </div>
                </td>
            </tr>
            <?php
        }
    }
    ?>
</table>
<p>&nbsp;</p>
<a href='/?menu=editmaterials&amp;imenu=<?=$this->values->imenu?>&amp;action=add'>
    <button type="button" class="btn btn-success showtooltip" data-toggle="tooltip" data-placement="right"
            title="добавить материал выбранного типа">
        <span class="glyphicon glyphicon-plus-sign"></span> Добавить материал
    </button>
</a><?php

if(!isset($ids))
    $ids = '';

echo vidjet_materiallist_pages($this, $ids, $idstype, $mquery, 'bootstrap', $this->values->status_id);

?><p>&nbsp;</p>
<p>&nbsp;</p>
