<?php
/**
 * @var \iSite $this
 */


namespace wpf\modules\oiv_update_db;

defined('_WPF_') or die();

if ( ! $this->requireAuthUser()->can('admin.oiv_update_db')) {
    $this->endRequest(403);
}

$this->getLogger()->write(LOG_INFO, __FILE__.':'.__LINE__, '_');

$this->getPostValue('action');

$valid_actions = array('apply', 'check');
$action = $this->values->action;
if (empty($action))
    $action = $valid_actions[0];

if ( ! in_array($action, $valid_actions))
    $this->badRequest();

require_once(__DIR__.'/functions.php');
include(__DIR__.'/action/'.$action.'.php');

