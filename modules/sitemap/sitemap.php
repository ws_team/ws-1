<?php

$this->data->title='Карта сайта - '.$this->data->title;
$this->data->iH1='Карта сайта';



//строим дерево разделов
function PrintTreeNodes($tree)
{

    $str='<ul class="tree">';

    foreach($tree as $row)
    {
        if($row['status_id'] == 2)
        {
            $str.= "<li class='treeType'><a href='$row[url]'>".$row['name'].'</a>';

            //пробуем получить материалы


            //подразделы
            if(count($row['childs']) > 0)
            {
                $str.=PrintTreeNodes($row['childs']);
            }

            $str.='</li>';
        }
    }

    $str.='</ul>';

    return($str);
}

//выводим материалы
$tree=PrintTreeNodes($this->data->material_types);