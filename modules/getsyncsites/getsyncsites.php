<?php

/*
 *
 * Модуль использовался для получения списка сайтов для синхронихации по запросу от сайта ОИВ или ОМСУ,
 * но теперь сайты ОИВ и ОМСУ сами могут формировать справочники для синхронизации сайтами
 *
 * Оставлю этот модуль на память, может пригодится еще
 *
 * */

defined('_WPF_') or die();

$this->GetPostValues(Array('from','material_from'));

if($this->values->material_from === '')
{
    $this->values->material_from = 0;
}

if ($this->values->from != '') {

    $out='';

    $query='SELECT s.*, 
        (SELECT count(sy.material_to) 
        FROM synctable_global sy 
        WHERE sy.material_from = '.$this->values->material_from.' AND sy.site_from = '."'".$this->values->from."'".' AND sy.site_to = s.dbname
        ) checked 
    FROM sites s
    WHERE s.parentid = (SELECT p.id FROM sites p WHERE dbname = '."'".$this->values->from."'".')
    ORDER BY s.name ASC';

    if($res=$this->dbquery($query))
    {
        $out=json_encode($res);
    }

    echo $out;

    die();

} else
{
    die();
}
