<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if ($this->values->id != '') {

    $query = 'SELECT "id" FROM "authors" WHERE "unit_id" = $1';

    $q_params = array(
        $this->values->id
    );

    $res = $this->dbquery($query, $q_params);

    if($res){
        $this->data->errortext='<p style="color: #ff0000;">Нельзя удалить подразделение, пока есть авторы, которые относятся к нему.</p>';
        return;
    }

    $query = 'DELETE FROM "units" WHERE "id" = $1';

    $q_params = array(
        $this->values->id
    );

    $res = $this->dbquery($query, $q_params);

    if($res){
        $this->data->errortext='Запись удалена!';
    }

}