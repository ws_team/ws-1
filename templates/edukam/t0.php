<?php
/**
 * @var \iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

global
    $sectypearr,
    $normaltypes;

if ( ! isset($sectypearr)) {
    foreach ($this->data->material_types as $mtype) {
        if ($mtype['id'] == $this->values->maintype) {
            $sectypearr = $mtype;
            break;
        }
    }
}

if (!empty($sectypearr) && count($sectypearr['childs']) > 0) {
    ?>
    <div class="secmenu">
    <div class="secmenu_wrapper">
        <?php

        //рисуем подменю
        if (isset($sectypearr)) {
            $mc = 0;

            foreach ($sectypearr['childs'] as $mtype) {

                ++$mc;
                ob_start();
                if ($mtype['showinmenu'] == '1') {

                    //если активный раздел
                    if ($mtype['id'] == $this->values->sectype) {
                        $styleclass='option active';
                        $secname=$mtype['name'];
                    } else {
                        $styleclass='option';
                    }

                    $url = '/'.$sectypearr['id_char'].'/'.$mtype['id_char'];

                    ?><a class="<?= $styleclass ?>" href="<?= $url ?>"><?= $mtype[$menuname] ?></a><?php

                    //рисуем подменю

                    $pi = 0;
                    $smcont = '';
                    $smbody = '';

                    foreach ($mtype['childs'] as $smtype) {
                        ++$pi;
                        if ($pi == 1) {

                            if ($mc == 1) {
                                $style='left_sto';
                            } elseif($mc == 2) {
                                $style='left_sto';
                            } elseif($mc == 3)
                            {
                                $style='left_sto';
                            } elseif($mc == 4)
                            {
                                $style='left_dvesti';
                            } else{
                                $style='';
                            }

                            $moredvestiarr = Array(
                                'Zakonoproektnaya-deyatelnost',
                                'Gosudarstvennye-programmy',
                                'Usloviya-prohozhdeniya',
                                'Poryadok-postupleniya',
                                'Podgotovka-kadrov'
                            );

                            if(in_array($mtype['id_char'], $moredvestiarr))
                            {
                                $style='left_dvesti';
                            }

                            //хак - смотрим размер подменю, если оно длинное, рисуем другие стили

                            // ВОТ ТАК ПЛОХО

                            // ВОТ ТАК "АЩЕАТЛИЧНА"
                            // там начиналось с 1-го идишника ) потом 2-х.... итд итп ))))
                            if (in_array($mtype['id'], array('51','14','65','46','105','75','137','171','178','184','191')))
                            {
                                $smcont = '<div class="nullblock smenu"><div class="submenu_container_long '.$style.'">';
                            }
                            else
                            {
                                $smcont = '<div class="nullblock smenu"><div class="submenu_container '.$style.'">';
                            }

                        }
                        if($smtype['showinmenu'] == '1'){


                            if(($mtype['parent_id'] != 170)||(in_array($smtype['id'], $normaltypes)))
                            {

                                if($smtype['id'] == $this->values->thirdtype)
                                {
                                    $styleclass='option active';
                                    $thirdname=$smtype['name'];
                                }
                                else
                                {
                                    $styleclass='option';
                                }

                                $smbody.= '<a class="'.$styleclass.'" href="'.'/'.$sectypearr['id_char'].'/'.$mtype['id_char'].'/'.$smtype['id_char'].'/'.'">'.$smtype[$menuname].'</a>';
                            }
                        }

                    }

                    //echo '</div>';

                    if($smbody != '')
                    {
                        $smcont.=$smbody;

                        if ($pi != 0) {
                            $smcont.= '</div></div>';
                            echo $smcont;

                        }
                    }
                }
                $item_html = ob_get_clean();
                ?>
                <span class="optionwr x-anc"><?= $item_html ?></span>
                <?php
            }

            if ($this->values->maintype == '1') {
                //http://open27.ru/ODOpenData/ChooseData
                ?>
                <span class="optionwr x-anc"><a class="option" href="/opendata">Открытые данные</a></span>
                <span class="optionwr x-anc"><a class="option" href="https://mizip.khabkrai.ru/Important/svobodnyj-port-Vanino/O-Svobodnom-porte">Свободный порт</a></span>
                <span class="optionwr x-anc" style="display:none;"><a class="option" href="/Vybory-2016">Выборы 2016</a></span>
                <span class="optionwr x-anc" style="display: none;"><a class="option" href="http://ogs.khabkrai.ru/Obschestvennyj-kontrol/Obschestvennyj-kontrol-Informaciya">Общественный контроль</a></span>
                <?php
            }
        }
        ?>
    </div>
</div>
    <?php
} else {
    ?>
    <div class="secmenu secmenu-placeholder" style="display:none;">
        <div class="secmenu_wrapper"></div>
    </div><?php
}
?>
