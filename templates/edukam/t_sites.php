<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

global $sites, $stypes;

include($this->locateTemplate('f_header'));


$this->data->iH1 = 'Системные настройки';

//статусы
$statuses=array('Не проверен','Активный','Отсутствует БД и файлы','Отсутствует БД','Отсутствуют файлы');

?>
<div class="container container--admin-title">
    <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
</div>
<div class="contentblock basemargin">
    <p class="errortext"><?= ! empty($this->data->errortext) ? $this->data->errortext : ''  ?></p>

    <form method="get" action="/?menu=sites">
        <p>Отображать тип: <select name="showType" id="showType">
                <option value="">Все</option>
                <?php
                if(is_array($stypes) && count($stypes) > 0)
                {
                    foreach ($stypes as $stype)
                    {
                        /*if($stype['id'] == $site['type_id'])
                            {
                            $selected='SELECTED';
                            }else{
                            $selected='';
                            }*/
                        $selected='';

                        if($this->values->showType == $stype[id])
                        {
                            $selected='SELECTED';
                        }

                        echo "<option value='$stype[id]' $selected>$stype[name]</option>";
                    }
                } ?>
            </select><input type="hidden" name="menu" value="sites">
            <input type="submit" value="Показать">
        </p></form>
    <p>&nbsp;</p>

    <table>
		<tr>
			<th>id</th>
			<th>Наименование</th>
			<th>Имя БД/<br>Логин БД/<br>Директория</th>
			<th>Домены</th>
			<th>Тип сайта/<br>Сайт "родитель"</th>
			<th>Статус</th>
			<th>Герб/<br>Флаг</th>
			<th>Действия</th>
		</tr>
		<?php

		if(is_array($sites) && count($sites) > 0){
			foreach ($sites as $site){

				$statusname=$statuses[$site['status_id']];

				$activatebtn='Развернуть';
				if(in_array($site['status_id'],Array(2,3,4)))
				{
					$activatebtn='<a href="/?menu=sites&id='.$site['id'].'&action=start">Развернуть</a>';
				}

				$sdprint=$site['domains'];
				$sdprint=str_replace(';','<br>',$sdprint);

				echo "<tr><form method='post' action='/?menu=sites'>
					<td><small>$site[id]<input type='hidden' name='id' value='$site[id]'></small></td>
					<td><small>$site[name]</small></td>
					<td><small>$site[dbname]<br>$site[dblogin]<br>$site[file_folder]</small></td>
					<td><small>$sdprint</small></td>
					<td><select name='type_id'>";

				if(is_array($stypes) && count($stypes) > 0){
					foreach ($stypes as $stype){

						if($stype['id'] == $site['type_id']){
							$selected='SELECTED';
						}else{
							$selected='';
						}

						echo "<option value='$stype[id]' $selected>$stype[name]</option>";
					}
				}

				//$site[typename]
				echo "</select><br><input type='text' name='parentid' value='$site[parentid]' style='width: 50px;'><br>$site[parentname]</td>
					<td>$statusname</td>
					<td><small>$site[emblem]<br>$site[flag]</small></td>
					<td><input type='hidden' name='action' value='edit'><input type='submit' value='Изменить'>
						<br><a href='/?menu=sites&id=$site[id]&action=check'>Проверить</a>
						<br>$activatebtn</td></form>
				</tr>";
			}
		}

		?>
	</table>
	<p>&nbsp;</p>
	<p><strong>Добавить сайт:</strong></p>
	<form method="post" action="">
		<table>
			<tr>
				<th>Наименование</th>
				<td><input type="text" name="name" style="width: 800px;"></td>
			</tr>
			<tr>
				<th>Имя БД</th>
				<td><input type="text" name="dbname" style="width: 800px;"></td>
			</tr>
			<tr>
				<th>Логин БД</th>
				<td><input type="text" name="dblogin" style="width: 800px;"></td>
			</tr>
			<tr>
				<th>Директория</th>
				<td><input type="text" name="file_folder" style="width: 800px;"></td>
			</tr>
			<tr>
				<th>Домены (ч/з ;)</th>
				<td><input type="text" name="domains" style="width: 800px;"></td>
			</tr>
			<tr>
				<th>Тип сайта:</th>
				<td><select name="type_id">
						<?php

						if(is_array($stypes) && count($stypes) > 0){
							foreach ($stypes as $stype){
								echo "<option value='$stype[id]'>$stype[name]</option>";
							}
						}

						?>
					</select></td>
			</tr>
			<tr>
				<th>Сайт "родитель":</th>
				<td><input type="text" name="parentid" style="width: 800px;"></td>
			</tr>
			<tr>
				<th>Статус:</th>
				<td>Не проверен</td>
			</tr>
			<tr>
				<th>Герб</th>
				<td><input type="text" name="emblem" style="width: 800px;"></td>
			</tr>
			<tr>
				<th>Флаг</th>
				<td><input type="text" name="flag" style="width: 800px;"></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="hidden" name="action" value="add">
					<input type="hidden" name="menu" value="sites">
					<input type="submit" value="Создать">
				</td>
			</tr>
		</table>
	</form>
	<p>&nbsp;</p>
	<!--<p><strong>Групповые действия:</strong></p>
	<br>Проверить все сайты
	<br>Развернуть все сайты-->
	<p>&nbsp;</p>
	<p><strong>Типы сайтов:</strong></p>
	<table>
		<tr>
			<th>id</th>
			<th>Название</th>
			<th>Директория</th>
			<th>Шаблон</th>
			<th>Действия</th>
		</tr>
		<?php

		if(is_array($stypes) && count($stypes) > 0){
			foreach ($stypes as $stype){
				echo "<tr>
					<td>$stype[id]</td>
					<td>$stype[name]</td>
					<td>$stype[templates_folder]</td>
					<td>template_$stype[id]</td>
					<td></td>
				</tr>";
			}
		}

		?>
	</table>
	<p>&nbsp;</p>
	<p><strong>Добавить тип сайта:</strong></p>
	<form method="post" action="">
		<table>
			<tr>
				<th>Наименование</th>
				<td><input type="text" name="name"></td>
			</tr>
			<tr>
				<th>Директория</th>
				<td><input type="text" name="templates_folder"></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="hidden" name="action" value="addtype">
					<input type="hidden" name="menu" value="sites">
					<input type="submit" value="Создать">
				</td>
			</tr>
		</table>
	</form>
</div>
<?php

include($this->locateTemplate('f_footer'));

?>