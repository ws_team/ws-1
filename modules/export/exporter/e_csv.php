<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$fieldDelimiter = ';';
$rowTerminator = "\r\n";

$headerLine = array();
foreach ($header as $colName) {
    if ($encoding != 'utf-8')
        $colName = iconv('utf-8', $encoding, $colName);
    $headerLine[] = '"'.$colName.'"';
}

echo implode($fieldDelimiter, $headerLine).$rowTerminator;

foreach ($outputItems as $outputItem) {
    $outputLine = array();
    foreach ($header as $col => $colName) {
        $colVal = isset($outputItem[$col]) ? $outputItem[$col] : '';
        if (is_array($colVal))
            $colVal = implode(' | ', $colVal);
        if ($encoding != 'utf-8')
            $colVal = iconv('utf-8', $encoding, $colVal);
        $colVal = strip_tags($colVal);
        $outputLine[] = '"'.str_replace('"', "''", $colVal).'"';
    }

    echo implode($fieldDelimiter, $outputLine).$rowTerminator;
}

$mediaType = 'text/csv';
$fileExt = '.csv';
