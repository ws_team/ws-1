<?php
//добьявляем переменные
$this->data->errortext='';

$this->data->iH1='Логи поисковых запросов';

$this->GetPostValues(Array('startdate','enddate','period'));

if($this->values->period == '')
{
    $this->values->period = 'month';
}

if(($this->values->startdate == ''))
{
    $this->values->startdate=date('d.m.Y');

    $starttime=MakeTimeStampFromStr($this->values->startdate);
    $endtime=MakeTimeStampFromStr($this->values->startdate) + 24*60*60;

    $this->values->enddate=date('d.m.Y',$endtime);

}



//получаем список изменений, добавлений материалов пользователем за выбранный период
$query='SELECT "site", "username", "userlogin", "userid", "actionname", count("objid") "ocnt"
FROM "userlogs" WHERE "actiondate" between '.$starttime.' AND '.$endtime.'
GROUP BY "site", "username", "userlogin", "userid", "actionname"
ORDER BY "site" ASC, "actionname" ASC, "username" ASC';

$query='SELECT "s".*, TO_CHAR("s"."dateadd", '."'DD.MM.YYYY HH24:MI:SS'".') "dateaddchar" FROM "searchlogs" "s" WHERE "s"."dateadd"
BETWEEN TO_DATE('."'".$this->values->startdate."','DD.MM.YYYY'".')
AND TO_DATE('."'".$this->values->enddate."','DD.MM.YYYY'".') ORDER BY "s"."dateadd" ASC';

//echo $query;

$this->data->stat=Array();

if($res=$this->dbquery($query))
{
    $this->data->stat=$res;
}

$this->data->jsdocready = "


        $('#startdate').datetimepicker({
            lang:'ru',
            timepicker:true,
            format:'d.m.Y'
        });

        $('#enddate').datetimepicker({
            lang:'ru',
            timepicker:true,
            format:'d.m.Y'
        });

        ";