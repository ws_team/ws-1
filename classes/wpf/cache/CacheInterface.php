<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

namespace wpf\cache;

interface CacheInterface {
    function has($key);
    function get($key);
    function set($key, $value, $expire = null);
    function delete($key);

    function hset($hname, $key, $value, $expire = null);
    function hget($hname, $key = null);
    function hdel($hname, $key = null);
}