<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

?>
<a href="/?menu=material_types&imenu=add"><button type="button" class="btn btn-success showtooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Добавить новый тип материала">
        <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;Добавить тип материала</button></a>
<p>&nbsp;</p>

    <div class="dropdown-list">
        <div class="material-type-columns material-type-columns_captions">
            <div class="material-type-columns__item material-type-columns__item_opener"></div>
            <div class="material-type-columns__item material-type-columns__item_number">№</div>
            <div class="material-type-columns__item material-type-columns__item_char-id">Букв. идентификатор</div>
            <div class="material-type-columns__item material-type-columns__item_name">Наименование</div>
            <div class="material-type-columns__item material-type-columns__item_name-en">En наименование</div>
            <div class="material-type-columns__item material-type-columns__item_parent-type-id">Родительский раздел</div>
            <div class="material-type-columns__item material-type-columns__item_nesting">Вложенность</div>
            <div class="material-type-columns__item material-type-columns__item_weight">Вес</div>
            <div class="material-type-columns__item material-type-columns__item_actions">Действия</div>
        </div>
    </div>

    <?php print PrintTypeRows($this->data->material_types); ?>

<p>&nbsp;</p>
<a href="/?menu=material_types&imenu=add"><button type="button" class="btn btn-success showtooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Добавить новый тип материала">
        <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;Добавить тип материала</button></a>