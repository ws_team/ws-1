<?php
/**
 * @var \iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


$material = MaterialGet($this, $this->values->id);

if (!empty($material)) {

    if($material['status_id'] == 3)
    {
        $material=false;
    }
    else
    {
        foreach ($material['params'] as $param) {
            if ($param['name'] == 'redirect_url' && !empty($param['value'])) {
                header('Location: '.$param['value']);
                exit;
            }
        }
    }
}

if($material['status_id'] == 3)
{
    $material=false;
}
else
{
    $this->data->atype=$material['type_id'];

    $word_counter = new Counter();
    $keycontent = mb_strtolower($material['content'],'utf8').mb_strtolower($material['anons'],'utf8');

// если длинна строки больше 50000
    if (mb_strlen($keycontent,'utf8')>50000)
    {
        // подбор слов
        $this->data->keywords= $word_counter->get_keywords(mb_substr($keycontent, 0, 50000,'utf8'));
    }
    else // если меньше
    {
        // подбор слов
        $this->data->keywords= $word_counter->get_keywords($keycontent);
    }
}

//подразделы при выбранном материале!!!
if(!$material)
{
    $this->values->menu = '404';
    header('Location: /?menu=404');
    die();
}