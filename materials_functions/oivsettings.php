<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

function MakeOIVSettings(\iSite $site)
{
    $oivsettings = null;

    if ($site->getCache()->has('oivsettings')) {
        $oivsettings = unserialize($site->getCache()->get('oivsettings'));
        if (is_array($oivsettings)) {
            foreach($oivsettings as $opt) {
                if ($opt == false) {
                    $site->getCache()->delete('oivsettings');
                    $oivsettings = null;
                    break;
                }
            }
        }
    }

    if (empty($oivsettings)) {
        //формирование натсроек сайта ОИВ
        $oivsettings = Array();
        $oivsettings['posts']=findTypeIdByChar($site, 'posts'); //id типа должностей руководства
        $oivsettings['minpost']=findMatIdByChar($site, 'mainMinister'); //id материала должности министра (самого главного)
        $oivsettings['doctypes']=findTypeIdByChar($site, 'Tdoc_types'); //id типа докуметов
        $oivsettings['dep_type']=findTypeIdByChar($site, 'Departamenty'); //id типа материала департаментов
        $oivsettings['dep_post']=findParamIdByName($site, $oivsettings['dep_type'], 'Должность руководителя'); //id параметра - должность руководителя департамента
        $oivsettings['dep_fio']=findParamIdByName($site, $oivsettings['dep_type'], 'ФИО руководителя'); //id параметра - ФИО руководителя департамента
        $oivsettings['ruk_type']=findTypeIdByChar($site, 'Rukovodstvo'); //id типа материала руководства
        $oivsettings['min_post']=findParamIdByName($site, $oivsettings['ruk_type'], 'Должность'); //id параметра - должность министра
        $oivsettings['min_date']=findParamIdByName($site, $oivsettings['ruk_type'], 'Дата вступления в должность'); //id параметра - дата вступления в должность министра
        $oivsettings['votes']=findTypeIdByChar($site, 'Oprosy'); //id типа материала опроса
        $oivsettings['vote_options']=findTypeIdByChar($site, 'Varianty-otvetov'); //id типа варианта ответа на опрос
        $oivsettings['vote_option_cnt']=findParamIdByName($site, $oivsettings['vote_options'], 'проголосовало (чел)'); //id типа кол-ва проголосовавших за вариант ответа
        $oivsettings['banners']=findTypeIdByChar($site, 'Bannery'); //id типа материалов баннеров
        $oivsettings['banner_types']=findTypeIdByChar($site, 'Tipy-bannerov'); //id типа материалов типа баннера
        $oivsettings['banner_url']=findParamIdByName($site, $oivsettings['banners'], 'URL'); //id характеристики URL баннера
        $oivsettings['banner_type']=findParamIdByName($site, $oivsettings['banners'], 'Тип баннера'); //id характеристики типа баннера
        $oivsettings['banner_small']=findMatIdByChar($site, 'Malyj-gorizontalnyj-292x120-px-'); //id малого гориз. баннера
        $oivsettings['banner_big']=findMatIdByChar($site, 'Banner-gorizontalnyj-610x120-'); //id большого гориз. баннера
        $oivsettings['banner_vert']=findMatIdByChar($site, 'Banner-v-bloke-slajdera-290h423-'); //id вертик. баннера
        $oivsettings['vacancytypes']=findTypeIdByChar($site, 'Tipy-vakansij'); //id типа материала типов вакансий
        $oivsettings['vacancytype']=findTypeIdByChar($site, 'Vakansii'); //id типа материала вакансий
        $oivsettings['vacancy_datetoparam']=findParamIdByName($site, $oivsettings['vacancytype'], 'Приём документов до'); //id параметра срока вакансий
        $oivsettings['vacancy_typeparam']=findParamIdByName($site, $oivsettings['vacancytype'], 'Тип вакансии'); //id параметра типа вакансий
        $oivsettings['vacancy_salaryparam']=findParamIdByName($site, $oivsettings['vacancytype'], 'Заработная плата'); //id параметра зар.плата вакансии
        $oivsettings['morefooterlinks']=findTypeIdByChar($site, 'Footerlinks'); //id типа материала доп. ссылок в футере

        $site->getCache()->set('oivsettings', serialize($oivsettings));
    }

    return($oivsettings);

}

function MakeOMSUSettings(\iSite $site)
{
    $oivsettings = null;

    if ($site->getCache()->has('oivsettings')) {
        $oivsettings = unserialize($site->getCache()->get('oivsettings'));

        if (is_array($oivsettings)) {
            foreach ($oivsettings as $opt) {
                if($opt == false) {
                    $site->getCache()->delete('oivsettings');
                    $oivsettings = null;
                    break;
                }
            }
        }

    }
    if (empty($oivsettings)) {
        //формирование натсроек сайта ОИВ
        $oivsettings=Array();

        $oivsettings['doctypes']=findTypeIdByChar($site, 'Tdoc_types'); //id типа докуметов
        $oivsettings['dep_type']=findTypeIdByChar($site, 'Departamenty'); //id типа материала департаментов
        $oivsettings['dep_post']=findParamIdByName($site, $oivsettings['dep_type'], 'Должность руководителя'); //id параметра - должность руководителя департамента
        $oivsettings['dep_fio']=findParamIdByName($site, $oivsettings['dep_type'], 'ФИО руководителя'); //id параметра - ФИО руководителя департамента
        $oivsettings['ruk_type']=findTypeIdByChar($site, 'Rukovodstvo'); //id типа материала руководства
        $oivsettings['min_post']=findParamIdByName($site, $oivsettings['ruk_type'], 'Должность'); //id параметра - должность министра
        $oivsettings['min_date']=findParamIdByName($site, $oivsettings['ruk_type'], 'Дата вступления в должность'); //id параметра - дата вступления в должность министра
        //$oivsettings['votes']=findTypeIdByChar($site, 'Oprosy'); //id типа материала опроса
        //$oivsettings['vote_options']=findTypeIdByChar($site, 'Varianty-otvetov'); //id типа варианта ответа на опрос
        //$oivsettings['vote_option_cnt']=findParamIdByName($site, $oivsettings['vote_options'], 'проголосовало (чел)'); //id типа кол-ва проголосовавших за вариант ответа
        $oivsettings['banners']=findTypeIdByChar($site, 'Bannery'); //id типа материалов баннеров
        $oivsettings['banner_types']=findTypeIdByChar($site, 'Tipy-bannerov'); //id типа материалов типа баннера
        $oivsettings['banner_url']=findParamIdByName($site, $oivsettings['banners'], 'URL'); //id характеристики URL баннера
        $oivsettings['banner_type']=findParamIdByName($site, $oivsettings['banners'], 'Тип баннера'); //id характеристики типа баннера
        $oivsettings['banner_small']=findMatIdByChar($site, 'Malyj-gorizontalnyj-292x120-px-'); //id малого гориз. баннера
        $oivsettings['banner_big']=findMatIdByChar($site, 'Banner-gorizontalnyj-610x120-'); //id большого гориз. баннера
        $oivsettings['banner_vert']=findMatIdByChar($site, 'Banner-v-bloke-slajdera-290h423-'); //id вертик. баннера
        $oivsettings['vacancytypes']=findTypeIdByChar($site, 'Tipy-vakansij'); //id типа материала типов вакансий
        $oivsettings['vacancytype']=findTypeIdByChar($site, 'Vakansii'); //id типа материала вакансий
        $oivsettings['vacancy_datetoparam']=findParamIdByName($site, $oivsettings['vacancytype'], 'Приём документов до'); //id параметра срока вакансий
        $oivsettings['vacancy_typeparam']=findParamIdByName($site, $oivsettings['vacancytype'], 'Тип вакансии'); //id параметра типа вакансий
        $oivsettings['vacancy_salaryparam']=findParamIdByName($site, $oivsettings['vacancytype'], 'Заработная плата'); //id параметра зар.плата вакансии
        //$oivsettings['morefooterlinks']=findTypeIdByChar($site, 'Footerlinks'); //id типа материала доп. ссылок в футере

        $site->getCache()->set('oivsettings', serialize($oivsettings));
    }

    return $oivsettings;

}