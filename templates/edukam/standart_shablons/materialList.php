<?php
/**
 * @var \iSite $this
 */

defined('_WPF_') or die();

$materials = MaterialList($this, $this->data->atype, 1, $this->values->perpage, 2);

?>

<?php include($this->partial('/xpage/breadcrumbs'));  ?>


<div class="container">
    <div class="wrap-content">

        <?php include($this->partial('/xpage/titleWithAdditionalButtons')); ?>

        <div class="container">
            <div id="ajaxPaginatorDestinationHtml" class="news-list">

                <?php

                foreach ($materials as $materialsItem){
                    printMaterialListItem($this, $materialsItem, false);
                }

                ?>

            </div>
        </div>
    </div>
</div>
<div class="container can-be-refreshed">

    <?php

    $allMaterials   = MaterialList($this, $this->data->atype, 1, 10000, 2);
    $materialsCount = count($allMaterials);

    print $this->data->blocks->render(
        'ajaxUniversalPaginator',
        'ajaxGetMaterialList',
        $this->data->atype,
        $this->values->perpage,
        1,
        $materialsCount
    );
    ?>

    <div class="preloader"><img src="/img/preloader.svg" class="preloader__image"></div>

</div>
