<?php
/**
 * Вставка динамического контента в закэшированный код страницы.
 *
 * @var \iSite $this
 * @var string $body
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

//подменяем голосование на годное
if ($pos = mb_strpos($body, '<!-- votingrender -->', null, 'UTF8'))
{
    $voting = $this->data->blocks->render('voting');

    $body = str_replace('<!-- votingrender -->','',$body);
    $body1 = mb_substr($body, 0, $pos,'UTF8');
    $body2 = mb_substr($body, $pos, null, 'UTF8');

    $body = $body1.$voting.$body2;
}