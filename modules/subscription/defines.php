<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


if (file_exists(dirname(__FILE__).'/defines-local.php'))
    include(dirname(__FILE__).'/defines-local.php');

define('SUBSCRIPTION_COOKIE', 'ssid');
define('SUBSCRIPTION_TABLE', 'subscribers');
define('SUBSCRIPTION_TABLE_SENDQUEUE', 'subscription_sendqueue');
define('SUBSCRIPTION_TABLE_LIST', 'subscriptionlists');
define('SUBSCRIPTION_TABLE_LIST_SUBSCRIBER', 'list_subscribers');

if ( ! defined('SUBSCRIPTION_SEND_BATCH_SIZE'))
    define('SUBSCRIPTION_SEND_BATCH_SIZE', 20000);// макс. размер выборки из очереди одного цикла рассылки
if ( ! defined('SUBSCRIPTION_SEND_MAIL_LIMIT'))
    define('SUBSCRIPTION_SEND_MAIL_LIMIT', 20000);// макс. число писем, рассылаемых за один цикл

if (!defined('SUBSCRIPTION_DAILY_SUBJECT'))
    define('SUBSCRIPTION_DAILY_SUBJECT', 'Новости дня {at_site_name}');

if (!defined('CSRF_COOKIE_LIFETIME'))
    define('CSRF_COOKIE_LIFETIME', 3600 * 24 * 30);

define('SUBSCRIPTION_SEND_DAILY', 1);
define('SUBSCRIPTION_SEND_IMMEDIATE', 2);

if ( ! defined('SUBSCRIPTION_DB_TIMEZONE'))
    define('SUBSCRIPTION_DB_TIMEZONE', 'Asia/Vladivostok');

if ( ! defined('SUBSCRIPTION_MAIL_MAX_ITEMS'))
    define('SUBSCRIPTION_MAIL_MAX_ITEMS', 20);// максимальное число материалов в одном письме
if (!defined('SUBSCRIPTION_ITEM_TEXT_MAXLEN'))
    define('SUBSCRIPTION_ITEM_TEXT_MAXLEN', 230);

if ( ! defined('SUBSCRIPTION_CONTROLPAGE_HEADING'))
    define('SUBSCRIPTION_CONTROLPAGE_HEADING', 'Информационная рассылка портала Хабаровского края');
