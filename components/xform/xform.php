<?php
/**
 * @var \iSite $site
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

function xform_get_form_fields(\iSite $site, $type_id, $field_sort_order = array(), $fields_options = array())
{
    $param_defs = MaterialGetParamDefs($site, $type_id);

    foreach ($param_defs as $param) {
        $field_name = 'field'.$param['id'];
        $field_hrname = $param['name'];

        $field_opts = array();
        if (isset($fields_options[$field_hrname])) {
            $field_opts = $fields_options[$field_hrname];
            unset($fields_options[$field_hrname]);
        }

        $fields[$field_name] = xform_get_field_def($site, $param, $field_opts);
    }
    
    $id_ctr = 1;
    foreach ($fields_options as $name => $field_opts) {
        $field_id = isset($field_opts['id']) ?
            $field_opts['id'] :
            'id'.($id_ctr++);
        $field_name = 'field'.$field_id;
        if ( ! isset($field_opts['label']))
            $field_opts['label'] = $name;

        $fields[$field_name] = xform_get_field_def($site, array(), $field_opts);
    }

    uasort($fields, function ($left, $right) use ($field_sort_order) {
        $order_left = xform_find_prefix($field_sort_order, $left['label']);
        $order_right = xform_find_prefix($field_sort_order, $right['label']);

        return $order_left - $order_right;
    });

    $fields['action'] = array(
        'value' => 'add',
        'type' => 'hidden',
    );

    return $fields;
}

function xform_get_field_def(\iSite $site, $param_info, $field_options = array())
{
    $field = array(
        'type' => 'textarea',
        'required' => true,
    );
    if ( ! empty($param_info['id'])) {
        $field['id'] = 'field'.$param_info['id'];
        $field['z_param_id'] = $param_info['id'];
    }
    if ( ! empty($param_info['valuetype'])) {
        $field['z_param_type'] = $param_info['valuetype'];
    }
    if ( ! empty($param_info['valuemattype'])) {
        $field['z_param_mattype'] = $param_info['valuemattype'];
    }
    if ( ! empty($param_info['name'])) {
        $field['label'] = $param_info['name'];
    }
    
    if (isset($param_info['valuetype'])
        && $param_info['valuetype'] == 'valuemat'
        && ! empty($param_info['valuemattype'])) {
        $field['type'] = 'select';
        $field['options'] = MaterialListNames($site, $param_info['valuemattype']);
    }
    
    if ( ! empty($field_options))
        $field = array_merge($field, $field_options);
    
    return $field;
}

function xform_dub_check(\iSite $site, $attrs)
{
    $cookieName = xform_dub_cookie_name($site);
    if (isset($_COOKIE[$cookieName])) {
        $hash_prev = hex2bin($_COOKIE[$cookieName]);
        if (empty($hash_prev))
            return false;

        $hash_cur = xform_dub_protect_hash($site, $attrs);

        return strcmp($hash_prev, $hash_cur) == 0;
    }

    return false;
}

function xform_dub_cookie_name(\iSite $site)
{
    return 'posted_'.str_replace('.php', '', $site->data->shabname);
}

function xform_dub_protect(\iSite $site, $attrs)
{
    $cookieName = xform_dub_cookie_name($site);
    $hash = xform_dub_protect_hash($site, $attrs);
    $site->addCookie($cookieName, bin2hex($hash), time() + 1800);
}

function xform_dub_protect_hash(\iSite $site, $attrs)
{
    $hash_mat = array();
    foreach ($attrs as $attr)
        $hash_mat[] = $site->values->$attr;
    $hash = md5(implode("\0", $hash_mat), true);
    return $hash;
}

function xform_add_material(\iSite $site, $fields, $cb_after_add = null)
{
    if ( ! $site->validateCsrfToken()) {
        header($_SERVER['SERVER_PROTOCOL'].' 400 Bad Request');
        echo 'Bad Request';
        exit;
    }

    $attrs = array_keys($fields);
    $insert_fields = array();

    $type_id = $site->data->atype;
    $name = '';
    $content  = '';
    $anons = '';
    $tags = '';

    $site->getPostValues($attrs);

    if (xform_dub_check($site, $attrs))
        return true;

    $material_fields = array('name', 'content', 'anons', 'tags');

    $content_trailer = array();

    foreach ($fields as $attr => $field) {
        $field_value = $site->values->$attr;
        $field_label = isset($field['label']) ? $field['label'] : $attr;

        if ($field_label == 'ФИО')
            $name = $field_value;

        if (substr($attr, 0, 6) == 'field_') {
            $attr = substr($attr, 6);

            if (in_array($attr, $material_fields)) {
                $$attr = $field_value;
                continue;
            }
        }

        $is_hidden = isset($field['type']) && $field['type'] == 'hidden';

        if ( ! $is_hidden && ! empty($field_value))
            $content_trailer[] = $field_label.': '.$field_value;
    }

    if (empty($anons) && ! empty($content))
        $anons = $content;

    $id = MaterialNew($site);
    // $insert_fields['id'] = $id;
    $insert_fields['type_id'] = $type_id;
    $insert_fields['name'] = $name;
    // $insert_fields['status_id'] = STATUS_HIDDEN;
    $insert_fields['language_id'] = LANG_RU;
    $insert_fields['id_char'] = MaterialGetNewCharId($site, $type_id, $name);

    $insert_fields['content'] = $content.
        ( ! empty($content_trailer) ?
            "<br/>---<br/>".implode("<br/>", $content_trailer) :
            ''
        );

    $insert_fields['anons'] = $anons;
    $insert_fields['extra_text1'] = $tags;

    $insert_fields['date_add'] = $insert_fields['date_edit'] = $insert_fields['date_event'] =
        array('!', 'NOW()');

    $errors = array();
    
    if (DbUpdate($site, 'materials', $insert_fields, array('id' => $id))) {
        foreach ($fields as $field_name => $field) {
            if ( ! isset($field['z_param_id']))
                continue;
            $field_value = $site->values->$field_name;
            $param_id = $field['z_param_id'];
            $param_type = $field['z_param_type'];
            if ( ! MaterialInsertParam($site, $id, $param_id, $param_type, $field_value)) {
                $errors[] = array('e_param', $param_id);
            }
        }
    } else {
        $errors[] = 'e_material';
    }

    if (empty($errors)) {
        xform_dub_protect($site, $attrs);
        $mat_data = array(
            'id' => $id,
            'id_char' => $insert_fields['id_char'],
            'type_id' => $type_id,
        );
        if (is_callable($cb_after_add))
            call_user_func($cb_after_add, $site, $fields, $mat_data);
    }

    return empty($errors);
}

function xform_find_prefix($prefixes, $str)
{
    foreach ($prefixes as $i => $prefix) {
        if (strncmp($prefix, $str, strlen($prefix)) == 0) {
            return $i;
        }
    }

    return count($prefixes);
}
