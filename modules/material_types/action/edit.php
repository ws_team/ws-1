<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

global $message, $status;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $in_attrs = array(
        'parent_id',
        'ordernum',
        'showinmenu',
        'name',
        'id',
        'id_char',
    );

    $languages = isset($this->data->languages) ? $this->data->languages : array('en');
    $lang_names = array();

    foreach ($languages as $lang) {
        $lang_name = $lang.'_name';
        $in_attrs[] = $lang_name;
        $lang_names[] = $lang_name;
    }

    $this->getPostValues($in_attrs);

    if( $this->values->parent_id != $this->values->id ){

        if (empty($this->values->parent_id) || $this->values->parent_id == $this->values->id) {
            $this->values->parent_name = '';
        }

        $foreign_names = array();
        foreach ($lang_names as $lang_name)
            $foreign_names[$lang_name] = $this->values->$lang_name;

        list($success, $message) = createMaterialType(
            $this,
            $this->values->name,
            $foreign_names,
            $this->values->id,
            $this->values->id_char,
            $this->values->parent_id,
            $this->values->ordernum,
            $this->values->showinmenu
        );
    }else{
        $success = false;
        $message = 'Тип записи не может быть своим же родителем';
    }

    $status = $success ? 'success' : 'error';

    if ($success) {
        $this->data->successtext = $message;
    } else {
        $this->data->errortext = $message;
    }
}

$this->values->imenu = 'edit';
