<?php
/**
 * @var \iSite $this
 */


if (!isset($this->values->mainname))
{
    if($this->data->language == LANG_RU)
    {
        $this->values->mainname = 'События';
    }
    else
    {
        $this->values->mainname = 'Governor';
    }
}

include($this->partial('core'));
