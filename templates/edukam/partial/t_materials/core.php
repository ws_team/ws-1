<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

global $materials,
       $material,
       $jsheaderarr,
       $jsfooterarr;

$this->wpf_include('components/materials/material_types_array.php');

$mainname = $this->values->mainname;

if ( ! isset($secname) && $this->values->sectype != '') {
    $secname='';
    $query = 'SELECT "name", "en_name" FROM "material_types" WHERE "id" = $1';
    if ($res=$this->dbquery($query, array($this->values->sectype))) {
        if(count($res) > 0) {
            if ($this->data->language == LANG_RU) {
                $secname = $res[0]['name'];
            } else {
                $secname = $res[0]['en_name'];
            }
        }
    }
}

$shabname = '';
$shabid = '';

$this->data = MakeMaterialHeadersMetaData($this);

if ($this->data->description == 'mat') {
    if (!isset($material)) {
        $material = MaterialGet($this, $this->values->id);
    }

    if($material['anons'] == '') {
        $this->data->description = $material['anons'];
    } else {
        $this->data->description = $this->data->title;
    }
}

$shabname = $this->data->shabname;
$shabid = $this->data->shabid;

if ($this->data->keywords == '') {
    $word_counter = new Counter();
    $keycontent = mb_strtolower($this->data->description.$this->data->title,'utf8');

    // если длинна строки больше 50000
    if (mb_strlen($keycontent,'utf8') > 50000)
    {
        // подбор слов
        $this->data->keywords = $word_counter->get_keywords(mb_substr($keycontent, 0, 50000,'utf8'));
    }
    else // если меньше
    {
        // подбор слов
        $this->data->keywords = $word_counter->get_keywords($keycontent);
    }
}

$path = $this->locateTemplate('settings/template_reassign');
if ($path)
    include_once($path);


if (isset($standart_shablons[$shabname])) {
    $shabname = $standart_shablons[$shabname];
}

//переадресация шаблонов на стандартные шаблоны (из настроек типа материала)

if ($shabid != '') {
    if ($this->values->id != '') {
        $query = 'SELECT "s"."file_name"
        FROM "standart_shablons" "s", "material_types" "t"
        WHERE "t"."id" = $1 AND "t"."template" = "s"."id"';
    } else {
        $query = 'SELECT "s"."file_name"
        FROM "standart_shablons" "s", "material_types" "t"
        WHERE "t"."id" = $1 AND "t"."template_list" = "s"."id"';
    }

    if ($tres = $this->dbquery($query, array($shabid))) {
        if (count($tres) > 0) {
            $shabname = 'standart_shablons/'.$tres[0]['file_name'];
        }
    }
}


if (substr($shabname, -4, 4) == '.php')
    $shabname = substr($shabname, 0, -4);

$is_homepage = trim(
    (($p = strpos($_SERVER['REQUEST_URI'], '?')) !== false ?
        substr($_SERVER['REQUEST_URI'], 0, $p) :
        $_SERVER['REQUEST_URI']
    ),
    '/') === '';

$bodycachename = $_SERVER['REQUEST_URI'].'uri'.$_SERVER['REQUEST_URI'];
$bodycachename = str_replace('/','_rsla_',$bodycachename);
$bodycachename = str_replace('\\','_lsla_',$bodycachename);
$bodycachename = str_replace('?','_qest_',$bodycachename);
$bodycachename = str_replace('&','_and_',$bodycachename);

$use_cache = defined('WPF_PAGECACHE') && constant('WPF_PAGECACHE')
    && $shabname != 'standart_shablons/pool'
    && $shabname != 'standart_shablons/voting'
    && $shabname != 'standart_shablons/main'
    && $shabname != 'f_materials_1'
    && ! $this->getUser()->isAdmin()
    && in_array($_SERVER['REQUEST_METHOD'], array('GET', 'HEAD'));

$cache_lifetime = isset($this->settings->cache['lifetime_htmlbody']) ?
    $this->settings->cache['lifetime_htmlbody'] :
    61;

$body = null;

if ($use_cache) {
    $body = HTMLCacheRead($this, $bodycachename);

    if ($is_homepage) {
        if ( ! strpos($body, 'contentblock m1_topban')
            || ! preg_match('/<div class="main_news">(.*?)<\/div>/', $match)) {
            $body = null;
        } else {
            $text = $match[1];
            $text = strip_tags($text);
            $text = trim($text);
            if (empty($text))
                $body = null;
        }
    }

    $jsfooterarr = ArrCacheRead($this, $bodycachename.'_jsarr');
    $jsheaderarr = ArrCacheRead($this, $bodycachename.'_jsarrh');
}

if (empty($body)) {

    $generated = date('d.m.Y H:i:s');

    $temptext = ob_get_contents();
    ob_clean();

    ob_start();

    $tpl_orig = $shabname;

    //подключаем шаблоны
    $path = $this->locateTemplate($shabname);

    //если нет спец шаблона типа материала, подключаем общий шаблон
    if ( ! $path) {
        $shabname = !empty($this->values->id) ? 'textmat' : 'treatment';
        $path = $this->locateTemplate('standart_shablons/' . $shabname);
    }

    $tpl_final = $shabname;

    $this->setPartialDir(basename($path, '.php'));

    $GLOBALS["localtemplatetime"]=microtime(true)-$GLOBALS["starttime"];

    include_once($path);

    ?><!-- tpl.orig:<?= $tpl_orig ?>--><!-- tpl.final:<?= $tpl_final ?> --><?php

    $body = ob_get_clean();

    if ( ! $this->isCachePrevented()) {
        HTMLCacheWrite($this, $body, $bodycachename, $cache_lifetime);
        ArrCacheWrite($this, $jsfooterarr, $bodycachename.'_jsarr');
        ArrCacheWrite($this, $jsheaderarr, $bodycachename.'_jsarrh');
    }
}

if ( ! empty($this->values->id) && ! empty($material)) {
    include($this->partial('/xmaterial/meta'));
}

$GLOBALS["templatemeta"]=microtime(true)-$GLOBALS["starttime"];

$path = $this->partial('/t_materials/insert_dynamics');
if ($path)
    include_once($path);

$GLOBALS["templatedynamics"]=microtime(true)-$GLOBALS["starttime"];

$omitLayout = $this->getTemplateOption('omit_layout');
if ( ! $omitLayout) {
    $path = $this->locateTemplate('f_header');
    if ($path)
        include_once($path);
}

if ( ! empty($temptext))
    print $temptext;

print $body;

if ($this->getUser()->isAdmin()) {
?><!-- bc:<?= $bodycachename ?>:$ --><?php
}

$GLOBALS["templatefooter"]=microtime(true)-$GLOBALS["starttime"];

if ( ! $omitLayout) {
    $path = $this->locateTemplate('f_footer');

    if ($path)
        include_once($path);
}
