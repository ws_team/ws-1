<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$valid_actions = array('download', 'index');

$this->getPostValue('action');
$action = $this->values->action;

if (empty($action)) {
    $this->redirect(array(
        'export',
        'action' => $valid_actions[0],
    ));
}

if ( ! in_array($action, $valid_actions))
    $this->badRequest();

$AuthInfo=$this->getUser()->getAuthInfo();
if($AuthInfo->autorized === 1)
{
    require_once('functions.php');

    require_once(__DIR__.'/action/'.$action.'.php');
}
else
{
    $this->forbidden();
}



