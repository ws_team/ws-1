<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


if (!defined('MATTYPE_USEFULRESRC'))
    define('MATTYPE_USEFULRESRC', 31);

if (!defined('MATTYPE_EVENTS'))
    define('MATTYPE_EVENTS', 2);
if (!defined('MATTYPE_EVENT'))
    define('MATTYPE_EVENT', 2);
if (!defined('MATTYPE_NEWS'))
    define('MATTYPE_NEWS', 3);
if (!defined('MATTYPE_ANNOUNCE'))
    define('MATTYPE_ANNOUNCE', 4);
if (!defined('MATTYPE_FORMEDIA'))
    define('MATTYPE_FORMEDIA', 7);

if (!defined('MATTYPE_VIDEO'))
    define('MATTYPE_VIDEO', 5);
if (!defined('MATTYPE_PHOTO'))
    define('MATTYPE_PHOTO', 6);
if (!defined('MATTYPE_DOCUMENTS'))
    define('MATTYPE_DOCUMENTS', 25);
if (!defined('MATTYPE_DOCUMENT'))
    define('MATTYPE_DOCUMENT', 25);

if (!defined('MATTYPE_HEAD'))
    define('MATTYPE_HEAD', 32);
if (!defined('MATTYPE_DEPARTMENT'))
    define('MATTYPE_DEPARTMENT',33);

if (!defined('MATTYPE_HOME'))
    define('MATTYPE_HOME', 1);

if (!defined('MATTYPE_GOVERNCONGRATS'))
    define('MATTYPE_GOVERNCONGRATS', 0);
if (!defined('MATTYPE_GOVERNAPPEAL'))
    define('MATTYPE_GOVERNAPPEAL', 0);
if (!defined('MATTYPE_GOVERNPUB'))
    define('MATTYPE_GOVERNPUB', 0);

if (!defined('MATTYPE_BANNER'))
    define('MATTYPE_BANNER', 33);

if (!defined('MATTYPE_SIGNEVENT_PARAM_TYPE'))
    define('MATTYPE_SIGNEVENT_PARAM_TYPE', 14);
if (!defined('MATTYPE_SIGNEVENT_PARAM_TYPE_EACHYEAR'))
    define('MATTYPE_SIGNEVENT_PARAM_TYPE_EACHYEAR', 553);
if (!defined('MATTYPE_SIGNEVENT_PARAM_TYPE_ONLYYEAR'))
    define('MATTYPE_SIGNEVENT_PARAM_TYPE_ONLYYEAR', 554);

if (!defined('MATTYPE_ACCREDFORM'))
    define('MATTYPE_ACCREDFORM', 0);

if (!defined('MATTYPE_EVENT_IMPORTANT'))
    define('MATTYPE_EVENT_IMPORTANT', 0);

