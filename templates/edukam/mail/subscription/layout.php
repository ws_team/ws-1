<?php
/**
 * @var \iSite $site
 * @var string $title
 * @var string $content
 * @var string $footer
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$hostname = isset($site->settings->primary_domain) ?
    $site->settings->primary_domain :
    $_SERVER['SERVER_NAME'];

$logoUrl = 'https://'.$hostname.'/img/logo3.png';

$footerText = 'Официальный сайт Хабаровского края и Правительства Хабаровского края';
$headerTitle = 'Правительство Хабаровского края';

$site->wpf_include('modules/settings/functions.php');

$footerText = subscription_settings($site, 'email.footer_text', $footerText);
$headerTitle = subscription_settings($site, 'email.header_title', $headerTitle);

if (function_exists('settings_get')) {
}

$title = str_replace('&amp;', '&', $title);

?>
<!DOCTYPE html>
<html>
<head>
    <title><?=$title?></title>
</head>
<body>
<header>
    <p><?php
        if (!empty($logoUrl)) {
        ?>
        <span>
            <img src="<?= htmlspecialchars($logoUrl) ?>" alt="Герб Хабаровского края" height="80" />
        </span>
    <?php
        }
        echo $headerTitle;
    ?>

    </p>
    <h1><?=$title?></h1>
</header>
<main>
    <?= $content ?>
</main>

<hr />
<footer>
    <?php
    if (!empty($footer)) {
        ?><p><small><?= $footer ?></small></p><?php
    }
    ?>
    <p><small>&copy; 2014-<?= date('Y') ?> <?= $hostname ?><br/>
        <?= $footerText ?></small></p>
</footer>
</body>
</html>