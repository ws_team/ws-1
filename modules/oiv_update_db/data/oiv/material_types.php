<?php
/**
 * Стандартные типы материалов ОИВ.
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

$mattypesarr = array();

/*
$mattypesarr[0]['name']='Объявления';
$mattypesarr[0]['en_name']='Advertisement';
$mattypesarr[0]['id_char']='Objavlenija';
$mattypesarr[0]['parent_char']='Vakansii';
$mattypesarr[0]['showinmenu']='1';
$mattypesarr[0]['ordernum']='0';
$mattypesarr[0]['template_list_char']='advertisement_list';
$mattypesarr[0]['template_char']='advertisement_detail';
$mattypesarr[0]['perpage']='0';
$mattypesarr[0]['hide_anons']='';
$mattypesarr[0]['hide_parent_id']='1';
$mattypesarr[0]['hide_files']='';
$mattypesarr[0]['hide_content']='';
$mattypesarr[0]['hide_extra_number']='1';
$mattypesarr[0]['hide_conmat']='';
$mattypesarr[0]['hide_date_event']='';
$mattypesarr[0]['hide_contacts']='1';
$mattypesarr[0]['mat_as_type']='';
$mattypesarr[0]['hide_tags']='';
$mattypesarr[0]['hide_lang']='';
$mattypesarr[0]['params']=Array();
*/
/*
$mattypesarr[]= array(
    'name' => 'Подписка',
    'en_name' => 'subscription',
    'id_char' => 'subscription',
    'template_list_char' => 'subscription',
    'parent_char' => null,
    'showinmenu' => '0',
);*/

/*$mattypesarr[]= array(
    'name' => 'Защита персональных данных',
    'en_name' => 'Protection of personal information',
    'id_char' => 'protection',
    'parent_char' => 'Ob-Upravlenii',
    'parent_char2' => 'O-Ministerstve',
    'parent_char3' => 'O-komitete',
    'showinmenu' => '1',
    'template_list_char' => 'document_list',
    'template_char' => 'news_detail'
);*/

// {{INSERT}}

return $mattypesarr;