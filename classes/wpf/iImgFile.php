<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


require_once('iFile.php');

//подкласс файлового класса для работы с изображениями - имеет свою директорию размещения и создания миниатюр, водяных знаков
class iImgFile extends iFile
{
    public $parentid;
    public $width;
    public $height;
    public $alt;
    public $title;
    public $error;
    public $warning;


    //Переделать! Принимать не обьект сайта, а только кусок его настроек и БД!!!
    public function __construct($siteObj, $fileid=null)
    {
        $this->siteObj=$siteObj;

        $this->dirpath=$this->siteObj->settings->path.'/photos';
        $this->SQLTable='rs_images';

        $this->error = new stdClass();

        $this->error->flag=0;
        $this->error->text='';

        $this->warning = new stdClass();

        $this->warning->flag=0;
        $this->warning->text='';

        //инициализируем имеющийся файл
        if ($fileid != null) {
            $this->RefreshFileFromDB($fileid);
        } else {//формируем заготовку для нового файла
            $this->id = null;
        }
    }

    function RefreshFileFromDB($fileid)
    {
        $fres=false;
        $this->id=$fileid;
        $query="SELECT * FROM ".$this->SQLTable." WHERE id = ".$this->id;

        if($res=$this->siteObj->dbquery($query))
        {
            $fres=true;

            $this->extension=$res[0]['extension'];
            $this->filename=$res[0]['filename'];
            $this->content=$res[0]['content'];
            $this->dateadd=$res[0]['dateadd'];
            $this->size=$res[0]['size'];
            $this->module_connect=$res[0]['module'];
            $this->module_item_connect=$res[0]['item'];

            $this->parentid=$res[0]['parentid'];
            $this->width=$res[0]['width'];
            $this->height=$res[0]['height'];
            $this->alt=$res[0]['alt'];
            $this->title=$res[0]['title'];

            //иконка файла (сделать!)

        }
        else
        {
            $this->error->flag=1;
            $this->error->text='
            <br>Данный файл не найден в БД';
        }
        return($fres);
    }

    public function Save($postname = null, $filename = null, $extension = null, $content = null, $module_connect, $module_item_connect, $width = null, $height = null, $alt = null, $title = null, $parentid = null)
    {

        $fres=false;


        if($filename != null)
        {
            $this->filename=$filename;
        }

        if($extension != null)
        {
            $this->extension=$extension;
        }

        if($content != null)
        {
            $this->content=$content;
        }

        if($module_connect != null)
        {
            $this->module_connect=$module_connect;
        }

        if($module_item_connect != null)
        {
            $this->module_item_connect=$module_item_connect;
        }

        if($width != null)
        {
            $this->width=$width;
        }

        if($height != null)
        {
            $this->height=$height;
        }

        if($alt != null)
        {
            $this->alt=$alt;
        }

        if($title != null)
        {
            $this->title=$title;
        }

        if($parentid != null)
        {
            $this->parentid=$parentid;
        }

        //сохраняем файл, если передан postname
        if($postname != '')
        {
            $this->Upload($postname);
        }

        //обновляем свойства файла
        if($this->id != null)
        {
            $query="UPDATE ".$this->SQLTable." SET extension = '".$this->extension."', filename = '".$this->filename."', content='".$this->content."', dateadd = NOW(), size = '".$this->size."', module = '".$this->module_connect."', item= '".$this->module_item_connect."', width= '".$this->width."', height= '".$this->height."', alt= '".$this->alt."', title= '".$this->title."', parentid= '".$this->parentid."' WHERE id = ".$this->id;
            if($res=$this->siteObj->dbquery($query))
            {
                $fres=true;
            }
        }
        else
        {
            $this->error->flag=1;
            $this->error->text.="
            <br>Нельзя соханить файл - произошла ошибка при получении id файла (скорее всего файл не передан)!";
        }

        return($fres);

    }

    public function Resize($iforiginal=true, $extension=false, $maxwidth='1024', $eqwidth=false, $eqheight=false)
    {

        $ifcrop=false;
        $error=false;
        $fres=false;

        //iforiginal - если true - то меняем текущий файл, иначе - создаем дочерний файл (например маленькую привьюшку)
        //maxwidth - максимальные значения раазмера изображения (если не задано точных размеров) - изображение сжымается/увеличивается до максимального размера
        //eqwidth, eqheight - Точные значения изображения. Изображение сжывается/увеличивается до точных показателей. Если задан только один размер, то изображение изменяет переданную сторону, а другую изменяет пропорционально.
        //ifcrop - если выбрано оба размера, то изображение изменяется до указанных размеров и обрезается, если ifcrop false, то изрбражение меняется до размеров не учитывая исходные пропорции
        //extension - расширение полученного изображения. Чтобы не запутаться, ихображения с префиксом лучше не сохранять в отличных от основного изображения расширениях
        //функция возвращает false если не получилось, либо id новой картинки


        if(!$extension)
        {
            $extension=$this->extension;
        }

        if(($this->width == 0)||($this->height == ''))
        {
            $error=true;
        }

        if(!$error)
        {

            //получаем значения новых размеры
            if($eqwidth||$eqheight)
            {

                //переданы обе точные значения
                if($eqwidth&&$eqheight)
                {


                    $newwidth=$eqwidth;
                    $newheight=round(($this->height * $newwidth) / $this->width);

                    if($newheight < $eqheight)
                    {
                        $newheight=$eqheight;
                        $newwidth=round(($this->width * $newheight) / $this->height);
                    }

                    if(($newheight != $eqheight)||($newwidth != $eqheight))
                    {
                        $ifcrop=true;
                    }


                }
                //передана точная высота
                elseif($eqheight)
                {

                }
                //передана точная шырина
                else
                {

                }

            }
            else
            {



                //преобразуем картинку до максимального значения (получаем новое значение height и width)
                if($this->width > $this->height)
                {
                    $newwidth=$maxwidth;
                    $newheight=round(($this->height * $newwidth) / $this->width);
                }
                else
                {
                    $newheight=$maxwidth;
                    $newwidth=round(($this->width * $newheight) / $this->height);
                }

            }


            //открываем текущую картинку для ресайза,
            //создаем новую картинку и ресайзим в нее оригинал
            $destination = $this->dirpath.'/'.$this->id.'.'.$this->extension;

            switch($this->extension)
            {
                case 'jpg':
                    $im=imagecreatefromjpeg($destination);
                    $im1=imagecreatetruecolor($newwidth,$newheight);
                    break;
                case 'jpeg':
                    $im=imagecreatefromjpeg($destination);
                    $im1=imagecreatetruecolor($newwidth,$newheight);
                    break;
                case 'png':
                    $im=imagecreatefrompng($destination);
                    $im1=imagecreatetruecolor($newwidth,$newheight);
                    imagealphablending($im1, false);
                    imagesavealpha($im1, true);
                    break;
                case 'bmp':
                    $im=imagecreatefrombmp2($destination);
                    $im1=imagecreatetruecolor($newwidth,$newheight);
                    break;
                case 'gif':
                    $im=imagecreatefromgif($destination);
                    $im1=imagecreatetruecolor($newwidth,$newheight);
                    break;
            }




            imagecopyresampled($im1,$im,0,0,0,0,$newwidth,$newheight,imagesx($im),imagesy($im));

            //если нужно - обрезаем изображение
            if($ifcrop)
            {
                $im2=imagecreatetruecolor($eqwidth, $eqheight);
                if($newwidth > $eqwidth)
                {

                    $cropx=(int)(($newwidth-$eqwidth)/2);
                    $cropy=0;

                }
                elseif($newheight > $eqheight)
                {

                    $cropy=(int)(($newheight-$eqheight)/2);
                    $cropx=0;

                }

                imagecopyresampled($im2, $im1, 0, 0, $cropx, $cropy, $eqwidth, $eqheight, $eqwidth, $eqheight);
                $im1=$im2;
                unset($im2);

                $newwidth=$eqwidth;
                $newheight=$eqheight;

            }

            //если работаем с текущим файлом
            if($iforiginal)
            {
                $outfile=$destination;

                if($extension != $this->extension)
                {
                    $this->extension=$extension;
                    $outfile=$this->dirpath.'/'.$this->id.'.'.$this->extension;
                }

                $this->width=$newwidth;
                $this->height=$newheight;

                $this->Save(null, $this->filename, $extension, $this->content, $this->module_connect, $this->module_item_connect, $newwidth, $newheight, $this->alt, $this->title, null);
                $fres=$this->id;
            }
            //создаем копию изображения
            else
            {
                $newimg=new iImgFile($this->siteObj);
                if($newimg->DoNew())
                {
                    $newimg->Save(null, $this->filename, $extension, $this->content, $this->module_connect, $this->module_item_connect, $newwidth, $newheight, $this->alt, $this->title, $this->id);

                    $outfile=$newimg->dirpath.'/'.$newimg->id.'.'.$newimg->extension;
                    $fres=$newimg->id;
                    unset($newimg);
                }
                else
                {
                    if(isset($im))
                    {imagedestroy($im);}
                    if(isset($im1))
                    {imagedestroy($im1);}

                    return(false);
                }
            }

            //записываем новое изображение на диск
            switch($extension)
            {
                case 'jpg':
                    imagejpeg($im1,$outfile,100);
                    break;
                case 'jpeg':
                    imagejpeg($im1,$outfile,100);
                    break;
                case 'png':
                    imagepng($im1,$outfile);
                    break;
                case 'gif':
                    imagegif($im1,$outfile);
                    break;
                case 'bmg':
                    imagebmp($im1,$outfile);
                    break;
            }

            //очищаем то с чем работали
            if(isset($im))
            {imagedestroy($im);}
            if(isset($im1))
            {imagedestroy($im1);}
        }
        else
        {
            $fres=false;
        }
        return($fres);

    }

    public function DoNew()
    {
        $fres=false;
        if($this->id == null)
        {

            //создаём новую пустую картинку без файла!!!
            $query="INSERT INTO ".$this->SQLTable."(id, extension, filename, content, dateadd, size, module, item, width, height, alt, title, parentid)
                    VALUES (null, '', '', '', NOW(), '0', -1, -1, 0, 0, '', '', NULL)";

            if($res=$this->siteObj->dbquery($query))
            {

                //получаем id файла
                $query="SELECT max(id) id FROM ".$this->SQLTable." WHERE filename = '".$this->filename."'";
                if($res=$this->siteObj->dbquery($query))
                {
                    $this->id=$res[0]['id'];
                    $fres=true;
                }

            }
            else
            {
                $this->error->flag=1;
                $this->error->text='
                        <br>Ошибка записи файла в БД!';
            }

        }

        return($fres);

    }

    public function Upload($postname)
    {

        $fres=false;

        if (isset ($_FILES[$postname]['tmp_name'])) {$userfile=$_FILES[$postname]['tmp_name'];} else {$userfile="";}
        if (isset ($_FILES[$postname]['size'])) {$userfile_size=$_FILES[$postname]['size'];} else {$userfile_size="";}
        if (isset ($_FILES[$postname]['type'])) {$userfile_type=$_FILES[$postname]['type'];} else {$userfile_type="";}
        if (isset ($_FILES[$postname]['name'])) {$userfile_name=$_FILES[$postname]['name'];} else {$userfile_name="";}

        if(($userfile == ''))
        {
            $this->error->flag=1;
            $this->error->text='
            <br>Указанный файл ('.$postname.') не передан!';
            //print_r($_FILES);
        }
        else
        {
            $this->size=$userfile_size;
            $this->filename=$userfile_name;

            $this->extension=GetFileExtension($this->filename);

            //определяем тип картинки и её размеры
            $smallsizes=getimagesize($userfile);
            $typecode=2;
            switch($smallsizes[2])
            {
                case '2':
                    //jpg
                    $typecode=2;
                    $this->extension='jpg';
                    break;
                case '3':
                    //png
                    $typecode=3;
                    $this->extension='png';
                    break;
                case '1':
                    //gif
                    $typecode=1;
                    $this->extension='gif';
                    break;
                case '6':
                    //bmp
                    $typecode=6;
                    $this->extension='bmp';
                    break;
            }

            if($smallsizes[2] != $typecode)
            {
                $this->error->flag=1;
                $this->error->text="
                <br>Загруженный файл не является изображением в формате jpg, png, gif или bmp!";
            }
            else
            {

                $this->width=$smallsizes[0];
                $this->height=$smallsizes[1];

                //получаем id файла (если null)
                if($this->id == null)
                {
                    $query="INSERT INTO ".$this->SQLTable."(id, extension, filename, content, dateadd, size, module, item, width, height, alt, title, parentid)
                    VALUES (null, '".$this->extension."', '".$this->filename."', '', NOW(), '".$this->size."', -1, -1, ".$this->width.", ".$this->height.", '', '', NULL)";

                    if($res=$this->siteObj->dbquery($query))
                    {

                        //получаем id файла
                        $query="SELECT max(id) id FROM ".$this->SQLTable." WHERE filename = '".$this->filename."'";
                        if($res=$this->siteObj->dbquery($query))
                        {
                            $this->id=$res[0]['id'];
                        }

                    }
                    else
                    {
                        $this->error->flag=1;
                        $this->error->text='
                        <br>Ошибка записи файла в БД!';
                    }
                }

                //перемещаем новый файл из темпа загрузки в директорию файлов сайта
                $destination = $this->dirpath.'/'.$this->id.'.'.$this->extension;

                if(!move_uploaded_file($userfile, $destination))
                {
                    $this->error->flag=1;
                    $this->error->text='
                    <br>Ошибка передачи файла!';
                }
                else
                {
                    $fres=true;
                }
            }

        }

        return($fres);

    }

}