<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

defined('_WPF_') or die();

global $action, $success;

$messages = opendata_messages($this);
$status_class = $success ? 'success' : 'error';

include($this->locateTemplate('f_header'));

?>
<div class="contentblock basemargin">
    <?php

    if ( ! empty($this->error->text)) {
        ?><div class="<?php if ($this->error->flag) {?>error<?php }
            ?> message"><?= $this->error->text ?></div>
        <?php
    }

    if (isset($message)) {
        ?><div class="message <?= $status_class ?>"><?= $message ?></div><?php
    }

    if (isset($messages)) {
        foreach ($messages as $message) {
            ?><div class="message <?= $status_class ?>"><?= $message ?></div><?php
        }
    }

    $csrf = $this->generateCsrfToken();

    $this->logWrite(LOG_DEBUG, __FILE__, serialize(opendata_import_state()));

    ?>
    <div>
        <a href="<?=
        htmlspecialchars($this->getRouteUrl(array(
                'opendata',
                'action' => 'clear',
                'csrf' => $csrf
            ))) ?>">очистить данные</a> |
        <?php
        if (opendata_import_is_running($this)) {
            ?>
            <a href="<?=
            htmlspecialchars($this->getRouteUrl(array(
                'opendata',
                'action' => 'import',
                'stage' => 'proceed',
                'csrf' => $csrf,
                'state' => serialize(opendata_import_state()),
            ))) ?>">продолжить импорт</a><?php
        } else {?>
            <a href="<?=
                htmlspecialchars($this->getRouteUrl(array(
                    'opendata',
                    'action' => 'import',
                    'stage' => 'seed',
                    'csrf' => $csrf
                ))) ?>">начать импорт</a><?php
        }
        ?>
    </div>
</div>
<?php

if ($action == 'import' && opendata_import_is_running($this)) {
    ?>
    <script>
        setTimeout(function () {
            var query = location.search || '';

            query = query.replace(/&?\bt=\d+/, '');
            query = query.replace(/&?stage=seed\b/, '');
            query += '&stage=proceed';
            query += '&t=' + (new Date()).getTime();

//            return;

            location.search = query;
        }, 3000);
    </script>
    <?php
}

include($this->locateTemplate('f_footer'));