<?php


global $users;

if ($this->getUser()->can('admin.users')) {

include_once($this->locateTemplate('f_header'));

    ?><style>
        div.table
        {
            display:table;
        }
        form.tr {
            display:table-row;
        }
        form.tr span {
            display: table-cell;
            padding-bottom: 30px;
            padding-top: 10px;
            border-bottom: 1px solid #b3b3b3;
        }
    </style>
    <div class="container container--admin-title">
        <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
    </div>
    <div class="contentblock basemargin">
        <p class="errortext"><?php echo $this->data->errortext;  ?></p>
        <table>
            <tr>
                <th width="200">E-mail<br>Организация/Должность</th>
                <th width="200">ФИО</th>
                <th width="200">Роль/Связка ОМСУ</th>
                <th width="120">Смена пароля/Статус</th>
                <th width="120">Разделы</th>
                <th></th>
            </tr>
        </table>
        <div width="100%" class="table-bootstrap table table-striped table-hover table-condensed">
            <?php

            if (isset($users) && count($users) > 0)
            {
                foreach($users as $user)
                {

                    //выводим связанные типы материалов
                    $materials='';

                    if(count($user['materials'] > 0))
                    {
                        foreach($user['materials'] as $material)
                        {
                            $materials.= "<div style='background-color: #c4fcab; padding: 3px; margin: 3px;'>$material[name]&nbsp;<a href='/?menu=users&action=delmattype&user=$user[id]&type=$material[id]'>x</a></div>";
                        }
                    }

                    if($materials == '')
                    {
                        $materials='все';
                    }
                    $omsuname='';
                    if($user['omsu'] != '')
                    {
                        $queryomsu='SELECT "name" FROM "materials" WHERE "id" = '.$user['omsu'];
                        if($resomsu=$this->dbquery($queryomsu))
                        {
                            if(count($resomsu) > 0)
                            {
                                $omsuname='<br><small>('.$resomsu[0]['name'].')</small>';
                            }
                        }
                    }

                    if($user['status'] == 0)
                    {
                        $trstyle='style="background-color: #ffdddd;"';
                        $stylecolor='background-color: #ffdddd;';
                    }
                    else
                    {
                        $trstyle='';
                        $stylecolor='';
                    }

                    echo "<form class='tr' method='post' action='' $trstyle>
                        <span style='width: 200px; $stylecolor'>
                        <input type='hidden' name='id' value='$user[id]'>
                        <input type='text' name='email' value='$user[email]' class=\"form-control\" placeholder='E-mail' style='width: 150px;'>
                        <br><input type='text' name='unit' value='$user[unit]' class=\"form-control\" placeholder='организация' style='width: 150px;'>
                        <br><input type='text' name='post' value='$user[post]' class=\"form-control\" placeholder='должность' style='width: 150px;'>
                        </span>
                        
                        <span style='width: 200px; $stylecolor'>
                        <input type='text' name='fio' value='$user[fio]' class=\"form-control\" placeholder='фамилия' style='width: 150px;'>
                        <br><input type='text' name='firstname' value='$user[firstname]' class=\"form-control\" placeholder='имя' style='width: 150px;'>
                        <br><input type='text' name='middlename' value='$user[middlename]' class=\"form-control\" placeholder='отчество' style='width: 150px;'>
                        </span>

                        <span style='width: 200px; $stylecolor'>
                        <select name='role_id' class='styler width-100'>".printUserRoleOptions($this,$user['role_id'])."</select>
                        <br><br>ОМСУ: 
                        <br><input type='text' name='omsu' class=\"form-control\" style='width: 153px; margin-top: 3px;' value='$user[omsu]'><br>$omsuname
                        </span>
                        
                        <span style='width: 120px; $stylecolor'>
                        <input type='password' name='pass' value='' class=\"form-control\" style='width: 100px;' placeholder='новый пароль'>
                        <br><select name='status' class='styler'>".printUserStatusOptions($this,$user['status'])."</select>
                        </span>
                        
                        <span style='width: 120px; $stylecolor'>$materials
                        <p onclick='addMatToUser($user[id]);' style='text-align: right;'><img class='svg add-icon' src='/images/add1.svg' /></p></span>
                        
                        <span $trstyle>
                        <input type='hidden' name='action' value='edit'>
                        <button type='submit' type=\"button\" class=\"btn btn-default showtooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Изменить пользователя\">
                <span class=\"glyphicon glyphicon-pencil\"></span>
            </button>
                        </span>
                    </form>";
                }
            }

            ?>
        </div><span style="color: #ff0000;">* новый пароль при смене должен удовлетворять следующим условиям: <b>длинна не менее 6 символов, должен содержать и цифры и буквы</b></span>

        <p>&nbsp;</p><p>&nbsp;</p><hr /><p>&nbsp;</p>
        <h2>Добавить пользователя:</h2>
        <table width="100%">
        <tr>
            <th style='width: 200px;'>E-mail<br>Организация/Должность</th>
            <th style='width: 200px;'>ФИО</th>
            <th style='width: 200px;'>Роль</th>
            <th style='width: 200px;'>Пароль/Статус</th>
            <th></th>
        </tr>
        </table>
        <div width="100%" class="table-bootstrap table table-striped table-hover table-condensed">
            <?php
            echo "<form class='tr' method='post' action=''>
                <span style='width: 200px;'>
                <input type='text' name='email' value='' class=\"form-control\" placeholder='E-mail' style='width: 150px;'>
                <br><input type='text' name='unit' class=\"form-control\" placeholder='Организация' style='width: 150px;'>
                <br><input type='text' name='post' class=\"form-control\" placeholder='Должность' style='width: 150px;'></span>
                
                <span style='width: 200px;'><input type='text' name='fio' value='' class=\"form-control\" placeholder='Фамилия' style='width: 150px;'>
                <br><input type='text' name='firstname' class=\"form-control\" placeholder='Имя' style='width: 150px;'>
                <br><input type='text' name='middlename' class=\"form-control\" placeholder='Отчество' style='width: 150px;'></span>
                
                <span style='width: 200px;'><select name='role_id' class='styler'>".printUserRoleOptions($this,2)."</select></span>
                
                <span style='width: 200px;'><input type='password' name='pass' value='' class=\"form-control\" placeholder='Пароль' style='width: 100px;'>
                <br><select name='status' class='styler'>".printUserStatusOptions($this,1)."</select></span>
                
                <span><input type='hidden' name='action' value='add'><input type='submit' class='styler' value='Добавить'></span>
            </form>"; ?>
        </div>


</div>
<div style="display: none;">
<div id="mtselectblock" style="margin: 10px;">
    <p>&nbsp;</p>
    <input type="hidden" id="adduser" value="">
    <select id="mtselect" class="styler maxheight">
    <?php

    print(PrintTypeOptions($this->data->material_types, '', 0));

    ?>
    </select>
    <button onclick="DoAddMatToUser();" class="styler">добавить</button>
</div>
</div>
<?php

    include_once($this->locateTemplate('f_footer'));

}
