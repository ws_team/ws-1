<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

function MaterialGet(\iSite $site, $id = '')
{
    if (!isset($site->values->id) && empty($id))
        $site->GetPostValue('id');

    //определяем id материала для получения

    if (empty($id)) {
        if (empty($site->values->id))
            return false;
        $id = intval($site->values->id);
    }

    //проверяем на наличае материала в кэшк

    if ($site->getCache()->has('material:'.$id)) {
        $material = unserialize($site->getCache()->get('material:'.$id));
        if ( ! empty($material) && ! empty($material['id']) && ! empty($material['name'])) {
            return $material;
        }
    }

    $query = <<<EOS
SELECT
    "m".*,
    TO_CHAR("m"."date_event",'DD.MM.YYYY HH24:MI') "date_event_str",
    (extract(epoch FROM "m"."date_event") - 10*60*60) "unixtime",
    (extract(epoch FROM "m"."date_edit") - 10*60*60) "editunixtime",
    (SELECT "t"."name" FROM "material_types" "t" WHERE "t"."id" = "m"."type_id") "typename",
    (SELECT "t"."mat_as_type" FROM "material_types" "t" WHERE "t"."id" = "m"."type_id") "mat_as_type"
FROM "materials" "m"
WHERE "m"."id" = $1
EOS;

    $qparams = array($id);

    $res = $site->dbquery($query, $qparams);

    if (empty($res))
        return false;

    $material = $res[0];

    $material['videos']=Array();
    $material['translations']=Array();
    $material['photos']=Array();
    $material['audios']=Array();
    $material['documents']=Array();
    $material['connected_materials'] = Array();
    $material['hidden_conn_materials'] = Array();
    $material['contacts']=Array();

    foreach (MaterialGetFiles($site, $material['id'], $material['filesordertype']) as $key => $files)
        $material[$key] = $files;

    $material['content']=str_replace('`',"'",$material['content']);
    $material['anons']=nl2br($material['anons']);

    if ($material['mat_as_type'] == 1) {
        $material['url'] = GetMaterialTypeUrl($site, $material['type_id'], $material['id'], $material['type_id']);
    } else {
        $material['url'] = GetMaterialTypeUrl($site, $material['type_id'], $material['id'], $material['type_id']).'/'.$material['id'];
    }

    if (isset($material['extra_text1'])) {
        global $setup;
        $material['tags'] = vidjet_tags(
            $material['extra_text1'],
            isset($setup['data']['tags_caption']) ? $setup['data']['tags_caption'] : 'Ключевые слова',
            !empty($site->settings->tags['limit_by_mattype']) ?
                GetMaterialTypeUrl($site, $material['type_id']) :
                '/tags'
        );
    } else {
        $material['tags'] = '';
    }

    //связанные материалы
    list(
        $material['connected_materials'],
        $material['hidden_conn_materials']
        ) = MaterialGetConnectedMaterials($site, $material['id']);

    //связанные контакты
    $material['contacts'] = MaterialGetContacts($site, $material['id']);

    $material['eventtime'] = date('H:i',$material['unixtime']);
    $material['eventdatename'] = russian_datetime_from_timestamp($material['unixtime'], false);

    //получаем связанные параметры и их значения
    $material['params'] = MaterialGetParams($site, $material['type_id'], $id);

    //Связанные наборы параметров
    $material['sparams'] = MaterialGetGroupParams($site, $material['type_id'], $id);

    //настройки виджетов
	$material['widgets']=Array();
	$queryv="SELECT * FROM material_vijsettings WHERE matid = ".$id;
	if($resv = $site->dbquery($queryv)){
		if(is_array($resv) && count($resv) > 0){
			foreach ($resv as $vjt){
				$material['widgets'][$vjt['vidjet']]=$vjt['value'];
			}
		}
	}

    $lifetime = 604800;
    $site->getCache()->set('material:'.$id, serialize($material), $lifetime);

    return $material;
}

function getFirstActiveMaterial(\iSite $site, $MatTypeCharId) {
    $MaterialTypeIdArray = getMaterialTypeIdByCharId($site, $MatTypeCharId);
    $MaterialList        = MaterialList($site, $MaterialTypeIdArray[0]['id'], 1, 1, STATUS_ACTIVE);
    $firstActiveMaterial = MaterialGet($site, $MaterialList[0]['id']);

    return $firstActiveMaterial;
}