<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

defined('_WPF_') or die();

$this->getPostValue('item');
$valid_items = array('subscribers', 'survey');

$export_item = $this->values->item;

if (empty($export_item)) {
    $this->redirect(array(
        'export',
        'action' => 'list',
    ));
}

if ( ! in_array($this->values->item, $valid_items))
    $this->badRequest();

if ( ! $this->requireAuthUser()->can('data.export.download:'.$export_item))
    $this->forbidden();

$data_fetch_script = __DIR__.'/download/'.$export_item.'.php';
list($header, $outputItems, $sheetName) = require_once($data_fetch_script);

ob_start();
list($media_type, $fileExt) = include(WPF_ROOT.'/modules/export/exporter/e_xls.php');
$content_body = ob_get_clean();

if ( ! isset($file_name_base))
    $file_name_base = $export_item;
$file_name = $file_name_base.'_'.date('Ymd-Hi').$fileExt;

if (empty($media_type))
    $media_type = 'application/octet-stream';

$this->setResponseHeader('Content-type', $media_type);
$this->setResponseHeader('Content-disposition', 'attachment; filename="'.$file_name.'"');
$this->setResponseHeader('Content-Transfer-Encoding', 'binary');

$this->sendResponse($content_body);

exit;