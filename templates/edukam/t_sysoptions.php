<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

include($this->locateTemplate('f_header'));

$maintenance_active = (bool)$this->getState('maintenance', false);

$csrfToken = $this->generateCsrfToken();
$csrfTokenEnc = urlencode($csrfToken);

$url_maintenance_on = '?menu=sysoptions&action=maintenance&toggle=on&csrf='.$csrfTokenEnc;
$url_maintenance_off = '?menu=sysoptions&action=maintenance&toggle=off&csrf='.$csrfTokenEnc;

$upcoming_release_active = $this->getState('upcoming_release');
$url_upcoming_release_on = '?menu=sysoptions&action=upcoming_release&toggle=on&csrf='.$csrfTokenEnc;
$url_upcoming_release_off = '?menu=sysoptions&action=upcoming_release&toggle=off&csrf='.$csrfTokenEnc;

$this->data->iH1 = 'Системные настройки';

?>
<div class="container container--admin-title">
    <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
</div>
<div class="contentblock basemargin">
    <p class="errortext"><?= ! empty($this->data->errortext) ? $this->data->errortext : ''  ?></p>
    <table>
        <tbody>
        <tr>
            <td>Режим &laquo;технические работы&raquo;</td>
            <td><?= $maintenance_active ? 'активен' : 'не активен'?></td>
            <td>
                <?php
                if ($maintenance_active) {
                    ?>
                    <span>включить</span>
                    <a href="<?= htmlspecialchars($url_maintenance_off) ?>">выключить</a>
                    <?php
                } else {
                    ?>
                    <a href="<?= htmlspecialchars($url_maintenance_on) ?>">включить</a>
                    <span>выключить</span>
                    <?php
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>Режим &laquo;идет релиз&raquo;</td>
            <td><?= $upcoming_release_active ? 'активен' : 'не активен' ?></td>
            <td>
                <?php
                if ($upcoming_release_active) {
                    ?>
                    <span >включить</span>
                    <a href="<?= htmlspecialchars($url_upcoming_release_off) ?>">выключить</a>
                    <?php
                } else {
                    ?>
                    <a href="<?= htmlspecialchars($url_upcoming_release_on) ?>">включить</a>
                    <span>выключить</span>
                    <?php
                }
                ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<?php

include($this->locateTemplate('f_footer'));

?>