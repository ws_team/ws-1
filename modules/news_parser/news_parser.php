<?php

include('simple_html_dom.php');

function crawle_khabarovsk_news($url, $date){

    $news_articles = array();

    // вытягиваем страницу
    $html = file_get_html($url.'/news/');

    if(!empty($html)) {

        // проходимся по каждой новости списка
        foreach ($html->find('p.news-item') as $news_item) {

            // определяем дату
            $date_string_elem = $news_item->find('font.font_oran_12', 0);

            if(isset($date_string_elem) && !empty($date_string_elem->plaintext)) {

                // парсим дату из строки
                $date_string = $date_string_elem->plaintext;
                $news_date = DateTime::createFromFormat('d.m.Y', $date_string);

                // парсим новость, если она актуальна
                if($news_date >= $date) {

                    // ищем ссылку на страницу с новостью
                    $news_link = $url.$news_item->find('a', 0)->href;
                    $news_html = file_get_html($news_link);

                    if(!empty($news_html)) {

                        // парсим новость
                        $news_container = $news_html->find('div.news-detail', 0);
                        if(!empty($news_container)) {

                            // парсим заголовок
                            $news_header = $news_container->find('h3', 0)->plaintext;

                            // парсим содержимое новости
                            $news_content = '';
                            $news_paragraths = $news_container->find('p');

                            if(count($news_paragraths) == 0) {
                                $news_paragraths = $news_container->find('span');
                            }

                            $news_anons = '';
                            if(count($news_paragraths) != 0) {
                                $news_anons = $news_paragraths[0]->plaintext;
                            }

                            foreach($news_paragraths as $news_p) {
                                $news_content .= '<p>'.$news_p->plaintext.'</p>';
                            }
                            $news_content = str_replace('&nbsp;', '', $news_content);

                            array_push($news_articles, array(
                                "header" => $news_header,
                                "anons" => $news_anons,
                                "content" => $news_content,
                                "date" => $news_date,
                                "url" => $news_link,
                                "city_id" => 23
                            ));
                        }
                    }
                }
            }
        }
    }

    return $news_articles;
}

function crawle_komsomolsk_news($url, $date)
{
    $news_articles = array();

    // вытягиваем страницу
    $html = file_get_html($url . '/news/');

    if (!empty($html)) {

        // проходимся по каждой новости списка
        foreach ($html->find('div.post') as $news_item) {

            // парсим дату новости из URL на страницу с новостью
            $news_link = $news_item->find('span.links a.readmore', 0)->href;
            if (isset($news_link) && !empty($news_link)) {
                preg_match("/\/news\/(\d{4})\/(\d{1,2})\/(\d{1,2})/i", $news_link, $output_array);
                if(count($output_array) == 4) {
                    $news_date = DateTime::createFromFormat('d.m.Y', $output_array[3].'.'.$output_array[2].'.'.$output_array[1]);

                }

                // парсим новость, если она актуальна
                if ($news_date >= $date) {

                    $news_html = file_get_html($url.$news_link);

                    if(!empty($news_html)) {
                        $news_container = $news_html->find('div .content', 0);

                        if(isset($news_container) && !empty($news_container)) {
                            // парсим заголовок
                            $news_header = $news_container->find('h1', 0)->plaintext;

                            // парсим содержимое новости
                            $news_content = '';
                            $news_paragraths = $news_container->find('p[!class]');

                            $news_anons = '';
                            if(count($news_paragraths) != 0) {
                                $news_anons = $news_paragraths[0]->plaintext;
                            }

                            foreach($news_paragraths as $news_p) {
                                $news_content .= '<p>'.$news_p->plaintext.'</p>';
                            }

                            $news_content = str_replace('&nbsp;', '', $news_content);

                            array_push($news_articles, array(
                                "header" => $news_header,
                                "anons" => $news_anons,
                                "content" => $news_content,
                                "date" => $news_date,
                                "url" => $news_link,
                                "city_id" => 24
                            ));

                        }
                    }
                }
            }
        }
    }

    return $news_articles;
}

function crawle_vacancies($url){

    $vacancies = array();
    $page_postfix = "/local_adm/mun_service/vacancies/results/?PAGEN_2=";
    $page_count = 1;

    $html = file_get_html($url . $page_postfix . "1");

    if(!empty($html)) {
        foreach ($html->find('font.text') as $page_font) {
            foreach ($page_font->find('a') as $page_a) {
                if (preg_match("/PAGEN_2=(\d)$/", $page_a->href) > 0) {
                    $splitted_href = explode("=", $page_a->href, 2);
                    if (count($splitted_href) == 2 && is_numeric($splitted_href[1])) {
                        if(intval($splitted_href[1]) > $page_count) {
                            $page_count = intval($splitted_href[1]);
                        }
                    }
                }
            }
        }
    }

for($page = 1; $page <= $page_count; $page++) {
    $html = file_get_html($url . $page_postfix . $page);

    if (!empty($html)) {
        foreach($html->find('p.news-item') as $vacancy) {
            $vacancy_url = $vacancy->find('a', 0);

            if(!empty($vacancy_url)) {
                $vacancy_html = file_get_html($url.($vacancy_url->href));

                if(isset($vacancy_html) && !empty($vacancy_html)) {
                    $vacancy_container = $vacancy_html->find('div.news-detail', 0);

                    if(!empty($vacancy_container)) {
                        $vacancy = array();

                        $vacancy['header'] = $vacancy_container->find('h3', 0)->plaintext;

                        $vacancy_content = '';
                        $vacancy_paragraths = $vacancy_container->find('p');

                        foreach($vacancy_paragraths as $news_p) {
                            $vacancy_content .= '<p>'.$news_p->plaintext.'</p>';
                        }
                        $vacancy["content"] = str_replace('&nbsp;', '', $vacancy_content);

                        array_push($vacancies, $vacancy);
                    }
                }
            }
        }
    }
}

    return $vacancies;
}

function crawle($site) {

    $user_id = 1;

    $user_query = 'SELECT "fio", "email", "role_id" FROM "users" WHERE "id"  = '.$user_id;

    if($res = $site->dbquery($user_query)) {
        // авторизация
        $site->autorize->answer='Авторизироваться удалось';

        $site->autorize->autorized=1;
        $site->autorize->username = "Учетные записи администратора ОИВ";
        $site->autorize->userlogin = $res[0]['fio'];
        $site->autorize->userid = $user_id;
        $site->autorize->userrole = 1;
        $site->autorize->email= $res[0]['email'];

        echo "<p>Пользователь: ".$site->autorize->email."</p>";

        $site->autorize->materialtypes=Array();

        $site->autorize->omsu='';
        $site->autorize->reg='';
    }

    // дата, начиная с которой будет браться адрес
    $crawler_date = new DateTime();
    $crawler_date->modify('-1 day');

    // ссылки для краулинга
    $khabarovsk_server_url = "http://www.khabarovskadm.ru";
    $komsomolsk_server_url = "http://www.kmscity.ru";
    $vacancies_server_url = "http://www.khabarovskadm.ru";

    $news_articles = array_merge(crawle_khabarovsk_news($khabarovsk_server_url, $crawler_date),
        crawle_komsomolsk_news($komsomolsk_server_url, $crawler_date));

    echo "<div>";
    echo "<h2>Новости</h2>";
    echo "<p>Распарсено новостей: ".count($news_articles)."</p>";


    // дата, за которую проверяем существование новости
    $sql_date = $crawler_date->modify('-2 day')->format('d-m-Y');
    $inserted_news = 0;

    foreach($news_articles as $article) {
        $query = 'SELECT count("id") "cid" FROM "materials" WHERE "type_id"=2 AND "language_id" = 1 AND "date_add" >= TO_DATE(\''
            .$sql_date.'\', \'DD-MM-YYYY\') AND "name" = \''.$article["header"].'\'';

        if($res = $site->dbquery($query)) {
            // если новость еще не добавлена
            if($res[0]['cid'] == 0) {
                $site->values->id='';
                $site->values->matforconnect = '';
                $site->values->id_char='';

                $site->values->name = $article['header'];
                $site->values->type_id = 2;
                $site->values->language_id = 1;
                $site->values->content = $article['content'];
                $site->values->date_add = $article['date'];
                $site->values->status_id = 1;
                $site->values->anons = $article['anons'];

                createMaterial($site);

                // добавляем связь с районом
                $get_inserted_query = 'SELECT "id" FROM "materials" WHERE "type_id"=2 AND "language_id" = 1 AND "name" = \''.$article["header"].'\'';

                if($inserted_res = $site->dbquery($get_inserted_query)) {
                    if(count($inserted_res) > 0) {
                        $site->values->matforconnect = $article['city_id'];
                        $site->values->id = $inserted_res[0]['id'];

                        DoConnectMaterial($site);
                    }
                }

                $inserted_news++;
            }
        }
    }

    echo "<p>Добавлено новостей: ".$inserted_news."</p>";
    echo "</div>";

    $vacancies = crawle_vacancies($vacancies_server_url);

    echo "<div>";
    echo "<h2>Результаты конкурсов</h2>";
    echo "<p>Распарсено результатов конкурсов: ".count($vacancies)."</p>";

    $inserted_vacancies = 0;

    foreach($vacancies as $vacancy) {
        $query = 'SELECT count("id") "cid" FROM "materials" WHERE "type_id" = 49 AND "language_id" = 1 AND "name" = \''.$vacancy["header"].'\'';

        if($res = $site->dbquery($query)) {
            if($res[0]['cid'] == 0) {

                $site->values->id='';
                $site->values->matforconnect = '';
                $site->values->id_char='';

                $site->values->name = $vacancy['header'];
                $site->values->type_id = 49;
                $site->values->language_id = 1;
                $site->values->content = $vacancy['content'];
                $site->values->date_add = new DateTime();
                $site->values->status_id = 1;

                createMaterial($site);

                $inserted_vacancies++;
            }
        }
    }

    echo "</div>";
    echo "<p>Добавлено результатов конкурсов: ".$inserted_vacancies."</p>";
    echo "</div>";
}

crawle($this);
