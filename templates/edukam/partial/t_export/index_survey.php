<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$mattype_id = FindTypeIdByChar($this, 'survey');

$this->getPostValue('page');

$page = intval($this->values->page);
if ($page <= 1)
    $page = 1;

$page_size = 20;
$offset = ($page - 1) * $page_size;

$query = <<<EOS
SELECT
    m.id,
    m.name,
    m.anons,
    m.id_char,
    m.extra_number,
    m.status_id,
    (
        SELECT MAX(s.num_answers)
        FROM survey s
        WHERE s.survey_id = m.id
    ) num_participants
FROM
    materials m
WHERE
    m.type_id = $1
    AND m.status_id != $2
ORDER BY extra_number
LIMIT $page_size
OFFSET $offset
EOS;

$q_params = array($mattype_id, STATUS_DELETED);

$survey_items = $this->dbquery($query, $q_params);

$query = <<<EOS
SELECT COUNT(*) num
FROM materials 
WHERE
    type_id = $1
    AND status_id <> $2
EOS;

$q_params = array($mattype_id, STATUS_DELETED);

$res = $this->dbquery($query, $q_params);

$total_items = ! empty($res) ? $res[0]['num'] : 0;


$base_url = GetMaterialTypeUrl($this, $mattype_id);

?>
<h2>Анкеты</h2>
<p>&nbsp;</p>
<table id="survey_items" class="survey-items">
    <thead>
        <tr>
            <th>ID</th>
            <th>Заголовок</th>
            <th>Статус</th>
            <th>Заполнили анкету</th>
            <th>Экспорт результатов</th>
            <th>Экспорт анкет</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($survey_items as $item) {
            $url = $base_url.'/'.$item['id'];
            $url_export = $this->getRouteUrl(array(
                'export',
                'action' => 'download',
                'item' => 'survey',
                'id' => $item['id']
            ));

            $url_allankets = $this->getRouteUrl(array(
                'export_allankets',
                'id' => $item['id']
            ));

            $url_results = $url.'?showresults=1';

            $status_name = $item['status_id'] == STATUS_ACTIVE ? 'активна' : 'скрыта';

            //получаем поля для группировки

            $sq_type_id = FindTypeIdByChar($this, 'survey-q');

            $queryg='SELECT m.id, m.name FROM materials m WHERE m.type_id = '.$sq_type_id.' AND m.status_id = 2 AND m.id IN (
                
                SELECT c.material_parent FROM material_connections c WHERE c.material_child = '.$item['id'].'
                
                UNION ALL
                
                SELECT z.material_child FROM material_connections z WHERE z.material_parent = '.$item['id'].'
                
            )';

            //krumo($query);

            $resg=$this->dbquery($queryg);

            $goptions='';

            if($resg && count($resg) > 0){
                foreach ($resg as $rowg)
                {
                    if(mb_strlen($rowg['name'],'utf8') > 50)
                    {
                        $rowg['name']=mb_substr($rowg['name'],0,50,'utf8').'...';
                    }

                    $goptions.='<option value="'.$rowg['id'].'">'.$rowg['name'].'</option>';
                }
            }


            ?>
            <tr>
                <td><?= $item['id'] ?></td>
                <td><a href="<?= htmlspecialchars($url) ?>" target="_blank"><?= $item['name'] ?></a></td>
                <td><?= $status_name ?></td>
                <td><?= $item['num_participants'] ?></td>
                <td>
                    <a href="<?= htmlspecialchars($url_export) ?>" target="_blank">экспорт в Excel (старый)</a>
                    | <a href="<?= htmlspecialchars($url_results) ?>" target="_blank">результаты</a>
                    <p>&nbsp;</p>
                    <form method="get" action="/?menu=anketexport" target="_blank">
                        <p style="margin-bottom: 2px;">Период:<br>от:<input type="text" name="datestart" id="datestart" style="width: 70px;"><br>до:<input type="text" name="dateend" id="dateend" style="width: 70px;"></p>
                        <p>Группировка экспорта:<select name="groupby">
                                <option value="">нет</option>
                                <?php echo $goptions; ?>
                            </select>
                        </p>
                        <input type="hidden" name="aid" value="<?= $item['id'] ?>">
                        <input type="hidden" name="mode" value="groupanddate">
                        <input type="hidden" name="menu" value="anketexport">
                        <input type="submit" value="Экспорт в Excel"></form>
                </td>
                <td>
                    <a href="./?menu=anketexport&mode=all&aid=<?= $item['id'] ?>">Экспорт всех данных анкетирования</a>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
    <?php

    if ($total_items > $page_size) {
        $num_pages = intval(ceil($total_items / $page_size));
        ?>
        <tfoot>
            <tr>
                <td>Страницы:</td>
                <td colspan="3">
                    <?php
                    foreach (range(1, $num_pages) as $npage) {
                        if ($npage == $page) {
                            ?><span class="current"><?= $page ?></span><?php
                        } else {
                            $page_route['page'] = $npage;
                            $page_url = $this->getRouteUrl($page_route);
                            ?><a href="<?= htmlspecialchars($page_url) ?>"><?= $npage ?></a><?php
                        }
                    }
                    ?>
                </td>
            </tr>
        </tfoot>
        <?php
    }

    ?>
</table>
<script>
    $(document).ready(function () {

        $('#datestart').datetimepicker({
            lang:'ru',
            timepicker:true,
            format:'d.m.Y'
        });
        $('#dateend').datetimepicker({
            lang:'ru',
            timepicker:true,
            format:'d.m.Y'
        });

    });
</script>

