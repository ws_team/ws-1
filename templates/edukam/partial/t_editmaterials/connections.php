<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


if ($this->data->mattype['hide_conmat'] != '1') {
    ?>
    <p>&nbsp;</p><a name="materials"></a>
    <h2 class="adminSubtitle">Связанные материалы:</h2>
    <form method="post" action="/?menu=editmaterials&amp;imenu=<?=
    $this->values->imenu ?>&amp;id=<?= $this->values->id; ?>&amp;action=edit">
        <table class="table table-striped table-hover table-condensed table-bootstrap">
            <tr>
                <th width='250'>Тип материала</th>
                <th>Наименование</th>
                <th>Статус</th>
                <th width="80">Вес</th>
                <th width='100' style="width: 100px;">Действия</th>
                <th width='12'></th>
            </tr>
            <?php

            $mcount=0;

            if(!empty($material['connected_materials']))
            {
                foreach($material['connected_materials'] as $cmat)
                {
                    ++$mcount;
                    if($cmat['status_id'] == 2)
                    {
                        $statustext="<span style='color:#00b101;' class=\"glyphicon glyphicon-eye-open\" title='$cmat[statusname]'></span>";
                    }
                    elseif($cmat['status_id'] == 1)
                    {
                        $statustext= "<span style='color:#d0bd05;' class=\"glyphicon glyphicon-eye-close\" title='$cmat[statusname]'></span>";
                    }
                    else
                    {
                        $statustext= "<span style='color:#b14100;' class=\"glyphicon glyphicon-trash\" title='$cmat[statusname]'></span>";
                    }
                    ?><tr>
                    <td><?=$cmat['type_name']?></td>
                    <td><?=$cmat['name']?></td>
                    <td><?=$statustext?></td>
                    <td>
                        <input class="" type=text style="width: 32px;height: 28px;margin: 1px;box-sizing: border-box;line-height: 30px;vertical-align: middle;" max=3
                               name="ordernum_<?=$cmat['id']?>" id="ordernum_<?=$cmat['id']?>" value="<?=$cmat['ordernum']?>" />
                        <button
                                onclick="if(checkEditInputs()){ ChangeOrdernumToConMat(<?=$material['id']?>, <?=$cmat['id']?>, <?= $this->values->imenu ?>, <?= $this->values->id ?>) } else{ showEditWarning(event, this) }"
                                type="button" class="btn btn-default showtooltip small"
                                data-toggle="tooltip"
                                data-placement="top"
                                style="width: 32px;"
                                data-original-title="изменить вес материала">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                    </td>
                    <td>
                        <button type="button"
                                class="btn btn-warning showtooltip small"
                                data-toggle="tooltip"
                                data-placement="bottom"
                                title="Отвязать материал"
                                onclick="if(checkEditInputs()){ unconnectMat(<?=$cmat['id']?>, <?=$this->values->imenu?>, <?=$this->values->id?>) } else{ showEditWarning(event, this) }">
                            <span class="glyphicon glyphicon-remove-sign"></span>
                        </button>&nbsp;
                        <button
                                onclick="if(checkEditInputs()){ document.location.href='/?menu=editmaterials&amp;imenu=<?=$cmat['type_id']?>&amp;id=<?=$cmat['id']?>&amp;action=edit'; } else{ showEditWarning(event, this) }"
                                type="button" class="btn btn-default showtooltip small" data-toggle="tooltip"
                                data-placement="bottom" title="Форма редактирования материала">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                    </td>
                    <td><input type='checkbox' name='cmat_<?=$cmat['id']?>' value='1'></td>
                    </tr><?php
                }
            }
            ?>
        </table><p>&nbsp;</p>
        <button type="button" onclick='if(checkEditInputs()){ connMatForm() } else{ showEditWarning(event, this) }' class="btn btn-success showtooltip" data-toggle="tooltip" data-placement="bottom" title="Связать материал">
            <span class="glyphicon glyphicon-link" ></span>&nbsp;Привязать другой материал к текущему!</button>&nbsp;
        <?php
        if($mcount > 0){
            ?><input type="hidden" name='deleteconmatgroup' value='1'>
            <button data-action="checkEditInputsBeforeAction" type="submit" class="btn btn-infobtn btn-warning showtooltip" data-toggle="tooltip"
                    data-placement="bottom" title="Все выбранные материалы будут отвязаны от текущего материала">
                <span class="glyphicon glyphicon-remove-sign"></span>&nbsp;Отвязать отмеченные материалы</button><?php
        } ?>&nbsp;
        <a data-action="checkEditInputsBeforeAction" href='/?menu=editmaterials&amp;imenu=<?=
                $this->values->imenu?>&amp;action=add&amp;doconnectmaterial=1&amp;matforconnect=<?=$this->values->id?>'><button type="button" class="btn btn-success showtooltip" data-toggle="tooltip" data-placement="bottom"
                    title="Добавить новый материал и связать его с текущим">
                <span class="glyphicon glyphicon-plus-sign"></span>
                <span class="glyphicon glyphicon-link" ></span>&nbsp;Добавить мат. и связать его с текущим
            </button>
        </a>
    </form>
    <form id="ChangeOrdernumToConMatForm" method="get" action="/">
        <input type="hidden" name="ChangeOrdernumToConMat" value="1">
        <input type="hidden" name="test" value="test">
        <input type="hidden" name="menu" value="editmaterials">
        <input type="hidden" name="imenu" value="<?php echo $this->values->imenu; ?>">
        <input type="hidden" name="id" value="<?php echo $this->values->id; ?>">
        <input type="hidden" name="action" value="edit">
        <input type="hidden" name="parentid" id="parentid" value="">
        <input type="hidden" name="childid" id="childid" value="">
        <input type="hidden" name="cordernum" id="cordernum" value="">
    </form>
    <div style="display: none;">
        <div id="connmatform">
            <form method="post" enctype="multipart/form-data" action="/?menu=editmaterials&amp;imenu=<?php echo $this->values->imenu; ?>&amp;id=<?php echo $this->values->id; ?>&amp;action=edit">
                <table width="100%">
                    <tr>
                        <th colspan="2">Привязать материал:</th>
                    </tr>
                    <tr>
                        <th width="190">Тип материала:</th>
                        <td><select id="mattypeforconnect" onchange="MaterialEditSelectType();" class="styler maxheight">
                                <option>---</option>
                                <?php echo PrintTypeOptions($this->data->material_types, '', 0) ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Материал</th>
                        <td>
                            <select id="matforconnect" name="matforconnect" class="styler" data-search="true"
                                data-search-limit="1"></select>
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="doconnectmaterial" value="1" />
                <input type="submit" value="Связать" class="styler" style="margin-top: 3px;" />
            </form>
        </div>
    </div>
<?php }