function BlocksAPI () {

}

// Массив подключенных функций блоков
BlocksAPI.prototype.attachedFunctions = [];
BlocksAPI.prototype.executeCallbacks = [];

// Метод для подключения своего JS-кода блока 
BlocksAPI.prototype.attachJS = function(func) {
	this.attachedFunctions.push(func);
}

// Метод для подключения своего JS-кода блока 
BlocksAPI.prototype.afterExecute = function(func) {
	this.executeCallbacks.push(func);
}

// Метод для запуска кода всех блоков в нужной обёртке
BlocksAPI.prototype.execute = function() {	
	for (var i = 0; i < this.attachedFunctions.length; i++) {
		if (typeof this.attachedFunctions[i] == "function") {
			this.attachedFunctions[i]();
		}
	};
	for (var i = 0; i < this.executeCallbacks.length; i++) {
		if (typeof this.executeCallbacks[i] == "function") {
			this.executeCallbacks[i]();
		}
	};
}

BlocksAPI = new BlocksAPI();

// Обёртка для безопасного $ = jQuery
(function($) {
	// По готовности документа выполняем весь код блоков
	$(document).ready($.proxy(BlocksAPI.execute, BlocksAPI));
})(jQuery);