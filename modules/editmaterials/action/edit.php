<?php
/**
 * @var \iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

defined('_WPF_') or die();

global
    $extratypes,
    $material,
    $selectors;

// @todo рабить
//ALTER TABLE materials
//ADD COLUMN filesordertype smallint;


//krumo($_REQUEST);
//die();

$this->data->iH1 = 'Изменение материала';

$this->data->mattype = GetMattypeValues($this, $this->values->imenu);

if ($this->values->clonematerial != '') {
    DoCloneMaterial($this);
}

//флаг отображения/скрытия виджета фото/видео в детализации материала
if($this->values->materialdetailed_vidjet == "1")
{
	/*$query="UPDATE material_vijsettings SET value = '".$this->values->materialdetailed_vidjet."'
	WHERE ";*/

	$queryvjt="INSERT INTO material_vijsettings(matid, vidjet, value) 
	VALUES(".$this->values->id.", 'matdetailed_photovideo', '".$this->values->matdetailed_photovideo."')";

	if($resvjt=$this->dbquery($queryvjt)) {

	}else{
		$queryvjt="UPDATE material_vijsettings SET value = '".$this->values->matdetailed_photovideo."'
		 WHERE matid = ".$this->values->id." AND vidjet = 'matdetailed_photovideo'";

		$resvjt=$this->dbquery($queryvjt);
	}

	delMatCache($this, $this->values->id);
	//$material = MaterialGet($this, $this->values->id);

}


if ($this->values->ChangeOrdernumToConMat) {

    //krumo($this->values->ChangeOrdernumToConMat);
    //die();

    $this->values->parentid = (int)$this->values->parentid;
    $this->values->childid = (int)$this->values->childid;
    $this->values->cordernum = (int)$this->values->cordernum;

    ChangeOrdernumToConMat(
        $this,
        $this->values->parentid,
        $this->values->childid,
        $this->values->cordernum
    );
}

//получаем связанные характеристики
$extratypes = GetExtraTypes($this, $this->values->imenu);

//получаем селекторы
$selectors = GetSelectors($this, $this->values->imenu);

//обработка изменения
if (!empty($this->values->name)) {

	//проверяем наличае ового столбца сортировки
	$ssort=-1;
	$queryfo="SELECT count(id) cid FROM materials WHERE filesordertype = 1 OR filesordertype IS NULL";
	if($res = $this->dbquery($queryfo))
	{
		if(count($res) > 0)
		{
			$ssort=$res[0]['cid'];
		}
	}

	//добавляем солбец типа сортировки файла
	if($ssort == -1)
	{
		$queryaddfs="ALTER TABLE materials ADD COLUMN filesordertype smallint";
		$res=$this->dbquery($queryaddfs);
	}

    $resarr = createMaterial($this);
    $this->data->errortext = $resarr[1];

    //добавляем значения характеристик
    if ($extratypes[0] != '') {
        DoAddExtraMaterials($this, $extratypes, $this->values->id);
    }

    if (count($selectors) > 0) {
        DoAddSelector($this, $selectors, $this->values->id);
    }
}

$this->getPostValues(Array('filesordertype'));
if($this->values->filesordertype !== '')
{
	$foquery="UPDATE materials set filesordertype = ".intval($this->values->filesordertype)." WHERE id = ".$this->values->id;
	$this->dbquery($foquery);
}

if ($this->values->extra_number2 !== '') {
    $this->dbquery('UPDATE materials SET extra_number2 = $1 WHERE id = $2', array(
        intval($this->values->extra_number2),
        $this->values->id
    ));
}

//удаление связки материалов
if (!empty($this->values->mat)) {
    doUnconnectMat($this, $this->values->mat);
}

//удаление файлов
if ($this->values->dfile != '') {

    $dfile = new MFile($this, $this->values->dfile);
    $dfile->doDeleteFile();
    unset($dfile);

    Solr_DeleteFile($this, $this->values->dfile);

    //очистка кэшей
    delMatCacheNeighbors($this, $this->values->id);
}

//удаление раздела файлов
if ($this->values->dpar != '') {
    //проверяем связи с разделами

    //удаляем связку раздела с файлами
    $query = 'DELETE FROM "file_pars_connections" WHERE "par_id" = $1';
    if ($res = $this->dbquery($query, array($this->values->dpar))) {
        //удаляем раздел
        $query = 'DELETE FROM "file_pars" WHERE "id" = $1';
        $this->dbquery($query, array($this->values->dpar));
    }

    delMatCache($this, $this->values->id);

    $material = MaterialGet($this, $this->values->id);
}

//удаление контакта
if ($this->values->dcont != '') {
    $dcont = $this->values->dcont;
    //удаление расписаний контакта

    $query = 'DELETE FROM "schedules" WHERE "contact_id" = $1';
    if ($resd = $this->dbquery($query, array($dcont))) {
        //удаление самого контакта
        $query = 'DELETE FROM "contacts" WHERE "id" = $1';
        $resd = $this->dbquery($query, array($dcont));
    }

    delMatCache($this, $this->values->id);
}

//формируем массив материала
$material = MaterialGet($this, $this->values->id);

if (!empty($material)) {
    $this->values->status_id = !empty($material['status_id']) ?
        $material['status_id'] :
        null;
    $this->values->language_id = !empty($material['language_id']) ?
        $material['language_id'] :
        null;
}

//удаление группы связанных материалов
if ($this->values->deleteconmatgroup == 1) {
    if (isset($material['connected_materials'][0])) {
        foreach ($material['connected_materials'] as $cmat) {
            $param = 'cmat_' . $cmat['id'];
            $this->GetPostValues(Array($param));

            if ($this->values->$param == 1) {
                doUnconnectMat($this, $cmat['id']);
            }
        }
    }
}

//удаление группы файлов
if ($this->values->deletefilegroup == 1) {
    //получаем переданные файлы
    foreach ($material['allfiles'] as $file) {
        $paramname = 'dfile_' . $file['id'];
        $this->GetPostValues(Array($paramname));

        if ($this->values->$paramname == 1) {
            //удаляем/отвязываем

            $dfile = new MFile($this, $file['id']);
            $dfile->doDeleteFile();
            unset($dfile);

            Solr_DeleteFile($this, $file['id']);
        }

    }

    //очистка кэшей
    delMatCacheNeighbors($this, $this->values->id);

    $material = MaterialGet($this, $this->values->id);
}

//добавление файла
if ($this->values->doaddfile) {

    $this->logWrite(LOG_DEBUG, __FILE__, __LINE__);
    $nfile = new MFile($this);
    $resarr = $nfile->DoSaveFile();
    unset($nfile);

    delMatCache($this, $this->values->id);

    //формируем массив материала
    $material = MaterialGet($this, $this->values->id);

    $this->data->errortext .= $resarr[1];
}

//изменение раздела файлов
if ($this->values->doeditfilepar) {
    if (($this->values->filepar != '') && ($this->values->fileparid != '')) {
        $this->getPostValues('orderby');

        if ($this->values->orderby == '') {
            $this->values->orderby = NULL;
        }

        $query = 'UPDATE "file_pars" SET "name" = $1, orderby = $2 WHERE "id" = $3';
        $q_params = array($this->values->filepar, $this->values->orderby, $this->values->fileparid);
        $res = $this->dbquery($query, $q_params);
        if (!empty($res)) {
            delMatCache($this, $this->values->id);
            $material = MaterialGet($this, $this->values->id);
        }
    }
}

//добавление раздела файлов
if ($this->values->doaddfilepar == '1') {
    $this->getPostValues('orderby');
    if ($this->values->orderby == '') {
        $this->values->orderby = NULL;
    }

    if ($this->values->filepar != '') {
        $fpid = '';
        $query = 'SELECT MAX("id")+1 "mid" FROM "file_pars"';
        if ($res = $this->dbquery($query)) {
            $fpid = $res[0]['mid'];

        } else {
            echo 'Ошибка - не могу получить таблицу типов файлов!';
        }

        if ($fpid == '') {
            $fpid = 1;
        }

        $query = 'INSERT INTO "file_pars" ("id","material_id","name","orderby") VALUES($1, $2, $3, $4)';

        $q_params = array($fpid, $this->values->id, $this->values->filepar, $this->values->orderby);
        $res = $this->dbquery($query, $q_params);

        if (!empty($res)) {
            delMatCache($this, $this->values->id);
            $material = MaterialGet($this, $this->values->id);
        } else {
            echo 'Ошибка занесения раздела файлов!';
        }
    } else {
        echo 'Ошибка - название раздела файлов пустое!';
    }
}

//связка файла
if ($this->values->fileforconnect != '') {
    DoConnectFile($this);
    $material = MaterialGet($this, $this->values->id);
}

//изменение файла
if ($this->values->doeditfile == '1') {
    $editfile = new MFile($this);
    $editfile->DoEditFile();
    unset($editfile);

    delMatCache($this, $this->values->id);

    //формируем массив материала
    $material = MaterialGet($this, $this->values->id);
}

//связка материала
if (($this->values->doconnectmaterial == '1') && ($this->values->matforconnect != '')) {
    DoConnectMaterial($this);
    //формируем массив материала
    $material = MaterialGet($this, $this->values->id);
}

//добавление контакты
if (!empty($this->values->doaddcont)) {
    DoAddCont($this);
    //формируем массив материала
    $material = MaterialGet($this, $this->values->id);
}

//изменение контакта
if ($this->values->doeditcont == '1') {
    DoEditCont($this);
    //формируем массив материала
    $material = MaterialGet($this, $this->values->id);
}

//добавление расписания контакта
if (!empty($this->values->doaddshed)) {
    DoAddShed($this);
    //формируем массив материала
    $material = MaterialGet($this, $this->values->id);
}

$this->getPostValues('clearmatcache');
if (!empty($this->values->clearmatcache)) {
    delMatCache($this, $this->values->id);
}

$material = MaterialGet($this, $this->values->id);

$this->getLogger()->write(LOG_INFO, __FILE__, __LINE__);

// если была отправлена форма изменения материала, но запускаем функции рассылки и синхронизации материала
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $this->callHook('materialedit', array(
        'material' => $material,
        'subscriptionDaily' => !empty($this->values->subscriptionDaily) ? $this->values->subscriptionDaily : null,
        'subscriptionCancel' => !empty($this->values->subscriptionCancel) ? $this->values->subscriptionCancel : null,
        'subscriptionImmediately' => !empty($this->values->subscriptionImmediately) ? $this->values->subscriptionImmediately : null,
    ));

	// проверяем, нужно ли запустить синхронизацию материала
    if($this->values->dosynctooiv == 1) {

        // для главного сайта (Хабкрай) используется специальная функция синхронизации
        if (strncmp($this->settings->DB->name, 'habkrai', 7) === 0) {
            SyncMatToOIV($this, $material);
        }
        else{
            SyncMatOivToOiv($this, $material);
        }
    }
}

if (!empty($this->values->redirect_to) || !empty($this->values->redirect_url)) {
    $redirect_url = !empty($this->values->redirect_url) ?
        $this->values->redirect_url :
        $this->values->redirect_to;

    ob_end_clean();
    $this->redirect($redirect_url, 303);
}

// обновляем последнего редактора материала
if(isset($_SESSION['autorize']['email']) && isset($this->values->id)){
    $query  = 'UPDATE materials SET last_redactor_email = $1 WHERE id = $2';
    $params = array($_SESSION['autorize']['email'], $this->values->id);

    $this->dbquery($query, $params);
}