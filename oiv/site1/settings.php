<?php
$setup=Array();

//настройки БД
$setup['DB']=Array();

$setup['DB']['type']     = 'postgresql';
$setup['DB']['name']     = 'edukam_db';
$setup['DB']['host']     = '127.0.0.1';
$setup['DB']['port']     = '5432';
$setup['DB']['login']    = 'edukam_admin';
$setup['DB']['pass']     = 'GhdbJsbjYb';
$setup['DB']['encoding'] = 'UTF8';
$setup['DB']['SID']      = 'orcl';

//настройки системы рассылки
$setup['email']              = Array();
$setup['email']['signature'] = 'Система рассылки сайта';

//настройки работы с картинками
$setup['files']['saveindb']  = true; //записывать или нет картинку в БД
$setup['files']['filetypes'] = Array();//массив типов загружаемых файлов
$setup['files']['filesize']  = 0;//максимальный размер загружаемого файла (0 - не проверять)

//базовые настройки сайта
$setup['data']['language']    = 'ru'; //язык по умолчанию
$setup['data']['breadcrumbs'] = "<a href='/'>Главная</a>";

$setup['data']['needwsdl']  = false;
$setup['data']['wsdlurl']   = 'http://127.0.0.1:8080/services/wsdl?wsdl';

if (file_exists('settings-local.php'))
	require_once('settings-local.php');

