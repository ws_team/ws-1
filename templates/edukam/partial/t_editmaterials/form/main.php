<?php
/**
 * @var \iSite $this
 * @var array $amterial
 * @var string $action_url
 * @var int $type_id
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

?>
<form method="post" data-role="saveDataForm" class="form-horizontal editform" action="<?= htmlspecialchars($action_url) ?>">
    <input type="hidden" id="checkEditMaterialField">
    <input type="hidden" name="dosynctooiv" value="1">
    <div class="form-group">
        <label for="type_id" class="col-sm-2 control-label required">Тип материала:</label>
        <div class="col-sm-9">
            <select class="styler" name="type_id" style="width: 100%;" data-role="saveDataInput">
                <?php
                print(PrintTypeOptions($this->data->material_types, $type_id, 0));
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label required">Заголовок:</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name" data-role="saveDataInput" value="<?=
            htmlspecialchars($material['name']) ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="status_id" class="col-sm-2 control-label required">Статус:</label>
        <div class="col-sm-9">
            <select name="status_id" id="status_id" class="styler" data-role="saveDataInput"><?php echo $status_selector; ?></select>
        </div>
    </div>
    <div class="form-group">
        <label for="id_char" class="col-sm-2 control-label required">Букв. идентификатор (en):</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" data-role="saveDataInput" name="id_char" id="id_char" value="<?= str_replace('&amp;quot;', '&quot;', htmlspecialchars($material['id_char'])) ?>">
        </div>
    </div>
    <?php if ($this->data->mattype['hide_parent_id'] != '1') { ?>
        <div class="form-group">
        <label for="parent_id" class="col-sm-2 control-label">Родительский материал:</label>
        <div class="col-sm-7">
            <input type="text" class="form-control" name="parent_id" id="parent_id" data-role="saveDataInput" value="<?=
            htmlspecialchars($material['parent_id']) ?>">
        </div>
        <div class="col-sm-1">
            <button <?php if($material['parent_id'] == ''){echo 'disabled';} ?> onclick="document.location.href='/?menu=editmaterials&amp;imenu=<?php
            echo $this->values->imenu; ?>&amp;id=<?php echo $material['parent_id']; ?>&amp;action=edit'" type="button"
                    class="btn btn-default showtooltip" data-toggle="tooltip" data-placement="bottom" title=""
                    data-original-title="Форма редактирования родительского материала">
                <span class="glyphicon glyphicon-pencil"></span>
            </button>
        </div>
        </div><?php
    } ?>
    <?php if ($this->data->mattype['hide_extra_number'] != '1') {
        ?><div class="form-group">
        <label for="extra_number" class="col-sm-2 control-label">Важный материал:</label>
        <div class="col-sm-9">
            <div class="input-group" style="height: auto;">
                <span class="input-group-addon"><div class="checkbox" style="min-height: 20px; padding-top: 0px; padding-right: 10px;">
                    <input type="checkbox" style="height: 100%; margin-left: 0px;" value="1" id="extra_number" name="extra_number" data-role="saveDataInput" <?php
                    if($material['extra_number'] == 1){echo 'checked';} ?>></div>
                </span>
                <span class="form-control" style="height: 100%;">
                    <small> - выберите, если материал важный (должен отображаться на главных страницах разделов)</small>
                </span>
            </div>
        </div>
        </div><?php
    } ?>
    <?php
    if ($this->data->mattype['hide_date_event'] != '1') {
    ?><div class="form-group">
        <label for="date_event" class="col-sm-2 control-label">Дата:</label>
        <div class="col-sm-9">
                <input type='text' class="form-control" name="date_event" id="date_event" data-role="saveDataInput" value="<?php
                echo $material['date_event_str'] ?>" />
        </div>
        </div><?php
        } ?>
    <?php

    if ($this->data->mattype['hide_lang'] != '1' ){
        ?><div class="form-group">
        <label for="language_id" class="col-sm-2 control-label">Язык:</label>
        <div class="col-sm-9">
            <select id="language_id" name="language_id" class="styler" data-role="saveDataInput">><?php

                echo $language_selector;

                ?></select>
        </div>
        </div><?php
    } ?>
    <?php
    if ($this->data->mattype['hide_anons'] != '1') {
        ?><div class="form-group">
        <label for="anons" class="col-sm-12 control-label" style="text-align: left;">Анонс (краткое содержание):</label>
        <div class="col-sm-12">
            <textarea class="notiny form-control" rows="3" id="anons" name="anons" style="width: 94.6%;" data-role="saveDataInput"><?php

                $material['anons']=str_replace('<br>','
',$material['anons']);

                echo htmlspecialchars($material['anons']); ?></textarea>
        </div>
        </div><?php
    }
    ?>
    <?php

    if ($this->data->mattype['hide_content'] != '1') {
        ?><div class="form-group">
        <label for="content" class="col-sm-12 control-label" style="text-align: left;">Содержание материала:</label>
        <div class="col-sm-12" style="height: auto;">
            <textarea data-role="saveDataTinymceEditor" id="content" class="form-control" rows="20" style="height: 500px; min-height: 300px; width: 97%;" name="content"><?php
                echo htmlspecialchars($material['content']); ?></textarea>
        </div>
        </div><?php
    }

    $authorsOptions = printAuthorsOptions($this, $material['author_id']);
    if($authorsOptions != ''):
    ?>

        <div class="form-group">
            <label for="filesordertype" class="col-sm-2 control-label">Автор материала:</label>
            <div class="col-sm-9">
                <select id="filesordertype" name="author_id" class="styler form-group__select_author-of-material" data-role="saveDataInput">

                    <?php echo $authorsOptions; ?>

                </select>
            </div>
        </div>

    <?php
    endif;

    if ($this->data->mattype['hide_tags'] != '1') {
        ?><div class="form-group">
        <label for="extra_text1" class="col-sm-2 control-label">Тэги (через запятую):</label>
        <div class="col-sm-9">
            <input name="extra_text1" class="form-control" id="extra_text1" type="text" data-role="saveDataInput"  value="<?php
            echo $material['extra_text1']; ?>">
        </div>
        </div><?php
    } ?>
    <?php if ($this->data->mattype['hide_ordernum'] != '1') {
        ?><div class="form-group">
        <label for="extra_number2" class="col-sm-2 control-label">Порядок сортировки:</label>
        <div class="col-sm-9">
            <input name="extra_number2" class="form-control" id="extra_number2" data-role="saveDataInput" type="text"  value="<?php
            echo $material['extra_number2']; ?>">
        </div>
        </div><?php
    } ?>
    <div class="form-group">
        <label for="filesordertype" class="col-sm-2 control-label">Порядок сортировки вложений:</label>
        <div class="col-sm-9">
            <select id="filesordertype" name="filesordertype" class="styler" data-role="saveDataInput">><?php

					$filesordertypes=Array();
					$filesordertypes[1]='По весу ↑';
					$filesordertypes[2]='По весу ↓';
					$filesordertypes[3]='По имени';

					foreach ($filesordertypes as $key => $value)
                    {
                        if($material['filesordertype'] == $key)
                        {
                            $selected='SELECTED';
                        }
                        else
                        {
                            $selected='';
                        }

                        echo("<option value='$key' $selected>$value</option>");

                    }

					?></select>
        </div>
    </div>
    <?php

    //выводим доп. характеристики, если они есть
    if (count($extratypes[1]) > 0)
    {
        echo PrintExtraParams($this, $extratypes, $this->values->id);
    }

    if (count($selectors) > 0)
    {
        echo PrintSelectors($this, $selectors, $this->values->id);
    }


    $this->wpf_include('modules/subscription/functions.php');

    if (defined('SUBSCRIPTION_ENABLED') && constant('SUBSCRIPTION_ENABLED')) {

        if (subscription_is_eligible_item($this, $material)) {

            $subscribeSchedule = get_subscribe_send_schedule($this, $material['id']);

            $currentTime       = time();
            $futureSendSign    = false;

            if(!empty($subscribeSchedule)): ?>

            <div class="form-group">
                <label class="col-sm-2 control-label with-checkbox">Время рассылки:</label>
                <div class="choice-list subscription-choice col-sm-9">
                    <?php foreach ($subscribeSchedule as $item):
                        if($currentTime < strtotime($item['schedule_time'])){
                            $futureSendSign = true;
                        } ?>
                        <div style='margin-bottom: 3px;'><?php echo date('d.m.Y H:i', strtotime($item['schedule_time'])) ?></div>
                    <?php endforeach; ?>
                </div>
            </div>

            <?php endif; ?>

            <div class="form-group">
                <label for="subscription-choice" class="col-sm-2 control-label with-checkbox">Рассылка материала (только при активном статусе материала):</label>
                <div class="choice-list subscription-choice col-sm-9">

                    <div>
                        <input type="checkbox" name="subscriptionDaily" value="true" id="subscription_daily" data-role="saveDataInput"
                            <?php echo (!empty($subscribeSchedule) || empty($this->settings->subscription['send_at_hours'])) ? ' disabled' : '' ?>/>
                        <label for="subscription_daily" style="font-weight: normal; <?php echo !empty($subscribeSchedule) ? ' color:gray' : '' ?>">
                            Добавить в ближайшую запланированную рассылку
                            <?php if(!empty($this->settings->subscription['send_at_hours'])): ?>
                                (ежедневно в
                                <?php echo is_array($this->settings->subscription['send_at_hours']) ? implode(' и ', $this->settings->subscription['send_at_hours']) : $this->settings->subscription['send_at_hours'] ?>
                                часов)
                            <?php endif; ?>
                        </label>
                    </div>

                    <div>
                        <input type="checkbox" name="subscriptionImmediately" value="true" id="subscription_immediate" data-role="saveDataInput"
                            <?php echo !empty($subscribeSchedule) || $futureSendSign ? ' disabled' : '' ?>/>
                        <label for="subscription_immediate" style="font-weight: normal; margin-top: 6px; <?php echo !empty($subscribeSchedule) || $futureSendSign ? ' color:gray' : '' ?>">
                            Выслать немедленно (отправится в ближайшее время, запланированная рассылка отменится)</label>
                    </div>

                    <div>
                        <input type="checkbox" name="subscriptionCancel" value="true" id="subscription_none" data-role="saveDataInput"
                            <?php echo !$futureSendSign ? ' disabled' : '' ?>/>
                        <label for="subscription_none" style="font-weight: normal; margin-top: 6px; <?php echo !$futureSendSign ? ' color:gray' : '' ?> ">
                            Отменить запланированную рассылку</label>
                    </div>

                </div>
            </div>

            <?php
        }
    }

    if(is_array($vidjets) && isset($vidjets['matdetailed_photovideo'])){

        if($vidjets['matdetailed_photovideo'] == '1'){
            $checked='checked';
        }
        else{
            $checked='';
        }

        echo '<div class="form-group">
            <label for="materialdetailed_vidjet" class="col-sm-2 control-label with-checkbox">Виджет фото/видео:</label>
            <div class="col-sm-9">
                <input type="hidden" name="materialdetailed_vidjet" data-role="saveDataInput" value="1"><input '.$checked.' type=checkbox value="1" name="matdetailed_photovideo"> - скрывать блок фото/видео в детализации материала (сверху слева)
            </div>
        </div>';
    }
    ?>

    <?php if($material['last_redactor_email']):
        $lastEditorData = getUserDataByEmail($this, $material['last_redactor_email']);
    ?>

        <div class="form-group">
            <label for="materialdetailed_vidjet" class="col-sm-2 control-label with-checkbox">Последний редактор:</label>
            <div class="col-sm-9">

                <?php
                if($lastEditorData){
                    echo "{$lastEditorData['fio']} {$lastEditorData['firstname']} {$lastEditorData['middlename']}, {$material['last_redactor_email']}";
                }
                else{
                    echo $material['last_redactor_email'];
                }
                ?>

            </div>
        </div>

    <?php endif; ?>

        <div id="seopannel" style="display: none">
            <div class="form-group">
                <label for="keywords" class="col-sm-12 control-label" style="text-align: left;">Ключевые слова:</label>
                <div class="col-sm-12">
            <textarea class="notiny form-control" rows="3" id="keywords" data-role="saveDataInput" name="keywords" style="width: 94.6%;"><?php

                $material['keywords']=str_replace('<br>','
',$material['keywords']);

                echo htmlspecialchars($material['keywords']); ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="description" class="col-sm-12 control-label" style="text-align: left;">Описание (description):</label>
                <div class="col-sm-12">
            <textarea class="notiny form-control" rows="3" id="description" name="description" style="width: 94.6%;" data-role="saveDataInput"><?php

                $material['description']=str_replace('<br>','
',$material['description']);

                echo htmlspecialchars($material['description']); ?></textarea>
                </div>
            </div>
        </div>

        <div id="formEditAlert" class="alert alert-warning" role="alert" style="display: none;">
            <span class="glyphicon glyphicon-exclamation-sign"></span>
            Внимание! Материал был изменен! Обязательно нажмите на кнопку "Сохранить" для сохранения изменений!
        </div>

        <button type="submit" class="btn btn-success showtooltip" data-toggle="tooltip" data-placement="bottom"
                title="сохранить изменения материала">
            <span class="glyphicon glyphicon-floppy-disk" ></span>&nbsp;Сохранить изменения!</button>&nbsp;
    <div
            onclick="window.open('<?=htmlspecialchars($material['url'])?>')"
            class="btn btn-infobtn btn-info showtooltip" data-toggle="tooltip" data-placement="bottom"
            title="как будет выглядеть опубликованный материал">
        <span class="glyphicon glyphicon-new-window"></span>&nbsp;Посмотреть на сайте</div>
    <div onclick="ShowHideId('seopannel')" class="btn btn-infobtn btn-info showtooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="показать/скрыть SEO настройки материала">
        <span class="glyphicon glyphicon-option-horizontal"></span>&nbsp;показать/скрыть SEO</div>

    <p>&nbsp;</p><p>&nbsp;</p>

    <h2 class="adminSubtitle">Дополнительные действия:</h2>
    <?php

    if($this->data->mattype['hide_parent_id'] != '1'){?>
        <a data-action="checkEditInputsBeforeAction" href="/?menu=editmaterials&amp;imenu=<?=$this->values->imenu?>&amp;action=add&amp;parent_id=<?=$this->values->id?>">
            <button
                    type="button"
                    class="btn btn-success showtooltip"
                    data-toggle="tooltip"
                    data-placement="bottom"
                    data-original-title="Добавить дочерний материал">
            <span class="glyphicon glyphicon-plus-sign"></span>
            <span class="glyphicon glyphicon-link"></span>&nbsp;Добавить дочерний материал
        </button></a><?php
    }

    $crurl='/?menu=editmaterials&amp;imenu='.$this->values->imenu.'&amp;id='.$this->values->id.'&amp;action=edit&amp;clearmatcache=1';

    ?>
    <a data-action="checkEditInputsBeforeAction" href="<?php echo $crurl ?>">
        <button
                type="button"
                class="btn btn-default showtooltip"
                data-toggle="tooltip"
                data-original-title="Очистить кэш материала (информация о материале сформируется в кэш заново)">
            <span class="glyphicon glyphicon-trash"></span>&nbsp;Очистить кэш материала
        </button>
    </a>
<p>&nbsp;</p>



<div class="hiddenModals">
    <div id="warningEditForm" class="warning-edit-form">
        <div class="warning-edit-form__message">
            У вас имеются несохраненные данные. При выполнении действия изменения будут сброшены. Сохранить изменения?
        </div>
        <button type="button"
                id="warningEditFormAnswerSave"
                class="btn btn-success warning-edit-form__btn-save"
                data-placement="bottom">
                <span class="glyphicon glyphicon-floppy-disk"></span>Сохранить
        </button>
        <button
                type="button"
                id="warningEditFormAnswerNotSave"
                class="btn btn-default">Продолжить без сохранения
        </button>
    </div>
</div>

</form>
<script>
    function ShowHideId(id) {
        $('#'+id).toggle(300);
    }
</script>