<?php


global $authors;


include_once($this->locateTemplate('f_header'));
?>

<div class="container container--admin-title">
    <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
</div>

<div class="contentblock basemargin contentblock_page-list-editor">

    <p class="errortext"><?php echo $this->data->errortext; ?></p>

    <?php

    if (isset($authors) && count($authors) > 0)
    { ?>

        <div class="titles-form-inputs form_page-list-editor">
            <div class='form__inputs form__inputs_flex'>
                <div class='wrap-inputs_form-authors'>Фамилия / Имя / Отчество</div>
                <div class='wrap-inputs_form-authors'>Должность / Подразделение</div>
            </div>
            <div class='form__buttons'></div>
        </div>

        <?php


        foreach($authors as $author)
        {

            echo "<form class='form form_page-list-editor form_list-authors' method='post'>

                        <div class='form__inputs form__inputs_flex'>
                            <input type='hidden' name='action' value='edit'>
                            <input type='hidden' name='id' value='$author[id]'>
                            
                            <div class='wrap-inputs_form-authors'>
                                <input type='text' name='surname' value='$author[surname]' class=\"form-control\" placeholder='Фамилия'>
                                <input type='text' name='first_name' value='$author[first_name]' class=\"form-control\" placeholder='Имя'>
                                <input type='text' name='second_name' value='$author[second_name]' class=\"form-control\" placeholder='Отчество'>
                            </div>
                            
                            <div class='wrap-inputs_form-authors'>
                                <input type='text' name='post' value='$author[post]' class=\"form-control\" placeholder='Должность'>
                                
                                <select name='unit_id' class='styler styler_select-units'>".printUnitsOptions($this, $author["unit_id"])."</select>
                            </div>
                           
                        </div>
              
                        <div class='form__buttons'>
                        
                            <button type=\"submit\" class=\"btn btn-default showtooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Сохранить изменения\">
                                <span class=\"glyphicon glyphicon-pencil\"></span>
                            </button>
                            
                            <a class='link_delete-item' href='/?menu=authors&action=delete&id=$author[id]'>
                                <button type=\"button\" class=\"btn btn-danger showtooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Удалить автора\">
                                    <span class=\"glyphicon glyphicon-remove-circle\"></span>
                                </button>
                            </a>
                            
                        </div>
                    
                    </form>";
        } ?>

        <?php
    } ?>

    <h2 class="adminSubtitle contentblock_title_add-new">Добавить автора:</h2>
    <div class="titles-form-inputs form_page-list-editor">
        <div class='form__inputs form__inputs_flex'>
            <div class='wrap-inputs_form-authors'>Фамилия / Имя / Отчество</div>
            <div class='wrap-inputs_form-authors'>Должность / Подразделение</div>
        </div>
        <div class='form__buttons'></div>
    </div>

    <?php

    echo "<form class='form form_page-list-editor' method='post'>

                        <div class='form__inputs form__inputs_flex'>
                            <input type='hidden' name='action' value='add'>
                            
                            <div class='wrap-inputs_form-authors'>
                                <input type='text' name='surname' class=\"form-control\" placeholder='Фамилия'>
                                <input type='text' name='first_name' class=\"form-control\" placeholder='Имя'>
                                <input type='text' name='second_name' class=\"form-control\" placeholder='Отчество'>
                            </div>
                            
                            <div class='wrap-inputs_form-authors'>
                                <input type='text' name='post' class=\"form-control\" placeholder='Должность'>
                                <select name='unit_id' class='styler styler_select-units'>".printUnitsOptions($this)."</select>
                            </div>
                           
                        </div>
              
                        <input type='submit' class='styler styler_btn-add-new' value='Добавить'>
                    
                    </form>";
    ?>

</div>

<?php

include_once($this->locateTemplate('f_footer'));
