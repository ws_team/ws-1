<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if (!empty($material) && !empty($material['content'])) {

    $content = $material['content'];

    ?>
    <div class="content-text" style="min-height: 300px;"><?= $content ?></div>
    <?php
}
