<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if (!$this->getUser()->can('admin.settings')) {
    header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
    exit;
}

include($this->locateTemplate('f_header'));

?>
<div class="contentblock basemargin">
    <?php

    if (!empty($this->error->text)) {
        ?><div class="<?php if ($this->error->flag) {?>error<?php } ?> message"><?=
            $this->error->text
            ?></div>
        <?php
    }

    include($this->locateTemplate('partial/t_settings/list'));

    ?>
</div>
<?php

include($this->locateTemplate('f_footer'));
