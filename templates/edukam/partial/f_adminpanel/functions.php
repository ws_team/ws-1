<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


function adminpanel_render(\iSite $site, $structure)
{
    ?>
    <ul class="nav navbar-nav"><?php
        foreach ($structure as $item_id => $item) {
            adminpanel_render_menu_item($site, $item, $item_id, true, false, true);
        }
    ?>
    </ul>
    <?php
}

function adminpanel_render_submenu(\iSite $site, $items)
{
    ?>
    <ul class="dropdown-menu">
        <?php
        foreach ($items as $item_id => $item) {
            adminpanel_render_menu_item($site, $item, $item_id, false, true);
        }
        ?>
    </ul><?php
}

function adminpanel_render_menu_item(\iSite $site, $item, $item_id, $add_separator = false, $dd_anchor = true, $isfirst=false)
{
    if ( ! empty($item['permission']) && ! $site->getUser()->can($item['permission']) )
        return;

    if($isfirst)
    {
        $dclass='class="dropdown"';
    }
    else
    {
        if(! empty($item['children']))
        {
            $dclass='class="dropdown-submenu"';
        }
        else
        {
            $dclass='';
        }

    }

    ?>
    <li <?php echo $dclass; ?> ><?php

        $item_name = $item[0];

        if($item_id == 'editmaterials' || ! empty($item['children']))
        {
            if($dd_anchor)
            {
                $item_id_str='id="adminpanel_'.$item_id.'"';
            }
            else
            {
                $item_id_str='';
            }

            if($isfirst)
            {
                $caret='<span class="caret"></span>';
            }
            else
            {
                $caret='';
            }

            echo '<a href="#" '.$item_id_str.' class="dropdown-toggle" data-toggle="dropdown">'.$item_name.$caret.'</a>';
            if($item_id == 'editmaterials')
            {
                echo MaterialTypesAdminMenuNew($site, $site->data->material_types, 0, 'list');
            }
            else
            {
                adminpanel_render_submenu($site, $item['children']);
            }

        }
        else
        {
            $url = isset($item['url']) ? $item['url'] : array(
                $item_id,
            );

            if (is_array($url))
                $url = $site->getRouteUrl($url);

            ?><a href="<?= htmlspecialchars($url) ?>"><?= $item_name ?></a><?php
        }


        /*if ($item_id == 'editmaterials') {
            echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$item_name.'<span class="caret"></span></a>';
            echo MaterialTypesAdminMenuNew($site, $site->data->material_types, 0, 'list');
        } elseif ( ! empty($item['children'])) {
            if ($dd_anchor) {
                ?><a href="#" id="adminpanel_<?= $item_id ?>" class="dropdown-toggle" data-toggle="dropdown"><?= $item_name ?><span class="caret"></span></a><?php
            } else {
                echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$item_name.'<span class="caret"></span></a>';
            }

            adminpanel_render_submenu($site, $item['children']);
        } else {
            $url = isset($item['url']) ? $item['url'] : array(
                $item_id,
            );

            if (is_array($url))
                $url = $site->getRouteUrl($url);

            ?><a href="<?= htmlspecialchars($url) ?>"><?= $item_name ?></a><?php
        }*/

        ?>
    </li>
    <?php

    /*if ($add_separator) {
        ?><li class="admin_space">&nbsp;|&nbsp;</li><?php
    }*/
}

function MaterialTypesAdminMenuNew(\iSite $site, $materials, $level, $action)
{
    ob_start();

    //получаем список материалов пользолвателя

    if (is_array($materials)) {
        include($site->partial('/f_adminpanel/material_types'));
    }

    return ob_get_clean();
}