<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$query = <<<EOS
CREATE TABLE IF NOT EXISTS user_attributes(
    "id" BIGSERIAL PRIMARY KEY,
    "user_id" BIGINT NOT NULL,
    "name" CHARACTER VARYING(100) NOT NULL,
    "value" CHARACTER VARYING(1000)
)
EOS;

$this->dbquery($query);

$query = 'CREATE INDEX ON "user_attributes"("user_id")';
$this->dbquery($query);