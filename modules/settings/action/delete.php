<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if (!$this->validateCsrfToken()) {
    $this->error->flag = 1;
    $this->error->text = 'Запрос не прошел проверку подлинности. Попробуете выполнить действие заново.';
    return;
}

$this->getPostValue('name');

if (empty($this->values->name)) {
    $this->error->flag = 1;
    $this->error->text = 'Пожалуйста, укажите имя параметра';
}

if (settings_delete($this, $this->values->name))
    $this->error->text = 'Параметр удален';
