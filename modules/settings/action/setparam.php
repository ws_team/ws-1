<?php
/**
 * @var \iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if (!$this->validateCsrfToken()) {
    $this->error->flag = 1;
    $this->error->text = 'Сбой при проверки корректности запроса. Попробуете выполнить действие заново.';
    return;
}

$this->getPostValues(array('name', 'value'));

if (empty($this->values->name) || !preg_match('/[a-z][\w.]*[a-z0-9]/', $this->values->name)) {
    $this->error->flag = 1;
    $this->error->text = 'Не заполнено или некорректное имя параметра.';
    return;
}

if (!settings_set($this, $this->values->name, $this->values->value)) {
    $this->error->text = 'Параметр не сохранен';
    return;
}