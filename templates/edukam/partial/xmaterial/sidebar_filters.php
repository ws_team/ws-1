<?php
/**
 * @var \iSite $this
 * @var array $filterMattypes
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

?>
<form id="filters">
    <?php
    $dialog_params = array(
        'data-datestart-field-id' => 'datestart_field',
        'data-dateend-field-id' => 'dateend_field',
        'data-calendars' => '2',
        'data-select-type' => 'range',
    );

    $link_text = 'Выбрать материалы за период';

    // Вызываем супер-крутанский блок который умеет и отрисовать календарь на месяц и выбрать одну дату и вызвать всплывашку для выбора диапазона
    print $this->data->blocks->render('list_date_filter', $link_text, $dialog_params);

    print $this->data->blocks->render('region_info', $this->values->region);
    if (isset($filterMattypes)) {
        print $this->data->blocks->render('region_filter', $filterMattypes);
    }
    print $this->data->blocks->render('keyword_filter');

    ?>
    <input type="hidden" name="page" value="<?php print $this->values->page; ?>">
    <input type="submit" value="Протестировать фильтры" style="display: none;">
</form>
