function showError(err) {
    if (typeof err == "undefined") {
        err = "Неизвестная ошибка. Пожалуйста свяжитесь с нами и опишите последовательность действий которые привели к данному сообщению.";
    }

    return showModal("#error_popup", err); // Пробрасываем возвращаемый Deferred наружу, чтобы можно было вешать коллбэк цепочкой от вызова этой функции    
}

function clallbackform(){
    // $.colorbox({inline:true, href:"#callbackform"});
    return showModal("#callbackform"); // Пробрасываем возвращаемый Deferred наружу, чтобы можно было вешать коллбэк цепочкой от вызова этой функции
}

// Показывает модальное окно расположенное в контейнере #modal-overlay, в качестве аргумента передаётся ID окна
function showModal(modal_id, desc) {
    // Если ID передан без символа "#", то добавляем его
    if (!modal_id.match(/\#/)) {
        modal_id = "#"+modal_id;
    }

    // Если элемент с переданным ID отсутствует на странице то возвращаем false.
    if (!$(modal_id).size()) {
        throw("Wrong modal window identificator, window '"+modal_id+"' not found!");
        return false;
    }

    // Создаём Deferred-объект (аналог promise)
    var def = jQuery.Deferred();

    var modals = $("#modal-overlay").find(".centerer").children();
    // Прячем все окна кроме нужного
    modals.not(modal_id).hide();
    // Нужное делаем отображаемым
    modals.filter(modal_id).show();

    if (typeof desc != "undefined") {
        modals.filter(modal_id).find(".desc:first").empty().text(desc);
    }

    // Плавно показываем overlay с окном
    $("#modal-overlay").stop().fadeIn(function() {
        // По завершению анимации показа нашего окна резолвим наш Deferred-объект, таким образом на это место можно легко повесить callback
        def.resolve();
    });

    // Возвращаем Deferred-объект (В момент вызова функции callback вешается как showModal(...).then(function() { ...callback... });)
    return def;
}

if (typeof DateDialogAPI != "undefined") {
    DateDialogAPI.prototype.showModal = showModal;
}

function SelectSpecialFontSize(size)
{

    var fontSize=18;
    var smallSize=14;

    if(size == 0)
    {
        fontSize=16;
        smallSize=13;
    }

    if(size == 2)
    {
        fontSize=22;
        smallSize=18;
    }



    //меняем размер на странице
    $('.secmenu > .option').css('font-size',fontSize+'px');
    $('.menu > .option').css('font-size',smallSize+'px');
    $('body').css('font-size',fontSize+'px');
    $('.material_list_name').each(function(){
        $(this).style('font-size',fontSize+'px','important');
    });



    //меняем переменную
    $.ajax({
        type: "POST",
        url: '/',
        async: true,
        data: "menu=main&fontSize="+size,
        success: function(data){

        }
    });


}

(function($) {
    if ($.fn.style) {
        return;
    }

    // Escape regex chars with \
    var escape = function(text) {
        return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    };

    // For those who need them (< IE 9), add support for CSS functions
    var isStyleFuncSupported = !!CSSStyleDeclaration.prototype.getPropertyValue;
    if (!isStyleFuncSupported) {
        CSSStyleDeclaration.prototype.getPropertyValue = function(a) {
            return this.getAttribute(a);
        };
        CSSStyleDeclaration.prototype.setProperty = function(styleName, value, priority) {
            this.setAttribute(styleName, value);
            var priority = typeof priority != 'undefined' ? priority : '';
            if (priority != '') {
                // Add priority manually
                var rule = new RegExp(escape(styleName) + '\\s*:\\s*' + escape(value) +
                    '(\\s*;)?', 'gmi');
                this.cssText =
                    this.cssText.replace(rule, styleName + ': ' + value + ' !' + priority + ';');
            }
        };
        CSSStyleDeclaration.prototype.removeProperty = function(a) {
            return this.removeAttribute(a);
        };
        CSSStyleDeclaration.prototype.getPropertyPriority = function(styleName) {
            var rule = new RegExp(escape(styleName) + '\\s*:\\s*[^\\s]*\\s*!important(\\s*;)?',
                'gmi');
            return rule.test(this.cssText) ? 'important' : '';
        }
    }

    // The style function
    $.fn.style = function(styleName, value, priority) {
        // DOM node
        var node = this.get(0);
        // Ensure we have a DOM node
        if (typeof node == 'undefined') {
            return this;
        }
        // CSSStyleDeclaration
        var style = this.get(0).style;
        // Getter/Setter
        if (typeof styleName != 'undefined') {
            if (typeof value != 'undefined') {
                // Set style property
                priority = typeof priority != 'undefined' ? priority : '';
                style.setProperty(styleName, value, priority);
                return this;
            } else {
                // Get style property
                return style.getPropertyValue(styleName);
            }
        } else {
            // Get CSSStyleDeclaration
            return style;
        }
    };

    $(document).ready(function() {
        if ($("#search-form").size()) {
            $("#search-form").find(".radios").on("click", "input[type='radio']", function() {
                $("#search-form").find(".radios").find("label.active").removeClass("active");

                // if ($(this).siblings("input[type=radio]").is(":checked")) {
                    $(this).siblings("label").addClass("active");
                // }
            });
        }

        if ($(".secmenu").size()) {
            SecMenuTop = $(".secmenu").offset().top;

            $(window).on("scroll.secmenu", function() {
                if ($(window).scrollTop() >= SecMenuTop && $(window).width() > 724) {
                    if (!$(".secmenu").hasClass("fixed")) {
                        $(".secmenu").addClass("fixed");
                    }
                } else {
                    $(".secmenu").removeClass("fixed");    
                }
            });
        }

        $(window).on("scroll.searchResults", function() {
            // Если страница результатов поиска
            if ($(".contentblock.search-results").size()) {
                // Если прокрутили ниже чем заголовок
                if ($(window).scrollTop() >= $(".contentblock.search-results").offset().top + 50) {
                    // Прибиваем заголовок
                    $(".contentblock.search-results").css("padding-top", $(".search-query-title").height() + 60 + 30);
                    $(".search-query-title").addClass("fixed");

                    // Вычисляем максимально допустимую позицию заголовка от верха страницы при которой он будет не ниже последнего результата поиска на странице
                    var lastRowTopOffset = $(".contentblock.search-results").offset().top + $(".contentblock.search-results").height() - $(".contentblock.search-results").find(".row.last").height() + 50;

                    // Если прибитый заголовок пытается налезть на последний результат поиска
                    if ($(window).scrollTop() >= lastRowTopOffset) {
                        // Меняем ему св-во top, чтобы не наезжал на последний результат
                        $(".search-query-title").css("top", lastRowTopOffset - $(window).scrollTop());
                    } else {
                        // Иначе, сбрасываем приоритетное значение св-ва top
                        $(".search-query-title").css("top", "");
                    }
                } else {
                    $(".contentblock.search-results").css("padding-top", 0);
                    $(".search-query-title").removeClass("fixed");                    
                }
            }            
        });

        $(document).on("keyup", ".search-query-title", function(e) {
            // Если таймаут уже отсчитывается и юзер повторно нажал кнопку, то перезапустить таймаут 
            if (typeof LiveSearchTimeout != "undefined") {
                clearTimeout(LiveSearchTimeout);
            }

            var $searchField = $(this);

            // Если был нажат Enter то стартуем сразу, иначе через секунду
            var delay = (e.keyCode == 13) ? 1 : 1000;

            // Функция в таймауте отсылающая AJAX-запрос
            LiveSearchTimeout = setTimeout(function() {

            var searchURL = document.location.href.replace(/searchtext\=[^&]*\&/, "searchtext="+$searchField.val()+"&");

                // $.ajax({
                //     method: "GET",
                //     url: searchURL,
                //     timeout: 30000,
                //     success: function(data) {
                //         updateMainContent(data);
                //         if (window.hasOwnProperty("history")) {
                //             window.history.pushState(null, "Поиск: "+$searchField.val(), searchURL);
                //         }

                //         if (typeof DateDialog != "undefined") {
                //             DateDialog.getFields();
                //         }
                //     },
                //     error: function(data) {
                //         throw("Can't update Live Search");
                //         throw(data);
                //     }
                // });

                $(".search-results-list-wrapper").load(searchURL.replace(/\s/g, "%20")+ " .search-results-list" , function() {
                    if (window.hasOwnProperty("history")) {
                        window.history.pushState(null, "Поиск: "+$searchField.val(), searchURL);
                    } 
                    if (typeof DateDialog != "undefined") {
                        DateDialog.getFields();
                    }
                });


                delete LiveSearchTimeout;

            }, delay);
        });

        $(".search-query-title").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "",
                    dataType: "json",
                    data : {
                        imenu: "GetPopularSearchPhrases",
                        phrase: request,
                        count: 5
                    },
                    success : function(data) {
                        response(data);
                    }
                });
            },
            minLength: 2
        });


        // Обработчик клика кнопки закрытия всплывашки о том, что сайт в тестовом режиме
        // KHABKRAI-477 
        $(".custom-closer").on("click", function(event) {
            $modal = $("#modal-overlay").find(".centerer").children().filter(":visible");
            $("#modal-overlay").stop().fadeOut(400, function() {
                $modal.stop().css("margin-top", 0);
                $("body").css("overflow", "");
            });   
            $modal.stop().animate({
                marginTop: -900
            }, 400);
            event.preventDefault();
        });

        // Функция для окна обратной связи, добавляет новое поле загрузки скриншота при заполнении предыдущего
        $("#callbackform").on("change", "input.file.autoclone", function(e) {
            // console.log(e);
            $fieldsWrapper = $("#callbackform").find(".screenfields");            
            fieldsCount = $fieldsWrapper.find(".screen-field").size();
            // Значение предыдущего изменённого поля
            var prevValue = e.currentTarget.value;
            // Если последнее изменённое поле не стало пустым
            if (prevValue != "") {
                // Создаём новое
                $fieldsWrapper.append('<div class="screen-field" data-screen-number="'+fieldsCount+'"><input type="file" name="screen['+fieldsCount+']" class="file autoclone"></div>');                
            // Иначе...
            } else {
                if (fieldsCount > 1) {
                    jQuery(e.currentTarget).parent().remove();
                    // Пересчитываем нумерацию для оставшихся
                    $fieldsWrapper.find(".screen-field").each(function(idx) {
                        $(this).attr("data-screen-number", idx);
                        $(this).find("input.file.autoclone").attr("name", "screen["+idx+"]");
                    });
                }
                // Удаляем пустое поле
            }
        });

        $("#callbackform").find(".screenshot-first").on("click", function() {
            $("#callbackform").find(".screen-field").slideDown();
        });

        jQuery("body").on("click", ".pseudo-radio", function() {

            jQuery(this).siblings("input.custom[type=radio]").click();

            var customRadios = jQuery("input.custom[type=radio]");

            customRadios.each(function() {
                if (jQuery(this).is(":checked")) {
                    jQuery(this).siblings(".pseudo-radio").addClass("checked");
                } else {
                    jQuery(this).siblings(".pseudo-radio").removeClass("checked");
                }

                // jQuery(this).removeAttr("checked");
            });         

        });

        jQuery("body").on("click", "label", function() {
            if (!jQuery(this).siblings(".pseudo-radio").size()) {
                return;
            }   

            jQuery(this).siblings("input.custom[type=radio]").click();

            var customRadios = jQuery("input.custom[type=radio]");

            customRadios.each(function() {
                if (jQuery(this).is(":checked")) {
                    jQuery(this).siblings(".pseudo-radio").addClass("checked");
                } else {
                    jQuery(this).siblings(".pseudo-radio").removeClass("checked");
                }
                // jQuery(this).removeAttr("checked");
            });         

        });

        jQuery("body").on("click", ".pseudo-checkbox", function() {

            var $realCheckbox = jQuery(this).siblings("input.custom[type=checkbox]")

            $realCheckbox.click();

            if ($realCheckbox.is(":checked")) {
                jQuery(this).addClass("checked");
            } else {
                jQuery(this).removeClass("checked");
            }       

        });

        jQuery("body").on("click", "label", function() {
            if (!jQuery(this).siblings(".pseudo-checkbox").size()) {
                return;
            }

            if (!jQuery(this).siblings("input.custom[type=checkbox]").is(":checked")) {
                jQuery(this).siblings(".pseudo-checkbox").addClass("checked");
            } else {
                jQuery(this).siblings(".pseudo-checkbox").removeClass("checked");
            }

            //return false;     
        });

        // Задаём radio-переключатели
        var customRadios = $("#search-form,#vote_form").find("input[type='radio']").not(".custom");
        // var customCheckboxes = jQuery("input[type='checkbox']").not(".custom");

        if (typeof customRadios != "undefined") {

            // Добавляем классы и вставляем 
            customRadios
                .addClass("custom")
                .after('<span class="pseudo-radio"></span>');

            customRadios.each(function() {
                if (jQuery(this).is(":checked")) {
                    jQuery(this).siblings(".pseudo-radio").addClass("checked");
                }   

                if (jQuery(this).hasClass("ajax-processed")) {
                    jQuery(this).removeClass("ajax-processed");
                }       
            });

            // jQuery(".pseudo-radio").off("click");

        }


        // Добавляем классы и вставляем 
        if (typeof customCheckboxes != "undefined") {

            customCheckboxes
            .addClass("custom")
            .after('<span class="pseudo-checkbox"></span>');

            customCheckboxes.each(function() {
                if (jQuery(this).is(":checked")) {
                    jQuery(this).siblings(".pseudo-checkbox").addClass("checked");
                }

                if (jQuery(this).hasClass("ajax-processed")) {
                    jQuery(this).removeClass("ajax-processed");
                }           
            });

            // jQuery(".pseudo-checkbox").off("click");

        }        

        $(".topmenu1-icon.search").on("click", function(event) {
            $("#search-form").stop().slideToggle();
            $(this).toggleClass("active");
            event.preventDefault();
            event.stopPropagation();
        });

        $(document).on("click", function(e) {
            if ($(".topmenu1-icon.search").hasClass("active") && !$(e.target).closest("form#search").length) {
                $(".topmenu1-icon.search").click();
            }
        });

        $(".search-closer").on("click", function(event) {
            if ($(".topmenu1-icon.search").hasClass("active")) {
                $(".topmenu1-icon.search").click();
            }
            event.preventDefault();
        });

        $("#search-form").find(".search-example").find("a").on("click", function(event) {
            $(".searchtext-field").val($(this).text());
            event.preventDefault();
        });

        $("#search-form").find("input[name='searchtype']").on("change", function() {
            if ($(this).val() == "documents") {
                $("#search-form").find(".dropdown").hide();
                $(".searchtext-field").addClass("wide");
            } else {
                $(".searchtext-field").removeClass("wide");
                $("#search-form").find(".dropdown").show();
            }
        });

        $(".footertop_scroll").jCarouselLite({
            btnNext: ".footertop_scroll_next",
            btnPrev: ".footertop_scroll_prev",
            visible: 4,
            scroll: 2,
            circular: true,
            auto: 3000
        });

        // валидатор формы обратки
        $("form#callback").find("input[type='submit']").on("click", function() {
            var validate = (($("form#callback").find("input[name='err_fio']").val() != '') && ($("form#callback").find("input[name='err_email']").val().match(/\S*\@\S*\.\S{2,6}/)) && ($("form#callback").find("textarea[name='err_content']").val() != '' && $("form#callback").find("textarea[name='err_content']").val().length >= 10));
            if (!validate) {
                // Выдать ошибку и навесить на её закрытие показ некорректно заполненой формы обратной связи
                showError("Не верно заполнены некоторые поля формы! (Описание ошибки - не менее 10 символов)").then(function() {
                    $("#error_popup").find(".popup-closer").on("click.error", function() {
                        showModal("#callbackform");
                    });
                });
                return false;
            }
        });

        // Если на странице присутствует элемент, который должен открывать диалог выбора даты
        if ($(".date-dialog-opener").size()) {
            $("body").on("click", ".date-dialog-opener", callDateDialog);
        }

        $("#date_dialog").find(".clear-range").on("click", function(e) {
            if (typeof DateDialog != "undefined") {
                DateDialog.reset();
            }

            e.preventDefault();
        });

        $("#date_dialog").find(".apply-filter").on("click", function(e) {
            console.log("DateDialog: apply");

            $("#search_date_filter").ajaxSubmit({
                success: function(result) {
                    updateMainContent(result);
                    DateDialog.getFields();
                }
            });

            var searchURL = document.location.origin + document.location.pathname + "?" + $("#search_date_filter").formSerialize();
            if (window.hasOwnProperty("history")) {
                window.history.pushState(null, "Поиск по дате: " + $(DateDialog.dateStartField).val() + " - " + $(DateDialog.dateEndField).val(), searchURL);
            }

            $("#date_dialog").find(".custom-closer").click();

            e.preventDefault();
        });

        $("#search_date_filter").on("submit", function() {
            var searchURL = document.location.origin + document.location.pathname + "?" + $("#search_date_filter").formSerialize();
            if (window.hasOwnProperty("history")) {
                window.history.pushState(null, "Поиск: по дате:"+ + $(DateDialog.dateStartField).val() + " - " + $(DateDialog.dateEndField).val(), searchURL);
            }

            $(this).ajaxSubmit({
                success: function(result) {
                    updateMainContent(result);
                    DateDialog.getFields();
                }
            });
        });

        $("a.callback").on("click", function() {
            showModal("callbackform");
        });

        // Автовертушка для новостей на главной
        if ($(".content.main").size() && $(".main_news").size()) {

            setInterval(function() {
                $items = $(".main_news").find(".option");

                if ($(".main_news").find(".option:hover").size()) { return; }

                var itemsCount = $items.size();
                var currentItem = $items.index($items.filter(".active"));
                var nextItem = (currentItem < itemsCount - 1) ?  currentItem + 1 : 0;
                $items.eq(nextItem).trigger("mouseover");
            }, 5000);

        }

        // Автовертушка для новостей на главной
        if ($(".content.main").size() && $(".main_groups_pages").size()) {

            setInterval(function() {
                $pages = $(".main_groups_pages").find(".main_groups_page");
                var pagesCount = $pages.size();
                var currentPage = $pages.index($pages.filter(".active"));
                var nextPage = (currentPage < pagesCount - 1) ?  currentPage + 1 : 0;
                $pages.eq(nextPage).trigger("click");
            }, 5000);

        }

    });

})(jQuery);

function updateMainContent(data) {
    $(".content.main").html(data);
    $(document).trigger("ready");
    $(window).scrollTop(0);
    $(window).trigger("scroll");
}

function showFullContact(id)
{
    $.colorbox({width:"984px", inline:true, href:"#fullcontact_"+id});
}

function showShareBlock(){
    $('.sharetext').hide(100);
    $('.shareblock').show(100);
}

//поиск по сайту
function doSearch()
{
    var sstr=$('#globalsearch').val();
    document.location.href='/search&search='+sstr;
}

function searchcont()
{
    var documenttype=$('#documenttype').val();
    var contsearch=$('#contsearch').val();

    document.location.href='/contacts/contacts-governments/&documenttype='+documenttype+'&contsearch='+contsearch;
}

//скролл влево
function jk_vjt_photo_lscroll(mwidth,step)
{
    var mscroll=mwidth-290;
    var oldmargin=$('#jk_vjt_photo_listscroll').css('left');

    if(oldmargin == 'auto')
    {
        oldmargin='0px';
    }

    oldmargin=oldmargin.replace("px","");




    var x=oldmargin*1+step;

    //alert(x);

    if(x > 0)
    {
        x=0;
    }

    //if(x < 0)
    //{
    var xstr=x+'px';

    //alert(xstr);
    $('#jk_vjt_photo_listscroll').animate({
        left: xstr
    },300);
    //}
}

//скрол_вправо
function jk_vjt_photo_rscroll(mwidth,step)
{
    var mscroll=mwidth-290;
    var oldmargin=$('#jk_vjt_photo_listscroll').css('left');

    if(oldmargin == 'auto')
    {
        oldmargin='0px';
    }

    oldmargin=oldmargin.replace("px","");




    var x=oldmargin*1-step;

    //alert(x);

    var check=0 - mscroll + 65;
    //alert(x+' - '+check);

    if(x < check)
    {
        x=check;
    }
        var xstr=x+'px';

        //alert(xstr);
        $('#jk_vjt_photo_listscroll').animate({
            left: xstr
        },300);
    //}



}

function ShowYoutubeVideo(id)
{

    var data='<div style="width: 800px; height: 600px; overflow: hidden;"><iframe width="800" height="600" src="http://www.youtube.com/embed/'+id+'" frameborder="0" allowfullscreen></iframe></div>';

    $.colorbox({width:"800px", height:"660px", html:data, onComplete:function(){


    }});
}

//смена страницы губернаторских программ
function ChangeMainGProgPage(page)
{
    $('.main_groups_page.active').removeClass('active');
    $('#main_groups_page_'+page).addClass('active');

    var left=(page-1)*246*4;

    $('.main_gprogs_scroll').animate({left:'-'+left+'px'}, 300);
}

function ChangeFooterLinksPage(page)
{
    $('.main_groups_page.active').removeClass('active');
    $('#footer_links_page_'+page).addClass('active');

    var left=(page-1)*1000;
    $('.footertop_scroll').animate({left:'-'+left+'px'}, 300);
}

//выбор большой фотографии
function selectPhoto(i)
{

    var photoname='/photos/'+photos[i]+'_x922.jpg';


    //обновляем фото
    aphoto=i;

    var authortext=authors[i];

    if(authortext != '')
    {
        authortext='Автор: '+authortext;
    }

    $('#photo_big').fadeOut('slow',function(){
        $('#photo_big').attr('src',photoname);
        $('#photo_big').fadeIn('slow');
        $('#photo_big_author').html(authortext);
    });


}

function nextPhoto()
{
    var j=aphoto+1;

    //alert(j);

    if(photos[j] == undefined)
    {
        j=1;
    }

    selectPhoto(j);
}

function prevPhoto()
{

    var j=aphoto-1;
    if(photos[j] == undefined)
    {
        j=(photos.length)-1;
        //alert(j);
    }

    selectPhoto(j);

}

//вывод детализации события на большом календаре
function showBigCalendEvent(id)
{
    var divid='#event_'+id;
    $.colorbox({inline:true, href:divid});
}

//вывод детализации анонса
function ShowAnons(id)
{

//получаем данные ajax
    $.ajax({
        type: "POST",
        url: '/',
        async: true,
        data: "menu=ajax&imenu=ShowAnons&id="+id,
        success: function(data){
            if(data == '-1')
            {
                $.colorbox({width:"425px", maxWidth:"95%", maxHeight:"95%", html:"Произошла ошибка, попробуйте повторить позже."});
            }
            else
            {
                $.colorbox({width:"978px", height:"500px", maxWidth:"95%", maxHeight:"95%", html:data, onComplete:function(){


                }});



            }
        }
    });

}

function vidget_selectSmiDate()
{
    var month=$('#monthselector').val();
    var year=$('#yearselector').val();

    document.location.href='/events/for-mass-media&year='+year+'&month='+month;
}

//выбрать месяц анонса
function vidget_selectAnonsDate(url)
{
    var month=$('#monthselector').val();
    var year=$('#yearselector').val();

    /*if(type == 127)
    {
        document.location.href='/events/announces/Kraevye-meropriyatiya&year='+year+'&month='+month;
    }
    else
    {
        document.location.href='/events/announces&year='+year+'&month='+month;
    }*/

    document.location.href=url+'&year='+year+'&month='+month;

}

//переключатель виджета видео/фото/online
function vidget_selectOnline(){

    $('#vidget_photo').removeClass('active');
    $('#vidget_video').removeClass('active');
    $('#vidget_online').addClass('active');

    $('#vidget_photo_content').hide(100);
    $('#vidget_video_content').hide(100);
    $('#vidget_online_content').show(100);

}
//переключатель виджета видео/фото
function vidget_selectVideo(){

    $('#vidget_photo').removeClass('active');
    $('#vidget_online').removeClass('active');
    $('#vidget_video').addClass('active');

    $('#vidget_photo_content').hide(100);
    $('#vidget_online_content').hide(100);
    $('#vidget_video_content').show(100);

}
function vidget_selectPhoto(){

    $('#vidget_video').removeClass('active');
    $('#vidget_online').removeClass('active');
    $('#vidget_photo').addClass('active');

    $('#vidget_video_content').hide(100);
    $('#vidget_online_content').hide(100);
    $('#vidget_photo_content').show(100);
}

function SelectVacansyForm(){

    if($('#onlyactual').is(':checked'))
    {
        var onlyactual=1;
    }
    else{
        var onlyactual=0;
    }

    var oiv=$('#oivselector').val();
    var region=$('#regionselector').val();

    var gsflag=$('#gsflag').val();
    var msflag=$('#msflag').val();

    document.location.href='/civil-service/competitions-vacancy/&region='+region+'&onlyactual='+onlyactual+'&oiv='+oiv+'&gsflag='+gsflag+'&msflag='+msflag;
}

function vidget_MainSelectNews(){
    $('#vidget_MainAnons').removeClass('active');
    $('#vidget_MainNews').addClass('active');

    $('#vidget_MainAnons_content').hide(100);
    $('#vidget_MainNews_content').show(100);
}

function vidget_MainSelectAnons(){
    $('#vidget_MainNews').removeClass('active');
    $('#vidget_MainAnons').addClass('active');

    $('#vidget_MainNews_content').hide(100);
    $('#vidget_MainAnons_content').show(100);
}

//отображение всплывашки с видео
function vidget_showVideoVidget()
{

}

//пепеключаем типы документов
 function vidget_changeFileType(id,url)
 {
     document.location.href=url+'/&documenttype='+id;
 }

//переключаем фото на главной
function vidget_changeMainPhoto(photo,url)
{
    var oldsrc=$('#main_photo a img').attr('src');

    if(oldsrc != '/photos/'+photo+'_xy370x296.jpg')
    {

        $('#main_photo').html('<a href="'+url+'"><img src="/photos/'+photo+'_xy370x296.jpg" width="370" height="296" border=0></a>');

        $('.main_news .option').removeClass('active');
        $('#option_mainPhoto_'+photo).addClass('active');
    }
}

function showAutoOptions()
{

    if($("#auto").prop('checked'))
    {
        $('#automark').show();
        $('#autonumber').show();
    }
    else{
        $('#automark').hide();
        $('#autonumber').hide();
    }
}