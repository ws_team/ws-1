<?php
/**
 * @var \iSite $this
 */


defined('_WPF_') or die();

// var_dump(__FILE__,__LINE__);exit;

require_once(__DIR__.'/functions.php');

$this->GetPostValues(array('mode', 'files', 'key', 'view', 'id','ext'));

//выводим архив списка файлов
if ($this->values->mode == 'group') {
    if ($this->values->files != '') {
        if (getfile_download_group($this, $this->values->files))
            $this->endRequest();
    }
} elseif (strncmp($this->values->mode, 'OD', 2) == 0) {
    getfile_opendata($this);

} elseif ( ! empty($this->values->id)) {

    //если файл - версия для слабовидящих
    $intid=(int)$this->values->id;

    if($this->values->id == $intid.'_sv')
    {
        //особо ничего не делаем
    }
    else
    {
        $this->values->id = $intid;
    }

    if ($this->values->id > 0) {
        if (getfile_download($this, $this->values->id))
            $this->endRequest();
    }
}

exit;
$this->redirect(array('404'));
