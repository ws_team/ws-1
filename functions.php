<?php

include_once('helpers/misc.php');

function check_for_number($str) {
	$i = strlen($str);
	while ($i--) {
		if (is_numeric($str[$i])) return true;
	}
	return false;
}

function checkpass($pass)
{
    $len=mb_strlen($pass, 'utf8');
    if($len < 6)
    {
        return(false);
    }

    //проверка на все цифры
	if(ctype_digit($pass))
	{
		return(false);
	}

    //проверка на наличае цифр
	if(!check_for_number($pass))
	{
		return(false);
	}


    return(true);
}

function putFtpFile($ftp,$ftpfilename,$folder,$localfile)
{
    $resu=false;

    if (function_exists('ftp_connect') && $connect=ftp_connect($ftp['host']))
    {
        if($res=ftp_login($connect, $ftp['user'], $ftp['pass']))
        {
            //проверка папки
            if(!ftp_chdir($connect,$folder))
            {
                ftp_mkdir($connect,$folder);

                if(!ftp_chdir($connect,$folder))
                {
                    $resu=false;
                }

            }

            ftp_pasv($connect, true);

            if(ftp_put($connect ,$ftpfilename ,$localfile , FTP_BINARY))
            {
                $resu=true;
            }
            else
            {
                $resu=false;
            }
        }

        ftp_close($connect);
    }

    return $resu;
}

function LogSearchResponse($site, $request, $searchtype, $search_category='', $datestart='', $dateend='', $sort='rel')
{

    //проверяем наличае таблицы поиска
    $query='SELECT count("dateadd") FROM "searchlogs"';
    if(!$res=$site->dbquery($query))
    {

        //создаём таблицу логов
        $query='CREATE TABLE "searchlogs" (
    "dateadd" DATE,
    "request" VARCHAR2 (255 BYTE),
    "searchtype" VARCHAR2 (12 BYTE),
    "search_category" VARCHAR2 (255 BYTE),
    "datestart" VARCHAR2 (127 BYTE),
    "dateend" VARCHAR2 (127 BYTE),
    "sort" VARCHAR2 (127 BYTE),
    CONSTRAINT searchlogs_pk PRIMARY KEY ("dateadd","request")
    )
    NOCACHE
    LOGGING';

        if(($res=$site->dbquery($query)) !== false)
        {
            $site->dbquery('COMMIT');
        }
    }

    //записываем логи в таблицу
    if($request != '')
    {
        $request = trim($request);
        $query = 'INSERT INTO "searchlogs" ("dateadd","request","searchtype","search_category","datestart","dateend","sort")
        (SELECT DATE(NOW()), $7, $2, $3, $4, $5, $6 WHERE NOT EXISTS
            (SELECT 1 FROM "searchlogs" WHERE "dateadd" = DATE(NOW()) AND "request" = $1))';
        $params = array($request, $searchtype, $search_category, $datestart, $dateend, $sort, $request);
        if (($res=$site->dbquery($query, $params)) !== false) {
            return true;
        }
    }
    return false;
}

function GetTopSearchRequests($site, $request, $count = 1) {
    $results = array();

    if($request != null && mb_strlen($request, 'UTF8') > 2 && $count > 0) {
        $query = 'SELECT * FROM (SELECT "request" FROM "searchlogs" WHERE "request" LIKE \''.mb_strtolower($request, 'UTF8')
            .'%\' GROUP BY "request" ORDER BY count(*) DESC) WHERE ROWNUM <= '.$count;

        if($res = $site->dbquery($query)) {
            if(count($res) > 0) {
                foreach($res as $row) {
                    array_push($results, $row['request']);
                }
            }
        }
    }

    return $results;
}

function Solr_PutJsonFile($update, $filepath) {

    $url_params = http_build_query($update);
// Create curl resource and URL
    $ch = curl_init('http://localhost:8983/solr/collection1/update/extract?commit=true&'.$url_params);

// Set POST fields
    curl_setopt($ch, CURLOPT_POST, true);

    curl_setopt($ch, CURLOPT_POSTFIELDS, array(
        'myfile' => version_compare(phpversion(), "5.5.0") >= 0 ?
            curl_file_create($filepath) :
            '@'.$filepath
    ));

// Return transfert
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// Get response result
    $output = curl_exec($ch);

// Get response code
    $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($responseCode == 200)
    {
        $res=true;
    }
    else
    {
        $res=false;
    }

// Close Curl resource
    curl_close($ch);

    return($res);
}

function Solr_GetEndpointUrl($endpoint, $params = array(), $collection = null)
{
    global $setup;

    $defaultCfg = array(
        'protocol' => 'http',
        'host' => '127.0.0.1',//''localhost',
        'port' => 8983,
        'basePath' => '/solr',
        'collection' => 'collection1',
    );

    $solrConfig = array_merge($defaultCfg,
        isset($setup['search']) ? $setup['search'] : array()
    );

    if (!isset($collection))
        $collection = $solrConfig['collection'];

    $url = $solrConfig['protocol'].'://'.$solrConfig['host'].':'.$solrConfig['port']
        .$solrConfig['basePath'].
        '/'.$collection.
        '/'.$endpoint;

    if (!empty($params)) {
        $query = array();
        foreach ($params as $key => $val) {
            if (is_bool($val))
                $val = $val === true ? 'true' : 'false';
            $query[] = $key.'='.rawurlencode($val);
        }
        $url .= '?'.implode('&', $query);
    }

    return $url;
}

function Solr_PutJSON($update)
{
    $solrUrl = Solr_GetEndpointURL('update', array('commit' => 'true'));
    $ch = curl_init($solrUrl);

    $update = json_encode($update, JSON_UNESCAPED_UNICODE);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $update);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

    curl_exec($ch);

    $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    return $responseCode == 200;
}

function Solr_AddFile($site, $id)
{
    //получаем данные документа
    $query = 'SELECT "f".*, (UNIX_TIMESTAMP("f"."date_event")) "unixtime", "t"."name" "typename", "f"."extension"
        FROM "files" "f", "file_types" "t" WHERE "f"."id" = $1 AND "f"."type_id" = "t"."id"';

    if ($resu = $site->dbquery($query, array($id))) {
        if (count($resu) > 0) {
            $res = $resu[0];

            //получаем раздел сайта

            $mat = Array();
            $mat['id'] = null;
            $mat['typename'] = null;
            $mat['language_id'] = null;

            $querymat = 'SELECT min("m"."id") "mid" FROM "materials" "m", "file_connections" "c"
            WHERE "m"."status_id" = $1 AND "c"."file_id" = $2 AND "c"."material_id" = "m"."id"';

            if ($resmat = $site->dbquery($querymat, array(STATUS_ACTIVE, $id))) {
                if (count($resmat) > 0) {
                    $mat = MaterialGet($site, $resmat[0]['mid']);
                }
            }

            //преобразуем текстовые поля к plan-тексту
            $res['author'] = strip_tags($res['author']);

            $res['content'] = str_replace('"', '&quot;', $res['content']);
            $res['name'] = str_replace('"', '&quot;', $res['name']);
            $res['author'] = str_replace('"', '&quot;', $res['author']);


            $res['url'] = '/?menu=getfile&id=' . $id;
            //запуск индексации

            $update = Array();

            if (!empty($site->settings->files->index_file_types) &&
                !in_array(strtolower($res['extension']), $site->settings->files->index_file_types)
            ) {
                $update['content'] = '';
                $update['id'] = $site->settings->DB->name . '_f' . $res['id'];
                $update['id_material'] = $mat['id'];
                $update['id_file'] = $res['id'];
                $update['url'] = $res['url'];
                $update['name'] = $res['name'];
                $update['typename'] = $res['typename'];
                $update['eventdate'] = $res['unixtime'];
                $update['lang'] = $mat['language_id'];
                $update['objtype'] = 'file';
                $update['typerank_i'] = SEARCH_TYPERANK_FILE;
                $update['site'] = $site->settings->DB->name;

                $update = array($update);
                $res = Solr_PutJSON($update);
            } else {
                $update['literal.id'] = $site->settings->DB->name . '_f' . $res['id'];
                $update['literal.id_material'] = $mat['id'];
                $update['literal.id_file'] = $res['id'];
                $update['literal.url'] = $res['url'];
                $update['literal.name'] = $res['name'];
                $update['literal.typename'] = $res['typename'];
                $update['literal.eventdate'] = $res['unixtime'];
                $update['literal.lang'] = $mat['language_id'];
                $update['literal.objtype'] = 'file';
                $update['literal.typerank_i'] = SEARCH_TYPERANK_FILE;
                $update['literal.site'] = $site->settings->DB->name;

                $res = Solr_PutJsonFile($update, $site->settings->path . '/photos/' . $id);
            }
            return ($res);
        }
    }

    return false;
}

function Solr_AddMaterial(\iSite $site, $id)
{
    //получаем материал
    $mat = MaterialGet($site, $id);
    Solr_DeleteMaterial($site, $id);

    if (isset($mat['status_id']) && $mat['status_id'] == STATUS_ACTIVE) {
        //не индексируем некоторые типы
	    if($site->settings->DB->name == 'habkrai')
	    {
		    $wrongtypesarray = array(
			    5,
			    17,
			    20,
			    21,
			    55,
			    56,
			    57,
			    58,
			    59,
			    60,
			    82,
			    87,
			    118,
			    149,
			    159,
			    161,
			    216,
			    217,
			    218,
			    219,
			    221,
			    234,
			    235,
			    236,
			    237,
			    238,
			    241,
			    242,
		    );
	    }
	    else
	    {
		    $wrongtypesarray = array();
	    }

        if (in_array($mat['type_id'],$wrongtypesarray)) {
            return false;
        }

        //преобразуем текстовые поля к plan-тексту
        $mat['content']=strip_tags($mat['content']);
        $mat['anons']=strip_tags($mat['anons']);
        $mat['tags']=strip_tags($mat['tags']);

        $mat['content']=str_replace('"','&quot;',$mat['content']);
        $mat['anons']=str_replace('"','&quot;',$mat['anons']);
        $mat['name']=str_replace('"','&quot;',$mat['name']);
        $mat['tags']=str_replace('"','&quot;',$mat['tags']);

        if(!empty($mat['contacts'])) {
            $joined_contacts = "\nКонтакты\n";

            foreach($mat['contacts'] as $contact) {
                $joined_contacts .= $contact['fulladdr']."\n";
            }
            $mat['contacts'] = strip_tags($joined_contacts);
            $mat['contacts'] = str_replace('"','&quot;',$mat['contacts']);

            $mat['content'] .= $mat['contacts'];
        }

        //главный тип материала
        $mat['parenttype'] = 1;

        //получаем все id типа материала

        $query = <<<EOS
WITH RECURSIVE temp1(id, parent_id) as (
    SELECT
        t1.id,
        t1.parent_id
    FROM material_types t1
    WHERE t1.id = $1

    UNION

    SELECT
        t2.id,
        t2.parent_id
    FROM material_types t2
    INNER JOIN temp1 ON temp1.parent_id = t2.id
)
SELECT id
FROM temp1
WHERE parent_id IS NULL OR parent_id = 0; 
EOS;

        $q_params = array($mat['type_id']);

        $res = $site->dbquery($query, $q_params, true);

        if ( ! empty($res)) {
            foreach((array)$res as $row) {
                if (empty($row['parent_id'])) {
                    $mat['parenttype'] = $row['id'];
                }
            }
        }

        $update = array();

        $update['id'] = $site->settings->DB->name.'_m'.$mat['id'];
        $update['id_material'] = $mat['id'];
        $update['id_file'] = null;
        $update['url'] = $mat['url'];
        $update['name'] = $mat['name'];
        $update['materialtype'] = $mat['parenttype'];
        $update['typename'] = $mat['typename'];
        $update['eventdate'] = (int)$mat['unixtime'];
        $update['anons'] = $mat['anons'];
        $update['content'] = $mat['content'];
        $update['tags'] = $mat['tags'];
        $update['author'] = '';
        $update['lang'] = $mat['language_id'];
        $update['objtype'] = 'site';
        $update['site'] = $site->settings->DB->name;
        $update['typerank_i'] = SEARCH_TYPERANK_MATERIAL;

        $update = array($update);
        $res = Solr_PutJSON($update);
    } else {
        $res = false;
    }

    return($res);
}

function Solr_DeleteFile($site, $id)
{
    $update=Array();
    $update['delete'] = $site->settings->DB->name.'_f'.$id;

    $res = Solr_PutJSON($update);
    return($res);
}

function Solr_DeleteMaterial(\iSite $site, $id)
{
    $update = array();
    $update['delete'] = $site->settings->DB->name.'_m'.$id;
    $res = Solr_PutJSON($update);
    return($res);
}

function Solr_Search(
    \iSite $site,
    $request,
    $searchtype,
    $search_category='',
    $page_start=1,
    $result_count=10,
    $datestart='',
    $dateend='',
    $sort='rel'
    )
{
    require_once('UTF8.php');
    require_once('Censure.php');
    require_once('ReflectionTypeHint.php');

    $errarr = Array();

    $errarr['response'] = array(
        'numFound' => 0,
        'results' => array(),
    );

    if (!preg_match("/[a-zA-Zа-яА-Я0-9]+/u",$request)) {
        return ($errarr);
    }

    if(mb_strlen($request,'utf8') < 2) {
        return ($errarr);
    }

    if ($searchtype == 'documents')
        $searchtype = 'file';

    // если не найден мат в запросе, то логируем запрос
    $request = mb_strtolower($request, "UTF8");
    if (!Text_Censure::parse($request) && !Text_Censure::parse(str_replace('0', 'о', $request))) {
        LogSearchResponse($site, $request, $searchtype, $search_category, $datestart, $dateend, $sort);
    };

    $request = strip_tags($request);
    $request = str_replace(' ','+',$request);

    $requestrow = $request;

    //поиск по текущему сайту
    $requestrow .= " AND site:" . $site->settings->DB->name;

    //поиск по материалам сайта
    if($searchtype == 'site')
    {
        $requestrow.=" AND objtype:site";
    }
    elseif ($searchtype == 'file') {
        $requestrow.=" AND objtype:file";
    }

    $sortparam = array('typerank_i desc');

    if($sort == 'new')
    {
        $sortparam[] = 'eventdate desc';
    } else {
        $sortparam[] = 'score desc';
    }

    //поиск по названию типа материала (по названию типа файла)
    if ( ! empty($search_category)) {
        if ($searchtype != 'site')
        {
            $requestrow .= " AND typename:$search_category";
        } elseif ($searchtype == 'site') {
            $requestrow .= " AND materialtype:$search_category";
        }
    }

    //ограничиваем результаты по дате
    //поиск по дате
    if (($datestart != '')&&($dateend == ''))
    {
        //переводим дату в timestamp
        $tstart = MakeTimeStampFromStr($datestart);
        $tend = $tstart+24*60*60;
        $requestrow .= " AND eventdate:[$tstart TO $tend]";

    }
    //поиск по промежутку дат
    elseif(($datestart != '')&&($dateend != ''))
    {
        //переводим дату в timestamp
        $tstart = MakeTimeStampFromStr($datestart);
        $tend = MakeTimeStampFromStr($dateend)+24*60*60;

        $requestrow .= " AND eventdate:[$tstart TO $tend]";
    }

    //ограничиваем результаты по кол-ву
    $start = ($page_start - 1) * $result_count;
    $solrUrl = Solr_GetEndpointUrl('select', array(
        'wt' => 'phps',
        'indent' => true,
        'q' => $requestrow,
        'sort' => implode(',', $sortparam),
        'defType' => 'edismax',
        'qf' => 'name^4 anons typename^1.5 content',
        'mm' => '1 < 75% 3 < -2',
        'pf' => 'content^20 name^10',
        'ps' => 4,
        'qs' => 2,
        'stopwords' => true,
        'lowercaseOperators' => true,
        'hl.fragsize' => 400,
        'indent' => true,
        'hl' => true,
        'hl.fl' => 'name,tags,anons,content',
        'hl.simple.pre' => '<em>',
        'hl.simple.post' => '</em>',
        'start' => $start,
        'rows' => $result_count,
    ), 'collection1');

    $serializedResult = file_get_contents($solrUrl);

    $result = unserialize($serializedResult);
    return($result);
}


function cutString($toCut, $size)
{
    if(mb_strlen($toCut, "utf-8") > $size)
    {
        return (mb_substr($toCut, 0, $size, "utf-8") . "...");
    }
    else return $toCut;
}

// Проверяет связана ли новость с ЖК
function isConnectedWithJK($item)
{
    return false;
}

function is_timestamp($timestamp)
{
    return ((string) (int) $timestamp === $timestamp)
    && ($timestamp <= PHP_INT_MAX)
    && ($timestamp >= ~PHP_INT_MAX);
}

function en_datetime_from_timestamp($timestamp, $needtime=true)
{


    $datetime='';
    $time=date('H:i',$timestamp);
    //die($time);
    $ntime=time();

    $date=date('d.m.Y',$timestamp);

    if($date == date('d.m.Y'))
    {
        if($needtime){
            $datetime='Today, '.$time;
        }
        else
        {
            $datetime='Today';
        }
    }
    elseif($date == date('d.m.Y',($ntime-24*60*60)))
    {
        //$datetime='Вчера, '.$time;
        if($needtime){
            $datetime='Yesterday, '.$time;
        }
        else
        {
            $datetime='Yesterday';
        }
    }
    else
    {

        if($needtime){
            $datetime=en_date($date).', '.$time;
        }
        else
        {
            $datetime=en_date($date);
        }
    }

    return($datetime);

}

function russian_datetime_from_timestamp($timestamp, $needtime=true, $needdaytoday=true, $needyear=true, $timezone = null)
{
    /*if (is_null($timezone)) {
        $tzName = 'UTC';
        $timezone = new DateTimeZone($tzName);
    }
    $dt = new DateTime('@'.intval($timestamp));
    $dt->setTimezone($timezone);
    $time = $dt->format('H:i');
    //die($time);
    $ntime=time();*/



    //$date=$dt->format('d.m.Y');

    $datetime='';
    $time=date('H:i',$timestamp);

    $ntime=$timestamp;

    $date=date('d.m.Y',$timestamp);


    if(($date == date('d.m.Y'))&&($needdaytoday))
    {
        if($needtime){
        $datetime='Сегодня, '.$time;
        }
        else
        {
        $datetime='Сегодня';
        }
    }
    elseif(($date == date('d.m.Y',($ntime-24*60*60)))&&($needdaytoday))
    {
        //$datetime='Вчера, '.$time;
        if($needtime){
            $datetime='Вчера, '.$time;
        }
        else
        {
            $datetime='Вчера';
        }
    }
    else
    {

        if($needtime){
            $datetime=russian_date($date, $needyear).', '.$time;
        }
        else
        {
            $datetime=russian_date($date, $needyear);
        }
    }

    return($datetime);

}

function date_from_bad_date($date)
   {
   $tpos=mb_strpos($date, 'T');
   
   $date=mb_substr($date,0,$tpos);
   
   // $date=str_replace('T','',$date);
   
   $date=explode('-',$date);
   
   $date=$date[2].'.'.$date[1].'.'.$date[0];
   
   return $date;
   }

function date_from_bad_date2($date)
  {
      $spacePos = mb_strpos($date, ' ');
      $date = mb_substr($date, 0, $spacePos);
      
      $date=explode('-',$date);  
      $date=$date[2].'.'.$date[1].'.'.$date[0];
          
      $date=russian_date($date);
   
      return $date;
  }

function date_from_bad_date3($date)
{
    $spacePos = mb_strpos($date, ' ');
    $date = mb_substr($date, 0, $spacePos);

    $date=explode('-',$date);
    $date=$date[1].'.'.$date[0];

    //echo $date;

    $date=russian_date2($date);

    return $date;
}

function calculate_age($date)
   {
   //$alldays=floor((time()-MakeTimeStampFromStr($date))/(60*60*24*365.25));
   
   $alldays=(time()-MakeTimeStampFromStr($date))/(60*60*24);
   
   
   $alldays=(int)$alldays;
   
   //print('<br>'.MakeTimeStampFromStr($date).'<br>'.time());
   
   //print($alldays);
   
   $years=(int)($alldays/365);
   
   $days=$alldays-$years*365;
   
   $mounth=(int)($days/30);
   
   $days-=$mounth*30;
   
   
   $age=Array();
   
   $age['years']=$years;
   $age['mounth']=$mounth;
   $age['days']=$days;
   
   return($age);
   
   }
   
function get_correct_str($num, $str1, $str2, $str3) {
    $val = $num % 100;

    if ($val > 10 && $val < 20) return $num .' '. $str3;
    else {
        $val = $num % 10;
        if ($val == 1) return $num .' '. $str1;
        elseif ($val > 1 && $val < 5) return $num .' '. $str2;
        else return $num .' '. $str3;
    }
}

function get_correct_str_without_number($num, $str1, $str2, $str3) {
    $val = $num % 100;

    if ($val > 10 && $val < 20) return $str3;
    else {
        $val = $num % 10;
        if ($val == 1) return $str1;
        elseif ($val > 1 && $val < 5) return $str2;
        else return $str3;
    }
}
   
function russian_age($age)
   {
   $rage='';
   //полных лет
   if($age['years'] > 0)
      {
      $rage=get_correct_str($age['years'], 'год', 'года', 'лет');
      }
   //полных месяцев
   elseif($age['mounth'] > 0)
      {
      $rage=get_correct_str($age['mounth'], 'месяц', 'месяца', 'месяцев');
      }
   //полных дней
   else
      {
      $rage=get_correct_str($age['days'], 'день', 'дня', 'дней');
      }
   return($rage);
   }
   
function russian_date_from_bad_date($date)
   {
   $date=date_from_bad_date($date);
   
   $date=russian_date($date);
   
   return $date;
   }

//переводим дату вида 01.01.2013 в строку 01 января 2013
function en_date($date)
{
    $date=explode(".", $date);
    switch ($date[1])
    {
        case 1: $m='January'; break;
        case 2: $m='February'; break;
        case 3: $m='March'; break;
        case 4: $m='April'; break;
        case 5: $m='May'; break;
        case 6: $m='June'; break;
        case 7: $m='July'; break;
        case 8: $m='August'; break;
        case 9: $m='September'; break;
        case 10: $m='October'; break;
        case 11: $m='November'; break;
        case 12: $m='December'; break;
    }
    return($date[0].'&nbsp;'.$m.'&nbsp;'.$date[2]);
}

//переводим дату вида 01.01.2013 в строку 01 января 2013
function russian_date($date, $needyear = true)
   {
   $date=explode(".", $date);
   switch ($date[1])
      {
      case 1: $m='января'; break;
      case 2: $m='февраля'; break;
      case 3: $m='марта'; break;
      case 4: $m='апреля'; break;
      case 5: $m='мая'; break;
      case 6: $m='июня'; break;
      case 7: $m='июля'; break;
      case 8: $m='августа'; break;
      case 9: $m='сентября'; break;
      case 10: $m='октября'; break;
      case 11: $m='ноября'; break;
      case 12: $m='декабря'; break;
      }
   return ($needyear) ? ($date[0].'&nbsp;'.$m.'&nbsp;'.$date[2]) : ($date[0].'&nbsp;'.$m);
   }

//переводим дату вида 01.2013 в строку январь 2013
function russian_date2($date)
{
    $date=explode(".", $date);
    if(mb_substr($date[0],0,1,'utf8') == '0')
    {
        $date[0]=mb_substr($date[0],1,1,'utf8');
    }
    switch ($date[0])
    {
        case 1: $m='январь'; break;
        case 2: $m='февраль'; break;
        case 3: $m='март'; break;
        case 4: $m='апрель'; break;
        case 5: $m='май'; break;
        case 6: $m='июнь'; break;
        case 7: $m='июль'; break;
        case 8: $m='август'; break;
        case 9: $m='сентябрь'; break;
        case 10: $m='октябрь'; break;
        case 11: $m='ноябрь'; break;
        case 12: $m='декабрь'; break;
    }
    return($m.'&nbsp;'.$date[1]);
}

function enDayOfWeek($date='')
{

    if($date == '')
    {
        $date=time();
    }


    if($date != (int)$date)
    {
        $timestamp=MakeTimeStampFromStr($date);
    }
    else
    {
        $timestamp=$date;
    }

    $wd=date('w',$timestamp);

    switch($wd)
    {
        case '1':
            $wd='Monday';
            break;

        case '2':
            $wd='Tuesday';
            break;

        case '3':
            $wd='Wednesday';
            break;

        case '4':
            $wd='Thursday';
            break;

        case '5':
            $wd='Friday';
            break;

        case '6':
            $wd='Saturday';
            break;

        case '0':
            $wd='Sunday';
            break;
    }

    return($wd);
}

function DayOfWeek($date='')
{

    if($date == '')
    {
        $date=time();
    }


    if($date != (int)$date)
    {
        $timestamp=MakeTimeStampFromStr($date);
    }
    else
    {
        $timestamp=$date;
    }

    $wd=date('w',$timestamp);

    switch($wd)
    {
        case '1':
            $wd='Понедельник';
            break;

        case '2':
            $wd='Вторник';
            break;

        case '3':
            $wd='Среда';
            break;

        case '4':
            $wd='Четверг';
            break;

        case '5':
            $wd='Пятница';
            break;

        case '6':
            $wd='Суббота';
            break;

        case '0':
            $wd='Воскресенье';
            break;
    }

    return($wd);
}

//Переводим строку даты в unix time (timestamp)=============================================================
function MakeTimeStampFromStr($str)
	{
	$iTimeStamp=0;
	$iDate=Array();
        if($str != '')
		{$iDate = explode('.',$str);}
	if(count($iDate) > 0)
		{
		//смотрим что передано (1-год, 2-год+мес, 3-год+мес+день)
		switch (count($iDate))
			{
			//только год
			case 1:
				$yearStr=$iDate[0];
                $yearStr=(int)$yearStr;
				if(strlen($yearStr) == 2)
					{
					$yearStr='20'.$yearStr;
					}
				$mountStr='0';
				$dayStr='0';
				break;
			case 2:
				$mountStr=$iDate[0];
                $mountStr=(int)$mountStr;
				$yearStr=$iDate[1];
                $yearStr=(int)$yearStr;
				if(strlen($yearStr) == 2)
					{
					$yearStr='20'.$yearStr;
					}
				if((substr($mountStr,0,1) == '0')&&(strlen($mountStr) == 2))
					{
					$mountStr=substr($mountStr,1,1);
					}
				$dayStr='0';
				break;
			case 3:
				$dayStr=$iDate[0];
                $dayStr=(int)$dayStr;
				$mountStr=$iDate[1];
                $mountStr=(int)$mountStr;
				$yearStr=$iDate[2];
                $yearStr=(int)$yearStr;
				if(strlen($yearStr) == 2)
					{
					$yearStr='20'.$yearStr;
					}
				if((substr($dayStr,0,1) == '0')&&(strlen($dayStr) == 2))
					{
					$dayStr=substr($dayStr,1,1);
					}
				if((substr($mountStr,0,1) == '0')&&(strlen($mountStr) == 2))
					{
					$mountStr=substr($mountStr,1,1);
					}
				break;
			}



		$iTimeStamp=mktime(1,0,0,$mountStr,$dayStr,$yearStr);
		
		
		}
	
	return $iTimeStamp;
	
	}

//Вычленяем расширение из имени файла=======================================================================
function GetFileExtension($filename)
   {
   $path_info = pathinfo($filename);
   if(!isset($path_info['extension']))
      {
      return('');
      }
   else
      {
      return $path_info['extension'];
      }
   }


//генерация случайной строки================================================================================
function random_string($length, $chartypes) {
   $chartypes_array=explode(",", $chartypes);
   // задаем строки символов. 
   //Здесь вы можете редактировать наборы символов при необходимости
   $lower = 'abcdefghijklmnopqrstuvwxyz'; // lowercase
   $upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; // uppercase
   $numbers = '1234567890'; // numbers
   $special = '^@*+-+%()!?'; //special characters
   $chars = "";
   // определяем на основе полученных параметров, 
   //из чего будет сгенерирована наша строка.
   if(in_array('all', $chartypes_array)){
         $chars = $lower.$upper.$numbers.$special;
   }
   else{
      if(in_array('lower', $chartypes_array))
              $chars = $lower;
      if(in_array('upper', $chartypes_array))
              $chars .= $upper;
      if(in_array('numbers', $chartypes_array))
              $chars .= $numbers;
      if(in_array('special', $chartypes_array))
              $chars .= $special;
   }
   // длина строки с символами
   $chars_length = (strlen($chars) - 1);
   // создаем нашу строку,
   //извлекаем из строки $chars символ со случайным 
   //номером от 0 до длины самой строки
   $string = $chars{rand(0, $chars_length)};
   // генерируем нашу строку
   for ($i = 1; $i < $length; $i = strlen($string)){
   // выбираем случайный элемент из строки с допустимыми символами
   $random = $chars{rand(0, $chars_length)};
   // убеждаемся в том, что два символа не будут идти подряд
   if ($random != $string{$i - 1}) $string .= $random;
   }
   // возвращаем результат
   return $string;
}



//Работа с BMP изображениями=============================================================================
function ConvertBMP2GD($src, $dest = false) {
if(!($src_f = fopen($src, "rb"))) {
return false;
}
if(!($dest_f = fopen($dest, "wb"))) {
return false;
}
$header = unpack("vtype/Vsize/v2reserved/Voffset", fread($src_f, 14));
$info = unpack("Vsize/Vwidth/Vheight/vplanes/vbits/Vcompression/Vimagesize/Vxres/Vyres/Vncolor/Vimportant",
fread($src_f, 40));

extract($info);
extract($header);

if($type != 0x4D42) { // signature "BM"
return false;
}

$palette_size = $offset - 54;
$ncolor = $palette_size / 4;
$gd_header = "";
// true-color vs. palette
$gd_header .= ($palette_size == 0) ? "\xFF\xFE" : "\xFF\xFF";
$gd_header .= pack("n2", $width, $height);
$gd_header .= ($palette_size == 0) ? "\x01" : "\x00";
if($palette_size) {
$gd_header .= pack("n", $ncolor);
}
// no transparency
$gd_header .= "\xFF\xFF\xFF\xFF";

fwrite($dest_f, $gd_header);

if($palette_size) {
$palette = fread($src_f, $palette_size);
$gd_palette = "";
$j = 0;
while($j < $palette_size) {
$b = $palette{$j++};
$g = $palette{$j++};
$r = $palette{$j++};
$a = $palette{$j++};
$gd_palette .= "$r$g$b$a";
}
$gd_palette .= str_repeat("\x00\x00\x00\x00", 256 - $ncolor);
fwrite($dest_f, $gd_palette);
}

$scan_line_size = (($bits * $width) + 7) >> 3;
$scan_line_align = ($scan_line_size & 0x03) ? 4 - ($scan_line_size &
0x03) : 0;

for($i = 0, $l = $height - 1; $i < $height; $i++, $l--) {
// BMP stores scan lines starting from bottom
fseek($src_f, $offset + (($scan_line_size + $scan_line_align) *
$l));
$scan_line = fread($src_f, $scan_line_size);
if($bits == 24) {
$gd_scan_line = "";
$j = 0;
while($j < $scan_line_size) {
$b = $scan_line{$j++};
$g = $scan_line{$j++};
$r = $scan_line{$j++};
$gd_scan_line .= "\x00$r$g$b";
}
}
else if($bits == 8) {
$gd_scan_line = $scan_line;
}
else if($bits == 4) {
$gd_scan_line = "";
$j = 0;
while($j < $scan_line_size) {
$byte = ord($scan_line{$j++});
$p1 = chr($byte >> 4);
$p2 = chr($byte & 0x0F);
$gd_scan_line .= "$p1$p2";
} $gd_scan_line = substr($gd_scan_line, 0, $width);
}
else if($bits == 1) {
$gd_scan_line = "";
$j = 0;
while($j < $scan_line_size) {
$byte = ord($scan_line{$j++});
$p1 = chr((int) (($byte & 0x80) != 0));
$p2 = chr((int) (($byte & 0x40) != 0));
$p3 = chr((int) (($byte & 0x20) != 0));
$p4 = chr((int) (($byte & 0x10) != 0));
$p5 = chr((int) (($byte & 0x08) != 0));
$p6 = chr((int) (($byte & 0x04) != 0));
$p7 = chr((int) (($byte & 0x02) != 0));
$p8 = chr((int) (($byte & 0x01) != 0));
$gd_scan_line .= "$p1$p2$p3$p4$p5$p6$p7$p8";
} $gd_scan_line = substr($gd_scan_line, 0, $width);
}

fwrite($dest_f, $gd_scan_line);
}
fclose($src_f);
fclose($dest_f);
return true;
}

function imagecreatefrombmp2($filename) {
$tmp_name = tempnam("/tmp", "GD");
if(ConvertBMP2GD($filename, $tmp_name)) {
$img = imagecreatefromgd($tmp_name);
unlink($tmp_name);
return $img;
} return false;
}

//транслитерация
function mb_transliterate($string)
{
    $table = array(
        'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D',
        'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
        'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N',
        'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
        'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH',
        'Ш' => 'SH', 'Щ' => 'SCH', 'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
        'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA',

        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd',
        'е' => 'e', 'ё' => 'yo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i',
        'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n',
        'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
        'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
        'ш' => 'sh', 'щ' => 'sch', 'ь' => '', 'ы' => 'y', 'ъ' => '',
        'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
    );

    $output = str_replace(
        array_keys($table),
        array_values($table),$string
    );

    // таеже те символы что неизвестны
    $output = preg_replace('/[^-a-z0-9._\[\]\'"]/i', ' ', $output);
    $output = preg_replace('/ +/', '-', $output);

    return $output;
}

class Counter
{
    var $origin_arr;
    var $modif_arr;
    var $min_word_length = 3;

    function explode_str_on_words($text)
    {

        $search = array ("'ё'",
            "'<script[^>]*?>.*?</script>'si",  // Вырезается javascript
            "'<[\/\!]*?[^<>]*?>'si",           // Вырезаются html-тэги
            "'([\r\n])[\s]+'",                 // Вырезается пустое пространство
            "'&(quot|#34);'i",                 // Замещаются html-элементы
            "'&(amp|#38);'i",
            "'&(lt|#60);'i",
            "'&(gt|#62);'i",
            "'&(nbsp|#160);'i",
            "'&(iexcl|#161);'i",
            "'&(cent|#162);'i",
            "'&(pound|#163);'i",
            "'&(copy|#169);'i",
            "'&#(\d+);'e");

        $replace = array ("е",
            " ",
            " ",
            "\\1 ",
            "\" ",
            " ",
            " ",
            " ",
            " ",
            chr(161),
            chr(162),
            chr(163),
            chr(169),
            "chr(\\1)");

        $text = str_replace ($search, $replace, $text);

        $del_symbols = array(",", ".", ";", ":", "\"", "#", "\$", "%", "^",
            "!", "@", "`", "~", "*", "-", "=", "+", "\\",
            "|", "/", ">", "<", "(", ")", "&", "?", "№", "\t",
            "\r", "\n", "{","}","[","]", "'", "“", "”", "•",
        );

        $text = str_replace($del_symbols, array(" "), $text);
        //$text = ereg_replace("( +)", " ", $text);
        $this->origin_arr = explode(" ", trim($text));
        return $this->origin_arr;
    }



    function count_words()
    {
        $tmp_arr = array();

        //не учитываемые слова
        foreach ($this->origin_arr as $val)
        {
            if (mb_strlen($val,'utf8')>=$this->min_word_length)
            {
                $val = mb_strtolower($val,'utf8');
                if (array_key_exists($val, $tmp_arr))
                {
                    $tmp_arr[$val]++;
                }
                else
                {
                    $tmp_arr[$val] = 1;
                }
            }
        }
        arsort ($tmp_arr);
        $this->modif_arr = $tmp_arr;
    }

    function get_keywords($text)
    {

        $WrongWords=Array("как", "для", "что", "или", "это", "этих",
            "всех", "вас", "они", "оно", "еще", "когда",
            "где", "эта", "лишь", "уже", "вам", "нет",
            "если", "надо", "все", "так", "его", "чем",
            "при", "даже", "мне", "есть", "раз", "два",
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
            "может","можно","могут","быть","Для","mdash","они",
            "который","которые","более","обычно","Они","случае",
            "случай","виде","нужно","того","Если","если","лучше",
            "Лучше","всего","Необходимо","необходимо","будут",
            "будет","тем","были","будут","будет","достаточно","числом",
            "меньше","меньшим","больше","большим","очень",
            "через","было","эти","которых","своей","начинает","был",
            "своих","Это","так","таких","чтобы","также","часто","больш",
            "начинать","какие","иначе","была","рода","временем",
            "качестве","только","этого","несколько","него","часть",
            "поэтому","либо","особенно","можем","другие","чаще","Вас","ndash",
            "вашему","ваше","вашего","наш","нашей","нашему","наши");

        $this->explode_str_on_words($text);
        $this->count_words();
        $arr=$this->modif_arr;
//$arr = array_slice($this->modif_arr, 0, 15);
        $str = "";


        $icount=0;
        foreach ($arr as $key=>$val)
        {
            if(!in_array($key, $WrongWords))
            {
                ++$icount;
                if($icount < 16)
                {
                    if($str != '')
                    {
                        $str.=', ';
                    }
                    $str .= $key;
                }
            }
        }
        return trim(mb_substr($str, 0, strlen($str)-2),'utf8');
    }
}

function CheckEmail($email)
{

    $pattern = '/^[a-z0-9_.\-]+@[a-z0-9_.\-]+\.[a-z0-9_.\-]+$/i';

    $res = preg_match($pattern, $email);

    return (bool)$res;

}

function settings_get(\iSite $site, $name, $fallback = null)
{
    $res = $site->dbquery('SELECT value FROM settings WHERE name = $1', array($name));
    if (!empty($res))
        return $res[0]['value'];

    return $fallback;
}

function settings_get_all(\iSite $site, $page = 1, $pageSize = 20)
{
    $page = intval($page);
    if ($page <= 0)
        $page = 1;
    if ($pageSize <= 0)
        $pageSize = 1;

    $offset = ($page - 1) * $pageSize;
    $limit = $pageSize;

    $res = $site->dbquery(
        'SELECT name,value FROM settings ORDER BY name ASC OFFSET '.$offset.' LIMIT '.$limit
    );

    return $res === false ? array() : $res;
}