<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

/** @lang PostgresPLSQL */
$query = <<<EOS
CREATE TABLE IF NOT EXISTS "settings"(
    "name" CHARACTER VARYING(100) NOT NULL PRIMARY KEY,
    "text" TEXT
)
EOS;

$this->dbquery($query);