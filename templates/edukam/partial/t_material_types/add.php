<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$this->getPostValues(array('parent_id', 'name', 'en_name', 'id_char'));

?>
<form method="post" action="">
    <table>
        <tr>
            <th class="necessary">Наименование</th>
            <td><input class="styler" type="text" name="name" value="<?= $this->values->name ?>"></td>
        </tr>
        <tr>
            <th>Наименование (En версия)</th>
            <td><input class="styler" type="text" name="en_name" value="<?= $this->values->en_name ?>"></td>
        </tr>
        <tr>
            <th>Буквенный индентификатор (En)</th>
            <td><input class="styler" type="text" name="id_char" value="<?= $this->values->id_char ?>"></td>
        </tr>
        <tr>
            <th>Родительский раздел</th>
            <td><select class="styler" name="parent_id">
                    <option value="">---</option>
                    <?php

                    print(PrintTypeOptions($this->data->material_types, $this->values->parent_id, 0));

                    ?>
                </select></td>
        </tr>

    </table>
    <input type="submit" value="Создать" class="styler" style="margin-top: 3px;">
    <input type="hidden" name="menu" value="material_types">
    <input type="hidden" name="action" value="add">
</form>
