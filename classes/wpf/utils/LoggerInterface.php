<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

namespace wpf\utils;

interface LoggerInterface {
    /**
     * @param $level
     * @param $sender
     * @param $message
     * @return string
     */
    function write($level, $sender, $message);
}