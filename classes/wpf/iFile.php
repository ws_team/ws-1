<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


//класс работы с файлом (создание, удаление, изменение, иконка, описание, название файла...)
class iFile
{
    public $siteObj;
    public $dirpath;
    public $SQLTable;
    public $path;
    public $extension;
    public $icon;
    public $filename;
    public $id;
    public $content;
    public $dateadd;
    public $size;
    public $module_connect;
    public $module_item_connect;
    public $error;
    public $warning;

    function RefreshFileFromDB($fileid)
    {
        $fres=false;
        $this->id=$fileid;
        $query="SELECT * FROM ".$this->SQLTable." WHERE id = ".$this->id;

        if($res=$this->siteObj->dbquery($query))
        {
            $fres=true;

            $this->extension=$res[0]['extension'];
            $this->filename=$res[0]['filename'];
            $this->content=$res[0]['content'];
            $this->dateadd=$res[0]['dateadd'];
            $this->size=$res[0]['size'];
            $this->module_connect=$res[0]['module'];
            $this->module_item_connect=$res[0]['item'];

            //иконка файла (сделать!)

        }
        else
        {
            $this->error->flag=1;
            $this->error->text='
            <br>Данный файл не найден в БД';
        }
        return($fres);
    }

    public function __construct($siteObj, $fileid=null)
    {
        $this->error->flag=0;
        $this->error->text='';

        $this->warning->flag=0;
        $this->warning->text='';

        $this->siteObj=$siteObj;
        $this->dirpath=$this->siteObj->settings->path.'/files';
        $this->SQLTable='rs_files';

        //инициализируем имеющийся файл
        if($fileid != null)
        {
            $this->RefreshFileFromDB($fileid);
        }
        //формируем заготовку для нового файла
        else
        {
            $this->id=null;
        }

    }

    public function Save($postname = null, $filename = null, $extension = null, $content = null, $module_connect, $module_item_connect)
    {

        $fres=false;


        if($filename != null)
        {
            $this->filename=$filename;
        }

        if($extension != null)
        {
            $this->extension=$extension;
        }

        if($content != null)
        {
            $this->content=$content;
        }

        if($module_connect != null)
        {
            $this->module_connect=$module_connect;
        }

        if($module_item_connect != null)
        {
            $this->module_item_connect=$module_item_connect;
        }

        //сохраняем файл, если передан postname
        if($postname != '')
        {
            $this->Upload($postname);
        }

        //обновляем свойства файла
        if($this->id != null)
        {
            $query="UPDATE ".$this->SQLTable." SET extension = '".$this->extension."', filename = '".$this->filename."', dateadd = NOW(), size = '".$this->size."', module = '".$this->module_connect."', item= '".$this->module_item_connect."' WHERE id = ".$this->id;
            if($res=$this->siteObj->dbquery($query))
            {
                $fres=true;
            }
        }
        else
        {
            $this->error->flag=1;
            $this->error->text="
            <br>Нельзя соханить файл - произошла ошибка при получении id файла (скорее всего файл не передан)!";
        }

        return($fres);

    }

    public function Upload($postname)
    {

        $fres=false;

        if (isset ($_FILES[$postname]['tmp_name'])) {$userfile=$_FILES['userfile']['tmp_name'];} else {$userfile="";}
        if (isset ($_FILES[$postname]['size'])) {$userfile_size=$_FILES['userfile']['size'];} else {$userfile_size="";}
        if (isset ($_FILES[$postname]['type'])) {$userfile_type=$_FILES['userfile']['type'];} else {$userfile_type="";}
        if (isset ($_FILES[$postname]['name'])) {$userfile_name=$_FILES['userfile']['name'];} else {$userfile_name="";}

        if(($userfile == '')||($userfile_size = 0))
        {
            $this->error->flag=1;
            $this->error->text='
            <br>Указанный файл ('.$postname.') не передан!';
        }
        else
        {
            $this->size=$userfile_size;
            $this->filename=$userfile_name;

            $this->extension=GetFileExtension($this->filename);


            //получаем id файла (если null)
            if($this->id == null)
            {
                $query="INSERT INTO ".$this->SQLTable."(id, extension, filename, content, dateadd, size, module, item)
                VALUES (null, '".$this->extension."', '".$this->filename."', '', NOW(), '".$this->size."', -1, -1)";

                if($res=$this->siteObj->dbquery($query))
                {

                    //получаем id файла
                    $query="SELECT max(id) id FROM ".$this->SQLTable." WHERE filename = '".$this->filename."'";
                    if($res=$this->siteObj->dbquery($query))
                    {
                        $this->id=$res[0]['id'];
                    }

                }
                else
                {
                    $this->error->flag=1;
                    $this->error->text='
                    <br>Ошибка записи файла в БД!';
                }
            }

            //перемещаем новый файл из темпа загрузки в директорию файлов сайта
            $destination = $this->dirpath.'/'.$this->id.'.'.$this->extension;

            if(!move_uploaded_file($userfile, $destination))
            {
                $this->error->flag=1;
                $this->error->text='
                <br>Ошибка передачи файла!';
            }
            else
            {
                //доп. проверка размера файла
                $size=filesize($destination);
                if($size > 0)
                {
                    $fres=true;
                }
                else
                {
                    $this->error->flag=1;
                    $this->error->text='
                <br>Ошибка передачи файла!';
                }
            }

        }

        return($fres);

    }

}