BlocksAPI.attachJS(function() {

    $('#ajaxPaginatorBtn').on('click', function() {

            let $this                 = $(this),
                $refreshingWrapper    = $this.closest('.can-be-refreshed'),
                $requestData          = new FormData(),
                ajaxHandler           = '/?menu=ajax&imenu=' + $this.attr("data-ajaxPaginatorAction");

            $requestData.append('materialTypeId', $this.attr("data-ajaxPaginatorMaterialTypeId"));
            $requestData.append('perpage', $this.attr("data-ajaxPaginatorPerpage"));
            $requestData.append('remainingAmount', $this.attr("data-ajaxPaginatorRemainingAmount"));
            $requestData.append('pageNumber', $this.attr("data-ajaxPageNumber"));

            $refreshingWrapper.addClass('can-be-refreshed_refreshing');

            $.ajax({
                type: 'POST',
                url: ajaxHandler,
                data: $requestData,
                dataType: 'JSON',
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.status == 0) {
                        console.log('Сервер вернул пустой ответ');
                    } else if (response.status == 'success') {

                        if(response.html){
                            $('#ajaxPaginatorDestinationHtml').append(response.html);
                        }

                        if(response.paginatorBtnText){
                            $this.html(response.paginatorBtnText);
                            $this.attr("data-ajaxPaginatorRemainingAmount", response.remainingAmount);
                            $this.attr("data-ajaxPageNumber", response.pageNumber);
                        }
                        else{
                            $this.hide();
                        }

                    } else if (response.status == 'fail') {
                        console.log('На сервере произошла ошибка, message:' + response.message);
                    } else {
                        console.log('На сервере произошла внезапная ошибка');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Произошла ошибка. Попробуйте еще раз или немного позже.');
                    console.log(textStatus + ': ' + errorThrown);
                },
                complete: function() {
                    $refreshingWrapper.removeClass('can-be-refreshed_refreshing');
                }
            });
        });


});
