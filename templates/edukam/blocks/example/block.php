<?php
    namespace example { // имя namespace должно соответствовать типу блока (названию папки)

        // Функция которая возвращает
        function block_arguments() {
            return array(
                1 => 'Первый аргумент, строковый, обязателен',
                2 => 'Второй аргумент, строковый',
                3 => array( // Третий аргумент, массив
                        // Карта возможных параметров передаваемых в массиве
                        'example_array_argument_item_1' => '',
                        'example_array_argument_item_2' => '',
                    )
                );
        }

        // Функция, которая генерирует контент блока и возвращает его при вставке на страницу через Blocks API
        function block_content() {
            $args = func_get_args();

            $content = '<b><i>Example block content</i></b><br><br>';

            return $content;
        }

    } // /namespace
?>