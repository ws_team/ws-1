<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

global $cacheinfo_typename;

$caches = array('arr', 'html', 'locality', 'linksarr', 'material', 'oivsettings');

$this->data->iH1 = 'Кэши данных';

?>
<table class="admin-table stats-table" id="cacheinfo_table">
    <caption>Кэши</caption>
    <thead>
        <tr>
            <th>Тип (префикс)</th>
            <th>Описание</th>
            <th>Объектов в кэше</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php

        $csrf = $this->generateCsrfToken();

        $url_flush = $this->getRouteUrl(array(
            'cacheinfo',
            'action' => 'flush',
            'csrf' => $csrf,
        ));

        $url_flush .= '&type={type}';

        foreach ($caches as $cache_type) {
            $cache_num_items = cacheinfo_get_items_count($this, $cache_type);
            ?>
            <tr>
                <td><code><?= $cache_type ?></code></td>
                <td><?= $cacheinfo_typename[$cache_type] ?></td>
                <td><?= $cache_num_items ?></td>
                <td><?php
                    if ($cache_num_items > 0) {
                        ?><a href="<?= str_replace('{type}', $cache_type, $url_flush) ?>">очистить</a><?php
                    }
                    ?>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
