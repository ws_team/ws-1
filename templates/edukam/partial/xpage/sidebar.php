<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();
?>

<div class="sidebar">
    <div class="material-info">
        <div class="material-info-item">
            <svg class="material-info-item__icon"><use xlink:href="/img/sprite.svg#decor--sidebar--iconCalendar"></use></svg>
            <div class="body-2 material-info-item__text">
                <?php echo formatDateNews($material['date_event']) ?>
            </div>
        </div>

        <?php

        $fileCount = count($material['allfiles']);

        if($fileCount != 0):

        ?>

        <div class="material-info-item">
            <svg class="material-info-item__icon"><use xlink:href="/img/sprite.svg#decor--sidebar--iconAttach"></use></svg>
            <div class="body-2 material-info-item__text">
                <?php

                switch ($fileCount){
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                        echo $fileCount . ' файлов';
                        break;
                    default:
                        switch (substr(strval($fileCount), -1)){
                            case '1':
                                echo $fileCount . ' файл';
                                break;
                            case '2':
                            case '3':
                            case '4':
                                echo $fileCount . ' файла';
                                break;
                            default:
                                echo $fileCount . ' файлов';
                                break;
                        }
                        break;
                }

                ?>
            </div>
        </div>

        <?php endif; ?>

        <div class="material-info-item">
            <svg class="material-info-item__icon"><use xlink:href="/img/sprite.svg#decor--sidebar--iconPrint"></use></svg>
            <div class="body-2 material-info-item__text">
                <a
                        href="<?php echo $_SERVER['REQUEST_URI'] ?>?media=print"
                        class="material-info-item__link"
                        target="_blank"
                        rel="noopener noreferrer"
                >
                    Версия для печати
                </a>
            </div>
        </div>
    </div>

    <?php

    $advertisementsTypeId   = getMaterialTypeIdByCharId($this, 'Obyavleniya');
    $advertisements         = MaterialList($this, $advertisementsTypeId[0]['id'], 1, 3, 2);

    if($advertisements):

    ?>

    <div class="sidebar__adverts">
        <div class="subtitle-1 sidebar__title-news">Объявления</div>

        <?php foreach ($advertisements as $advertisementsItem): ?>

            <div class="adverts-item adverts-item--sidebar">
                <div class="subtitle-2 adverts-item__title">
                    <a href="<?php echo $advertisementsItem['url'] ?>" class="adverts-item__link"><?php echo $advertisementsItem['name'] ?></a>
                </div>
                <div class="body-2 adverts-item__description"><?php echo $advertisementsItem['anons'] ?></div>
            </div>

        <?php endforeach; ?>

        <a href="/events/Obyavleniya" class="label-1 show-more-link show-more-link--sidebar">Все объявления</a>
    </div>

    <?php

    endif;

    $newsTypeId = getMaterialTypeIdByCharId($this, 'Novosti');
    $news       = MaterialList($this, $newsTypeId[0]['id'], 1, 3, 2);

    if($news):

    ?>

    <div class="sidebar__news">
        <div class="subtitle-1 sidebar__title-news">Последние новости</div>

        <?php foreach ($news as $newsItem): ?>

            <div class="sidebar__news-item">
                <div class="body-1 sidebar__news-item-date"><?php echo formatDateNews($newsItem['date_event']) ?></div>
                <div class="body-2 sidebar__news-item-title">
                    <a href="#" class="sidebar__news-item-link"><?php echo $newsItem['name'] ?></a>
                </div>
            </div>

        <?php endforeach; ?>

        <a href="/events/Novosti" class="label-1 show-more-link show-more-link--sidebar show-more-link--sidebar-news">Все новости</a>
    </div>

    <?php endif; ?>

</div>