<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


function bootstrap_requestmodule(\iSite $khabkrai)
{
    global $oivsettings;

    if (defined('WPF_OIV') && constant('WPF_OIV')
        || defined('WPF_OMSU') && constant('WPF_OMSU')) {
        $oivsettings = defined('WPF_OMSU') && constant('WPF_OMSU') ?
            MakeOMSUSettings($khabkrai) :
            MakeOIVSettings($khabkrai);

        $oivsubpatch = $khabkrai->settings->path;
        $oivsubpatch = str_replace($khabkrai->settings->pathglobal, '', $oivsubpatch);

        if (empty($khabkrai->values->requeststr))
            $khabkrai->values->requeststr = str_replace($oivsubpatch, '', $khabkrai->values->requeststr);

        $khabkrai = ParseOIVURL($khabkrai);
    } else {
        //разбираем переданную строку запроса
        //главные переменные
        $khabkrai = ParseSiteURL($khabkrai);
    }

    if (empty($khabkrai->values->maintype)) {
        $khabkrai->values->maintype = '1';
    }

    //модуль переводим на материалы
    if ( ($khabkrai->values->menu == '' || $khabkrai->values->menu == 'main')
        && !empty($requestarr[0])) {
        $khabkrai->values->menu = 'materials';
    }
}

function parse_path()
{
    $path = array();
    if (isset($_SERVER['REQUEST_URI'])) {
        $request_path = explode('?', $_SERVER['REQUEST_URI']);

        $path['base'] = '';
        if (substr($_SERVER['SCRIPT_FILENAME'], -4, 4) == '.php'
            && strncmp($_SERVER['SCRIPT_FILENAME'], $request_path[0], strlen($_SERVER['SCRIPT_FILENAME'])) == 0) {
            $path['base'] = $_SERVER['SCRIPT_FILENAME'];
        }

        $path['call'] = substr($request_path[0], strlen($path['base']) + 1);
        $path['call_parts'] = explode('/', $path['call']);

        $path['query_utf8'] = '';
        $path['query'] = '';

        if (isset($request_path[1])) {
            $path['query_utf8'] = urldecode($request_path[1]);
            $path['query'] = utf8_decode(urldecode($request_path[1]));
        }

        parse_str($path['query'], $query_vars);
        $path['query_vars'] = $query_vars;
    }

    return $path;
}