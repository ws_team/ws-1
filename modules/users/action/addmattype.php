<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$this->GetPostValues(Array('user','type'));
if ($this->values->user != '' && $this->values->type != '') {

    // @todo сделать id=nextval(user_rights_seq)
    //пробуем добавить
    $tcnt = 0;
    $query = 'SELECT count("id") "cid" FROM "user_rights" WHERE "goal_id" = $1 AND "type_id" = $2';
    if (($res=$this->dbquery($query, array($this->values->user, $this->values->type))) !== false) {
        if (count($res) > 0)
            $tcnt = $res[0]['cid'];
    }

    if ($tcnt == 0) {
        $mid = 1;
        $qmid = 'SELECT (MAX("id") + 1) "mid" FROM "user_rights"';
        if ($res=$this->dbquery($qmid)) {
            if(count($res) > 0){
                $mid = $res[0]['mid'];
            }
        }

        if($mid == '')
        {
            $mid=1;
        }

        $type = $this->values->type;
        $goal = $this->values->user;

        $query = 'INSERT INTO "user_rights" ("id","type_id","goal_id") VALUES ($1, $2, $3)';
        $res = $this->dbquery($query, array($mid, $type, $goal));
    }
}