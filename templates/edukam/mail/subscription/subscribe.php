<?php
/**
 * @param string $confirmUrl
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

$layout = 'layout';

ob_start();
?>
<p>Для подтверждения подписки на новостную рассылку портала Правительства Хабаровского края, пройдите по ссылке:<br />
    <a href="<?=htmlspecialchars($confirmUrl)?>"><?=htmlspecialchars($confirmUrl)?></a>
</p>
<p>Если вы не оформляли подписку или не хотите подписываться, просто проигнорируйте это письмо.</p>
<?php

$content = ob_get_clean();

$title = 'Подтверждение подписки';

include($layout.'.php');