<?php
    namespace ajaxUniversalPaginator { // имя namespace должно соответствовать типу блока (названию папки)

        // Функция которая возвращает
        function block_arguments() {
            return array();
        }

        // Функция, которая генерирует контент блока и возвращает его при вставке на страницу через Blocks API
        function block_content($ajaxAction, $materialType, $perpage, $pageNumber, $materialsCount = 100) {

            $remainingAmount = $materialsCount - $perpage;

            if ($remainingAmount <= 0) {
                return '';
            }
            elseif ($remainingAmount > $perpage){
                $paginatorBtnText = "Загрузить еще $perpage из $remainingAmount";
            }
            else{
                $paginatorBtnText = "Загрузить еще $remainingAmount";
            }

            ob_start();

            ?>

            <button
                    class="btn-show-more"
                    id="ajaxPaginatorBtn"
                    data-ajaxPaginatorMaterialTypeId="<?php echo $materialType ?>"
                    data-ajaxPaginatorAction="<?php echo $ajaxAction ?>"
                    data-ajaxPaginatorPerpage="<?php echo $perpage ?>"
                    data-ajaxPaginatorRemainingAmount="<?php echo $remainingAmount ?>"
                    data-ajaxPageNumber="<?php echo $pageNumber ?>"
            >
                <?php echo $paginatorBtnText ?>
            </button>

            <?php
            return ob_get_clean();
        }

    } // /namespace
