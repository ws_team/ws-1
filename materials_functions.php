<?php

include_once('materials_functions/materials.php');

//функция создаёт привьюшку pdf файла
function MakePDFPreview($site, $pdfid, $x, $y)
{
    $pdfpatch=$site->settings->path.'/photos/'.$pdfid;
    $imagepatch=$site->settings->path.'/photos/'.$pdfid.'_xy'.$x.'x'.$y.'.jpg';
    $imagepatchbig=$site->settings->path.'/photos/'.$pdfid.'_jpg';


    exec('convert '.$pdfpatch.'[0] '.$imagepatchbig);

    if(file_exists($imagepatch))
    {

        if($x > $y)
        {
            $bigsize=$x;
        }else
        {
            $bigsize=$y;
        }

        list($w,$h) = getimagesize($imagepatch);

        if($w > $h){
            exec("convert ".$imagepatchbig." -resize x".$bigsize." -quality 100 ".$imagepatch);
        }else{
            exec("convert ".$imagepatchbig." -resize ".$bigsize." -quality 100 ".$imagepatch);
        }

        exec("convert ".$imagepatch." -gravity Center -crop ".$x."x".$y."+0+0 ".$imagepatch);

        return(true);
    }
    else{
        return(false);
    }
}

require_once('materials_functions/syncmat_oiv.php');



function mb_strcasecmp($str1, $str2, $encoding = null) {
    if (null === $encoding) { $encoding = mb_internal_encoding(); }
    return strcmp(mb_strtoupper($str1, $encoding), mb_strtoupper($str2, $encoding));
}

//сортируем массив связанным материалов по полю сортировки
function myCmp($a, $b) {

    if ($a['ordernum'] === $b['ordernum'])
    {
        if(mb_strcasecmp($a['name'], $b['name']) > 0)
        {
            return 0;
            //print("<br> $a[name] < $b[name]");
        }
        else
        {
            //print("<br> $a[name] > $b[name]");
            return 1;
        }
    }

    //echo '-';

    return $b['ordernum'] > $a['ordernum'] ? 1 : -1;
}

//сортировка массива по дате материала
function myCmpDate($a, $b)
{
    if ($a['unixtime'] === $b['unixtime'])
    {
        return 0;
    }
    return $a['unixtime'] > $b['unixtime'] ? 1 : -1;
}

require_once('materials_functions/matcache.php');

include('materials_functions/oivsettings.php');

function findTemplateIdByChar($site, $char, $type='list')
{
    $materiallist = intval($type == 'list');

    if (strpos($char, '.php') !== false) {
        $file_char = $char;
        $key_char = str_replace('.php', '', $file_char);
    } else {
        $key_char = $char;
        $file_char = $key_char . '.php';
    }

    $query = 'SELECT "id" FROM "standart_shablons" WHERE "file_name" IN($1, $2) AND "materiallist" = $3';
    $q_params = array($key_char, $file_char, $materiallist);
    $res = $site->dbquery($query, $q_params);

    if ( ! empty($res)) {
        return($res[0]['id']);
    }

    return false;
}

/**
 * @param $site
 * @param $char
 * @return string numeric|bool
 */
function findTypeIdByChar($site, $char)
{
    // @todo cache
    $query = 'SELECT "id" FROM "material_types" WHERE "id_char" = $1';

    $res = $site->dbquery($query, array($char));
    if ( ! empty($res))
        return($res[0]['id']);

    return false;
}

function MaterialParamFindByName($site, $mattypeid, $name)
{
    static $cached = array();
    $ret = false;

    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'mattype='.$mattypeid.';name="'.$name.'"');

    $key = $mattypeid.'.'.$name;
    if (isset($cached[$key]))
        return $cached[$key];

    if ( ! empty($mattypeid)) {
        $query = 'SELECT * FROM "extra_mattypes" WHERE lower("name") = lower($1) AND "type_id" = $2';
        $q_params = array(mb_strtolower($name), $mattypeid);

        $res = $site->dbquery($query, $q_params);
        if ( ! empty($res)) {
            $res = $res[0];
            $site->logWrite(LOG_DEBUG,__FUNCTION__, '..found id='.$res['id']);
            $cached[$key] = $res;
            $ret = $res;
        } else {
            $res = false;
            $site->logWrite(LOG_WARNING, __FUNCTION__,
                '..not found; query=<<<'.$query.'>>>;params='.serialize($q_params).';e='.pg_last_error());
        }
    }

    return $ret;
}

function findMatParamByName($site, $mattypeid, $name)
{
    return MaterialParamFindByName($site, $mattypeid, $name);
}

function findParamIdByName($site, $mattypeid, $name)
{
    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'mattype='.$mattypeid.';name="'.$name.'"');

    $param = findMatParamByName($site, $mattypeid, $name);
    if ( ! empty($param))
        return $param['id'];

    return false;
}

function findMatIdByChar($site, $char, $type_id = null)
{
    $query = 'SELECT "id" FROM "materials" WHERE "id_char" = $1';
    $q_params = array($char);

    if ( ! empty($type_id)) {
        $query .= ' AND "type_id" = $2';
        $q_params[] = $type_id;
    }

    $res = $site->dbquery($query, $q_params);
    if ( ! empty($res))
        return($res[0]['id']);

    return false;
}

include(__DIR__.'/materials_functions/materialtypes.php');
include(__DIR__.'/materials_functions/create_materialtype.php');

//получаем экстра-поля материала по его id
function GetMaterialParams($site, $id)
{
    return MaterialParamGetAll($site, $id);
}

function MaterialParamGetAll(\iSite $site, $mat_id)
{
    $params = Array();

    $query = <<<EOS
SELECT "t".*
FROM "extra_mattypes" "t"
WHERE "type_id" = (SELECT "type_id" FROM "materials" WHERE "id" = $1)
EOS;

    $q_params = array($mat_id);

    $res = $site->dbquery($query, $q_params);

    if ( ! empty($res)) {
        foreach ($res as $row) {
            $params[$row['id']] = $row;

            //значения

            switch ($row['valuetype']) {
                case 'valuedate':

                    $query = <<<EOS
SELECT
    "{$row['valuetype']}" "value",
    (UNIX_TIMESTAMP("valuedate")) "unixtime"
FROM "extra_materials"
WHERE
    "material_id" = $1
    AND "type_id" = $2
    AND ("selector_id" IS NULL OR "selector_id" = 0)
EOS;
                    $q_params = array($mat_id, $row['id']);

                    break;

                case 'valuemat':

                    $query = <<<EOS
SELECT
    "m"."name" "valuename",
    "m"."id" "value"
FROM
    "materials" "m",
    "extra_materials" "e"
WHERE
    "m"."id" = "e"."valuemat"
    AND "e"."material_id" = $1
    AND "e"."type_id" = $2
    AND ("e"."selector_id" IS NULL OR "e"."selector_id" = 0)
    AND "m"."status_id" = $3
EOS;
                    $q_params = array($mat_id, $row['id'], STATUS_ACTIVE);

                    break;

                default:

                    $query = <<<EOS
SELECT "{$row['valuetype']}" "value"
FROM "extra_materials"
WHERE
    "material_id" = $1
    AND "type_id" = $2
    AND ("selector_id" IS NULL OR "selector_id" = 0)
EOS;
                    $q_params = array($mat_id, $row['id']);

                    break;
            }

            $eres = $site->dbquery($query, $q_params);

            if ( ! empty($eres)) {
                $params[$row['id']]['value'] = $eres[0]['value'];

                if (isset($eres[0]['valuename'])) {
                    $params[$row['id']]['valuename'] = $eres[0]['valuename'];
                }

                if (isset($eres[0]['unixtime'])) {
                    $params[$row['id']]['unixtime'] = $eres[0]['unixtime'];
                }
            }
        }
    }

    return $params;
}

//получаем все документы активных материалов переданного типа
function GetTypeDocs($site, $type, $doctype=4)
{

    if($site->settings->DB->type == 'postgresql')
    {
        $query='SELECT "f".*, "m"."name" "material_name",

        extract(epoch FROM "f"."date_event") "unixtime",
    CONCAT('."'/?menu=getfile&id='".',"f"."id") "url"

    FROM "files" "f", "file_connections" "c", "materials" "m"
    WHERE "f"."id" = "c"."file_id" AND "m"."id" = "c"."material_id" AND "f"."type_id" = '.$doctype.'
    AND "m"."type_id" = '.$type.' AND "m"."status_id" = 2
    ORDER BY "f"."date_event" DESC NULLS LAST, "f"."ordernum" ASC LIMIT 10 OFFSET 0';
    }
    else
    {
        $query='SELECT * FROM (SELECT "f".*, "m"."name" "material_name", ROWNUM rnum,

    (round(("f"."date_event" - to_date('."'".'01011970'."'".','."'".'ddmmyyyy'."'".'))*86400) - 10*60*60) "unixtime",
    CONCAT('."'/?menu=getfile&id='".',"f"."id") "url"

    FROM "files" "f", "file_connections" "c", "materials" "m"
    WHERE "f"."id" = "c"."file_id" AND "m"."id" = "c"."material_id" AND "f"."type_id" = '.$doctype.'
    AND "m"."type_id" = '.$type.' AND "m"."status_id" = 2
    ORDER BY "f"."date_event" DESC NULLS LAST, "f"."ordernum" ASC) WHERE rnum < 9';
    }

    if($res=$site->dbquery($query))
    {

        return $res;
    }
    else{
        return false;
    }

}

function GetRegionOMSU($site, $id)
{

    $omsu=false;

    $query='SELECT "id" FROM "materials" WHERE "type_id" = 162 AND "id" IN
    (

    SELECT "material_child" FROM "material_connections" WHERE "material_parent" = '.$id.'

    UNION ALL

     SELECT "material_parent" FROM "material_connections" WHERE "material_child" = '.$id.'

    )';

    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            $omsu=MaterialGet($site, $res[0]['id']);
        }
    }

    return $omsu;
}

function GetParamMaterials($site, $id)
{
    $query = <<<EOS
SELECT
    "e"."material_id" "id",
    "m"."date_event"
FROM
    "extra_materials" "e",
    "materials" "m"
WHERE
    "e"."valuemat" = $1
    AND "e"."material_id" = "m"."id"
    AND "m"."status_id" = $2
ORDER BY "date_event" DESC
EOS;

    $q_params = array($id, STATUS_ACTIVE);

    $res = $site->dbquery($query, $q_params);
    return $res;
}

/**
 * @param numeric $mattype
 * @param numeric $doctype
 * @param numeric $omsu
 * @param string $datestart
 * @param string $dateend
 * @return array(string query, array query_params)
 */
function GetOIVFilesQuery($mattype, $doctype, $omsu, $datestart, $dateend)
{
    $q_params = array(FILETYPE_DOCUMENT, STATUS_ACTIVE, $mattype);
    $query = <<<EOS
FROM
    "files" "f"
    INNER JOIN "file_connections" "fc" ON "fc"."file_id" = "f"."id"
WHERE
    "f"."type_id" = $1
    AND {{q_files_selector}}
EOS;

    $q_files_selector = '{{q_files_selector_where}}';

    $q_files_selector_where = array();
    $q_files_selector_where[] = <<<EOS
            "fc"."material_id" IN(
                SELECT "id"
                FROM "materials"
                WHERE
                    "type_id" = $3
                    AND "status_id" = $2
            )
EOS;

    // тип документа - материал
    if ( ! empty($doctype)) {
        $q_params[] = $doctype;
        $pnum = count($q_params);

        $q_files_selector_where[] = <<<EOS
         "fc"."material_id" IN (
            SELECT "material_id" FROM "extra_materials" WHERE "valuemat" = \$$pnum
         )
EOS;
    }

    //если передан ОМСУ
    if ( ! empty($omsu)) {
        $q_params[] = $omsu;
        $pnum = count($q_params);
        $q_files_selector_where[] = <<<EOS
        "fc"."material_id" IN (
            SELECT "id"
            FROM "materials"
            WHERE
                "type_id" = $3
                AND "status_id" = $2
                AND "id" IN(
                    SELECT "material_child"
                    FROM "material_connections"
                    WHERE "material_parent" = \$$pnum
                    
                    UNION ALL

                    SELECT "material_parent"
                    FROM "material_connections"
                    WHERE "material_child" = \$$pnum
                )
        )
EOS;
    }

    $q_files_selector = str_replace('{{q_files_selector_where}}',
        implode(' AND ', $q_files_selector_where),
        $q_files_selector
    );

    $query = str_replace('{{q_files_selector}}', $q_files_selector, $query);

    //даты
    //если передан период дат
    if ( ! empty($datestart)) {
        if (empty($dateend))
            $dateend = $datestart;

        $startstamp = MakeTimeStampFromStr($datestart);
        $endstamp = MakeTimeStampFromStr($dateend) + 24*60*60-1;

        $q_params[] = $startstamp;
        $pnum1 = count($q_params);
        $q_params[] = $endstamp;
        $pnum2 = $pnum1 + 1;

        $query .= <<<EOS
    AND EXTRACT(EPOCH FROM "f"."date_event"::TIMESTAMP WITH TIME ZONE) >= \$$pnum1
    AND EXTRACT(EPOCH FROM "f"."date_event"::TIMESTAMP WITH TIME ZONE)  < \$$pnum2
EOS;
    }

    return array($query, $q_params);
}

function GetOIVFileTypes(
    \iSite $site,
    $mattype=166,
    $doctype='',
    $omsu='',
    $datestart='',
    $dateend='',
    $typemat = null
    )
{
    if (is_null($typemat))
        $typemat = '221';

    list($filesQuery, $q_params) = GetOIVFilesQuery($mattype, null, $omsu, $datestart, $dateend);

    $q_params[] = $typemat;
    $pnum = count($q_params);

    $query = <<<EOS
WITH files_query(id, material_id) AS (SELECT f.id, fc.material_id $filesQuery)
SELECT
    "m"."id",
    "m"."name",
    "m"."extra_number2",
    COUNT(files_query.id) "cnt"
FROM
    "materials" "m"
    INNER JOIN extra_materials exmt ON exmt.valuemat = m.id
    INNER JOIN files_query ON files_query.material_id = exmt.material_id
WHERE
    "m"."type_id" = \$$pnum
    GROUP BY "m"."id","m"."name", "m"."extra_number2"
	ORDER BY "m"."extra_number2" ASC NULLS LAST
EOS;

    list($query, $q_params) = $site->dbqueryExpand($query, $q_params);

    $res = $site->dbquery($query, $q_params, true);
    $filetypes = array();
    if ( ! empty($res)) {
        $filetypes = $res;
    }

    return $filetypes;
}

function GetOIVFilesCount($site, $mattype=166, $type='', $omsu='', $datestart='', $dateend='')
{
    list($filesQuery, $q_params) = GetOIVFilesQuery(
        $mattype,
        $type,
        $omsu,
        $datestart,
        $dateend
    );

    $query = 'SELECT COUNT(f.*) "cnt" '.$filesQuery;
    $res = $site->dbquery($query, $q_params, true);

    if ( ! empty($res))
        return $res[0]['cnt'];
}

function GetOIVFiles($site, $mattype=166, $type='', $omsu='', $datestart='', $dateend='')
{
    list($filesQuery, $q_params) = GetOIVFilesQuery($mattype, $type, $omsu, $datestart, $dateend);

    $q_params[] = DB_TIME_BIAS_HOURS * 3600;
    $pnum = count($q_params);

    $query = <<<EOS
SELECT
    "f".*,
    CONCAT('/?menu=getfile&id=', TRIM(CAST("f"."id" AS CHARACTER(10)))) "url",
    round((EXTRACT (EPOCH FROM "f"."date_event")) + \$$pnum) "unixtime"
    $filesQuery
ORDER BY "f"."date_event" DESC NULLS LAST
EOS;

$site->logWrite(LOG_DEBUG, __METHOD__, "query=".$query);
$site->logWrite(LOG_DEBUG, __METHOD__, "p4=".$q_params[4]."p5=".$q_params[5].",p6=".$q_params[6]);
    $res = $site->dbquery($query, $q_params);

    if ( ! empty($res)) {

        foreach ($res as $i => $row) {
            $cecutient = $site->settings->path.'/photos/'.$row['id'].'_sv';
            $row['cecutient'] = file_exists($cecutient);
            $res[$i] = $row;
        }

        return $res;
    }

    return array();
}

function GetOIVVacancyList(
    \iSite $site,
    $vactypes,
    $type,
    $region='',
    $oiv='',
    $status='',
    $datestart='',
    $dateend='',
    $page=1,
    $perpage=10
    )
{

    //type
    //valid - дата
    //url
    //name
    //ministry (министерства)
    //salary
    //extra_text1 (тэги)


    global
        $vacarr,
        $oivsettings;

    if(!is_array($vacarr))
    {
        $vacarr=GetOIVVacancyes($site, $vactypes, $type, $region, $oiv, $status, $datestart, $dateend);
    }

    $ids='';

    //цикл по вакансиям
    foreach($vacarr as $vac)
    {
        if($ids != '')
        {
            $ids.=',';
        }
        $ids.=$vac['id'];
    }

    //получаем список вакансий
    $vacancyes=Array();

    $startElemPosition  = $page == 1 ? 1 : $page * $perpage - $perpage + 1;
    $endElemPosition    = $page * $perpage;

    $i=0;
    $j=-1;
    foreach($vacarr as $vac)
    {
        ++$i;
        if(($i>=$startElemPosition)&&($i <= $endElemPosition))
        {
            ++$j;

            $vacancyes[$j]=MaterialGet($site, $vac['id']);

            //дата приёма док-тов
            if(isset($vacancyes[$j]['params'][$oivsettings['vacancy_datetoparam']]['unixtime']))
            {
                if($vacancyes[$j]['params'][$oivsettings['vacancy_datetoparam']]['unixtime'] != '')
                {
                    $vacancyes[$j]['valid']=$vacancyes[$j]['params'][$oivsettings['vacancy_datetoparam']]['unixtime'];
                }
            }

            //зар. плата
            if(isset($vacancyes[$j]['params'][$oivsettings['vacancy_salaryparam']]['value']))
            {
                if($vacancyes[$j]['params'][$oivsettings['vacancy_salaryparam']]['value'] != '')
                {
                    $vacancyes[$j]['salary']=$vacancyes[$j]['params'][$oivsettings['vacancy_salaryparam']]['value'];
                }
            }

            //министерства
            $vacancyes[$j]['ministry']='';



            if ( ! empty($vacancyes[$j]['params'][$oivsettings['vacancy_typeparam']]['valuename'])) {
                $vacancyes[$j]['type'] = $vacancyes[$j]['params'][$oivsettings['vacancy_typeparam']]['valuename'];
            }

            //типы
        }
    }

    /*

    $i=0;
    $j=-1;
    foreach($vacarr as $vac)
    {
        ++$i;
        if(($i>=$page)&&($i < $perpage))
        {
            ++$j;

            $vacancyes[$j]=MaterialGet($site, $vac['id']);

            //дата приёма док-тов
            if(isset($vacancyes[$j]['params'][$oivsettings['vacancy_datetoparam']]['unixtime']))
            {
                if($vacancyes[$j]['params'][$oivsettings['vacancy_datetoparam']]['unixtime'] != '')
                {
                    $vacancyes[$j]['valid']=$vacancyes[$j]['params'][$oivsettings['vacancy_datetoparam']]['unixtime'];
                }
            }

            //зар. плата
            if(isset($vacancyes[$j]['params'][$oivsettings['vacancy_salaryparam']]['value']))
            {
                if($vacancyes[$j]['params'][$oivsettings['vacancy_salaryparam']]['value'] != '')
                {
                    $vacancyes[$j]['salary']=$vacancyes[$j]['params'][$oivsettings['vacancy_salaryparam']]['value'];
                }
            }

            //министерства
            $vacancyes[$j]['ministry']='';



            if ( ! empty($vacancyes[$j]['params'][$oivsettings['vacancy_typeparam']]['valuename'])) {
                $vacancyes[$j]['type'] = $vacancyes[$j]['params'][$oivsettings['vacancy_typeparam']]['valuename'];
            }

            //типы
        }
    }

      */

    //министерство

    return $vacancyes;
}

//список вакансий (хабкрай вакансии) новый
function GetVacancyListNew(
    \iSite $site,
    $type,
    $region='',
    $oiv='',
    $status='',
    $datestart='',
    $dateend='',
    $page=1,
    $perpage=10
    )
{
    $vacarr=GetVacancyListNewArr($site, $type, $region, $oiv, $status, $datestart, $dateend);

    //получаем список вакансий
    $vacancyes=Array();

    $i=0;
    $j=-1;

    $startval=0;
    $endval=0;

    $startval=($page-1)*$perpage;
    $endval=$page*$perpage;

    if(isset($vacarr[0]))
    {
        foreach($vacarr as $vac)
        {
            ++$i;
            if(($i>=$startval)&&($i <= $endval))
            {
                ++$j;

                $vacancyes[$j]=MaterialGet($site, $vac['id']);

                $fullmaterial=$vacancyes[$j];

                $vacancyes[$j]['date_event']=$vac['date_event'];
                $vacancyes[$j]['valid']=$vac['valid'];
                $vacancyes[$j]['unixtime']=$vac['unixtime'];

                $zarplata='';
                $queryz='SELECT "id" FROM "extra_mattypes" WHERE "name" LIKE '."'Заработная плата'".'';
                if($resz=$site->dbquery($queryz))
                {
                    if(count($resz) > 0)
                    {
                        foreach($resz as $rowz)
                        {
                            if($zarplata == '')
                            {
                                if(isset($fullmaterial['params'][$rowz['id']]['value']))
                                {
                                    $zarplata=$fullmaterial['params'][$rowz['id']]['value'];

                                }
                            }
                        }
                    }
                }

                if($zarplata != '')
                {
                    $vacancyes[$j]['salary']=$zarplata;
                }

                //министерства
                $vacancyes[$j]['ministry']='';

                $minarr=Array('91','46');

                if(isset($vacancyes[$j]['connected_materials']))
                {
                    foreach($vacancyes[$j]['connected_materials'] as $cm)
                    {
                        if(in_array($cm['type_id'],$minarr))
                        {
                            if($vacancyes[$j]['ministry'] != '')
                            {
                                $vacancyes[$j]['ministry'].=', ';
                            }
                            $vacancyes[$j]['ministry'].=$cm['name'];
                        }
                    }
                }


                $vacancyes[$j]['type']='';

                foreach($vacancyes[$j]['params'] as $param)
                {
                    if(!isset($param['valuename']))
                    {
                        $param['valuename']='';
                    }

                    if($param['valuename'] == 'Муниципальная служба')
                    {
                        $vacancyes[$j]['type']='Муниципальная служба';
                    }
                    elseif($param['valuename'] == 'Госслужба')
                    {
                        $vacancyes[$j]['type']='Госслужба';
                    }
                }

                //типы

                if($vacancyes[$j]['type'] == '')
                {
                    $vacancyes[$j]['type']='Другое';
                }

            }

        }
    }

    return($vacancyes);

}

//массив вакансий (хабкрай вакансии) новый
function GetVacancyListNewArr(
    \iSite $site,
    $type,
    $region='',
    $oiv='',
    $status='',
    $datestart='',
    $dateend='',
    $page=1,
    $perpage=10)
{
    $vactypes='47, 93, 83, 165, 212';

    //запрос вакансий
    $queryold='SELECT "m"."id",
    "m"."date_event" "validdate",
    (SELECT round(("m"."date_edit" - to_date('."'01-01-1970','DD-MM-YYYY'".')) * (86400) - 10*60*60) as dt FROM dual) "unixtime",
    "m"."date_edit" "date_event"
    FROM "materials" "m" WHERE "m"."type_id" IN ('.$vactypes.') AND "m"."status_id" = 2 AND "m"."language_id" = 1
    AND "m"."id" NOT IN
        (
        SELECT "pd"."material_id" FROM "extra_materials" "pd"
        WHERE "pd"."valuedate" IS NOT NULL AND "pd"."type_id" IN
            (
            SELECT "pdt"."id" FROM "extra_mattypes" "pdt" WHERE "pdt"."name" LIKE '."'Приём документов до'".'
            )
        )
        ';


    $querynew='SELECT "mn"."id",
    (

        SELECT MAX("cc"."valuedate") FROM "extra_materials" "cc" WHERE "cc"."material_id" = "mn"."id" AND "cc"."type_id" IN
        (
            SELECT "ccpdtn"."id" FROM "extra_mattypes" "ccpdtn" WHERE "ccpdtn"."name" LIKE '."'Приём документов до'".'
        )

    ) "validdate",
    (SELECT round(("mn"."date_event" - to_date('."'01-01-1970','DD-MM-YYYY'".')) * (86400) - 10*60*60) as dt FROM dual) "unixtime",
    "mn"."date_edit" "date_event"
    FROM "materials" "mn"

    WHERE "mn"."type_id" IN ('.$vactypes.') AND "mn"."status_id" = 2 AND "mn"."language_id" = 1
    AND "mn"."id" IN
        (
        SELECT "pdn"."material_id" FROM "extra_materials" "pdn"
        WHERE "pdn"."valuedate" IS NOT NULL AND "pdn"."type_id" IN
            (
            SELECT "pdtn"."id" FROM "extra_mattypes" "pdtn" WHERE "pdtn"."name" LIKE '."'Приём документов до'".'
            )
        )';


    $query='SELECT "mt".*,

     (SELECT round(("mt"."validdate" - to_date('."'01-01-1970','DD-MM-YYYY'".')) * (86400) - 10*60*60) as dt FROM dual) "valid"

     FROM ('.$queryold.' UNION ALL '.$querynew.') "mt" WHERE 1 = 1 ';




    //ограничители в зависимости от переданных параметров
    //ограничиваем - выводим только с заполненными зар. платами
    $query.=' AND "mt"."id" IN (

SELECT "zp"."material_id" FROM "extra_materials" "zp" WHERE "zp"."type_id" IN (SELECT "id" FROM "extra_mattypes" WHERE "name" LIKE '."'Заработная плата'".')

)';

    //если передан статус
    if($status != '')
    {
        if($status === '0')
        {

            $query.=' AND "mt"."validdate"  >= SYSDATE
            ';

        }
        elseif($status === '1')
        {
            $query.=' AND "mt"."validdate" < SYSDATE
            ';
        }
    }

    //если передан период дат
    if(($datestart != '')&&($dateend != ''))
    {
        $endstamp=MakeTimeStampFromStr($dateend)+24*60*60-1;
        $dateendn=date('d.m.Y',$endstamp);
        $query.=' AND "mt"."date_event" >= '."TO_DATE('$datestart','DD.MM.YYYY')";
        $query.=' AND "mt"."date_event" < '."TO_DATE('$dateendn','DD.MM.YYYY')";
    }


    //если передана конкретная дата
    elseif($datestart != '')
    {
        $endstamp=MakeTimeStampFromStr($datestart)+24*60*60-1;
        $dateendn=date('d.m.Y',$endstamp);
        $query.=' AND "mt"."date_event" >= '."TO_DATE('$datestart','DD.MM.YYYY')";
        $query.=' AND "mt"."date_event" < '."TO_DATE('$dateendn','DD.MM.YYYY')";
    }

    /*if($onlyactual == 1)
    {
        $now=date('d.m.Y');
        $query.=' AND "date_event" >= TO_DATE('."'$now','DD.MM.YYYY'".')';
    }*/

    if(($type === 0)||($type === '0'))
    {
        $gsflag=1;
        $msflag=0;
    }
    elseif(($type === 1)||($type === '1'))
    {
        $gsflag=0;
        $msflag=1;
    }
    else{
        $gsflag=0;
        $msflag=0;
    }

    if($type === '')
    {
        $gsflag=-1;
        $msflag=-1;
    }

    //echo 'type - '.$type.' gs - '.$gsflag.' ms - '.$msflag;

    if($gsflag == 1)
    {
        $query.=' AND "mt"."id" IN (

    SELECT "p"."material_id" FROM "extra_materials" "p", "materials" "pvm"
    WHERE "p"."valuemat" = "pvm"."id" AND "pvm"."name" = '."'Госслужба'".')';
    }

    if($msflag == 1)
    {
        $query.=' AND "mt"."id" IN (

    SELECT "p"."material_id" FROM "extra_materials" "p", "materials" "pvm"
    WHERE "p"."valuemat" = "pvm"."id" AND "pvm"."name" = '."'Муниципальная служба'".')';
    }

    if(($msflag == 0)&&($gsflag == 0))
    {
        $query.=' AND "mt"."id" NOT IN (

    SELECT "p"."material_id" FROM "extra_materials" "p", "materials" "pvm"
    WHERE "p"."valuemat" = "pvm"."id" AND "pvm"."name" = '."'Госслужба'".')';

        $query.=' AND "mt"."id" NOT IN (

    SELECT "p"."material_id" FROM "extra_materials" "p", "materials" "pvm"
    WHERE "p"."valuemat" = "pvm"."id" AND "pvm"."name" = '."'Муниципальная служба'".')';
    }

    if($region != '')
    {
        $query.=' AND "mt"."id" IN (

        SELECT "c"."material_child" "mid" FROM "material_connections" "c" WHERE "c"."material_parent" = '.$region.'

        UNION ALL

        SELECT "cc"."material_parent" "mid" FROM "material_connections" "cc" WHERE "cc"."material_child" = '.$region.'

        )';
    }

    if($oiv != '')
    {
        $query.=' AND "mt"."id" IN (

        SELECT "c"."material_child" "mid" FROM "material_connections" "c" WHERE "c"."material_parent" = '.$oiv.'

        UNION ALL

        SELECT "cc"."material_parent" "mid" FROM "material_connections" "cc" WHERE "cc"."material_child" = '.$oiv.'

        )';
    }

    $query.=' ORDER BY "mt"."unixtime" DESC';


    if($res=$site->dbquery($query))
    {
        return($res);
    }
    else
    {
        return false;
    }


}

function GetVacancyList($site, $type, $region='', $oiv='', $status='', $datestart='', $dateend='', $page=1, $perpage=10)
{

    //type
    //valid - дата
    //url
    //name
    //ministry (министерства)
    //salary
    //extra_text1 (тэги)


    global $vacarr;

    if(!is_array($vacarr))
    {
        $vacarr=GetVacancyes($site, $type, $region, $oiv, $status, $datestart, $dateend);
    }

    $ids='';

    //цикл по вакансиям
    foreach($vacarr as $vac)
    {
        if($ids != '')
        {
            $ids.=',';
        }
        $ids.=$vac['id'];
    }

    //получаем список вакансий
    $vacancyes=Array();

    $i=0;
    $j=-1;
    foreach($vacarr as $vac)
    {
        ++$i;
        if(($i>=$page)&&($i < $perpage))
        {
            ++$j;

            $vacancyes[$j]=MaterialGet($site, $vac['id']);


            if(!isset($vacancyes[$j]['editunixtime']))
            {
                delMatCache($site, $vac['id']);
                $vacancyes[$j]=MaterialGet($site, $vac['id']);
            }

            //дата приёма док-тов
            /*if(isset($vacancyes[$j]['params'][37]['unixtime']))
            {
                if($vacancyes[$j]['params'][37]['unixtime'] != '')
                {
                    $vacancyes[$j]['valid']=$vacancyes[$j]['params'][37]['unixtime'];
                }
            }*/
            $fullmaterial=$vacancyes[$j];


            $dateto='';
            $queryz='SELECT "id" FROM "extra_mattypes" WHERE "name" LIKE '."'Приём документов до'".'';
            if($resz=$site->dbquery($queryz))
            {
                if(count($resz) > 0)
                {
                    foreach($resz as $rowz)
                    {
                        if($dateto == '')
                        {
                            if(isset($fullmaterial['params'][$rowz['id']]['value']))
                            {
                                $dateto=$fullmaterial['params'][$rowz['id']]['unixtime'];
                            }
                        }
                    }
                }
            }

            if($dateto != '')
            {

                    $vacancyes[$j]['valid']=$dateto;

            }
            else
            {
                $vacancyes[$j]['valid']=$vacancyes[$j]['unixtime'];
                $vacancyes[$j]['unixtime']=$vacancyes[$j]['editunixtime'];
            }


            //зар. плата
            /*if(isset($vacancyes[$j]['params'][13]['value']))
            {
                if($vacancyes[$j]['params'][13]['value'] != '')
                {
                    $vacancyes[$j]['salary']=$vacancyes[$j]['params'][13]['value'];
                }
            }*/

            $zarplata='';
            $queryz='SELECT "id" FROM "extra_mattypes" WHERE "name" LIKE '."'Заработная плата'".'';
            if($resz=$site->dbquery($queryz))
            {
                if(count($resz) > 0)
                {
                    foreach($resz as $rowz)
                    {
                        if($zarplata == '')
                        {
                            if(isset($fullmaterial['params'][$rowz['id']]['value']))
                            {
                                $zarplata=$fullmaterial['params'][$rowz['id']]['value'];

                            }
                        }
                    }
                }
            }

            if($zarplata != '')
            {
               $vacancyes[$j]['salary']=$zarplata;
            }

            //министерства
            $vacancyes[$j]['ministry']='';

            $minarr=Array('91','46');

            foreach($vacancyes[$j]['connected_materials'] as $cm)
            {
                if(in_array($cm['type_id'],$minarr))
                {
                    if($vacancyes[$j]['ministry'] != '')
                    {
                        $vacancyes[$j]['ministry'].=', ';
                    }
                    $vacancyes[$j]['ministry'].=$cm['name'];
                }
            }

            $vacancyes[$j]['type']='';

            //типы
            if(isset($vacancyes[$j]['params'][14]['value']))
            {
                if($vacancyes[$j]['params'][14]['value'] == '1223')
                {

                    if($vacancyes[$j]['type'] != '')
                    {
                        $vacancyes[$j]['type'].=', ';
                    }

                    $vacancyes[$j]['type'].='Госслужба';
                }
            }

            if(isset($vacancyes[$j]['params'][15]['value']))
            {
                if($vacancyes[$j]['params'][15]['value'] == '1225')
                {

                    if($vacancyes[$j]['type'] != '')
                    {
                        $vacancyes[$j]['type'].=', ';
                    }

                    $vacancyes[$j]['type'].='Муниципальная служба';
                }
            }

            if($vacancyes[$j]['type'] == '')
            {
                $vacancyes[$j]['type']='Другое';
            }

        }

    }





    //министерство
    //krumo($vacancyes);

    //uasort($vacancyes, 'myCmpDate');

    return $vacancyes;
}

function GetOIVVacancyStatuses($site, $vtypes, $type, $region='', $oiv='', $status='', $datestart='', $dateend='')
{

    global $vacarr, $oivsettings;

    $statuses=Array();


    if(!is_array($vacarr))
    {
        $vacarr=GetOIVVacancyes($site, $vtypes, $type, $region, $oiv, $status, $datestart, $dateend);
    }


    $statuses[0]['name']='Приём документов';
    $statuses[0]['id']='0';
    $statuses[0]['cnt']='0';

    $statuses[1]['name']='Архив';
    $statuses[1]['id']='1';
    $statuses[1]['cnt']='0';



    $ids='';

    //цикл по вакансиям
    foreach($vacarr as $vac)
    {
        if($ids != '')
        {
            $ids.=',';
        }
        $ids.=$vac['id'];
    }


    //получаем кол-во актуальных вакансий
    $query='SELECT count("material_id") "cnt" FROM "extra_materials" WHERE "valuedate" >= SYSDATE AND "valuedate" IS NOT NULL
    AND "material_id" IN ('.$ids.') AND "type_id" = '.$oivsettings['vacancy_datetoparam'];

    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            $statuses[0]['cnt']=$res[0]['cnt'];
        }
    }

    //получаем кол-во архивных вакансий
    $query='SELECT count("material_id") "cnt" FROM "extra_materials" WHERE "valuedate" < SYSDATE AND "valuedate" IS NOT NULL
    AND "material_id" IN ('.$ids.') AND "type_id" = '.$oivsettings['vacancy_datetoparam'];

    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            $statuses[1]['cnt']=$res[0]['cnt'];
        }
    }


    return($statuses);

}

function GetVacancyStatuses($site, $type, $region='', $oiv='', $status='', $datestart='', $dateend='')
{

    global $vacarr;

    $statuses=Array();


    //if(!is_array($vacarr))
    //{
        //$vacarr=GetVacancyes($site, $type, $region, $oiv, $status, $datestart, $dateend);
        $vacarr=GetVacancyListNewArr($site, $type, $region, $oiv, $status, $datestart, $dateend);

    //}


    $statuses[0]['name']='Приём документов';
    $statuses[0]['id']='0';
    $statuses[0]['cnt']='0';

    $statuses[1]['name']='Архив';
    $statuses[1]['id']='1';
    $statuses[1]['cnt']='0';



    $ids='';

    //цикл по вакансиям
    /*foreach($vacarr as $vac)
    {
        if($ids != '')
        {
            $ids.=',';
        }
        $ids.=$vac['id'];
    }*/


    //получаем кол-во актуальных вакансий


    if(isset($vacarr[0]))
    {
        foreach($vacarr as $vac)
        {
            if($vac['valid'] >= time())
            {
                ++$statuses[0]['cnt'];
            }
            else
            {
                ++$statuses[1]['cnt'];
            }
        }
    }


    /*$query='SELECT count("material_id") "cnt" FROM "extra_materials" WHERE "valuedate" >= SYSDATE AND "valuedate" IS NOT NULL
    AND "material_id" IN ('.$ids.') AND "type_id" = 37';

    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            $statuses[0]['cnt']=$res[0]['cnt'];
        }
    }*/

    //получаем кол-во архивных вакансий
    /*$query='SELECT count("material_id") "cnt" FROM "extra_materials" WHERE "valuedate" < SYSDATE AND "valuedate" IS NOT NULL
    AND "material_id" IN ('.$ids.') AND "type_id" = 37';

    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            $statuses[1]['cnt']=$res[0]['cnt'];
        }
    }*/


    return($statuses);

}

function GetOIVVacancyTypes($site, $vtypes, $type, $region='', $oiv='', $status='', $datestart='', $dateend='')
{
    global $vacarr, $oivsettings, $khabkrai;

    $types=Array();

    if(!is_array($vacarr))
    {
        $vacarr=GetOIVVacancyes($site, $vtypes, $type, $region, $oiv, $status, $datestart, $dateend);
    }


    //Госслужба
    //Не госслужба
    //Другое

    /*$types[0]['name']='Госслужба';
    $types[0]['id']='0';
    $types[0]['cnt']='0';

    $types[1]['name']='Муниципальная служба';
    $types[1]['id']='1';
    $types[1]['cnt']='0';

    $types[2]['name']='Другое';
    $types[2]['id']='2';
    $types[2]['cnt']='0';*/

    $cnts=Array();

    $query='SELECT "id", "name" FROM "materials" WHERE "status_id" = 2 AND "type_id" = '.$oivsettings['vacancytypes'];
    if($res=$khabkrai->dbquery($query))
    {
        foreach($res as $key => $row)
        {
            $types[$key]['id']=$row['id'];
            $types[$key]['name']=$row['name'];
            $types[$key]['cnt']=0;
            $cnts[$row['id']]=0;
        }
    }



    $ids='';

    //цикл по вакансиям
    if ( !  empty($vacarr)) {
        foreach($vacarr as $vac)
        {
            if (isset($vac['params'][$oivsettings['vacancy_typeparam']]['value'])) {
                $val=$vac['params'][$oivsettings['vacancy_typeparam']]['value'];

                ++$cnts[$val];
            }
        }
    }

    $newtype=Array();
    foreach($types as $type)
    {
        $type['cnt']=$cnts[$type['id']];
        $newtype[]=$type;
    }



    return($newtype);

}

function GetVacancyTypes($site, $type, $region='', $oiv='', $status='', $datestart='', $dateend='')
{
    global $vacarr;

    $types=Array();



    if(!is_array($vacarr))
    {
        $vacarr=GetVacancyes($site, $type, $region, $oiv, $status, $datestart, $dateend);
    }


    //Госслужба
    //Не госслужба
    //Другое

    $types[0]['name']='Госслужба';
    $types[0]['id']='0';
    $types[0]['cnt']='0';

    $types[1]['name']='Муниципальная служба';
    $types[1]['id']='1';
    $types[1]['cnt']='0';

    $types[2]['name']='Другое';
    $types[2]['id']='2';
    $types[2]['cnt']='0';


    $ids='';

    //цикл по вакансиям
    foreach($vacarr as $vac)
    {
        if($ids != '')
        {
            $ids.=',';
        }
        $ids.=$vac['id'];
    }


    //получаем кол-во госслужбы
    $query='SELECT count("material_id") "cnt" FROM "extra_materials" WHERE "valuemat" = 1223 AND "material_id" IN ('.$ids.')';

    //krumo($query);

    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            $types[0]['cnt']=$res[0]['cnt'];
        }
    }

    //получаем кол-во мун.службы

    $query='SELECT count("material_id") "cnt" FROM "extra_materials" WHERE "valuemat" = 1225 AND "material_id" IN ('.$ids.')';

    //krumo($query);

    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            $types[1]['cnt']=$res[0]['cnt'];
        }
    }

    //получаем кол-во иного
    $query='SELECT count("id") "cnt" FROM "materials" WHERE "id" IN ('.$ids.') AND "id" NOT IN (

    SELECT "material_id" FROM "extra_materials" WHERE "valuemat" = 1225 AND "material_id" IN ('.$ids.')

    ) AND "id" NOT IN (

    SELECT "material_id" FROM "extra_materials" WHERE "valuemat" = 1223 AND "material_id" IN ('.$ids.')

    )';

    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            $types[2]['cnt']=$res[0]['cnt'];
        }
    }

    return($types);

}

//функция выводит оивы, на которые есть вакансии согласно фильтру с кол-вов вакансий
function GetVacancyOIVS($site, $type, $region='', $oiv='', $status='', $datestart='', $dateend='')
{
    //global $vacarr;

    $oivs=Array();

    //if(!is_array($vacarr))
    //{
        //GetVacancyListNewArr()
        $vacarr=GetVacancyListNew($site, $type, $region, $oiv, $status, $datestart, $dateend, 1, 99999999);
    //}

    //krumo($vacarr);

    //получаем список ОИВ-ов
    $query='SELECT * FROM "materials" WHERE "type_id" IN (46,91) AND "status_id" = 2 AND "language_id" = 1
    ORDER BY "name" ASC';
    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            $i=-1;
            //цикл по ОИВ-ам
            foreach($res as $row)
            {
                ++$i;
                $oivs[$i]['id']=$row['id'];
                $oivs[$i]['name']=$row['name'];
                $oivs[$i]['cnt']=0;

                //цикл по вакансиям
                foreach($vacarr as $vac)
                {
                    if(isset($vac['connected_materials']))
                    {
                        if(count($vac['connected_materials'])>0)
                        {
                            foreach($vac['connected_materials'] as $v)
                            {
                                if($v['id'] == $row['id'])
                                {
                                    ++$oivs[$i]['cnt'];
                                }
                            }
                        }
                    }
                }

            }
        }
    }

    return($oivs);

}

//функция получает ВСЕ возможные вакансии
function GetOIVVacancyes($site, $types, $type, $region='', $oiv='', $status='', $datestart='', $dateend='', $ifglobal=true)
{

    global $oivsettings;

    if($ifglobal){
        global $vacarr;
    }


    $vactypes=$types;
    $query='SELECT "id", "date_event" FROM "materials" WHERE "type_id" IN ('.$vactypes.') AND "status_id" = 2 AND "language_id" = 1';


    //ограничиваем - выводим только с заполненными зар. платами
    $query.=' AND "id" IN (

SELECT "zp"."material_id" FROM "extra_materials" "zp" WHERE "zp"."type_id" IN (SELECT "id" FROM "extra_mattypes" WHERE "name" LIKE '."'Заработная плата'".')

)';

    //если передан статус
    if($status != '')
    {
        if($status === '0')
        {

            $query.=' AND "id" IN (
            SELECT "material_id" FROM "extra_materials" WHERE "type_id" = '.$oivsettings['vacancy_datetoparam'].' AND "valuedate" >= SYSDATE
            )';

        }
        elseif($status === '1')
        {
            $query.=' AND "id" IN (
            SELECT "material_id" FROM "extra_materials" WHERE "type_id" = '.$oivsettings['vacancy_datetoparam'].' AND "valuedate" < SYSDATE
            )';
        }
    }

    //если передан период дат
    if(($datestart != '')&&($dateend != ''))
    {
        $endstamp=MakeTimeStampFromStr($dateend)+24*60*60-1;
        $dateendn=date('d.m.Y',$endstamp);
        $query.=' AND "date_event" >= '."TO_DATE('$datestart','DD.MM.YYYY')";
        $query.=' AND "date_event" < '."TO_DATE('$dateendn','DD.MM.YYYY')";
    }


    //если передана конкретная дата
    elseif($datestart != '')
    {
        $endstamp=MakeTimeStampFromStr($datestart)+24*60*60-1;
        $dateendn=date('d.m.Y',$endstamp);
        $query.=' AND "date_event" >= '."TO_DATE('$datestart','DD.MM.YYYY')";
        $query.=' AND "date_event" < '."TO_DATE('$dateendn','DD.MM.YYYY')";
    }

    /*if($onlyactual == 1)
    {
        $now=date('d.m.Y');
        $query.=' AND "date_event" >= TO_DATE('."'$now','DD.MM.YYYY'".')';
    }*/



    //echo 'type - '.$type.' gs - '.$gsflag.' ms - '.$msflag;

    if($type != '')
    {
        $query.=' AND "id" IN (

        SELECT "et"."material_id" FROM "extra_materials" "et" WHERE "et"."type_id" = '.$oivsettings['vacancy_typeparam'].' AND "et"."valuemat" = '.$type.'

        )';
    }

    if($region != '')
    {
        $query.=' AND "id" IN (

        SELECT "c"."material_child" "mid" FROM "material_connections" "c" WHERE "c"."material_parent" = '.$region.'

        UNION ALL

        SELECT "cc"."material_parent" "mid" FROM "material_connections" "cc" WHERE "cc"."material_child" = '.$region.'

        )';
    }

    if($oiv != '')
    {
        $query.=' AND "id" IN (

        SELECT "c"."material_child" "mid" FROM "material_connections" "c" WHERE "c"."material_parent" = '.$oiv.'

        UNION ALL

        SELECT "cc"."material_parent" "mid" FROM "material_connections" "cc" WHERE "cc"."material_child" = '.$oiv.'

        )';
    }


    //echo '<pre>'.$query.'</pre>';

    $query.= ' ORDER BY "date_event" DESC';


    $ids=Array();
    //получаем список id
    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            foreach($res as $row)
            {

                $ids[]=$row['id'];
            }
        }
    }

    if(count($ids) > 0)
    {
        foreach($ids as $id)
        {
            $mat=MaterialGet($site, $id);
            $materials[]=$mat;
        }
    }
    else{
        $materials=Array();
    }


    $vacarr=$materials;

    //krumo($vacarr);

    return($materials);

}

//функция получает ВСЕ возможные вакансии
function GetVacancyes($site, $type, $region='', $oiv='', $status='', $datestart='', $dateend='', $ifglobal=true)
{

    if($ifglobal){
        global $vacarr;
    }


    $vactypes='47, 93, 83, 165, 212';
    $query='SELECT "id", "date_event" FROM "materials" WHERE "type_id" IN ('.$vactypes.') AND "status_id" = 2 AND "language_id" = 1';


    //ограничиваем - выводим только с заполненными зар. платами
    $query.=' AND "id" IN (

SELECT "zp"."material_id" FROM "extra_materials" "zp" WHERE "zp"."type_id" IN (SELECT "id" FROM "extra_mattypes" WHERE "name" LIKE '."'Заработная плата'".')

)';

    //если передан статус
    if($status != '')
    {
        if($status === '0')
        {

            $query.=' AND "id" IN (
            SELECT "material_id" FROM "extra_materials" WHERE "type_id" = 37 AND "valuedate" >= SYSDATE
            )';

        }
        elseif($status === '1')
        {
            $query.=' AND "id" IN (
            SELECT "material_id" FROM "extra_materials" WHERE "type_id" = 37 AND "valuedate" < SYSDATE
            )';
        }
    }

    //если передан период дат
    if(($datestart != '')&&($dateend != ''))
    {
        $endstamp=MakeTimeStampFromStr($dateend)+24*60*60-1;
        $dateendn=date('d.m.Y',$endstamp);
        $query.=' AND "date_event" >= '."TO_DATE('$datestart','DD.MM.YYYY')";
        $query.=' AND "date_event" < '."TO_DATE('$dateendn','DD.MM.YYYY')";
    }


    //если передана конкретная дата
    elseif($datestart != '')
    {
        $endstamp=MakeTimeStampFromStr($datestart)+24*60*60-1;
        $dateendn=date('d.m.Y',$endstamp);
        $query.=' AND "date_event" >= '."TO_DATE('$datestart','DD.MM.YYYY')";
        $query.=' AND "date_event" < '."TO_DATE('$dateendn','DD.MM.YYYY')";
    }

    /*if($onlyactual == 1)
    {
        $now=date('d.m.Y');
        $query.=' AND "date_event" >= TO_DATE('."'$now','DD.MM.YYYY'".')';
    }*/

    if(($type === 0)||($type === '0'))
    {
        $gsflag=1;
        $msflag=0;
    }
    elseif(($type === 1)||($type === '1'))
    {
        $gsflag=0;
        $msflag=1;
    }
    else{
        $gsflag=0;
        $msflag=0;
    }

    if($type === '')
    {
        $gsflag=-1;
        $msflag=-1;
    }

    //echo 'type - '.$type.' gs - '.$gsflag.' ms - '.$msflag;

    if($gsflag == 1)
    {
        $query.=' AND "id" IN (

    SELECT "p"."material_id" FROM "extra_materials" "p" WHERE "p"."valuemat" = 1223)';
    }

    if($msflag == 1)
    {
        $query.=' AND "id" IN (

    SELECT "p"."material_id" FROM "extra_materials" "p" WHERE "p"."valuemat" = 1225)';
    }

    if(($msflag == 0)&&($gsflag == 0))
    {
        $query.=' AND "id" NOT IN (

    SELECT "p"."material_id" FROM "extra_materials" "p" WHERE "p"."valuemat" = 1223)';

        $query.=' AND "id" NOT IN (

    SELECT "p"."material_id" FROM "extra_materials" "p" WHERE "p"."valuemat" = 1225)';
    }

    if($region != '')
    {
        $query.=' AND "id" IN (

        SELECT "c"."material_child" "mid" FROM "material_connections" "c" WHERE "c"."material_parent" = '.$region.'

        UNION ALL

        SELECT "cc"."material_parent" "mid" FROM "material_connections" "cc" WHERE "cc"."material_child" = '.$region.'

        )';
    }

    if($oiv != '')
    {
        $query.=' AND "id" IN (

        SELECT "c"."material_child" "mid" FROM "material_connections" "c" WHERE "c"."material_parent" = '.$oiv.'

        UNION ALL

        SELECT "cc"."material_parent" "mid" FROM "material_connections" "cc" WHERE "cc"."material_child" = '.$oiv.'

        )';
    }


    //echo '<pre>'.$query.'</pre>';

    $query.=' ORDER BY "date_event" DESC';

    $ids=Array();
    //получаем список id
    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            foreach($res as $row)
            {

                $ids[]=$row['id'];
            }
        }
    }

    if(count($ids) > 0)
    {
        foreach($ids as $id)
        {
            $mat=MaterialGet($site, $id);
            $materials[]=$mat;
        }
    }
    else{
        $materials=Array();
    }


    $vacarr=$materials;

    //krumo($vacarr);

    return($materials);

}

//получаем все регионы
function GetAllVilages($site)
{
    $villages=Array();



    $site->values->orderby = 'name';
    $site->values->ordersort = 'ASC';

    $villarr=MaterialList($site, 209, 1, 1000, 2);

    $i=-1;
    foreach($villarr as $vil)
    {
        ++$i;
        $villages[$i]['id']=$vil['id'];
        $villages[$i]['name']=$vil['name'];
        $villages[$i]['url']=$vil['url'];
        $villages[$i]['region_name']='';

        //получаем регион
        $query='SELECT MAX("m"."name") "name" FROM "materials" "m" WHERE "m"."status_id" = 2 AND "m"."type_id" = 21
        AND "m"."id" IN
        (
        SELECT "material_child" "id" FROM "material_connections" WHERE "material_parent" = '.$vil['id'].'
        UNION ALL
        SELECT "material_parent" "id" FROM "material_connections" WHERE "material_child" = '.$vil['id'].'
        )';

        if($res=$site->dbquery($query))
        {
            if((count($res) > 0)&&($res[0]['name'] != ''))
            {
                $villages[$i]['region_name']=$res[0]['name'];
            }
        }

    }

    return $villages;
}

function MakeMaterialHeadersMetaData(\iSite $site)
{
    $title=$site->data->title;
    $breadcrumbs='<a class="breadcrumbs__link" href="/">Главная</a>';
    $oldlink='';
    $typestr='';
    $lasttype='';

    $i=0;
    $cnt=count($site->data->matURLArr);

    foreach($site->data->matURLArr as $el)
    {
        $title=$el['name'].' - '.$title;
        ++$i;
        if($el['type'] == 'type')
        {
            if($typestr != '')
            {
                $typestr.='_';
            }
            $typestr.=$el['id'];
            $lasttype=$el['id'];
        }

        if($i != $cnt)
        {
            if($el['type'] == 'type')
            {
                $oldlink.='/'.$el['char'];
            }
            else
            {
                $oldlink.='/'.$el['id'];
            }

            $breadcrumbs.=' / <a class="breadcrumbs__link" href="'.$oldlink.'">'.$el['name'].'</a>';
        }
    }

    if($cnt == 0)
    {
        $site->data->shabname='f_materials_1.php';
        $site->data->shabid=1;
    }
    else
    {
        if($site->data->matURLArr[$cnt-1]['type'] == 'type')
        {
            $site->data->shabname = 'f_materials_'.$typestr.'.php';
            $site->data->description = $title;
        }
        else{
            $site->data->shabname = 'f_material_'.$lasttype.'.php';
            $site->data->description = 'mat';
        }

        $site->data->shabid=$lasttype;
    }


    $site->data->title=$title;
    $site->data->breadcrumbs = '<div class="body-2 breadcrumbs">'.$breadcrumbs.'</div>';

    if(isset($site->data->matURLArr[$cnt-1]))
    {
        $site->data->iH1=$site->data->matURLArr[$cnt-1]['name'];
    }

    return $site->data;
}

//получение контактов ОМСУ
function GetOMSUContacts($site, $material)
{
    $omsu_contacts=Array();

    //контакты района
    $omsu_contacts['adm']['title'] = $material['name'];

    if(isset($material['params'][39]['value']))
    {
        $omsu_contacts['adm']['site'] = $material['params'][39]['value'];
    }elseif($material['params'][107]['value']){
        $omsu_contacts['adm']['site'] = $material['params'][107]['value']; // для поселений на локале (на проде у этого поля другой id = 107)
    }

    if(isset($material['contacts'][0]['fulladdr']) && ($material['contacts'][0]['fulladdr'] != ''))
    {
        $omsu_contacts['adm']['addr']=$material['contacts'][0]['fulladdr'];
    }

    if(isset($material['contacts'][0]['email']) && ($material['contacts'][0]['email'] != ''))
    {
        $omsu_contacts['adm']['email']=$material['contacts'][0]['email'];
    }

    if(isset($material['contacts'][0]['tel']) && ($material['contacts'][0]['tel'] != ''))
    {
        $omsu_contacts['adm']['phones']['Приёмная']=$material['contacts'][0]['tel'];
    }

    if(isset($material['contacts'][0]['fax']) && ($material['contacts'][0]['fax'] != ''))
    {
        $omsu_contacts['adm']['phones']['Факс'] = $material['contacts'][0]['fax'];
    }


    $schedf='';
    foreach($material['contacts'][0]['schedules'] as $schedule)
    {
        if($schedf != '')
        {
            $schedf.='<br>';
        }
        $schedf.=$schedule['name'].': '.$schedule['content'];
    }

    $omsu_contacts['adm']['sched']=$schedf;

    if ($head = GetRegionHeadInfo($site, $material['id'])) {

        $head = MaterialGet($site,$head['id']);

        $omsu_contacts['head']['title']=$head['name'];
        $omsu_contacts['head']['subtitle']=$head['anons'];

        if(isset($head['params'][8]['value']) && ($head['params'][8]['value'] != ''))
        {
            $omsu_contacts['head']['subtitle']=$head['params'][8]['value'];
        }

        if(isset($head['contacts'][0]['fulladdr']) && ($head['contacts'][0]['fulladdr'] != ''))
        {
            $omsu_contacts['head']['addr']=$head['contacts'][0]['fulladdr'];
        }

        if(isset($head['contacts'][0]['email']) && ($head['contacts'][0]['email'] != ''))
        {
            $omsu_contacts['head']['email']=$head['contacts'][0]['email'];
        }

        if(isset($head['contacts'][0]['tel']) && ($head['contacts'][0]['tel'] != ''))
        {
            $omsu_contacts['head']['phones']['Приёмная']=$head['contacts'][0]['tel'];
        }
    }

    if(count($omsu_contacts) > 0)
    {
        return $omsu_contacts;
    }

    return false;
}

//получение информации о главе района
function GetRegionHeadInfo($site, $regionid)
{
    //проверяем что передано - поселение или регион

    $type='';

    $query='SELECT "type_id" FROM "materials" WHERE "id" = '.$regionid;
    if ($res=$site->dbquery($query)) {
        if (count($res) > 0) {
            $type = $res[0]['type_id'];
        }
    }

    $mattypes = Array('209','162');

    if (in_array($type,$mattypes)) {
        $idomsu = $regionid;
    } else {
        //получаем id ОМСУ
        $query='SELECT MAX("m"."id") "id" FROM "materials" "m" WHERE "m"."status_id" = 2 AND "m"."type_id" = 162 AND "m"."id" IN
    (
    SELECT "material_child" "id" FROM "material_connections" WHERE "material_parent" = '.$regionid.'

    UNION ALL

    SELECT "material_parent" "id" FROM "material_connections" WHERE "material_child" = '.$regionid.'
    )';

        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                if($res[0]['id'] != '')
                {
                    $idomsu=$res[0]['id'];
                }
            }
        }
    }

    if (isset($idomsu)) {
        $omsumat = MaterialGet($site, $idomsu);

        //получаем id главы
        $query='SELECT MAX("m"."id") "id" FROM "materials" "m" WHERE "m"."status_id" = 2 AND "m"."type_id" = 119 AND "m"."id" IN
    (
    SELECT "material_child" "id" FROM "material_connections" WHERE "material_parent" = '.$idomsu.'

    UNION ALL

    SELECT "material_parent" "id" FROM "material_connections" WHERE "material_child" = '.$idomsu.'
    )';

        //echo($query);

        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                if($res[0]['id'] != '')
                {
                    $id=$res[0]['id'];
                }
            }
        }
    }

    if(isset($id)){
        $mat=MaterialGet($site, $id);

        $head=Array();
        $head['id']=$mat['id'];
        $head['name']=$mat['name'];
        if(isset($mat['photos'][0]))
        {
            $head['photo']='/photos/'.$mat['photos'][0]['id'].'_xy98x133.jpg';
        }
        else{
            $head['photo']='';
        }

        if(isset($mat['params'][8]['value'])){
            $head['post']=$mat['params'][8]['value'];
        }
        else{
            $head['post']='';
        }

        if(isset($mat['params'][29]['value'])){
            $head['setdate']=$mat['params'][29]['unixtime'];
        }
        else{
            $head['setdate']='';
        }

        $head['url']=$mat['url'];

        if($omsumat['type_id'] == '162')
        {
            //$head['url']=$omsumat['url'].'/Rukovoditel';
            $head['url']=$omsumat['url'].'/omsu-contacts';
        }
        elseif($omsumat['type_id'] == '209')
        {
            $head['url']=$omsumat['url'].'/Settlement-head';
        }


        return($head);}
    else{
        return false;
    }

}


function GetOneLevelMenu($site, $type, $material=NULL)
{
    $parents = GetTypeParents($site,$type);

    $arr = $site->data->material_types;

    $normarr=$arr;

    foreach($parents as $parent)
    {

        foreach($arr as $el)
        {
            if($el['id'] == $parent)
            {
                $normarr=$el['childs'];
            }
        }
        $arr=$normarr;
    }

    foreach($arr as $el)
    {
        if($el['id'] == $type)
        {

            $arr=$el['childs'];
        }
    }

    //генерируем новые ссылки
    if ($material != NULL) {
        $newarr=Array();

        $wrongtypes=Array('215');

        $cityes=Array('732','733');

        if(in_array($material,$cityes))
        {
            $wrongtypes[]='209';
        }

        foreach($arr as $key => $value)
        {
            if(!in_array($value['id'],$wrongtypes))
            {
                //дополнительная проверка на необходимость вывода
                if ($value['id'] == '165') {
                    //проверяем наличае вакансий у ОМСУ

                    $value['url']=GetMaterialTypeUrl($site, $value['id'], $material, $value['parent_id']);
                    $newarr[$key]=$value;
                } elseif ($value['id'] == '166') {
                    //проверяем наличае документов у ОМСУ

                    $value['url']=GetMaterialTypeUrl($site, $value['id'], $material, $value['parent_id']);

                    $newarr[$key]=$value;
                } elseif ($value['id'] == '163') {

                    $region=$material;

                    $value['url']=GetMaterialTypeUrl($site, $value['id'], $material, $value['parent_id']);

                    $newarr[$key]=$value;
                } else {
                    $value['url']=GetMaterialTypeUrl($site, $value['id'], $material, $value['parent_id']);
                    $newarr[$key]=$value;
                }
            }
        }
        $arr=$newarr;
    }

    return($arr);
}

//получаем подразделы текущего раздела
function GetTypeSubtypes($site, $type)
{
    $parents = GetTypeParents($site, $type);
    $parents = array_reverse($parents);
    $arr = $site->data->material_types;

    foreach ($parents as $parent) {
        foreach ($arr as $el) {
            if ($el['id'] == $parent) {
                $arr=$el['childs'];
            }
        }
    }

    foreach ($arr as $el) {
        if ($el['id'] == $type) {
            $arr = $el['childs'];
        }
    }

    return($arr);
}


//получение информацмм о деревне
function GetVillageInfo($site, $id)
{
    $id=(int)$id;
    if ($id > 0) {
        $village=MaterialGet($site, $id);

        $villageinfo=Array();
        $villageinfo['id']=$village['id'];
        $villageinfo['name']=$village['name'];

        $villageinfo['logo']='';
        if(isset($village['photos']))
        {
            if(count($village['photos']) > 0)
            {
                $villageinfo['logo']='/photos/'.$village['photos'][0]['id'].'_x290.jpg';
            }
        }

        if(isset($village['params'][32]))
        {
            $villageinfo['oktmo']=$village['params'][32]['value'];
        }
        else{
            $villageinfo['oktmo']='';
        }

        //контакты
        $villageinfo['contacts']=$village['contacts'];

        $villageinfo['url']=$village['url'];

        //получаем главу администрации
        $query='SELECT "m"."name",
         "m"."id", (SELECT MAX("g"."valuename") FROM "extra_materials" "g"
         WHERE "g"."material_id" = "m"."id" AND "type_id" = 8) "post"
         FROM "materials" "m"
            WHERE "m"."type_id" = 119 AND "m"."status_id" = 2 AND "m"."id" IN (

            SELECT "material_parent" "id" FROM "material_connections" WHERE "material_child" = '.$villageinfo['id'].'

            UNION ALL

            SELECT "material_child" "id" FROM "material_connections" WHERE "material_parent" = '.$villageinfo['id'].'

            ) ORDER BY "m"."id" DESC';

        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                $villageinfo['adm_head_id']=$res[0]['id'];
                $villageinfo['adm_head_name']=$res[0]['name'];
                $villageinfo['adm_head_post']=$res[0]['post'];
            }
        }

        return($villageinfo);

    }else{
        return false;
    }
}


//получение информации о районе
function GetRegionInfo($site, $id)
{
    $id = (int)$id;
    if($id > 0)
    {
        $region=MaterialGet($site, $id);

        $reginfo=Array();

        $reginfo['id']=$region['id'];
        $reginfo['name']=$region['name'];
        $reginfo['logo']='/templates/khabkrai/img/regions/'.$region['id'].'.png';


        //получаем показатели
        $reginfo['square']='';
        $reginfo['birth']='';
        $reginfo['population']='';
        $region['center']=Array();

        if(isset($region['params']['33']))
        {
            if(isset($region['params']['33']['value'])&&($region['params']['33']['value'] != ''))
            {
                $reginfo['center']['id']=$region['params']['33']['value'];
                $reginfo['center']['type_id']=$region['params']['33']['valuemattype'];
                $reginfo['center']['name']=$region['params']['33']['valuename'];
                $reginfo['center']['url']=GetMaterialTypeUrl($site, $region['params']['33']['valuemattype'], $region['params']['33']['value']).'/'.$region['params']['33']['value'];
            }
        }

        if(isset($region['params']['36']))
        {
            if(isset($region['params']['36']['value'])&&($region['params']['36']['value'] != ''))
            {
                $reginfo['square']=$region['params']['36']['value'];
            }
        }

        if(isset($region['params']['34']))
        {
            if(isset($region['params']['34']['value'])&&($region['params']['34']['value'] != ''))
            {
                $reginfo['birth']=$region['params']['34']['value'];
            }
        }

        if(isset($region['params']['35']))
        {
            if(isset($region['params']['35']['value'])&&($region['params']['35']['value'] != ''))
            {
                $reginfo['population']=$region['params']['35']['value'];
            }
        }

        //получаем кол-во мун. образований
        $reginfo['villagesnum']=0;
        $reginfo['villagesnum_city']=0;
        $reginfo['villagesnum_village']=0;

        //krumo($region);

        //получаем администрацию
        if(count($region['connected_materials']) > 0)
        {
            foreach($region['connected_materials'] as $cm)
            {
                if($cm['type_id'] == 209)
                {
                    //определяем тип поселения
                    $fullmat=MaterialGet($site, $cm['id']);

                    if(isset($fullmat['params']))
                    {
                        foreach ($fullmat['params'] as $param)
                        {
                            if($param['name'] == 'Тип поселения')
                            {
                                if($param['value'] == 'город')
                                {
                                    ++$reginfo['villagesnum_city'];
                                }
                            }
                        }
                    }

                    ++$reginfo['villagesnum'];
                }
            }
        }

        $reginfo['villagesnum_village']=$reginfo['villagesnum']-$reginfo['villagesnum_city'];

        //принудительно прописываем хабаровск - костыль!
        if($region['id'] == '23')
        {
            $region['connected_materials'][]=MaterialGet($site, 732);
        }

        //получаем администрацию
        if(count($region['connected_materials']) > 0)
        {
            foreach($region['connected_materials'] as $cm)
            {
                if($cm['type_id'] == 162)
                {
                    $reginfo['adm_id']=$cm['id'];
                    $reginfo['adm_name']=$cm['name'];
                    $reginfo['adm_url']=$cm['url'];
                }
            }
        }



        //получаем главу администрации
        if(isset($reginfo['adm_id']))
        {
            $query='SELECT "name", "id" FROM "materials"
            WHERE "type_id" = 119 AND "status_id" = 2 AND "id" IN (

            SELECT "material_parent" "id" FROM "material_connections" WHERE "material_child" = '.$reginfo['adm_id'].'

            UNION ALL

            SELECT "material_child" "id" FROM "material_connections" WHERE "material_parent" = '.$reginfo['adm_id'].'

            ) ORDER BY "id" DESC';

            if($res=$site->dbquery($query))
            {
                if(count($res) > 0)
                {
                    $reginfo['adm_head_id']=$res[0]['id'];
                    $reginfo['adm_head_name']=$res[0]['name'];
                    $reginfo['adm_head_url']=GetMaterialTypeUrl($site, 119, $res[0]['id']).'/'.$res[0]['id'];
                }
            }
        }

        //krumo($reginfo);

        return($reginfo);

        //krumo($region);

    }
    else{
        return false;
    }
}

//получаем название материала по его id
function GetMaterialNameById($site, $id)
{
    if($id == '')
    {
        return(false);
    }
    else{
        $id=(int)$id;
        $query='SELECT "name" FROM "materials" WHERE "id" = '.$id;
        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                return($res[0]['name']);
            }
            else{
                return(false);
            }
        }
        else{
            return(false);
        }
    }
}

//получаем название типа материала по его id
function GetMattypeNameById($site, $id)
{
    if($id == '')
    {
        return(false);
    }
    else{
        $id=(int)$id;
        $query='SELECT "name" FROM "material_types" WHERE "id" = '.$id;
        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                return($res[0]['name']);
            }
            else{
                return(false);
            }
        }
        else{
            return(false);
        }
    }
}

//формируем массивы типов материалов
function GepTypeParentTypes(\iSite $site, $id = null)
{
    $tarr = Array();

    $q_params = array();
    if (empty($id)) {
        $query = <<<EOS
SELECT
    "m".*,
    (
        SELECT "t"."name"
        FROM "material_types" "t"
        WHERE "t"."id" = "m"."parent_id"
    ) "parent_name"
FROM "material_types" "m"
WHERE
    "m"."parent_id" IS NULL
    OR "m"."parent_id" = 0
ORDER BY
    "m"."ordernum" ASC NULLS LAST,
    "m"."name" ASC
EOS;
    } else {
        $query = <<<EOS
SELECT
    "m".*,
    (
        SELECT "t"."name"
        FROM "material_types" "t"
        WHERE "t"."id" = "m"."parent_id"
    ) "parent_name"
FROM "material_types" "m"
WHERE
    "m"."parent_id" = $1
ORDER BY
    "m"."ordernum" ASC NULLS LAST,
    "m"."name" ASC
EOS;
        $q_params[] = $id;
    }

    $res = $site->dbquery($query, $q_params);

    if ( ! empty($res)) {
        foreach ($res as $i => $row) {
            $url = GetMaterialTypeUrl($site, $row['id']);

            $tarr[$i] = $row;

            if ($site->data->language != LANG_RU) {
                $lang_name = $site->data->languagename.'_name';
                $tarr[$i]['name'] = $row[$lang_name];
            }

            $tarr[$i]['url'] = $url;
            $tarr[$i]['childs'] = GepTypeParentTypes($site, $row['id']);
        }
    }

    return $tarr;
}

function GetTypeChilds(\iSite $site, $id)
{
    $types=Array();

    $query = 'SELECT "name", "id" FROM "material_types" WHERE "parent_id" = '.$id;
    if ($res=$site->dbquery($query)) {
        if(count($res) > 0) {
            $types = $res;
        }
    }

    return($types);
}

function materialTypeHasChildren(\iSite $site, $type_id)
{
    $query = 'SELECT COUNT(*) num FROM material_types WHERE parent_id = $1';
    $q_params = array($type_id);
    $res = $site->dbquery($query, $q_params);

    if (empty($res)) {
        throw new \wpf\exception\db\QueryFailed($query, $q_params);
    }

    return $res[0]['num'] > 0;
}

/**
 * @param iSite $site
 * @param $type_id
 * @return array
 */
function GetTypeTree(\iSite $site, $type_id)
{
    $query = <<<EOS
WITH RECURSIVE cur(id) AS (
    SELECT id FROM material_types WHERE parent_id = $1
    UNION
    SELECT mt.id FROM material_types mt INNER JOIN cur ON cur.id = mt.parent_id
) SELECT id FROM cur
EOS;

    $q_params = array($type_id);
    $res = $site->dbquery($query, $q_params, true);
    $ret = array($type_id);

    if ( ! empty($res)) {
        foreach ($res as $item)
            $ret[] = $item['id'];
    }

    return $ret;
}

function GetTypeParents($site,$id)
{

    $arr = Array();
    
    $query = 'SELECT "parent_id" FROM "material_types" WHERE "id" = $1 AND "id" != "parent_id"';

    $res = $site->dbquery($query, array($id));

    if ( ! empty($res)) {

        $parent_id = $res[0]['parent_id'];
        //if ( ! empty($parent_id) && $parent_id!== '25') {
        if ( ! empty($parent_id) ) {

            $arr = GetTypeParents($site,$parent_id);
            $arr[] = $parent_id;
        }
    }

    return($arr);
}

//функция сохранения html куска в кэш
function HTMLCacheWrite(\iSite $site, $html, $name, $seconds = null)
{
    $site->getCache()->set('html:'.$name, $html, $seconds);
    return true;
}

//функция чтения html куска из кэша
function HTMLCacheRead(\iSite $site, $name)
{
    $key = 'html:'.$name;
    $cache = $site->getCache();

    if ( ! $cache->has($key))
        return false;

    return $cache->get($key);
}

//функция сохранения массива в кэш
function ArrCacheWrite(\iSite $site, $arr, $name)
{
    if (empty($name))
        return false;

    $site->getCache()->set('arr:'.$name, serialize($arr));
    return true;
}

//функция чтения массива из кэша
function ArrCacheRead(\iSite $site, $name)
{
    $cache = $site->getCache();
    $key = 'arr:'.$name;

    if ( ! $cache->has($key))
        return false;
    $arr = unserialize($cache->get($key));
    return $arr;
}


include_once('materials_functions/parse_url.php');

//функция сохранения логов
function SaveLog(\iSite $site, $logactionname, $logobjid)
{
    $log='error';
    list($x1,$x2)=explode('.',strrev($_SERVER['HTTP_HOST']));
    $xdomain=$x1.'.'.$x2;

    if($x1 == (int)$x1){
        $xdomain=$_SERVER['HTTP_HOST'];
    }
    else{
        $xdomain=strrev($xdomain);
    }

    $logsite=$site->settings->DB->login;
    $logusername=$site->autorize->username;
    $loguserlogin=$site->autorize->email;

    //die($loguserlogin);

    $loguserid=$site->autorize->userid;

    if(isset($site->autorize->omsu) && $site->autorize->omsu !='')
    {
        $omsu=MaterialGet($site,$site->autorize->omsu);
        $logusername.=' ('.$omsu['name'].')';
        $loguserid.='/'.$omsu['id'];
    }


    $logkey=md5('wpf'.$logsite.'save'.$logusername.$logobjid.'wpf');
    // $url='http://'.$xdomain.'?menu=logger&site='.$logsite.'&username='.urlencode($logusername).'&userlogin='.urlencode($loguserlogin).'&userid='.urlencode($loguserid).'&actionname='.urlencode($logactionname).'&objid='.urlencode($logobjid).'&mode=save&key='.$logkey;
    $logstr = 'site='.$logsite.'&username='.urlencode($logusername).'&userlogin='.urlencode($loguserlogin).'&userid='.urlencode($loguserid).'&actionname='.urlencode($logactionname).'&objid='.urlencode($logobjid).'&mode=save&key='.$logkey;
    $url='http://127.0.0.1/?menu=logger&'.$logstr;

    // $url=urlencode($url);

    // @todo разобраться с функционалом, переработать
    // $log = file_get_contents($url);

    return($log);
}

//функция выводит типы как опшены в селекте
function PrintTypeOptions($types, $atype, $level=0, $ignoreMatTypeId = null)
{
    $rows='';

    foreach($types as $mattype)
    {

        if( $mattype['id'] == $ignoreMatTypeId ){
            continue;
        }

        //считаем кол-во отступов
        $otst='';
        if($level > 0)
        {
            for($i=0; $i<$level; ++$i)
            {
                $otst.='&nbsp;&nbsp;&nbsp;';
            }
        }

        if($atype == $mattype['id'])
        {
            $selected='SELECTED';
        }
        else
        {
            $selected='';
        }


        if(mb_strlen($mattype['name'],"utf8") > 75)
        {
            $mattype['name']=mb_substr($mattype['name'],0,75,"utf8").'...';
            $mattype['name']=str_replace('�','',$mattype['name']);

        }


        $rows.="<option value='$mattype[id]' $selected>$otst$mattype[name]</option>";

        //проверяем на наличие подтипов
        if(is_array($mattype['childs']))
        {
            $rows.=PrintTypeOptions($mattype['childs'], $atype, ($level+1), $ignoreMatTypeId);
        }



    }

    return($rows);

}

//опции выбора городов
function GetCityOptions($site, $acity='', $citymattype=12, $parent_id='NULL', $level='')
{
    $cityes='';

    if($parent_id != 'NULL')
    {
        $parent_id = 'AND "parent_id" = '.$parent_id;
    } else {
        $parent_id = 'AND ("parent_id" IS NULL OR "parent_id" = '."''".')';
    }

    $query = 'SELECT * FROM "materials"
        WHERE "type_id" = '.$citymattype.
            ' AND status_id = '.STATUS_ACTIVE.' '.
            $parent_id.' '.
        'ORDER BY "name" ASC';


    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            foreach($res as $row)
            {
                if($row['id'] == $acity)
                {
                    $selected='SELECTED';
                }
                else
                {
                    $selected='';
                }

                $cityes.="<option value=\"$row[id]\" $selected>$level $row[name]</option>";

                $cityes.=GetCityOptions($site,$acity,$citymattype,$row['id'],$level.'-');
            }
        }
    }

    return($cityes);
}

function GetCities($site, $parent_city = null)
{
    $ret = array();
    $query = 'SELECT "id", "name" FROM "materials" WHERE "type_id" = $1 AND "status_id" != $2 AND "language_id" = $3';
    $params = array(MATTYPE_DISTRICT, STATUS_DELETED, $site->data->language);
    if (is_null($parent_city))
        $query .= ' AND "parent_id" IS NULL';
    else {
        $params[] = $parent_city;
        $query .= ' AND "parent_id" = $'.count($params);
    }
    $res = $site->dbquery($query, $params);
    if (!empty($res)) {
        foreach ($res as $item)
            $ret[$item['id']] = $item['name'];
    }
    return $ret;
}

//получаем список id материалов, у которых в параметрах указан переданный модуль
function GetParamMaterialsIds($site, $mat)
{
    $materials=Array();

    $query='SELECT "e"."material_id", "m"."type_id" FROM "extra_materials" "e", "materials" "m"
    WHERE "m"."id" = "e"."material_id" AND "m"."status_id" = 2 AND "e"."valuemat" = '.$mat;
    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            $materials=$res;
        }
    }

    return($materials);
}

require_once('materials_functions/material_list.php');

//получаем первое фото материала
function MaterialGetFirstPhoto($site, $id)
{
    $photo = false;

    $query = 'SELECT
            MIN("i"."file_id") "photo"
        FROM "file_connections" "i",
            "files" "f"
        WHERE "f"."id" = "i"."file_id"
            AND "f"."type_id" = $2
            AND "i"."material_id" = $1
            AND "f"."ordernum" = (
                SELECT MIN("fo"."ordernum")
                FROM "file_connections" "io",
                    "files" "fo"
                WHERE "fo"."id" = "io"."file_id"
                    AND "fo"."type_id" = $2 AND "io"."material_id" = $1
            )';

    if ($res = $site->dbquery($query, array($id, FILETYPE_IMAGE))) {
        if(count($res) > 0) {
            $photo = $res[0]['photo'];
        }
    }

    return $photo;
}

//получаем первое видео материала
function MaterialGetFirstVideo($site, $id)
{
    $video = false;
    return $video;
}

//подтипы
function GetSubTypes($site, $type, $arr)
{
    $ret = GetTypeTree($site, $type);
    array_shift($ret);
    return $ret;
}

//рекурсивная функция получения всех типов материала URL
function GetMaterialTypeUrlRec($site, $type)
{
    $url='';

    $query="SELECT * FROM material_types WHERE id = $type";
    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            foreach($res as $row)
            {
                //$url='/'.$row['id_char'].$url;

                if($row['showinmenu'] == 1)
                {
                    $url='/'.$row['id_char'].$url;
                }
                else
                {

                }

                if(($row['parent_id'] != '')&&($row['parent_id'] != '0'))
                {
                    $url=GetMaterialTypeUrlRec($site, $row['parent_id']).$url;
                }

            }
        }
    }

    return($url);
}

/**
 * @param iSite $site
 * @param int $id
 * @return bool|array
 */
function GetMaterialType($site, $id)
{
    $query = 'SELECT * FROM material_types WHERE "id" = $1';
    $rows = $site->dbquery($query, array($id));
    if (empty($rows))
        return false;
    return $rows[0];
}

//рекурсивная функция - получение id всех дочерних материалов
function GetChildMaterials($site,$id,$first=true)
{
    $out='';

    $query='SELECT "id" FROM "materials" WHERE "parent_id" = '.$id;
    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            foreach($res as $row)
            {
                if(($out != '')||(!$first))
                {
                    $out.=',';
                }
                $out.=$row['id'];
                $first=false;
                $out.=GetChildMaterials($site,$row['id'],$first);

            }
        }
    }

    return($out);
}

//формитруем полный url типов материалов
// @todo отследить использование, упростить
function GetMaterialTypeUrl($site, $type, $matid=NULL, $mattype=NULL)
{
    $url = '';

    if (!empty($type)) {
        $query = <<<EOS
SELECT * FROM (
    WITH RECURSIVE temp1(id, id_char, parent_id, mat_as_type, PATH, LEVEL) as (
        SELECT
            t1.id,
            t1.id_char,
            t1.parent_id,
            t1.mat_as_type,
            CAST(t1.id as VARCHAR(11)) as PATH,
            1
            FROM material_types t1
            WHERE t1.id = $1
    
        UNION
    
        SELECT
            t2.id,
            t2.id_char,
            t2.parent_id,
            t2.mat_as_type,
            CAST ( temp1.PATH || '->' || t2.id AS VARCHAR(11)),
            LEVEL + 1
        FROM material_types t2
        INNER JOIN temp1 ON temp1.parent_id = t2.id
    )
    SELECT * FROM temp1 ORDER BY PATH
) pt
EOS;
        $q_params = array($type);

        $res = $site->dbquery($query, $q_params);

        if ( ! empty($res)) {
            foreach($res as $row) {
                $urlid = '';

                //если материал может быть подтипом
                if ($row['mat_as_type'] == 1) {
                    if (!empty($matid)) {
                        //массив родителей
                        $parents = GetTypeParents($site, $row['id']);
                        if ( ! in_array($mattype, $parents)) {

                            //определяем тип материала - если совпадает, то просто пишем материал
                            if ($mattype == $row['id'])
                            {
                                $urlid = $matid;
                            } else {
                                //ищим материал, связанный с переданным текущего типа - проверяем связки и параметры
                                $query = <<<EOS
SELECT min("m"."id") "id"
FROM "materials" "m"
WHERE
"m"."status_id" = $2
AND "m"."type_id" = $1
AND "m"."id" IN (
    SELECT "material_parent" "id" FROM "material_connections" WHERE "material_child" = $3

    UNION ALL

    SELECT "material_child" "id" FROM "material_connections" WHERE "material_parent" = $3

    UNION ALL

    SELECT "valuemat" "id" FROM "extra_materials" WHERE "material_id" = $3
)
EOS;

                                $q_params = array($row['id'], STATUS_ACTIVE, $matid);

                                $res = $site->dbquery($query, $q_params);
                                if (!empty($res)) {
                                    if ($res[0]['id'] != '') {

                                        $url='/'.$res[0]['id'].$url;
                                    }
                                }
                            }
                        }
                    }
                }

                if (!empty($urlid)) {
                    $url = '/'.$row['id_char'].'/'.$urlid.$url;
                } else {
                    $url = '/'.$row['id_char'].$url;
                }
            }
        }
    }

    return $url;
}

require_once('materials_functions/material_get.php');
//получаем материал

function MaterialGetFiles(\iSite $site, $id, $ordertype=1)
{
	return MaterialFilesGetAll($site, $id, $ordertype);
}

function MaterialFilesGetAll(\iSite $site, $id, $ordertype=1)
{


	switch ($ordertype)
	{
		case '2':
			$ordername='"f"."ordernum" DESC';
			break;
		case '3':
			$ordername='"f"."name" ASC';
			break;
		default:
			$ordername='"f"."ordernum" ASC';
			break;
	}



    $ret = array();
    //файлы
    $query = 'SELECT
            "f"."id",
            "c"."material_id",
            "f"."name",
            "f"."name_en",
            "f"."type_id",
            "f"."content",
            "f"."author",
            TO_CHAR("f"."date_event", '."'DD.MM.YYYY HH24:MI'".') "date_event",
            "t"."name" "typename",
            "f"."ordernum",
            "f"."extension" "extension",
            "f"."size" "size",
            (SELECT max("p1"."name")
                FROM "file_pars" "p1", "file_pars_connections" "p2"
                WHERE "p2"."material_id" = $1
                    AND "p1"."id" = "p2"."par_id"
                    AND "p2"."file_id" = "f"."id"
            ) "par_name",
            (SELECT max("p11"."orderby")
                FROM "file_pars" "p11",
                    "file_pars_connections" "p21"
                WHERE "p21"."material_id" = $1
                    AND "p11"."id" = "p21"."par_id"
                    AND "p21"."file_id" = "f"."id"
            ) "par_order",
            (SELECT max("p3"."par_id")
                FROM "file_pars_connections" "p3"
                WHERE "p3"."material_id" = $1
                    AND "p3"."file_id" = "f"."id"
            ) "par_id"
        FROM
            "files" "f",
            "file_types" "t",
            "file_connections" "c"
        WHERE
            "f"."id" = "c"."file_id"
            AND "c"."material_id" = $1
            AND "f"."type_id" = "t"."id"
        ORDER BY "f"."type_id" ASC,
            "par_order" ASC NULLS LAST,
            "par_name" ASC,
            '.$ordername.',
            "f"."id" ASC';


    $query = str_replace(' "extension"',' "fextension"', $query);

    if ($res=$site->dbquery($query, array($id))) {

        foreach($res as $row)
        {
            if(isset($row['fextension']))
            {
                $row['extension']=$row['fextension'];
            }

            $filename_sv = $site->settings->path . '/photos/' . $row['id'] .'_sv';

            if(file_exists($filename_sv))
            {
                $row['cecutient'] = true;
            }
            else
            {
                $row['cecutient'] = false;
            }

            switch($row['type_id'])
            {
                case '2':
                    //видео с ютуба
                    if($row['content'] != '')
                    {
                        $vurl = $row['content'];
                        $you_url = parse_url($vurl);

                        if (isset($you_url['host'])
                            && $you_url['host'] == 'youtu.be') {
                            $vid = trim($you_url['path'], '/');
                        } else {
                            $my_array_of_vars = array();
                            if (isset($you_url['query']))
                                parse_str($you_url['query'], $my_array_of_vars);
                            $vid = isset($my_array_of_vars['v']) ?
                                $my_array_of_vars['v'] :
                                '';
                        }

                        if(($vid == '')||($vid == 'NULL')||($vid == null))
                        {
                            $vid = preg_replace('/https?:\/\/www\.youtube\.com\/watch\?v=/','',$vurl);
                            $vid = preg_replace('/https?:\/\/www\.youtube\.com\/watch\?v=/','',$vid);
                            $vid = preg_replace('/https?:\/\/www\.youtube\.com\/embed\//','',$vid);
                            $vid = preg_replace('/https?:\/\/youtu\.be\//', '', $vid);
                        }

                        $row['content']='https://www.youtube.com/embed/'.$vid;
                        $row['youtubeid']=$vid;

                        //thumb
                        $row['thumb']='https://img.youtube.com/vi/' . $vid . '/mqdefault.jpg';

                        //загруженное видива
                    }else{

                        $row['youtubeid']=false;
                        $row['content']='/photos/'.$row['id'];
                        $row['thumb']='/photos/'.$row['id'].'_thumb.png';
                    }

                    $ret['videos'][]=$row;
                    break;
                case '1':
                    $ret['photos'][]=$row;
                    break;
                case '3':
                    $ret['audios'][]=$row;
                    break;
                case '5':
                    $ret['translations'][]=$row;
                    break;
                default:
                    $ret['documents'][]=$row;
                    break;
            }

            $ret['allfiles'][]=$row;
        }
    }
    return $ret;
}

/**
 * @param iSite $site
 * @param int $id
 * @return array
 * @deprecated
 * @use MaterialGetFiles
 */
function GetMaterialFiles($site, $id)
{
    return MaterialGetFiles($site, $id);
}

function FileGetInfo(\iSite $site, $file_id)
{
    return FileInfoGet($site, $file_id);
}

function FileInfoGet(\iSite $site, $file_id)
{
    if (empty($site))
        return null;

    $query = 'SELECT * FROM files WHERE id = $1';
    $res = $site->dbquery($query, array($file_id));
    return empty($res) ? null : $res[0];
}

/**
 * @param iSite $site
 * @param int $id
 * @return array
 */
function MaterialGetContacts($site, $id)
{
    $contacts = array();
    $query='SELECT "c".*,
        (SELECT "m"."name" FROM "materials" "m" WHERE "m"."id" = "c"."city") "cityname"
        FROM "contacts" "c"
        WHERE "c"."material_id" = $1 ORDER BY "c"."addr" ASC, "c"."id" ASC';
    if($res=$site->dbquery($query, array($id)))
    {
        if(count($res) > 0)
        {
            //связанные расписания
            $i=-1;
            foreach($res as $row)
            {
                ++$i;

                $full_address = trim($res[$i]['addr']);

                if(isset($res[$i]['building']) && $res[$i]['building'] != '') {
                    $full_address .= ', д. '.trim($res[$i]['building']);
                }

                if(isset($res[$i]['office']) && $res[$i]['office'] != '') {
                    $full_address .= ', каб. '.trim($res[$i]['office']);
                }

                $res[$i]['fulladdr'] = $full_address;

                $res[$i]['schedules']=Array();

                $querys='SELECT * FROM "schedules" WHERE "contact_id" = $1 ORDER BY "id" ASC';

                if($ress=$site->dbquery($querys, array($row['id'])))
                {
                    if(count($ress) > 0)
                    {
                        $res[$i]['schedules']=$ress;
                    }
                }
            }

            $contacts = $res;
        }
    }
    return $contacts;
}

/**
 * @param iSite $site
 * @param int $id
 * @return array(<connected_materials>, <hidden_conn_materials>)
 */
function MaterialGetConnectedMaterials($site, $id)
{
    $connected_active = array();
    $connected_hidden = array();

    //получаем список id связанных материалов
    $query = <<<EOS
SELECT "material_child" "cid", "ordernumparent" "ordernum" FROM "material_connections"
WHERE "material_parent" = $1
UNION ALL
SELECT "material_parent" "cid", "ordernumchild" "ordernum" FROM "material_connections"
WHERE "material_child" = $1
EOS;

    $conmatordernums = Array();

    $res = $site->dbquery($query, array($id));
    if (!empty($res)) {
        $ids = array();
        foreach ($res as $row) {
            $conmatordernums[$row['cid']] = $row['ordernum'];
            $ids[] = $row['cid'];
        }

        $site->values->orderby = 'conmat';
        $site->values->ordersort = 'asc';
        $ids = implode(',', $ids);

        $conn_materials = MaterialList($site, '', 1, 1000, '', $ids);

        if (!empty($conn_materials)) {
            $jcm=-1;
            $jcmHid = -1;
            foreach($conn_materials as $mat)
            {
                if ((isset($conmatordernums[$mat['id']])) && ($conmatordernums[$mat['id']] !== ''))
                {
                    $mat['ordernum'] = $conmatordernums[$mat['id']];
                }
                else
                {
                    $mat['ordernum'] = $mat['extra_number2'];
                }
                if ($mat['status_id'] == STATUS_ACTIVE) {
                    $connected_active[++$jcm] = $mat;
                } else {
                    $connected_hidden[++$jcmHid] = $mat;
                }
            }

            uasort($connected_active, 'myCmp');
            $connected_active = array_reverse($connected_active);

            uasort($connected_hidden, 'myCmp');
            $connected_hidden = array_reverse($connected_hidden);
        }
    }

    return array($connected_active, $connected_hidden);
}

function MaterialGetParams(\iSite $site, $type_id, $mat_id)
{
    $ret = array();

    $query = 'SELECT "t".* FROM "extra_mattypes" "t" WHERE "type_id" = $1';
    $res = $site->dbquery($query, array($type_id));
    if ( ! empty($res)) {
        foreach($res as $row) {
            $ret[$row['id']] = $row;

            //значения

            switch($row['valuetype'])
            {
                case 'valuedate':

                    $query = <<<EOS
SELECT
    "{$row['valuetype']}" "value",
    UNIX_TIMESTAMP("valuedate") "unixtime"
FROM "extra_materials"
WHERE
    "material_id" = $1
    AND "type_id" = $2
    AND ("selector_id" IS NULL OR "selector_id" = 0)
EOS;

                    $params = array($mat_id, $row['id']);
                    break;

                case 'valuemat':
                    $query = <<<EOS
SELECT
    "m"."id" "value",
    "m"."name" "valuename"
FROM
    "materials" "m",
    "extra_materials" "e"
WHERE
    "m"."id" = "e"."valuemat"
    AND "e"."material_id" = $1
    AND "e"."type_id" = $2
    AND ("e"."selector_id" IS NULL OR "e"."selector_id" = 0)
    AND "m"."status_id" = $3 
EOS;

                    $params = array($mat_id, $row['id'], STATUS_ACTIVE);
                    break;

                default:

                    $query = <<<EOS
SELECT
    "{$row['valuetype']}" "value"
FROM "extra_materials"
WHERE
    "material_id" = $1
    AND "type_id" = $2
    AND ("selector_id" IS NULL OR "selector_id" = 0)
EOS;

                    $params = array($mat_id, $row['id']);
                    break;
            }

            $eres = $site->dbquery($query, $params);
            if ( ! empty($eres)) {
                foreach ($eres[0] as $attr => $val) {
                    $ret[$row['id']][$attr] = $val;
                }
            }
        }
    }

    return $ret;
}

function MaterialGetGroupParams($site, $type_id, $id)
{
    $ret = array();

    $query='SELECT "t".*, "s"."id" "sid", "m"."name" "groupname", "m"."id" "groupid"
    FROM "extra_mattypes" "t", "extra_selectors" "s", "materials" "m"
    WHERE "t"."type_id" = "s"."mattype_id"
    AND "s"."type_id" = $1 AND "m"."type_id" = "s"."mattype_id"
    ORDER BY "m"."name" ASC, "t"."id" ASC';


    if($res=$site->dbquery($query, array($type_id)))
    {
        if(count($res) > 0)
        {
            foreach($res as $row)
            {
                if (!isset($ret[$row['groupid']]))
                    $ret[$row['groupid']] = array();
                $ret[$row['groupid']][$row['id']]=$row;

                //значения
                $params = array($id, $row['id'], $row['groupid']);
                switch($row['valuetype'])
                {
                    case 'valuedate':

                        $query='SELECT "'.$row['valuetype'].'" "value", (UNIX_TIMESTAMP("valuedate")) "unixtime"
                        FROM "extra_materials"
                        WHERE "material_id" = $1 AND "type_id" = $2 AND "selector_id" = $3';

                        break;

                    case 'valuemat':

                        $query='SELECT "m"."name" "valuename", "m"."id" "value"
                        FROM "materials" "m", "extra_materials" "e"
                        WHERE "m"."id" = "e"."valuemat" AND "e"."material_id" = $1 AND "e"."type_id" = $2
                        AND "e"."selector_id" = $3';

                        break;

                    default:

                        $query='SELECT "'.$row['valuetype'].'" "value" FROM "extra_materials"
                        WHERE "material_id" = $1 AND "type_id" = $2 AND "selector_id" = $3';

                        break;
                }

                if($eres=$site->dbquery($query, $params))
                {
                    if(count($eres) > 0)
                    {
                        $ret[$row['groupid']][$row['id']]['value']=$eres[0]['value'];
                        if(isset($eres[0]['valuename']))
                        {
                            $ret[$row['groupid']][$row['id']]['valuename']=$eres[0]['valuename'];
                        }
                        if(isset($eres[0]['unixtime']))
                        {
                            $ret[$row['groupid']][$row['id']]['unixtime']=$eres[0]['unixtime'];
                        }
                    }
                }
            }
        }
    }

    return $ret;
}

//строим полную ссылку на тип материала
function createTypeFullLink(\iSite $site, $type_id)
{
    return GetMattypeUrl($site, $type_id);
}

function GetMattypeUrl(\iSite $site, $type_id)
{
    $query = <<<EOS
WITH RECURSIVE rec(url_suffix,parent_id) AS(
    SELECT id_char, parent_id
    FROM material_types
    WHERE id = $1

    UNION

    SELECT
        CONCAT(t.id_char, '/', rec.url_suffix) url_suffix,
        t.parent_id
    FROM
        material_types t 
        INNER JOIN rec ON rec.parent_id = t.id
)
SELECT CONCAT('/', rec.url_suffix) url
WHERE rec.parent_id IS NULL OR rec.parent_id = 0
EOS;

    $q_params = array($type_id);

    $res = $site->dbquery($query, $q_params, true);

    if ( ! empty($res))
        return $res[0]['url'];

    return false;
}

include('materials_functions/create_material.php');

//функция меняет сортировку в таблице связки материалов
function ChangeOrdernumToConMat(\iSite $site, $parentid, $childid, $ordernum)
{
    $ordernum = (int)$ordernum;

    //обрабатываем связку родитель - потомок
    $parent_affected = DbUpdate($site,
        'material_connections',
        array(
            'ordernumparent' => $ordernum
        ), array(
            'material_parent' => $parentid,
            'material_child' => $childid,
        ));

    /*
    $query='UPDATE "material_connections" SET "ordernumparent" = '.$ordernum.'
    WHERE "material_parent" = '.$parentid.' AND "material_child" = '.$childid;
    $site->dbquery($query);
    */

    //обрабатываем связку потомок - ребенок
    $child_affected = DbUpdate($site,
        'material_connections',
        array(
            'ordernumchild' => $ordernum,
        ), array(
            'material_parent' => $childid,
            'material_child' => $parentid,
        ));

    /*
    $query='UPDATE "material_connections" SET "ordernumchild" = '.$ordernum.'
    WHERE "material_parent" = '.$childid.' AND "material_child" = '.$parentid;
    $site->dbquery($query);
    */

    if ($parent_affected || $child_affected) {
        $site->callHook('materialconnectionreorder', array(
            'parent_id' => $parentid,
            'child_id' => $childid,
            'ordernum' => $ordernum,
        ));
    }
}

//функция связки материала
function DoConnectMaterial(\iSite $site)
{
    $err=false;
    $errtext='';

    if(($site->values->matforconnect != '')&&($site->values->id != ''))
    {
        //пробуем связать материалы
        $query='INSERT INTO "material_connections" ("material_parent", "material_child", "ordernumparent", "ordernumchild")
        VALUES('.$site->values->id.','.$site->values->matforconnect.', 0, 0)';

        if($res=$site->dbquery($query))
        {
            $site->callHook('materialconnect', array(
                'parent_id' => $site->values->id,
                'child_id' => $site->values->matforconnect,
            ));
        }
        else
        {
            $err=true;
            $errtext=' - ошибка: не получилось записать в БД';
        }

    }
    else
    {
        $err=true;
        $errtext=' - ошибка: не передан материал для связки';
    }

    return Array(!$err,$errtext);
}
