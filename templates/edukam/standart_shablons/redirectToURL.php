<?php
/**
 * @var \iSite $this
 */

defined('_WPF_') or die();

$url = '/';

if ( ! empty($material)) {

    if(isset($material['params']) && count($material['params']) > 0)
    {
        foreach ($material['params'] as $param)
        {
            if($param['name'] == 'url')
            {
                $url=$param['value'];
            }
        }
    }
}


$this->redirect($url);