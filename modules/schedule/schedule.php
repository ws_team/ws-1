<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$this->getPostValues(array('key', 'task'));

if (empty($this->values->key) || $this->values->key != $this->settings->schedule->api_key) {
    // $this->setResponseCode('403 Forbidden');
    exit;
}

$this->logWrite(LOG_DEBUG, basename(__FILE__), __LINE__);

if ( ! empty($this->values->task)) {
    $task = $this->values->task;
    $taskScript = dirname(__FILE__).'/task/'.$task.'.php';
    if (file_exists($taskScript)) {
        $this->logWrite(LOG_DEBUG, basename(__FILE__).' '.__LINE__, 'Executing task '.$task);
        include($taskScript);
    }
}
