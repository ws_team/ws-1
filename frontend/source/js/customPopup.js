$(document).ready(function() {

    /*
    *
    * при добавлении нового модального окна нужно присвоить ему id, а кнопке-тригеру дать data-role="popupTrigger"
    * и data-target="popup_{id модального окна}"
    *
    * */

    var $popup              = $('[data-role="popup"]'),
        $popupContent       = $('[data-role="popupContent"]'),
        $popupTrigger       = $('[data-role="popupTrigger"]'),
        $popupCloseButton   = $('[data-role="popupCloseBtn"]');

    $popupTrigger.on('click', function(e){

        e.preventDefault();

        var popupDataSelector = $(this).attr('data-target').replace('popup_', '');

        $('#' + popupDataSelector + '').fadeIn(200);
    });


    $(document).on('click', function (e) {

        if (
            !$popupContent.is(e.target) && $popupContent.has(e.target).length === 0 &&
            !$popupTrigger.is(e.target) && $popupTrigger.has(e.target).length === 0
        ) {
            $popup.fadeOut(200);
        }

    });

    $popupCloseButton.on('click', function(e) {

        e.preventDefault();
        $popup.fadeOut(200);
    });
});
