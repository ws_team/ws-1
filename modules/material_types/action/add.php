<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$lang_names = array();

if ( ! empty($this->data->languages)) {
    $languages = $this->data->languages;
} else {
    $languages = array('en');
}

$in_attrs = array('parent_id', 'name', 'id_char');
$lang_names = array();

foreach ($languages as $lang) {
    $lang_name = $lang.'_name';
    $lang_names[] = $lang_name;
    $in_attrs[] = $lang_name;
}



$this->getPostValues($in_attrs);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (empty($this->values->parent_id)) {
        $this->values->parent_name = '';
    }

    $en_name = array();
    foreach ($lang_names as $lang_name) {
        $en_name[$lang_name] = $this->values->$lang_name;
    }


    //krumo($this);

    //пробуем создать материал
    list($mat_id, $message) = createMaterialType(
        $this,
        $this->values->name,
        $en_name,
        NULL,
        $this->values->id_char,
        $this->values->parent_id
    );

    //echo $mat_id.'-'.$message.'';

    //die();

    if ($mat_id) {
        $this->setFlash('status', 'success');
        $this->setFlash('message', $message);
        $this->redirect(array(
            'material_types',
            'imenu' => 'edit',//$this->values->imenu,
            'action' => 'edit',
            'id' => $mat_id,
        ));
    } else {
        $status = 'error';

        //die('error');

        $this->values->imenu = 'add';
    }
}
