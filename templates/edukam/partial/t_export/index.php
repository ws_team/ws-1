<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

switch ($this->values->item) {
    case 'survey':
        include($this->partial('index_survey'));
        break;
    default:
        break;
}
