<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$this->GetPostValues(Array('iid', 'name', 'valuetype', 'valuemattype'));

if (empty($this->values->name) || empty($this->values->valuetype) || empty($this->values->iid)) {
    $status = 'warning';
    $message = array();
    if (empty($this->values->name))
        $message[] = ' - не задано наименование доп. характеристики';
    if (empty($this->values->valuetype))
        $message[] = ' - не задан тип доп. характеристики';
    if (empty($this->values->iid))
        $message[] = ' - не выбрана доп. характеристика';
    
    $message = implode("\n", $message);
    return;
}

list($success, $message) = MaterialTypeExtraUpdate(
    $this,
    $this->values->iid,
    $this->values->name,
    $this->values->valuetype,
    $this->values->valuemattype
);

$status = $success ? 'success' : 'error';