<?php
/**
 * @var \iSite $this
 * @var array $material
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if (!empty($material['documents'])) {
    $files = vjt_print_documents($material['documents'], null, true);
    if (!empty($files)) {
        ?>
        <div class="content-attachments content-documents links-group"><?= $files ?></div>
        <?php
    }
}