<?php


if (!defined('PASS_EXPIRE_TIME'))
    define('PASS_EXPIRE_TIME', 120*24*60*60);

if (!defined('WPF_PAGECACHE'))
    define('WPF_PAGECACHE', true);

define('STATUS_HIDDEN', 1);
define('STATUS_ACTIVE', 2);
define('STATUS_DELETED', 3);
define('STATUS_ANY', '');

define('LANG_RU', 1);
define('LANG_EN', 2);

//  db_time - app_time
if (!defined('DB_TIME_OFFSET'))
    define('DB_TIME_OFFSET', -3600 * 10);

if ( ! defined('DB_TIME_BIAS_HOURS'))
    define('DB_TIME_BIAS_HOURS', -10);

define('FILETYPE_IMAGE', 1);
define('FILETYPE_VIDEO', 2);
define('FILETYPE_AUDIO', 3);
define('FILETYPE_DOCUMENT', 4);
define('FILETYPE_BROADCAST', 5);

if (!defined('MATTYPE_HOME'))
    define('MATTYPE_HOME', 1);

define('SEARCH_TYPERANK_FILE', 1);
define('SEARCH_TYPERANK_MATERIAL', 10);

if (!defined('DB_TIMEZONE'))
    define('DB_TIMEZONE', 'Asia/Vladivostok');

if ( ! defined('SUBSCRIPTION_DB_TIMEZONE'))
    define('SUBSCRIPTION_DB_TIMEZONE', constant('DB_TIMEZONE'));

define('ROLE_ADMIN', 1);
define('ROLE_CMANAGER', 2);

if ( ! defined('MATTYPE_EVENT'))
    define('MATTYPE_EVENT', 0);

if ( ! defined('MATTYPE_EVENT_IMPORTANT'))
    define('MATTYPE_EVENT_IMPORTANT', 0);

if (!defined('SUBSCRIPTION_SEND_INTERVAL_MINUTES'))
    define('SUBSCRIPTION_SEND_INTERVAL_MINUTES', '5');
