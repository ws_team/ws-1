<?php
/**
 * @var \iSite $this
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

defined('_WPF_') or die();

//if ( ! $this->requireAuthUser()->can('admin.sysoptions')) {
//	$this->endRequest(403);
//}

//die('step1');

$this->data->title = 'Битые файлы';
$this->data->iH1 = 'Битые файлы';
$this->data->keywords = '';
$this->data->description = 'Битые файлы';
$this->data->breadcrumbsmore = '';
$this->data->errortext = '';

//получаем список сайтов
global $sites, $stypes, $files, $wrongfiles;
$sites=Array();
$files=Array();
$wrongfiles=Array();

$query="SELECT s.*, t.name typename, (select p.name FROM sites p WHERE p.id = s.parentid) parentname FROM sites s, sites_types t WHERE s.type_id = t.id";
$query.=" ORDER BY t.name ASC, s.parentid ASC, s.name ASC";

if($res = $this->dbquery($query)){

    //die('step2');

	if(is_array($res) && count($res) > 0){
		$sites=$res;

        //получаем список файлов
	    foreach ($sites as $site)
	    {

            //die('step3');

	        //устанавливаем соединение с БД сайта
            //$siteobj=$this;
            //$siteodj->settings->DB->name=$site['dbname'];
            //$siteodj->settings->DB->login=$site['dblogin'];
            //$siteodj->settings->DB->pass=$site['dbpass'];

            //$siteodj->dbclose();


            unset($siteodj);
            unset($setup);
            unset($versions);

            //die('step4');

            include_once('/home/habkrai/data/www/html.3/oiv/'.$site['file_folder'].'/settings.php');
            include_once('/home/habkrai/data/www/html.3/oiv/'.$site['file_folder'].'/versions.php');

            //print_r($setup);

            //die('step5');

            $siteodj = new iSite($setup,$versions, 0);

            //die('step6');

            if($siteodj)
            {

                //die('step7');

                //получаем список вложэений файлов, привязанных к активным материалам
                $query='SELECT f.* FROM files f WHERE f.type_id = 4 AND f.id IN ( SELECT fc.file_id FROM file_connections fc WHERE fc.material_id IN 
                    (SELECT m.id FROM materials m WHERE m.status_id = 2))';
                if($res = $siteodj->dbquery($query))
                {
                    //die('step8');

                    //krumo($res);

                    if(count($res) > 0){
                        foreach ($res as $dbfile)
                        {
                            $putfileinfo=false;
                            $filemat='';
                            $filesize='';
                            $filedatecreate='';
                            $filedateedit='';
                            $fileexist=true;

                            //получаем характеристики файла на диске (размер, дата редактирования, дата создания)
                            $filepatch='/home/habkrai/data/www/html.3/oiv/'.$site['file_folder'].'/photos/'.$dbfile['id'];
                            if(file_exists($filepatch))
                            {
                                $filesize=filesize($filepatch);
                                if($filesize == 0)
                                {
                                    $putfileinfo=true;
                                    $filedatecreate=date("F d Y H:i:s.", filectime($filepatch));
                                    $filedateedit=date("F d Y H:i:s.", filemtime($filepatch));
                                    $filesize=0;
                                }
                            }
                            else
                            {
                                $fileexist=false;
                                $putfileinfo=true;
                                $filedatecreate='';
                                $filedateedit='';
                                $filesize='';
                            }

                            //die('step9');

                            if($putfileinfo)
                            {

                                //вносим новые данные о файле
                                $dbfile['site']=$site['name'];

                                $domain=explode(';',$site['domains']);
                                $domain=$domain[0];

                                $dbfile['domain']=$domain;

                                $dbfile['filedatecreate']=$filedatecreate;
                                $dbfile['filedateedit']=$filedateedit;
                                $dbfile['filesize']=$filesize;

                                //получаем материал и пользователя изменившего материал с данным файлом
                                $query='SELECT m.id, m.name, m.type_id, m.date_add, m.date_edit, m.user_id, 
                                (SELECT u.fio FROM users u WHERE u.id = m.user_id) fio,
                                (SELECT u.email FROM users u WHERE u.id = m.user_id) email  
                                FROM materials m 
                                WHERE m.id IN 
                                    (SELECT fc.material_id FROM file_connections fc WHERE fc.file_id = '.$dbfile['id'].')';
                                if($mres=$siteodj->dbquery($query))
                                {
                                    //die('step10');

                                    $dbfile['materialid']=$mres[0]['id'];
                                    $dbfile['materialtypeid']=$mres[0]['type_id'];
                                    $dbfile['materialname']=$mres[0]['name'];
                                    $dbfile['materialuserid']=$mres[0]['user_id'];
                                    $dbfile['materialuseremail']=$mres[0]['email'];
                                    $dbfile['materialuserfio']=$mres[0]['fio'];
                                    $dbfile['materialdate_add']=$mres[0]['date_add'];
                                    $dbfile['materialdate_edit']=$mres[0]['date_edit'];

                                }

                                //сохраняем в массив ошибочных файлов
                                $wrongfiles[]=$dbfile;
                                //krumo($dbfile);
                            }

                        }
                    }
                }

            }

            //die('step11');

        }
	}
}

//die('step12');