<?php

if (($this->autorize->autorized == 1)&&($this->autorize->userrole == 1)) {

    //include_once($this->settings->path.$this->settings->templateurl.'/f_header.php');
    include_once($this->locateTemplate('f_header'));


//page body===========================================================================
?>
    <div class="container container--admin-title">
        <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
    </div>
    <div class="contentblock basemargin">
        <p class="errortext"><?php echo $this->data->errortext;  ?></p>

        <table width="100%">
            <tr>
                <th>Название</th>
                <th>Файл</th>
                <th>Типы материалов</th>
                <th>Типы шаблона</th>
                <th>Описание</th>
                <th>Действия</th>
            </tr>
            <?php

            $query='SELECT "s".* FROM "standart_shablons" "s" ORDER BY "s"."name" ASC';
            if($res=$this->dbquery($query))
            {
                if(count($res) > 0)
                {
                    foreach($res as $row)
                    {

                        if($row['materiallist'] == 1)
                        {
                            $stype='Список материалов';
                        }
                        else{
                            $stype='Детализация материала';
                        }

                        //получаем список типов материалов шаблона
                        $qparams = array($row['id']);
                        if($row['materiallist'] == 1)
                        {
                            $querys='SELECT "id", "name" FROM "material_types" WHERE "template_list" = $1';
                        }
                        else{
                            $querys='SELECT "id", "name" FROM "material_types" WHERE "template" = $1';
                        }

                        $mtypes='';

                        if($ress=$this->dbquery($querys, $qparams))
                        {
                            if(count($ress) > 0)
                            {
                                foreach($ress as $rows)
                                {
                                    if($mtypes != '')
                                    {
                                        $mtypes.=', ';
                                    }
                                    $mtypes.="<a href='/?menu=material_types&imenu=edit&id=$rows[id]'>$rows[name]</a>";
                                }
                            }
                        }


                        echo "<tr>
                            <td>$row[name]</td>
                            <td>$row[file_name]</td>
                            <td><small>$mtypes</small></td>
                            <td>$stype</td>
                            <td><small>$row[content]</small></td>
                            <td><a href='#' onclick=\"showEditStandartTemplateForm('$row[id]', '$row[name]', '$row[file_name]', $row[materiallist], '$row[content]');return false\"><img class='svg edit-icon' src='/images/edit.svg' /></a></td>
                        </tr>";
                    }
                }
            }

            ?>
        </table>
        <div style="display: none;">
            <div id="STShabEditForm" style="padding: 15px;">
                <form method="post" action="/">
                    <input type="hidden" name="menu" value="standarttemplates">
                    <input type="hidden" name="action" value="edit">
                    <input type="hidden" name="template_id" value="" id='shab_id'>
                    <h2>Изменение шаблона:</h2>
                    <p style="padding-bottom: 5px;"><span style="width: 200px; display: inline-block;">Наименование шаблона:</span>
                        <input type="text" class="styler" name="name" value="" id="shab_name"></p>
                    <p style="padding-bottom: 5px;"><span style="width: 200px; display: inline-block;">Файл шаблона:</span>
                        <input type="text" class="styler" name="file_name" value="" id="shab_file_name"></p>
                    <p style="padding-bottom: 5px;"><span style="width: 200px; display: inline-block; vertical-align: top;">Описание шаблона:</span>
                        <textarea class="styler" name="content" style="width: 300px; height: 100px;" id="shab_content"></textarea></p>
                    <p style="padding-bottom: 5px;"><span style="width: 200px; display: inline-block;">Тип шаблона:</span>
                        <select class="styler" name="materiallist" id="shab_materiallist">
                            <option value="1">Список материалов</option>
                            <option value="0">Детализация материала</option>
                        </select></p>
                    <p>&nbsp;</p>
                    <input class="styler" type="submit" value="Изменить">
                </form>
            </div>
        </div>

        <p>&nbsp;</p><p>&nbsp;</p>
        <h2 class="adminSubtitle">Добавить стандартный шаблон:</h2>
        <form method="post" action="/">
            <input type="hidden" name="menu" value="standarttemplates">
            <input type="hidden" name="action" value="add">
            <p style="padding-bottom: 5px;"><span style="width: 200px; display: inline-block;">Наименование шаблона:</span>
            <input type="text" class="styler" name="name" value=""></p>
            <p style="padding-bottom: 5px;"><span style="width: 200px; display: inline-block;">Файл шаблона:</span>
                <input type="text" class="styler" name="file_name" value=""></p>
            <p style="padding-bottom: 5px;"><span style="width: 200px; display: inline-block; vertical-align: top;">Описание шаблона:</span>
                <textarea class="styler" name="content" style="width: 300px; height: 100px;"></textarea></p>
            <p style="padding-bottom: 5px;"><span style="width: 200px; display: inline-block;">Тип шаблона:</span>
                <select class="styler" name="materiallist">
                    <option value="1">Список материалов</option>
                    <option value="0">Детализация материала</option>
                </select></p>
            <p>&nbsp;</p>
            <input class="styler" type="submit" value="Добавить">
        </form>



</div>

<?php

    //include_once($this->settings->path.$this->settings->templateurl.'/f_footer.php');
    include_once($this->locateTemplate('f_footer'));

}