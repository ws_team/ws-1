<div class="breadcrumbs"><?php echo $this->data->breadcrumbs; ?></div>
<h1><?php echo $this->data->iH1; ?></h1>
<p class='material_date'><?php echo russian_datetime_from_timestamp($material['unixtime']); ?></p>
<div class="contentblock" style="padding:30px; width: 924px; min-height: 300px;">
    <?php

    //выводим виджет
    echo vidjet_material_photovideo($this);

    //выводим контент
    echo $material['content'];

    //выводим связанные сущности

    ?>
</div>
<?php

//выводим материалы по теме

?>