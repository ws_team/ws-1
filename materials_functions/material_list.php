<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


//получаем список материалов
function MaterialList(
    \iSite $site,
    $type = '',
    $page = 1,
    $perpage = null,
    $status='',
    $ids='',
    $idstype='',
    $mquery='',
    $light=false,
    $lightwithvideo=false,
    $orderby=null,
    $ordersort=null)
{
    //если передан дополнительный тип материалов
    if($type == '')
    {
        if($site->values->sectype != '')
        {
            $type = $site->values->sectype;
        }
        else
        {
            $type = $site->values->maintype;
        }
    }

    //если инвестпортал - добавляем в языки английскую версию

    $langstypes=Array();
    $langstypes[]=124; //новости
    $langstypes[]=96; //Инвестиционные предложения
    $langstypes[]=97; //Успешные проекты
    $langstypes[]=35; //полезные ресурсы

    if($site->settings->DB->name == 'invest' && in_array($type, $langstypes)){

        if ( ! empty($site->data->language) && $site->data->language > 2)
        {
            $languagewhere='AND "m"."language_id" IN (2, '.$site->data->language.') AND m.id NOT IN (
            
                select en.id FROM materials en WHERE en.language_id = 2 AND en.type_id = '.$type.' AND en.date_event IN 
                    (SELECT own.date_event FROM materials own WHERE own.status_id = 2 AND own.type_id = '.$type.' AND own.language_id = '.$site->data->language.')
            )';
        }
        else
        {
            if ( ! empty($site->data->language))
            {
                $languagewhere='AND "m"."language_id" = '.$site->data->language;
            }
        }

    }
    else
    {
        $languagewhere='';
        if ( ! empty($site->data->language))
        {
            $languagewhere='AND "m"."language_id" = '.$site->data->language;
        }
    }

    if(!empty($site->values->language_id))
    {
        $languagewhere='AND "m"."language_id" = '.$site->values->language_id;
    }

    $searchwhere='';
    if (!empty($site->values->search))
    {
        $searchwhere='AND lower("m"."name") LIKE lower('."'%".$site->values->search."%'".')';
    }

    $start = $perpage * ($page - 1);
    $end = $start + $perpage;

    if ($status != '') {
        $statuswhere='AND "m"."status_id" = '.$status;
    } else {
        $statuswhere='AND "m"."status_id" IN ('.STATUS_ACTIVE.','.STATUS_HIDDEN.')';
    }

    if ($ids != '') {
        $idwhere = ' AND "m"."id" IN ('.$ids.')';
        $typewhere = '';

        if($idstype != '')
        {
            $idwhere .= 'AND "m"."type_id" = '.$idstype;
        }

    } else {
        $typewhere = 'AND "m"."type_id" = '.$type;
        $idwhere='';
    }

    if($mquery != '') {
        $idwhere = ' AND "m"."id" IN ('.$mquery.')';
        $typewhere='';

    }

    $orderby_orig = $orderby;

    if (!isset($orderby))
        $orderby = isset($site->values->orderby) ? $site->values->orderby : 'date_event';
    if (!isset($ordersort))
        $ordersort = isset($site->values->ordersort) ? $site->values->ordersort : 'DESC';

    switch($ordersort)
    {
        case 'ASC':
        case 'asc':
            break;
        default:
            $ordersort = 'DESC NULLS LAST';
            break;
    }

    $orderby_field = $orderby;

    switch($orderby)
    {
        case 'name':
            $orderby='"m"."'.$orderby.'" '.$ordersort.', "m"."date_event" DESC';
            break;

        case 'id':
            $orderby='"m"."'.$orderby.'" '.$ordersort.', "m"."date_event" DESC';
            break;

        case 'id_char':
            $orderby='"m"."'.$orderby.'" '.$ordersort.', "m"."date_event" DESC';
            break;

        case 'status_id':
            $orderby='"m"."'.$orderby.'" '.$ordersort.', "m"."date_event" DESC';
            break;

        case 'conmat':
            break;

        case 'position':
        case 'weight':
            $orderby = '"m"."extra_number2" '.$ordersort;
            break;

        default:
            $orderby = 'date_event';
            $orderby = '"m"."'.$orderby.'" '.$ordersort.'';
            break;
    }

    if($orderby_field == 'conmat')
    {
        $orderby = '"m"."name" DESC';
    }
    elseif (!$orderby_orig && !strpos($orderby, 'extra_number2'))
    {
        $orderby = '"m"."extra_number2" ASC NULLS LAST, '.$orderby;
    }
    $orderby = 'ORDER BY '.$orderby;

    //if($site->settings->DB->name == 'invest' && in_array($type, $langstypes)){
    //    echo '<!-- order '.$orderby.' -->';
    //}

    if ($light) {
        $videosubquery='';
    } else {
        $videosubquery=',
                (SELECT
                        MIN("i"."file_id")
                    FROM "file_connections" "i",
                        "files" "f"
                    WHERE "f"."id" = "i"."file_id"
                        AND "f"."type_id" = 2
                        AND "i"."material_id" = "m"."id"
                        AND "f"."ordernum" = (
                            SELECT MIN("fo"."ordernum")
                            FROM "file_connections" "io",
                                "files" "fo"
                            WHERE "fo"."id" = "io"."file_id"
                                AND "fo"."type_id" = 2
                                AND "io"."material_id" = "m"."id"
                        )
                ) "videoid"';
    }


    $query= <<<EOS
SELECT
    "m".*,
    (UNIX_TIMESTAMP("m"."date_event") -10*60*60) "unixtime",
    (UNIX_TIMESTAMP("m"."date_edit") -10*60*60) "editunixtime",
    (
        SELECT "l"."name"
        FROM "languages" "l"
        WHERE "l"."id" = "m"."language_id"
    ) "languagename",
    (
        SELECT "s"."name"
        FROM "material_statuses" "s"
        WHERE "s"."id" = "m"."status_id"
    ) "statusname", (
        SELECT "ty"."name"
        FROM "material_types" "ty"
        WHERE "ty"."id" = "m"."type_id"
    ) "type_name", (
        SELECT "ty"."mat_as_type"
        FROM "material_types" "ty"
        WHERE "ty"."id" = "m"."type_id"
    ) "mat_as_type", (
        SELECT MIN("i"."file_id")
        FROM
            "file_connections" "i",
            "files" "f"
        WHERE
            "f"."id" = "i"."file_id"
            AND "f"."type_id" = 1
            AND "i"."material_id" = "m"."id"
            AND "f"."ordernum" = (
                SELECT MIN("fo"."ordernum")
                FROM "file_connections" "io",
                    "files" "fo"
                WHERE "fo"."id" = "io"."file_id"
                    AND "fo"."type_id" = 1
                    AND "io"."material_id" = "m"."id"
            )

    ) "photo" $videosubquery

FROM "materials" "m"
WHERE 1=1 $typewhere $idwhere $statuswhere $searchwhere $languagewhere
$orderby
LIMIT $perpage OFFSET $start;
EOS;


    if (isset($_REQUEST['printThisQuery']) && $_REQUEST['printThisQuery'] == 'printThisQuery'
        && ($site->autorize->autorized == 1) && ($site->autorize->userrole == 1)) {
        krumo($query);
    }

    if (($res = $site->dbquery($query)) !== false) {
        if (isset($_REQUEST['printThisQuery'])
            && ($site->autorize->autorized == 1) && ($site->autorize->userrole == 1)) {
            krumo($query);
        }

        //формируем полные ссылки на материалы в списке
        $i=-1;
        foreach ($res as $row) {
            ++$i;

            //проверяем, дорабатываем видео
            if (!empty($row['videoid'])) {
                $queryvid='SELECT "id", "name", "content" FROM "files" WHERE "id" = $1';

                $resvid = $site->dbquery($queryvid, array($row['videoid']));
                if (!empty($resvid)) {
                    if($resvid[0]['content'] != '')
                    {
                        $res[$i]['video'] = $resvid[0]['content'];

                        $vurl = $resvid[0]['content'];
                        parse_str( parse_url( $vurl, PHP_URL_QUERY ), $my_array_of_vars );

                        if(isset($my_array_of_vars['v']))
                        {
                            $vid = $my_array_of_vars['v'];
                        }
                        else{
                            $vid = '';
                        }


                        if($vid == '')
                        {
                            $vid = str_replace('https://www.youtube.com/watch?v=','',$vurl);
                            $vid = str_replace('http://www.youtube.com/watch?v=','',$vid);
                            $vid = str_replace('http://www.youtube.com/embed/','',$vid);
                            $vid = str_replace('https://youtu.be/', '', $vid);
                        }

                        $res[$i]['video']=$vid;
                        $res[$i]['youtubeid']=$vid;

                        //thumb
                        $res[$i]['thumb']='https://img.youtube.com/vi/' . $vid . '/mqdefault.jpg';

                    } else {
                        $res[$i]['video'] = '/photos/'.$resvid[0]['content'];
                        $res[$i]['thumb']='/photos/'.$resvid[0]['id'].'_thumb.png';
                        $res[$i]['youtubeid']=false;
                    }
                }
            } else {
                $res[$i]['video'] = '';
            }

            $res[$i]['content'] = str_replace('`',"'",$res[$i]['content']);
            $res[$i]['anons'] = nl2br($res[$i]['anons']);

            //получаем url типов материалов

            if ($row['mat_as_type'] == 1) {
                $res[$i]['url'] = GetMaterialTypeUrl($site, $row['type_id'], $row['id'],
                    $row['type_id']);
            } else {
                $res[$i]['url'] = GetMaterialTypeUrl($site, $row['type_id'], $row['id'],
                        $row['type_id']).'/'.$row['id'];
            }

            //формируем тэги

            $res[$i]['tags'] = vidjet_tags($row['extra_text1'],
                isset($setup['data']['tags_caption']) ? $setup['data']['tags_caption'] : 'Ключевые слова',
                !empty($site->settings->tags['limit_by_mattype']) ?
                    GetMaterialTypeUrl($site, $row['type_id']) :
                    '/tags'
            );


            //получаем связанные материалы

            if (!$light && !$lightwithvideo) {

                $queryc = 'SELECT "c"."material_child" "mid"
                    FROM "material_connections" "c"
                    WHERE "c"."material_parent" = $1

                    UNION ALL

                    SELECT "co"."material_parent" "mid"
                    FROM "material_connections" "co"
                    WHERE "co"."material_child" = $1
                    ';

                $cids='';

                if ($resc=$site->dbquery($queryc, array($row['id']))) {
                    if (count($resc) > 0) {
                        foreach ($resc as $rowc) {
                            if ($cids != '') {
                                $cids .= ',';
                            }

                            $cids .= $rowc['mid'];
                        }
                    }
                }

                $res[$i]['cids']=$cids;

            } else {
                $res[$i]['cids']='';
            }

            //получаем контакты

            if(!$light && !$lightwithvideo){

                $queryс='SELECT "c".*,
    (SELECT "m"."name" FROM "materials" "m" WHERE "m"."id" = "c"."city") "cityname"
    FROM "contacts" "c"
    WHERE "c"."material_id" = $1 ORDER BY "c"."addr" ASC, "c"."id" ASC';

                if ($resс = $site->dbquery($queryс, array($row['id'])))
                {
                    if(count($resс) > 0)
                    {
                        //связанные расписания
                        $ij=-1;
                        foreach($resс as $rowс)
                        {
                            ++$ij;

                            $full_address = trim($resс[$ij]['addr']);

                            if(isset($resс[$ij]['building']) && $resс[$ij]['building'] != '') {
                                $full_address .= ', д. '.trim($resс[$ij]['building']);
                            }

                            if(isset($resс[$ij]['office']) && $resс[$ij]['office'] != '') {
                                $full_address .= ', каб. '.trim($resс[$ij]['office']);
                            }

                            $resс[$ij]['fulladdr'] = $full_address;

                            $resс[$ij]['schedules']=Array();

                            $querys='SELECT * FROM "schedules" WHERE "contact_id" = '.$row['id'].' ORDER BY "id" ASC';

                            if ($ress=$site->dbquery($querys)) {
                                if(count($ress) > 0)
                                {
                                    $resс[$ij]['schedules']=$ress;
                                }
                            }
                        }

                        $res[$i]['contacts'] = $resс;
                    }
                }
            } else {
                $res[$i]['contacts']=Array();
            }
        }

        return $res;
    }

    return false;
}

function MaterialsCount(\iSite $site, $type_id)
{
    $limit = $site->values->perpage;
    if ($limit < 1)
        $limit = 1;

    $offset = ($site->values->page - 1)* $limit;
    if ($offset < 0)
        $offset = 0;

    $query = <<<EOS
SELECT count(*) num
FROM "materials"
WHERE
    type_id = $1
    AND status_id = $2
    AND language_id = $3
LIMIT $limit OFFSET $offset
EOS;

    $q_params = array($type_id, STATUS_ACTIVE, $site->data->language);

    $res = $site->dbquery($query, $q_params);
    if ( ! empty($res))
        return intval($res[0]['num']);

    return false;
}