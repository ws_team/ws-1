BlocksAPI.attachJS(function() {

    ListDateFilterAPI = function() {
        // Форма в которой содержится поле фильтра
        this.form = $("form#filters");        
        // Само поле фильтра
        this.formFieldStart = $("input[name='list_date_filter_datestart']");
        this.formFieldEnd = $("input[name='list_date_filter_dateend']");
    }

    // Список коллбэков на апдейт фильтра
    ListDateFilterAPI.prototype.updateCallbacks = [];

    // Добавить коллбэк на событие обновления фильтра
    ListDateFilterAPI.prototype.addUpdateCallback = function(callback) {
        this.updateCallbacks.push(callback);
    }

    // Запустить процесс обновления фильтра
    ListDateFilterAPI.prototype.update = function(datestart, dateend) {
        console.log('list date setter triggered');
        this.formFieldStart.val(datestart);
        this.formFieldEnd.val(dateend);

        // Кличем все коллбэки обновления которые были навешаны извне
        // Так сделано чтобы можно было навесить кастомный коллбэк отправки формы, 
        // скорее всего он будет где-то в другом месте
        for (var i = 0; i < this.updateCallbacks.length; i++) {
            this.updateCallbacks[i](datestart, dateend);
        };
    }

    ListDateFilterAPI.prototype.getDates = function() {
        return [this.formFieldStart.val(), this.formFieldEnd.val()];
    }

    ListDateFilterAPI.prototype.reset = function() {
        this.clearSelect();
        this.update('', '');
    };

    // ListDateFilterAPI.prototype.drawCalendar = function() {
    //     // Собираем календарик из jQuery-плагина
    //     $(".block-single-calendar").find(".single-calendar").DatePicker({
    //         flat: true, // хз
    //         date: ListDateFilter.getDate(),   // предварительно отжатые даты
    //         // current: showDate, // текущая дата?
    //         calendars: 1, // количество отображемых месяцев
    //         mode: 'single', // режим выбора: (одиночное значение/множественное значение/диапазон)
    //         start: 1, // начало недели (1 = понедельник)
    //         format: 'd.m.Y', // юзаемый формат данных
    //         locale: { // локализация
    //             daysShort : ['Пн','Вт','Ср','Чт','Пт','Сб','Вс'],
    //             daysMin : ['Пн','Вт','Ср','Чт','Пт','Сб','Вс'],
    //             months : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
    //             monthsShort : ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
    //             week : "Нед"
    //         },
    //         // коллбэки-чебуреки
    //         // onShow: $.proxy(this.onShow, this),
    //         // onHide: $.proxy(this.onHide, this),
    //         onChange: $.proxy(ListDateFilter.set, ListDateFilter),
    //         // onRender: $.proxy(this.onRender, this)
    //     });
    // }

    ListDateFilterAPI.prototype.clearSelect = function() {
        // $(".block-single-calendar").find(".single-calendar").DatePickerClear();        
    }    

    // ListDateFilterAPI.prototype.setDisplayMonth = function(m, y) {
    //     this.clearSelect();
    //     var date = '01.' + m + '.' + y;

    //     this.setDisplayDate(date);
    // }    

    // ListDateFilterAPI.prototype.setDisplayDate = function(date) {
    //     $(".block-single-calendar").find(".single-calendar").DatePickerSetDate(date, true);
    // }    

    ListDateFilter = new ListDateFilterAPI();

	// $(document).ready(function() {
 //        $.proxy(ListDateFilter.drawCalendar, ListDateFilter)();
	// });

	// $monthSelect = $(".block-single-calendar").find("select[name='single_calendar_month']");
	// $yearSelect = $(".block-single-calendar").find("select[name='single_calendar_year']");

	// $monthSelect.on("change", function() {
 //        var m = (parseInt($monthSelect.val()) < 10) ? '0' + $monthSelect.val() : $monthSelect.val();
 //        var y = $yearSelect.val();

 //        $.proxy(ListDateFilter.setDisplayMonth, ListDateFilter, m, y)();
	// 	console.log("Single Calendar Block: month change");
	// });

	// $yearSelect.on("change", function() {
 //        var m = (parseInt($monthSelect.val()) < 10) ? '0' + $monthSelect.val() : $monthSelect.val();
 //        var y = $yearSelect.val();

 //        $.proxy(ListDateFilter.setDisplayMonth, ListDateFilter, m, y)();        
	// 	console.log("Single Calendar Block: year change");
	// });

	// if ($monthSelect.hasOwnProperty('styler')) {
	// 	$monthSelect.styler();
	// 	$yearSelect.styler();
	// }
});

