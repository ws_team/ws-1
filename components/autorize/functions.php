<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\components\autorize;

/**
 * @param \iSite $site
 * @return bool
 */
function auth_limit_attempts(\iSite $site)
{
    if($_SESSION['authtryes'] > 10 && 0)
    {
        $_SESSION['authtryes']=0;

        $blocktime = time() + 5 * 60;
        $nowtime = time();
        $_SESSION['authblock'] = $blocktime;

        //заносим блокировку
        //проверяем существование пользователя

        if( (!empty($site->values->autorize_login) || !empty($site->values->autorize_email))
            && !empty($site->values->autorize_pass)
        ) {
            //определяем id или email, формируем запрос
            $q_params = array();
            if($site->values->autorize_email != '')
            {
                $q_params[] = $site->values->autorize_email;
                $q_params[] = $site->values->passhash;
                $query='SELECT * FROM "users" WHERE "email" IS NOT NULL AND "email" = $1 AND "pass" = $2';
            }
            else
            {
                $q_params[] = $site->values->autorize_login;
                $q_params[] = $site->values->passhash;
                $query='SELECT * FROM "users" WHERE "fio" IS NOT NULL AND "fio" = $1 AND "pass" = $2';
            }

            //получаем данные о пользователе
            if($res=$site->dbquery($query, $q_params))
            {
                //пользователь существует - блокируем учетку в БД
                $query='UPDATE users SET bloctime = $blocktime WHERE id = $1';
                $site->dbquery($query, array($res[0]['id']));
            }
        }

        $min = $blocktime - $nowtime;
        $mins = round($min/60);

        if ($mins < 1) {
            $min=get_correct_str($min, 'секунду', 'секунды', 'секунд');
        }
        else{
            $min=$mins;
            $min=get_correct_str($min, 'минуту', 'минуты', 'минут');
        }

        $site->autorize->answer =
            'Ошибка при атворизации - возможность авторизации времено заблокирована. Попробуйте через '.$min;

        return false;
    } else {
        //проверка блокировки в базе
        if ( (!empty($site->values->autorize_login) || !empty($site->values->autorize_email))
            && !empty($site->values->autorize_pass)
        ) {
            $q_params = array();
            //определяем id или email, формируем запрос
            if (!empty($site->values->autorize_email)) {
                $q_params[] = $site->values->autorize_email;
                $q_params[] = $site->values->passhash;
                $query = 'SELECT * FROM "users" WHERE "email" IS NOT NULL AND "email" = $1 AND "pass" = $2';
            } else {
                $q_params[] = $site->values->autorize_login;
                $q_params[] = $site->values->passhash;
                $query='SELECT * FROM "users" WHERE "fio" IS NOT NULL AND "fio" = $1 AND "pass" = $2';
            }

            //получаем данные о пользователе
            if($res=$site->dbquery($query, $q_params)) {
                //проверяем блокировку
                $sec=($res[0]['bloctime'] - time());

                if($sec > 0)
                {

                    $min=$sec;
                    $mins=round($min/60);

                    if($mins < 1)
                    {
                        $min=get_correct_str($min, 'секунду', 'секунды', 'секунд');
                    }
                    else{
                        $min=$mins;
                        $min=get_correct_str($min, 'минуту', 'минуты', 'минут');
                    }

                    //авторизация блокирована в сессии
                    $site->autorize->answer =
                        'Ошибка при авторизации - возможность авторизации времено заблокирована. Попробуйте через '.$min;

                    return false;
                }
            }
        }
    }
    return true;
}

function auth_lock(\iSite $site)
{
    $nowtime = time();

    if ($_SESSION['authblock'] > $nowtime) {
        $min = $_SESSION['authblock'] - $nowtime;

        $mins = round($min/60);

        if ($mins < 1) {
            $min = get_correct_str($min, 'секунду', 'секунды', 'секунд');
        } else{
            $min = $mins;
            $min = get_correct_str($min, 'минуту', 'минуты', 'минут');
        }

        $site->autorize->answer = 'Ошибка при авторизации - возможность авторизации времено заблокирована. Попробуйте через '.$min;
        $_SESSION['authtryes'] = 0;

        return false;
    }

    return true;
}

function auth_change_password(\iSite $site)
{
    //смена пароля
    if(isset($_REQUEST['new_autorize_pass']) && isset($_SESSION['changepass']) && isset($_SESSION['changepasslogin']))
    {
        $site->GetPostValues(Array('new_autorize_pass','new_autorize_pass2'));

        //krumo($site->values->new_autorize_pass);

        //проверяем новый пароль
        if(checkpass($site->values->new_autorize_pass))
        {

            //меняем пароль
            if($site->values->new_autorize_pass != $site->values->new_autorize_pass2)
            {
                //krumo($site->values->new_autorize_pass,$site->values->new_autorize_pass2,$_REQUEST);
                $site->autorize->answer='Ошибка смены пароля - введенные пароли не совпадают';
            }
            else
            {
                if($site->values->new_autorize_pass == $_SESSION['changepass'])
                {
                    $site->autorize->answer='Ошибка смены пароля - новый пароль совпадает со старым.';
                }
                else
                {
                    $passdays=time()+PASS_EXPIRE_TIME;

                    //krumo($passdays);

                    //$md5pass = get_password_hash($site->values->new_autorize_pass);
                    $md5pass = get_password_hash($site->values->new_autorize_pass,1,$_SESSION['changepasslogin']);

                    //krumo($md5pass);

                    //$md5passold = get_password_hash($_SESSION['changepass']);

                    $query= "UPDATE users SET pass = $1, passtime = $passdays WHERE".' "email" IS NOT NULL AND "email" = $2'.
                        ' ';

                    $q_params = array($md5pass, $_SESSION['changepasslogin']);
                    if($res=$site->dbquery($query, $q_params))
                    {
                        $_REQUEST['autorize_email']=$_SESSION['changepasslogin'];
                        $_REQUEST['autorize_pass']=$site->values->new_autorize_pass;

                        //krumo($query,$q_params);

                        unset($_SESSION['changepasslogin']);
                        unset($_SESSION['changepass']);
                    }
                    else{
                        $site->autorize->answer='Ошибка смены пароля - не удалось сохранить новый пароль.';
                    }
                }
            }

        }else{
            $site->autorize->answer='Ошибка смены пароля - пароль не удовлетворяет требования безопасности.';
        }
    }
}

function get_password_hash($password,$type=0,$login='')
{

	if($type==0)
	{
		return md5($password);
	}
	else
	{
		$pass=$password.'f6hal_'.$login.'fGh73c'.$password;
		return hash('sha256', $pass);
}
}

/**
 * @param \iSite $site
 * @return bool
 */
function auth_login(\iSite $site)
{
    $logged = false;

    if (isset($_REQUEST['autorize_login'])
        || isset($_REQUEST['autorize_pass'])
        || isset($_REQUEST['autorize_email']))
    {
        $oivadmins = include('data/oivadmins.php');
        $omsuusers = include('data/omsuusers.php');

        $site->GetPostValues(array('autorize_login' ,'autorize_pass', 'autorize_email'));

        $site->values->passhash='____';

        //определяем время смены пароля - для понимания, как просчитывать старый хэш
        $query="SELECT passtime FROM users WHERE email IS NOT NULL AND email = '".$site->values->autorize_email."'";

        //krumo($query);
        if($res=$site->dbquery($query))
        {

            $newpasstime=MakeTimeStampFromStr('30.01.2018');

            $passtime=$res[0]['passtime'];

            $minustime=($passtime-PASS_EXPIRE_TIME);
            $minnewpasstime=$newpasstime+PASS_EXPIRE_TIME;

            //echo '<!-- passtime - '.$passtime.'  startnewtime = '.$newpasstime.'  expiretime = '.PASS_EXPIRE_TIME.'  minustime = '.$minustime.'  minnewpasstime = '.$minnewpasstime.'-->';

            if(($passtime-PASS_EXPIRE_TIME) < $newpasstime)
            {
                $site->values->passhash = get_password_hash($site->values->autorize_pass);
            }
            else
            {
                $site->values->passhash = get_password_hash($site->values->autorize_pass, 1, $site->values->autorize_email);
            }

            //krumo($site->values->passhash);

        }

        //хэш пароля
        //$site->values->passhash = get_password_hash($site->values->autorize_pass);

        ++$_SESSION['authtryes'];

        if ( ! auth_lock($site))
            return false;

        if ( ! auth_limit_attempts($site))
            return false;

        if ( ! isset($oivadmins[$site->values->autorize_email])) {
            $oivadmins[$site->values->autorize_email] = array();
            $oivadmins[$site->values->autorize_email]['pass'] = false;
        }

        if ( !empty($site->values->autorize_login)
            || !empty($site->values->autorize_email)
            || !empty($site->values->autorize_pass)
        ) {
            $logged = auth_login_omsu($site, $omsuusers);
            if (!$logged)
                $logged = auth_login_oivadmin($site, $oivadmins);

            if (!$logged)
                $logged = auth_login_regular($site);
        } else {
            $site->autorize->answer = 'Ошибка авторизации - пустой логин или пароль';
        }
    }

    return $logged;
}

function auth_login_omsu(\iSite $site, $omsuusers)
{
    if (isset($omsuusers[$site->values->autorize_email])
        && !empty($site->values->autorize_pass)
        && !empty($omsuusers[$site->values->autorize_email]['pass'])
        && ($omsuusers[$site->values->autorize_email]['pass'] == $site->values->autorize_pass)) {

        $auth_rec = $omsuusers[$site->values->autorize_email];

        if ( ! empty($auth_rec['nologin']))
            return false;

        $db_name = $auth_rec['db'];

        $is_main_khabkrai = strncmp('habkrai', $site->settings->DB->name, 7) == 0;
        $is_own = strncmp($db_name, $site->settings->DB->name, strlen($db_name)) == 0;

        if ( ! $is_main_khabkrai && ! $is_own)
            return false;

        $site->autorize->answer = 'Авторизироваться удалось';

        $site->autorize->autorized = 1;
        $site->autorize->username = 'Учетные записи ОМСУ';
        $site->autorize->userlogin='';
        $site->autorize->userid = 5;
        $site->autorize->userrole = ROLE_CMANAGER;
        $site->autorize->email = $site->values->autorize_email;

        $userRec = $omsuusers[$site->values->autorize_email];

        if ($is_main_khabkrai) {
            $site->autorize->materialtypes = expand_materialtypes_list($site, array(4,162,163,164,165,166,208));
            $site->autorize->omsu = $userRec['omsu'];
            $site->autorize->reg = $userRec['reg'];
        } elseif ($site->settings->DB->name == $userRec['db']) {
            $site->autorize->materialtypes = Array();
        } else {
            $site->autorize->materialtypes = Array(0);
        }

        if ($is_main_khabkrai && ( ! empty($site->autorize->reg) || ! empty($site->autorize->omsu))) {
            $bound_materials = array();
            if ( ! empty($site->autorize->reg))
                $bound_materials[] = $site->autorize->reg;
            if ( ! empty($site->autorize->omsu))
                $bound_materials[] = $site->autorize->omsu;

            $site->autorize->bound_materials = $bound_materials;
        }

        authinfo_store($site);

        $site->autorize->answer = 'Авторизироваться удалось';
        return true;
    }

    return false;
}

/**
 * @param \iSite $site
 */
function authinfo_store(\iSite $site)
{
    authinfo_update($site->autorize);
}

function authinfo_update($authInfo)
{
    foreach (authinfo_fields() as $fld)
        $_SESSION['autorize'][$fld] = isset($authInfo->$fld) ? $authInfo->$fld : '';
}

function authinfo_restore(\iSite $site)
{
    foreach (authinfo_fields() as $fld) {
        $site->autorize->$fld = isset($_SESSION['autorize'][$fld]) ? $_SESSION['autorize'][$fld] : '';
    }
}

function authinfo_reset($authInfo)
{
    foreach (authinfo_fields() as $fld) {
        $authInfo->$fld = '';
    }
    $authInfo->autorized = 0;
}

function authinfo_fields()
{
    return array(
        'autorized',
        'username',
        'userlogin',
        'userid',
        'userrole',
        'materialtypes',
        'omsu',
        'reg',
        'email',
        'answer',
        'bound_materials',
    );
}

function auth_login_oivadmin(\iSite $site, $oivadmins)
{
    if ( (is_array($oivadmins[$site->values->autorize_email]))
        && !empty($site->values->autorize_pass)
        && !empty($oivadmins[$site->values->autorize_email]['pass'])
        && ($oivadmins[$site->values->autorize_email]['pass'] == $site->values->autorize_pass)
        ) {

        $auth_rec = $oivadmins[$site->values->autorize_email];

        if ( ! empty($auth_rec['nologin']))
            return false;

        $site->autorize->answer = 'Авторизироваться удалось';

        $is_main_khabkrai = strncmp('habkrai', $site->settings->DB->name, 7) == 0;

        $site->autorize->autorized = 1;
        $site->autorize->username = 'Учетные записи администратора ОИВ';
        $site->autorize->userlogin = '';
        $site->autorize->userid = -9;
        $site->autorize->userrole = !empty($auth_rec['role']) ?
            $auth_rec['role'] :
            ROLE_ADMIN;

        $site->autorize->email = $site->values->autorize_email;

        $site->autorize->materialtypes = $is_main_khabkrai && ! empty($auth_rec['materialtypes']) ?
            expand_materialtypes_list($site, (array)$auth_rec['materialtypes']) :
            array();

        if ( ! empty($auth_rec['bound_materials']) && $is_main_khabkrai) {
            $site->autorize->bound_materials = (array)$auth_rec['bound_materials'];
        }
        $site->autorize->omsu = '';
        $site->autorize->reg = '';

        authinfo_store($site);

        $site->autorize->answer = 'Авторизироваться удалось';
        return true;
    }

    return false;
}

function expand_materialtypes_list(\iSite $site, $types)
{
    $ret = array();

    foreach ($types as $type) {
        $ret = array_merge($ret, GetTypeTree($site, $type));
    }

    $ret = array_unique($ret);

    if (empty($ret))
        $ret[] = 0;

    return $ret;
}

/**
 * @param \iSite $site
 * @return bool
 */
function auth_login_regular(\iSite $site)
{
    //определяем id или email, формируем запрос
    $q_params = array();
    if (!empty($site->values->autorize_email)) {
        $query='SELECT * FROM "users"
            WHERE "email" IS NOT NULL
                AND "email" = $1 AND status = $2 AND "pass" = $3';
        $q_params[] = $site->values->autorize_email;
    } elseif (!empty($site->values->autorize_login)) {
        $query='SELECT * FROM "users"
            WHERE "fio" IS NOT NULL
                AND "fio" = $1 AND status = $2 AND "pass" = $3';
        $q_params[] = $site->values->autorize_login;
    } else {
        autorize_login_invalid($site);
        return;
    }
    $q_params[] = 1;
    $q_params[] = $site->values->passhash;

    //получаем данные о пользователе
    $res = $site->dbquery($query, $q_params);
    if ( ! empty($res)) {
        if (auth_password_expiry($site, $res[0]) === false)
            return false;

        $site->autorize->answer = 'Авторизироваться удалось';

        $site->autorize->autorized = 1;
        $site->autorize->username = $res[0]['fio'];
        $site->autorize->userlogin = '';
        $site->autorize->userid = $res[0]['id'];
        $site->autorize->userrole = $res[0]['role_id'];
        $site->autorize->email = $res[0]['email'];

        $site->autorize->omsu = $res[0]['omsu'];
        $site->autorize->reg = '';

        if ($site->autorize->omsu != '') {
            $site->autorize->reg = auth_get_region_from_omsu($site, $site->autorize->omsu);
        }

        if ($site->autorize->username == '') {
            $site->autorize->username = 'ФИО не определено';
        }

        //получаем разделы
        $site->autorize->materialtypes = get_allowed_materialtypes($site, $site->autorize->userid);

        $attrs = load_user_attributes($site, $site->autorize->userid);

        foreach ($attrs as $attr => $val) {
            if ( ! isset($site->autorize->$attr) || $site->autorize->$attr === '')
                $site->autorize->$attr = $val;
        }

        //записываем данные в php сессию
        authinfo_store($site);

        SaveLog($site, 'Авторизация на сайте '.$site->settings->DB->login, 0);
    } else {
        autorize_login_invalid($site);
    }
}

function load_user_attributes(\iSite $site, $uid)
{
    $query = 'SELECT name, "value" FROM "user_attributes" WHERE user_id = $1';
    $fetch = $site->dbquery($query, array($uid));
    $ret = array();

    if ($fetch === false) {
        // 95% таблица "user_attributes" не существует
        $site->resetError();
        return $ret;
    }

    if ( ! empty($fetch)) {
        foreach ($fetch as $record) {
            $attr = $record['name'];
            if ( ! isset($ret[$attr])) {
                $ret[$attr] = $record['value'];
            } else {
                if ( ! is_array($ret[$attr]))
                    $ret[$attr] = array($ret[$attr]);
                $ret[$attr][] = $record['value'];
            }

        }
    }

    return $ret;
}

function autorize_login_invalid(\iSite $site)
{
    $site->autorize->answer = 'Ошибка при атворизации - не верное сочетание логина и пароля';
    //echo '<!-- authorize '.$site->values->passhash.' !>';

    $time=date('d.m.Y H:i:s');

    $site->getLogger()->write(LOG_INFO, __FUNCTION__, "wrong user authorization: ".$site->values->autorize_email.' check hash: '.$site->values->passhash.' time: '.$time);

    SaveLog(
        $site,
        'Попытка авторизации под логином "'.$site->values->autorize_email.'" '.
        $site->settings->DB->login,
        0
    );
}

function auth_password_expiry(\iSite $site, $userrec)
{
    //проверяем срок действия пароля
    $now = time();

    if (empty($userrec['passtime']) || $userrec['passtime'] == '') {
        $newpasstime = $now + PASS_EXPIRE_TIME;
        $userrec['passtime'] = $newpasstime;

        $queryupd='UPDATE users SET passtime = $1
            WHERE "email" IS NOT NULL AND "email" = $2 AND "pass" = $3';
        $site->dbquery($queryupd, array($newpasstime, $site->values->autorize_email, $site->values->passhash));
    }

    $passtime = $userrec['passtime'] - $now;
    if ($passtime <= 0) {
        $site->autorize->answer = 'Ошибка при авторизации - срок действия пароля истек';
        $_SESSION['changepass']=$site->values->autorize_pass;
        $_SESSION['changepasslogin']=$site->values->autorize_email;

        return(false);
    }

    return true;
}

function auth_logout(\iSite $site)
{
    $site->getUser()->logout();
}

function auth_get_region_from_omsu(\iSite $site, $omsu)
{
    //смотрим связанный регион
    $queryreg = <<<EOS
SELECT
    "m".*
FROM "materials" "m"
WHERE
    "m"."type_id" = $1
    AND "m"."id" IN(
        SELECT "cm"."material_child" "id"
        FROM "material_connections" "cm"
        WHERE "cm"."material_parent" = $2
        
        UNION

        SELECT "pm"."material_parent" "id" FROM "material_connections" "pm"
        WHERE "pm"."material_child" = $2
    )
EOS;

    $q_params = array(21, $omsu);
    $resreg = $site->dbquery($queryreg, $q_params);

    if ( ! empty($resreg)) {
        return $resreg[0]['id'];
    }
}

function editmaterials_restrict_subdivision($permission, \wpf\auth\IdentityInterface $user)
{// @todo test
    if (strncmp($permission, 'editmaterials', 13) != 0)
        return false;

    $allowed_types = $user->getAttribute('materialtypes');
    if (empty($allowed_types))// не установлено ограничение по на разделы
        return false;

    $permission = explode(':', $permission);
    if (empty($permission[1]))// не указан тип материала
        return false;

    $type = $permission[1];

    if (in_array($type, $allowed_types))// доступ в раздел (или один из предков раздела) разрешен
        return false;// не применять ограничение

    // применить ограничение
    return true;
}

function editmaterials_restrict_bound($permission, \wpf\auth\IdentityInterface $user, $params)
{
    $perm = explode(':', $permission);

    if (strncmp($perm[0], 'editmaterials', 13) != 0)
        return false;// не подходящий тип разрешения

    if (empty($perm[2]) || ! is_numeric($perm[2]))
        return false;// не указан ид. материала

    $mat_id = $perm[2];

    $bound_mats = $user->getAttribute('bound_materials');

    if (empty($bound_mats))
        return false;// связки не назначены

    $query = <<<EOS
SELECT COUNT(*) num
FROM material_connections
WHERE
    material_parent = $1 AND material_child IN($2)
    OR material_child = $1 AND material_parent IN($2)
EOS;
    $q_params = array($mat_id, $bound_mats);

    $site = $params['site'];
    list($query, $q_params) = $site->dbqueryExpand($query, $q_params);

    $res = $site->dbquery($query, $q_params);
    if ( ! empty($res) && empty($res[0]['num'])) {
        // нет ни одной связки
        return true;// применить ограничение
    }

    return false;
}

/**
 * @param \iSite $site
 * @param string $login
 * @param string $password
 * @return array|bool
 */
function find_user(\iSite $site, $login, $password)
{
    $user = find_user_phpfile($site, $login, $password);
    if ( ! $user)
        $user = find_user_db($site, $login, $password);

    return $user;
}

function find_user_phpfile(\iSite $site, $login, $password)
{
    $user_rec = null;

    $check_lists = array('oivadmins', 'omsuusers');

    foreach ($check_lists as $list_name) {
        $file_path = __DIR__.'/data/'.$list_name.'.php';
        if (file_exists($file_path)) {
            $userlist = (array)include($file_path);
            if (isset($userlist[$login])) {
                $user_rec = $userlist[$login];
                if ( ! empty($user_rec['pass']) && $user_rec['pass'] == $password) {
                    if ( ! isset($user_rec['id']) && isset($user_rec['userid'])) {
                        $user_rec['id'] = $user_rec['userid'];
                    }
                }
                break;
            }
        }
    }

    return $user_rec;
}

function find_user_db(\iSite $site, $login, $password)
{
    $hash='_____';

    //определяем время смены пароля - для понимания, как просчитывать старый хэш
    $query="SELECT passtime FROM users WHERE email IS NOT NULL AND email = '".$login."'";
    if($res=$site->dbquery($query))
    {

        $newpasstime=MakeTimeStampFromStr('30.01.2018');

        $passtime=$res[0]['passtime'];
        if(($passtime-PASS_EXPIRE_TIME) < $newpasstime)
        {
            $hash = get_password_hash($password);
        }
        else
        {
            $hash = get_password_hash($password, 1, $login);
        }
    }

    //$hash = get_password_hash($password);
    $query = 'SELECT * FROM users WHERE email = $1 AND pass = $2 AND status = $3';
    $q_params = array($login , $hash, 1);
    $res = $site->dbquery($query, $q_params);
    if (empty($res))
        return false;
    return $res[0];
}

function get_allowed_materialtypes(\iSite $site, $id)
{
    $materialtypes = array();

    $query = 'SELECT "type_id" FROM "user_rights" WHERE "goal_id" = $1';
    $res = $site->dbquery($query, array($id));

    if ( ! empty($res)) {
        $materialtypes = array();
        foreach($res as $row) {
            $materialtypes[] = $row['type_id'];
            $arr = GetSubTypes($site, $row['type_id'], $materialtypes);
            if (count($arr) > 0) {
                foreach($arr as $val) {
                    $materialtypes[] = $val;
                }
            }
        }
    }

    return $materialtypes;
}