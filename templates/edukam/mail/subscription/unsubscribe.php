<?php
/**
 * @param string $confirmUrl
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

$layout = 'layout';

ob_start();
?>
    <p>Для подтверждения прекращения подписки на новостную рассылку портала Правительства Хабаровского края,
        пройдите по ссылке:<br />
        <a href="<?=htmlspecialchars($confirmUrl)?>"><?=htmlspecialchars($confirmUrl)?></a>
    </p>
    <p>Если вы не собирались прекращать подписку, просто проигнорируйте это письмо.</p>
<?php

$content = ob_get_clean();

$title = 'Прекращение подписки, подтверждение';

include($layout.'.php');