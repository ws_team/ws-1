$(document).ready(function() {

    $('[data-role="gallery-grid"]').masonry({
        itemSelector: '.grid-item',
        columnWidth: 475
    });
});
