<?php

if (($this->autorize->autorized == 1)&&($this->autorize->userrole == 1)) {
    // include_once($this->settings->path.$this->settings->templateurl.'/f_header.php');
    include_once($this->locateTemplate('f_header'));


//page body===========================================================================
?>
    <div class="container container--admin-title">
        <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
    </div>
    <div class="contentblock basemargin">
        <p class="errortext"><?php echo $this->data->errortext;  ?></p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <table>
            <tr>
                <th>Ширина</th>
                <th>Высота</th>
                <th>Вод. знак<font color="red"><sup>*</sup></font></th>
                <th>Действия</th>
            </tr>
            <?php

            if(!isset($imagesizes)){
                $imagesizes=Array();
                $query='SELECT * FROM "files_imagesizes" ORDER BY "width" ASC, "height" ASC';
                if($res=$this->dbquery($query))
                {
                    if(count($res) > 0)
                    {
                        $imagesizes=$res;
                    }
                }
            }

            if(count($imagesizes) > 0)
            {
                foreach($imagesizes as $cache)
                {
                    echo "<tr>
                        <td>$cache[width]</td>
                        <td>$cache[height]</td>
                        <td></td>
                        <td><a href=\"/?menu=imagecaches&action=delete&width=$cache[width]&height=$cache[height]\"><img class='svg delete-icon' src='/images/trash.svg' /></a></td>
                    </tr>";
                }
            }

            ?>
        </table>
        <p>&nbsp;</p><font color="red"><sup>*</sup></font> - находится в разработке
        <p>&nbsp;</p>
        <h2>Добавить размер кэша:</h2>
        <form method="post" action="/?menu=imagecaches">
            Ширина: <input type="text" name="width" class="styler">&nbsp; Высота: <input type="text" name="height" class="styler"><input type="hidden" name="action" value="add">&nbsp; <input type="submit" value="добавить" class="styler">
        </form>


    </div>
<?php

    // include_once($this->settings->path.$this->settings->templateurl.'/f_footer.php');
    include_once($this->locateTemplate('f_footer'));

}