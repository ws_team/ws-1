<?php
/**
 * WPF (what a PHP Framework?) - PHP framework for site creation
 * PHP Version 5.2.0
 * MySQL Version 5.1
 * Version 0.94hk - не совместим с версией без модуля материалов!!!
 * @package wpf
 * @link https://bitbucket.org/desfpc/wpf (good publick version)
 * @link https://bitbucket.org/ws_team/cms-fw-wpf-ws-edition (WhiteSoft company version)
 * @author Sergey Peshalov (desfpc) <desfpc@gmail.com>
 * @copyright 2014 Sergey Peshalov
*/


define('_WPF_', 1);
define('WPF_CLASSROOT', dirname(__FILE__));

require_once('iSite.php');

require_once('MFile.php');
require_once('iFile.php');
require_once('getWSDL.php');
require_once('iEmail.php');
require_once('iSMS.php');

require_once('iMessage.php');
require_once('iPay.php');
require_once('watermark.php');

//подменяем функцию обработки ошибок PHP, что бы всунуть их в свойства ошибки и нотиса фрамеворка
$phperrors='';
$phpfatalerrors='';

// функция обработки ошибок
function myErrorHandler($errno, $errstr, $errfile, $errline)
{
    global $phperrors;
    global $phpfatalerrors;

    if (!(error_reporting() & $errno)) {
        // Этот код ошибки не включен в error_reporting
        return;
    }

    switch ($errno) {
        case E_USER_ERROR:
            $phpfatalerrors.= "<b>ERROR:</b> [$errno] $errstr<br />\n";
            $phpfatalerrors.= "  Фатальная ошибка в строке $errline файла $errfile";
            $phpfatalerrors.= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            $phpfatalerrors.= "Завершение работы...<br />\n";
            if (defined('WPF_DEBUG') && constant('WPF_DEBUG'))
                echo $phpfatalerrors;
            exit(1);
            break;

        case E_USER_WARNING:
            $phperrors.= "<b>WARNING:</b> [$errno] $errstr<br />\n";
            break;

        case E_USER_NOTICE:
            $phperrors.= "<b>NOTICE:</b> [$errno] $errstr<br />\n";
            break;

        default:
            $phperrors.= "Неизвестная ошибка: [$errno] $errstr; файл: $errfile; строка: $errline<br />\n";
            break;
    }

    if (defined('WPF_DEBUG') && constant('WPF_DEBUG'))
        echo $phperrors;

    /* Не запускаем внутренний обработчик ошибок PHP */
    return true;
}

$old_error_handler = set_error_handler("myErrorHandler");
