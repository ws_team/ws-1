<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$query = array();

$query[] = 'CREATE SEQUENCE "material_types_id_seq"';
$query[] = 'SELECT setval(\'material_types_id_seq\', (SELECT MAX("id") + 10 FROM "material_types"))';
$query[] = 'ALTER TABLE material_types ALTER COLUMN "id" SET DEFAULT nextval(\'material_types_id_seq\')';
$query[] = 'ALTER SEQUENCE "material_types_id_seq" OWNED BY "material_types"."id"';

$query[] = '.clear';
$query[] = 'CREATE SEQUENCE "materials_id_seq"';
$query[] = 'SELECT setval(\'materials_id_seq\', (SELECT MAX("id") + 25 FROM "materials"))';
$query[] = 'ALTER TABLE materials ALTER COLUMN "id" SET DEFAULT nextval(\'materials_id_seq\')';
$query[] = 'ALTER SEQUENCE materials_id_seq OWNED BY "materials"."id"';

$query[] = '.clear';
$query[] =  'CREATE SEQUENCE "extra_mattypes_id_seq"';
$query[] = 'SELECT setval(\'extra_mattypes_id_seq\', (SELECT MAX("id") + 10 FROM "extra_mattypes"))';
$query[] = 'ALTER TABLE extra_mattypes ALTER COLUMN "id" SET DEFAULT nextval(\'extra_mattypes_id_seq\')';
$query[] = 'ALTER SEQUENCE extra_mattypes_id_seq OWNED BY "extra_mattypes"."id"';

$query[] = '.clear';
$query[] = 'CREATE SEQUENCE "extra_selectors_id_seq"';
$query[] = 'SELECT setval(\'extra_selectors_id_seq\', (SELECT MAX("id") + 10 FROM "extra_selectors"))';
$query[] = 'ALTER TABLE "extra_selectors" ALTER COLUMN "id" SET DEFAULT nextval(\'extra_selectors_id_seq\')';
$query[] = 'ALTER SEQUENCE "extra_selectors_id_seq" OWNED BY "extra_selectors"."id"';

$skip = false;
foreach ($query as $q) {
    if ($q == '.clear') {
        $skip = false;
        continue;
    }
    if ($skip)
        continue;

    try {
        $this->dbquery($q);
    } catch (\Exception $ex) {

    }
    $this->resetError();
}
