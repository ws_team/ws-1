<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

defined('_WPF_') or die();

global
    $status_selector,
    $language_selector,
    $type_id,
    $extratypes,
    $selectors,
    $material;

$action_url = $this->getRouteUrl(array(
    'editmaterials',
    'imenu' => $material['type_id'],
    'id' => $this->values->id,
    'action' => 'edit',
));

//массив шаблонов, в которых нужна необходимость скрывать виджет фото
$hidevijetarr=Array('news_detail','text','textmat','text_new');

//проверяем, выводить или нет кнопку скрытия шаблона (проверяем шаблон детализации типа материала)
$photovidjethide=false;
$vidjets=Array();
$tempquery="SELECT t.file_name FROM standart_shablons t, material_types mt WHERE t.id = mt.template AND mt.id = ".$material['type_id'];
if($tempres=$this->dbquery($tempquery))
{
	if(is_array($tempres) && count($tempres) > 0)
	{
		$template=$tempres[0]['file_name'];

		if(in_array($template, $hidevijetarr)){
			$photovidjethide=true;

			$vidjets['matdetailed_photovideo']=0;

			//проверяем наличае поля в таблице материалов
			$queryvj="SELECT count(vidjet) vc FROM material_vijsettings";
			if($resvj = $this->dbquery($queryvj))
			{

			}
			else
			{
				//создаём таблички для виджетов
				$query1="CREATE TABLE material_vijsettings
(
  matid bigint NOT NULL,
  vidjet character varying(31) NOT NULL,
  value character varying(511),
  CONSTRAINT material_vijsettings_pkey PRIMARY KEY (matid, vidjet)
);";
				$query2="CREATE INDEX material_vijsettings_matid_idx
  ON material_vijsettings
  USING btree
  (matid);";

				$query3="CREATE INDEX material_vijsettings_matid_vidjet_idx
  ON material_vijsettings
  USING btree
  (matid, vidjet COLLATE pg_catalog.\"default\");";


				$query4="CREATE INDEX material_vijsettings_vidjet_idx
  ON material_vijsettings
  USING btree
  (vidjet COLLATE pg_catalog.\"default\");";

				$do1=$this->dbquery($query1);
				$do2=$this->dbquery($query2);
				$do3=$this->dbquery($query3);
				$do4=$this->dbquery($query4);
			}

			$queryvj="SELECT value FROM material_vijsettings WHERE matid = $material[id] AND vidjet = 'matdetailed_photovideo'";
			if($resvj = $this->dbquery($queryvj))
			{
				$vidjets['matdetailed_photovideo']=$resvj[0]['value'];
			}

		}

	}
}

if ( ! empty($material['type_id']) && $material['type_id'] != $this->values->imenu) {
    $this->redirect($action_url);
}

include($this->partial('form/main'));

?>
    <?php

    include('connections.php');
    include('attachments.php');

    if($this->data->mattype['hide_contacts'] != '1'){
    ?>
        <p>&nbsp;</p><p>&nbsp;</p>
        <hr />
        <p>&nbsp;</p>
        <h2>Контакты:</h2>

    <table width="100%" class="table table-striped table-hover table-condensed table-bootstrap">
        <tr>
            <th>Адрес</th>
            <th width="24">Дом</th>
            <th width="24">Оф.</th>
            <th>Тел.</th>
            <th>Факс</th>
            <th>Email</th>
            <!--<th width="100">Расписания</th>-->
            <th width="100">Действия</th>
        </tr>
        <?php
        if(count($material['contacts']) > 0)
        {
            foreach($material['contacts'] as $contact)
            {
                $schedules='';
                if(count($contact['schedules']) > 0)
                {
                    foreach($contact['schedules'] as $schedule)
                    {
                        $schedules.="<p style='font-size: 9px;'>$schedule[name]: $schedule[content]</p>";
                    }
                }

                ?><tr>
                <td valign=top><?=$contact['addr']?></td>
                <td valign=top><?=$contact['building']?></td>
                <td valign=top><?=$contact['office']?></td>
                <td valign=top><?=$contact['tel']?></td>
                <td valign=top><?=$contact['fax']?></td>
                <td valign=top><?=$contact['email']?></td>
                <!--<td valign=top>
                    <?=$schedules?>
                    <button onclick="showAddSheduleForm(<?=$contact['id']?>);" class='styler' onclick=''>Добавить</button>
                    <div class='nullblock'>
                        <div id='addshedule_<?=$contact['id']?>' class='addshedule'>
                            <form method="post" enctype="multipart/form-data" action="/?menu=editmaterials&amp;imenu=<?php echo $this->values->imenu; ?>&amp;id=<?php echo $this->values->id; ?>&amp;action=edit">
                                <table width='100%'>
                                    <tr>
                                        <th colspan='2'>Доб. расписание:</th>
                                    </tr>
                                    <tr>
                                        <th>Название (дни):</th>
                                        <td><input type='text' name='sname' style='styler'></td>
                                    </tr>
                                    <tr>
                                        <th>Значение (время):</th>
                                        <td><input type='text' name='scontent' style='styler'></td>
                                    </tr>
                                </table>
                                <input type='hidden' name=''>
                                <input type="hidden" name="doaddshed" value="<?=$contact['id']?>">
                                <input type="submit" value="Добавить" class="styler" style="margin: 3px;">
                            </form>
                        </div>
                    </div>
                </td>-->
                <td>
                    <!--<a onclick="doDelCont(<?=$contact['id']?>, <?=$this->values->imenu?>, <?=$this->values->id?>);">
                        <img class='svg delete-icon' src='/images/trash.svg' />
                    </a>&nbsp;<a
                        onclick="editContForm(<?=$contact['id']?>, '<?=$contact['addr']?>',  '<?=$contact['tel']?>', '<?=$contact['fax']?>', '<?=$contact['email']?>', '<?=$contact['building']?>', '<?=$contact['office']?>');">
                        <img class='svg edit-icon' src='/images/edit.svg' />
                    </a>
-->
                    <button type="button" class="btn btn-danger showtooltip small" data-toggle="tooltip"
                            data-placement="bottom" title="Удалить контакт"
                            onclick="if(checkEditInputs()){ doDelCont(<?=$contact['id']?>, <?=$this->values->imenu?>, <?=$this->values->id?>) } else{ showEditWarning(event, this) }">
                        <span class="glyphicon glyphicon-remove-sign"></span>
                    </button>&nbsp;<button
                            onclick="if(checkEditInputs()){ editContForm(<?=$contact['id']?>, '<?=$contact['addr']?>',  '<?=$contact['tel']?>', '<?=$contact['fax']?>', '<?=$contact['email']?>', '<?=$contact['building']?>', '<?=$contact['office']?>') } else{ showEditWarning(event, this) }"
                            type="button" class="btn btn-default showtooltip small" data-toggle="tooltip"
                            data-placement="bottom" title="Форма редактирования контакта">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </button>
                </td>
                </tr><?php
            }
        } ?>
    </table><p>&nbsp;</p>
        <button
                type="button"
                onclick='if(checkEditInputs()){ addContForm(); } else{ showEditWarning(event, this) }'
                class="btn btn-success showtooltip"
                data-toggle="tooltip"
                data-placement="bottom"
                data-original-title="Добавить новый контакт">
            <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;Добавить новый контакт
        </button>


    <p>&nbsp;</p>
    <div style="display: none;">
        <div id="addcontform">
            <form method="post" enctype="multipart/form-data" action="/?menu=editmaterials&amp;imenu=<?php echo $this->values->imenu; ?>&amp;id=<?php echo $this->values->id; ?>&amp;action=edit">
                <table>
                    <tr>
                        <th colspan="2">Добавить контакт:</th>
                    </tr>
                    <tr style="display: none;">
                        <th>Нас. пункт:</th>
                        <td><select name="city" class="styler"><?php echo GetCityOptions($this,''); ?></select></td>
                    </tr>
                    <tr style="display: none;">
                        <th >Координаты:</th>
                        <td>Широта:&nbsp;<input type="text" name="latitude" style="width:50px;" class="styler">&nbsp;&nbsp;Долгота:&nbsp;<input type="text" name="longitude" style="width:50px;" class="styler"></td>
                    </tr>
                    <tr>
                        <th>Адрес:</th>
                        <td><input type="text" class="styler" name="addr"></td>
                    </tr>
                    <tr>
                        <th>Телефон:</th>
                        <td><input type="text" class="styler" name="tel"></td>
                    </tr>
                    <tr>
                        <th>Факс:</th>
                        <td><input type="text" class="styler" name="fax"></td>
                    </tr>
                    <tr>
                        <th>e-mail:</th>
                        <td><input type="text" class="styler" name="email"></td>
                    </tr>
                    <tr>
                        <th>Дом:</th>
                        <td><input type="text" class="styler" name="building"></td>
                    </tr>
                    <tr>
                        <th>Офис:</th>
                        <td><input type="text" class="styler" name="office"></td>
                    </tr>
                </table>
                <input type="hidden" name="doaddcont" value="1">
                <input type="submit" value="Добавить" class="styler" style="margin-top: 3px;">
            </form>
        </div>
        <div id="editcontform">
            <form method="post" enctype="multipart/form-data" action="/?menu=editmaterials&imenu=<?php echo $this->values->imenu; ?>&id=<?php echo $this->values->id; ?>&action=edit">
                <table>
                    <tr>
                        <th colspan="2">Изменить контакт:<input type="hidden" name="acont" id="edit_cont"></th>
                    </tr>
                    <tr>
                        <th>Адрес:</th>
                        <td><input type="text" id="edit_addr" class="styler" name="addr"></td>
                    </tr>
                    <tr>
                        <th>Телефон:</th>
                        <td><input type="text" id="edit_tel" class="styler" name="tel"></td>
                    </tr>
                    <tr>
                        <th>Факс:</th>
                        <td><input type="text" id="edit_fax" class="styler" name="fax"></td>
                    </tr>
                    <tr>
                        <th>e-mail:</th>
                        <td><input type="text" id="edit_email" class="styler" name="email"></td>
                    </tr>
                    <tr>
                        <th>Дом:</th>
                        <td><input type="text" id="edit_building" class="styler" name="building"></td>
                    </tr>
                    <tr>
                        <th>Офис:</th>
                        <td><input type="text" id="edit_office" class="styler" name="office"></td>
                    </tr>
                </table>
                <input type="hidden" name="doeditcont" value="1"><input type="submit" value="Изменить" class="styler" style="margin-top: 3px;">
            </form>
        </div>
    </div>
<?php
}

?>