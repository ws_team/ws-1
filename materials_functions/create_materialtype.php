<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


function createMaterialTypeCheckPrecondition(\iSite $site, $name, $name_en, $id=NULL, $id_char='', $parent_id=NULL)
{

    $err = false;
    $errtext = array();

    //проверки введенных данных
    //проверяем на наличае такого парент_иди

    if ( ! empty($parent_id)) {
        $query = 'SELECT count("id") cid FROM "material_types" WHERE "id" = $1';
        $res = $site->dbquery($query, array($parent_id));

        if (empty($res)) {
            $err = true;
            $errtext[] = "не удалась проверка родительского типа материала";
        } elseif (empty($res[0]['cid'])) {
            $err = true;
            $errtext[] = ' - ошибка: не существует переданный родительский тип материала!';
        }
    }

    //проверяем наименование
    if (empty($name)) {
        $err = true;
        $errtext[] = " - ошибка: не введено название типа материала";
    }

    //получаем колличество таких текстовых id
    $query = 'SELECT count("id") cid FROM "material_types" WHERE "id_char" = $1';
    $q_params = array($id_char);

    if ( ! empty($id))
    {
        $q_params[] = $id;
        $query.=' AND "id" <> $'.count($q_params);
    }

    $res = $site->dbquery($query, $q_params);

    /*krumo($q_params);
    exit;*/

    if ( ! empty($res)) {
        if($res[0]['cid'] > 0) {
            $err = true;
            $errtext[] = " - ошибка: буквенный идентификатор типа материала уже используется";
        }
    } else {
        $err = true;
        $errtext[] = " - ошибка: не удалась проверка буквенного идентификатора типа материала";
    }

    if ($name_en == '') {
        $errtext[] = " - замечание: не введено английское название типа материала";
    }

    if ( ! empty($id)) {
        $query = 'SELECT COUNT("id") FROM "material_types" WHERE "id" = $1';
        $q_params = array($id);
        $res = $site->dbquery($query, $q_params);
        if (empty($res)) {
            $err = true;
            $errtext[] = ' - ошибка: не удалась проверка идентификатора типа материала';
        }
    }

    return array($err, $errtext);
}

//создание/изменение типа материала
function createMaterialType(\iSite $site, $name, $name_en, $id=NULL, $id_char='', $parent_id=NULL, $ordernum=0, $showinmenu=0)
{
    $site->logWrite(LOG_DEBUG, __FUNCTION__, __LINE__);
    $site->getPostValues(Array(
        'name',
        'en_name',
        'id_char',
        'parent_id',
        'parent_name',
        'ordernum',
        'showinmenu',
        'valuetype',
        'valuemattype',
        'type_id',

        'hide_anons',
        'hide_content',
        'hide_date_event',
        'hide_tags',
        'hide_parent_id',
        'hide_extra_number',
        'hide_contacts',
        'hide_lang',
        'hide_files',

        'hide_conmat',
        'template_list',
        'template',
        'perpage',
        'mat_as_type',
        'hide_ordernum'
    ));

    //проверяем/формируем текстовый id
    if ($id_char == '') {
        $id_char = mb_transliterate($name);
        $id_char = str_replace(' ','_',$id_char);
    }

    $args = func_get_args();

    list($err, $errtext) = call_user_func_array('createMaterialTypeCheckPrecondition', $args);

    if (empty($ordernum))
        $ordernum = 0;

    if (empty($parent_id))
        $parent_id = null;

    $is_new = empty($id);

	//проверяем на дублирование буквенного id материала
	if($is_new)
	{
		$querymt="SELECT count(id) cid FROM material_types WHERE id_char = '".$id_char."'";
	}
	else
	{
		$querymt="SELECT count(id) cid FROM material_types WHERE id_char = '".$id_char."' AND id <> $id";
	}

	if($resmt = $site->dbquery($querymt))
	{
		$cid=$resmt[0]['cid'];
		if($cid > 0)
		{
			$id_char=$id_char.time();
		}
	}else{
		$id_char=$id_char.time();
	}


    //krumo($is_new);

    if (empty($err)) {
        //если передан ID типа - изменяем тип материала
        if ( ! $is_new) {
            $err = false === updateMaterialType($site, $name, $name_en, $id, $id_char, $parent_id, $ordernum, $showinmenu);
        } else {// добавляем новый тип материала
            $site->logWrite(LOG_DEBUG, __FUNCTION__, __LINE__);
            $id = insertMaterialType($site, $name, $name_en, $id_char, $parent_id, $ordernum, $showinmenu);

            //krumo($id);

            $err = $id === false;
        }

        if (empty($err))
            $errtext[] = " - информация успешно изменена";
        else
            $errtext[] = " - ошибка: не удалось изменить информацию в БД";
    }

    $errtext = implode("\n", $errtext);

    $success = $err ? false : $is_new ? $id : true;

    return array($success, $errtext);
}

function insertMaterialType(\iSite $site, $name, $name_en, $id_char='', $parent_id=NULL, $ordernum=0, $showinmenu=0)
{

    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'id_char='.$id_char);

    //$res = $site->dbquery('SELECT nextval(\'material_types_id_seq\') id');


    //определяем новый тип - выпиливаем нафиг секвенции - они судя по всему есть не везде
    $idquery="SELECT max(id) mid FROM material_types";
    if($res=$site->dbquery($idquery))
    {
        if(is_array($res) && count($res) > 0)
        {
            $id=$res[0]['mid']+1;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }

    //krumo('newid',$id);

    $parent_id = intval($parent_id);
    $ordernum = intval($ordernum);

    //проверем тип материала на уникальность


    $columns = array(
        'id' => $id,
        'id_char' => $id_char,
        'name' => $name,
        'parent_id' => $parent_id,
        'ordernum' => $ordernum,
        'showinmenu' => $showinmenu,
    );

    if (is_scalar($name_en)) {
        $columns['en_name'] = $name_en;
    } else {
        foreach ($name_en as $attr => $val)
            $columns[$attr] = $val;
    }

    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'inserting '.serialize($columns));

    if (DbInsert($site, 'material_types', $columns)) {
        $site->callHook('materialtypesave', array(
            'id' => $id,
            'attributes' => $columns,
        ));
        $site->callHook('materialtypecreate', array(
            'id' => $id,
            'attributes' => $columns,
        ));

        $site->logWrite(LOG_DEBUG, __FUNCTION__, 'inserted id='.$id);
        return $id;
    }

    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'failed to insert');

    return false;
}

function updateMaterialType(\iSite $site, $name, $name_en, $id=NULL, $id_char='', $parent_id=NULL, $ordernum=0, $showinmenu=0)
{
    $zero_attrs = array(
        'hide_anons',
        'hide_date_event',
        'hide_content',
        'hide_tags',
        'hide_parent_id',
        'hide_contacts',
        'hide_extra_number',
        'hide_lang',
        'hide_files',
        'hide_conmat',
        'hide_ordernum',
        'mat_as_type',
    );

    foreach ($zero_attrs as $attr)
        if (empty($site->values->$attr))
            $site->values->$attr = 0;

    $site->values->perpage = (int)$site->values->perpage;
    $ordernum = (int)$ordernum;
    $parent_id = (int)$parent_id;
    $showinmenu = (int)$showinmenu;

    $columns = array(
        'name' => $name,
        'id_char' => $id_char,
        'parent_id' => intval($parent_id),
        'ordernum' => intval($ordernum),
        'showinmenu' => intval($showinmenu),
    );

    if (is_scalar($name_en)) {
        $columns['en_name'] = $name_en;
    } else {
        foreach ($name_en as $attr => $val)
            $columns[$attr] = $val;
    }

    $copy_input = $zero_attrs;
    array_push($copy_input, 'template_list', 'template', 'perpage');

    foreach ($copy_input as $attr) {
        if (isset($site->values->$attr))
            $columns[$attr] = $site->values->$attr;
    }

    $ret =  (bool)DbUpdate($site, 'material_types', $columns, array('id' => $id));
    if ($ret) {
        $site->callHook('materialtypesave', array(
            'id' => $id,
            'attributes' => $columns,
        ));
        $site->callHook('materialtypechange', array(
            'id' => $id,
            'attributes' => $columns,
        ));
    }

    return true;
}
