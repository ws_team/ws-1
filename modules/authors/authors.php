<?php
/**
 * @var \iSite $this
 */


defined('_WPF_') or die();

global $authors;

$this->data->errortext='';

$requiredFields = array(
    'surname' => 'Фамилия',
    'first_name' => 'Имя',
    'second_name' => 'Отчество',
    'unit_id' => 'Подразделение'
);

$checkRequiredFields = function($site) use ($requiredFields){
    $result = array(
        'success' => true,
        'emptyFields' => '',
    );

    foreach ($requiredFields as $field => $label){
        if(!isset($site->values->$field) || $site->values->$field == ''){
            $result['emptyFields'] .= $label . ', ';
        }
    }

    if($result['emptyFields'] != ''){
        $result['success'] = false;
        $result['emptyFields'] = substr($result['emptyFields'], 0, -2);
    }

    return $result;
};

function checkFieldValueInDB($site, $fieldName, $fieldValue, $tableName){

    $query = "SELECT $fieldName FROM $tableName WHERE $fieldName = $fieldValue";

    $res = $site->dbquery($query);

    if($res){
        return true;
    }
    else{
        return false;
    }
}

if (!empty($this->values->action)){
    $controller_script = dirname(__FILE__).'/action/'.
        str_replace(array('.', '/', '~', '*', '?'), '', $this->values->action).'.php';
    if (file_exists($controller_script)) {
        include($controller_script);
    } else {
        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
        exit;
    }
}

$authors = getAuthorsList($this);

$this->data->iH1 = 'Администрирование авторов';

