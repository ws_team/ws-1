<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

defined('_WPF_') or die();

$query = array();

$query[] = <<<EOS
ALTER TABLE subscribers ADD "token_subscribe" CHARACTER VARYING(255) 
EOS;

foreach ($query as $q)
    $this->dbquery($q);