<?php

function getAuthorsStat($site){

    $site->GetPostValues(Array('startdate', 'enddate', 'unit_id', 'author_id'));

    if(($site->values->startdate == ''))
    {
        $site->values->startdate = date('d.m.Y');

        $endtime = MakeTimeStampFromStr($site->values->startdate) + 24*60*60;

        $site->values->enddate = date('d.m.Y',$endtime);
    }

    if($site->values->unit_id){
        $unitCondition = 'AND units.id = ' . $site->values->unit_id;
    }
    else{
        $unitCondition = '';
    }

    if($site->values->author_id){
        $authorCondition = 'AND authors.id = ' . $site->values->author_id;
    }
    else{
        $authorCondition = '';
    }

    $query = <<<end
SELECT 
materials.id as material_id,
materials.type_id as material_type_id,
materials.name as material_name,
materials.date_add as material_date_add,
material_types.mat_as_type as material_as_type,
authors.surname as author_surname,
authors.first_name as author_first_name,
authors.second_name as author_second_name,
authors.unit as author_unit
FROM materials
INNER JOIN
(
SELECT authors.id, authors.surname, authors.first_name, authors.second_name, units.name as unit 
FROM authors, units
WHERE authors.unit_id = units.id $unitCondition $authorCondition
) as authors
ON authors.id = materials.author_id
INNER JOIN material_types on material_types.id = materials.type_id
WHERE materials.date_add BETWEEN TO_DATE('{$site->values->startdate}','DD.MM.YYYY') AND TO_DATE('{$site->values->enddate}','DD.MM.YYYY')
ORDER BY materials.date_add DESC
end;

    $statData = $site->dbquery($query);

    if($statData){

        foreach ($statData as $key => $value){

            $statData[$key]['material_date_add'] = date('d.m.Y H:i', strtotime($statData[$key]['material_date_add']));

            if ($statData[$key]['material_as_type'] == 1) {
                $statData[$key]['material_url'] = GetMaterialTypeUrl($site, $statData[$key]['material_type_id'], $statData[$key]['material_id'], $statData[$key]['material_type_id']);
            } else {
                $statData[$key]['material_url'] = GetMaterialTypeUrl($site, $statData[$key]['material_type_id'], $statData[$key]['material_id'], $statData[$key]['material_type_id']).'/'.$statData[$key]['material_id'];
            }

            $statData[$key]['material_url'] = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') .  $_SERVER['SERVER_NAME'] . $statData[$key]['material_url'];
        }
    }

    return $statData;

}