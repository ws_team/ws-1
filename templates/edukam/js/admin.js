//var adduser='';

function doDelExtraParam(id, type)
{
    if(confirm('Вы уверены, что следует удалить характеристику?'))
    {
        document.location.href='/?menu=material_types&imenu=edit&id='+type+'&action=delextratype&iid='+id;
    }
}

function ChangeOrdernumToConMat(parentid, childid, imenu, id)
{
    var ordernum=$('#ordernum_'+childid).val();

    /*$('#parentid').val(parentid);
    $('#childid').val(childid);
    $('#cordernum').val(ordernum);



    $('#ChangeOrdernumToConMatForm').submit();*/

    document.location.href='/?menu=editmaterials&imenu='+imenu+'&id='+id+'&action=edit&ChangeOrdernumToConMat=1&parentid='+parentid+'&childid='+childid+'&cordernum='+ordernum;

}

//форма редактирования стандартного шаблона
function showEditStandartTemplateForm(id, name, filename, type, content)
{

    $('#shab_name').val(name);
    $('#shab_file_name').val(filename);
    $('#shab_materiallist').val(type);
    $('#shab_content').html(content);
    $('#shab_id').val(id);

    $.colorbox({width:"600px", height:"450px", inline:true, href:"#STShabEditForm"});
    $('.styler').trigger('refresh');
}

function DoAddMatToUser()
{
    var type=$('#mtselect').val();
    var adduser= $('#adduser').val();
    document.location.href='/?menu=users&action=addmattype&user='+adduser+'&type='+type;
}

function addMatToUser(id)
{
    $('#adduser').val(id);
    $.colorbox({width:"400px", height:"350px", inline:true, href:"#mtselectblock"});
}

function deleteMatType(id)
{
    if(confirm('Вы уверены, что следует удалить тип материала?'))
    {
        document.location.href='/?menu=material_types&action=del&id='+id;
    }
}

function doDelCont(cont, imenu, id)
{
    if(confirm('Вы уверены, что следует удалить контакт?'))
    {
        document.location.href='/?menu=editmaterials&imenu='+imenu+'&id='+id+'&action=edit&dcont='+cont+'#contacts';
    }
}

function unconnectMat(mat, imenu, id)
{
    if(confirm('Вы уверены, что следует отвязать материал?'))
    {
        document.location.href='/?menu=editmaterials&imenu='+imenu+'&id='+id+'&action=edit&mat='+mat+'#materials';
    }
}

function deleteFilePar(pid, imenu, id)
{

    var ctext='Вы уверены, что следует удалить раздел?';

    if(confirm(ctext))
    {
        //<a href='/?menu=editmaterials&imenu=".$this->values->imenu."&id=".$this->values->id."&action=edit&dfile=$file[id]'>

        document.location.href='/?menu=editmaterials&imenu='+imenu+'&id='+id+'&action=edit&dpar='+pid+'#files';
    }
}

function deleteMatFile(fid, ccnt, imenu, id)
{

    ccnt=ccnt*1;

    var ctext='Вы уверены, что следует ';

    if(ccnt > 1)
    {
        ctext+='отвязать файл от материала?';
    }
    else
    {
        ctext+='удалить файл?';
    }

    if(confirm(ctext))
    {
        //<a href='/?menu=editmaterials&imenu=".$this->values->imenu."&id=".$this->values->id."&action=edit&dfile=$file[id]'>

        document.location.href='/?menu=editmaterials&imenu='+imenu+'&id='+id+'&action=edit&dfile='+fid+'#files';
    }
}

function clearMatCache(url)
{
    document.location.href=url;
}

function editFileParForm(par_id, par_name, par_order)
{

    $('#fileparid').val(par_id);
    $('#filepar').val(par_name);
    $('#fileparorder').val(par_order);

    $.colorbox({width:"928px", inline:true, href:"#editfileparform"});
    $('.styler').trigger('refresh');

}

function editFileForm(file_id, file_typename, file_name, file_name_en, file_content, file_date_event, file_author, file_ordernum, par_id, name_off)
{

    $('#file_id').val(file_id);
    $('#file_typename').val(file_typename);
    $('#file_name').val(file_name);
    $('#file_name_en').val(file_name_en);
    $('#file_content').val(file_content);
    $('#file_date_event').val(file_date_event);
    $('#file_author').val(file_author);
    $('#file_ordernum').val(file_ordernum);
    $("#file_par_sel option[value='" + par_id + "']").prop("selected",true);

    if(name_off == '1')
    {
        $('#name_off').attr('checked','checked');
        $('#name_off').checked = true;
    }
    else
    {
        $('#name_off').removeAttr('checked');
        $('#name_off').checked = false;
    }



    $.colorbox({width:"928px", inline:true, href:"#editfileform"});
    $('.styler').trigger('refresh');

}

function showhidefileparams()
{
    var ftype=$('#addfile_type').val();

    if((ftype == '2')||(ftype == '5'))
    {
        $('#addfile_fileinput').attr('accept','.mp4');
        $('#addfile_size').val('53477376');
        $('#addfile_url').show();
        $('#addfile_file').show();
        $('#addfile_date').hide();
        $('#addfile_author').hide();
    }
    else
    {
        //alert(ftype+'- яху!');
        $('#addfile_url').hide();
        $('#addfile_file').show();
        $('#addfile_date').show();
        $('#addfile_author').show();
        $('#addfile_par').show();

    }

    if(ftype == '1')
    {
        $('#addfile_fileinput').attr('accept','.png, .gif, .jpg, .jpeg, .bmp');
        $('#addfile_size').val('1048576');
        $('#addfile_fileinput').trigger('refresh');

    }
    if(ftype == '3')
    {
        $('#addfile_fileinput').attr('accept','.mp3');
        $('#addfile_size').val('10485760');
        $('#addfile_fileinput').trigger('refresh');

    }
    if(ftype == '4')
    {
        $('#addfile_fileinput').attr('accept','.doc, .docx, .xls, .xlsx, .ppt, .pptx, .pdf, .zip, .rar, .rtf, .mp4, .m4v, .mov');
        $('#addfile_size').val('20571520');
        $('#addfile_fileinput').trigger('refresh');

    }
}

function addFileForm()
{
    var $addFileFormForm = $('#addfileformform'),
        nextOrderNum     = $('[data-role="materialFilesList"]').find('tr').length;

    $addFileFormForm[0].reset();

    $addFileFormForm.find(':input[name="ordernum"]').val(nextOrderNum);

    $('#addfile_type').val('1');
    showhidefileparams();
    $.colorbox({width:"928px", height:"570px", inline:true, href:"#addfileform"});
    $('.styler').trigger('refresh');
}

function addFileParForm(){

    $.colorbox({width:"928px", inline:true, href:"#addfileparform"});
    $('.styler').trigger('refresh');

}

function connectFileForm()
{
    $.colorbox({width:"928px", inline:true, href:"#connectfileform"});
    $('.styler').trigger('refresh');
}

function addContForm()
{
    $.colorbox({width:"928px", inline:true, href:"#addcontform"});
    $('.styler').trigger('refresh');
}

function editContForm(acont, addr, tel, fax, email, building, office){

    $('#edit_addr').val(addr);
    $('#edit_tel').val(tel);
    $('#edit_fax').val(fax);
    $('#edit_email').val(email);
    $('#edit_building').val(building);
    $('#edit_office').val(office);
    $('#edit_cont').val(acont);

    $.colorbox({width:"928px", inline:true, href:"#editcontform"});
    $('.styler').trigger('refresh');
}

function connMatForm()
{
    if (window.$) {
        $.colorbox({width:"928px", height:"400px", inline:true, href:"#connmatform"});
        $('.styler').trigger('refresh');
    }
}

function reloadMaterialSelectOptions(query)
{
    var inputType = $('#mattypeforconnect');
    var tid = inputType.val();

    query = query || '';

    window.console&&console.log('[admin] reloadMaterialSelectOptions(%s)', query);

    //получаем материалы выбранного типа
    $.ajax({
        type: 'POST',
        url: '/',
        async: false,
        data: "menu=materials_ajax&imenu=giveMeAMaterialsByType&id=" + tid + '&query=' + encodeURIComponent(query),
        success: function(data){
            var select = $('#matforconnect');
            if (data != -1)
            {
                select.html(data);
            }
            else
            {
                select.html('');
            }
            select.trigger('refresh');
        }
    });
}

function MaterialEditSelectType()
{
    window.console&&console.log('[admin] MaterialEditSelectType');
    reloadMaterialSelectOptions();
}

function MaterialEditSelectTypeFile()
{
    //получаем id выбранного типа
    var tid = $('#mattypeforconnectfile').val();

    //получаем материалы выбранного типа
    $.ajax({
        type: 'POST',
        url: '/',
        async: false,
        data: 'menu=materials_ajax&imenu=giveMeAMaterialsByType&id=' + tid,
        success: function(data) {
            if (data != -1) {
                $('#matforconnectfile').html(data);
            } else {
                $('#matforconnectfile').html('');
            }
            $('#matforconnectfile').trigger('refresh');
            MaterialEditSelectMatFile();
        }
    });
}

function MaterialEditSelectMatFile()
{
    //получаем id выбранного типа
    var tid = $('#matforconnectfile').val();

    //получаем материалы выбранного типа
    $.ajax({
        type: "POST",
        url: '/',
        async: false,
        data: "menu=materials_ajax&imenu=giveMeAFilesByMaterial&id="+tid,
        success: function(data){

            if (data != -1)
            {
                $('#fileforconnect').html(data);
            }
            else
            {
                $('#fileforconnect').html('');
            }
            $('#fileforconnect').trigger('refresh');
        }
    });
}

function showAddSheduleForm(id) {

    if($('#addshedule_'+id).css('display') == 'none')
    {
        $('.addshedule').hide();
        $('#addshedule_'+id).show();
    }
    else
    {
        $('#addshedule_'+id).hide();
    }
}


function performClick(elemId) {
    var elem = document.getElementById(elemId);
    if(elem && document.createEvent) {
        var evt = document.createEvent("MouseEvents");
        evt.initEvent("click", true, false);
        elem.dispatchEvent(evt);
    }
}

function deleteFile(fileid) {
    allfilesArr[fileid].url = 'deleted';
    $('#fileBlock'+fileid).hide(300);
}


var allfiles=0;
var allfilesArr=Array();


function doAddGroupFiles(id)
{
    var files = '';

    allfilesArr.forEach(function (item, i, arr) {

        if(item.url != 'deleted'){
            files+=item.url+';';
        }

    });

    if(files != ''){

        $('#AddGroupFilesBtn').hide();
        $('#progress').show();

        //files=JSON.stringify(files);

        //ajax запихуиваем загруженные файлы в материал стандартными функциями
        $.ajax({
            type: "POST",
            url: '/',
            async: true,
            data: "menu=ajax&imenu=doAddGroupImages&id="+id+"&files="+files,
            success: function(data){

                if(data == '1')
                {
                    //success
                }
                else
                {
                    console.log('Ошибка при добавлении изображений');
                    console.log(data);
                }
            },
            error: function(data){
                console.log('Непредвиденная ошибка');
            },
            complete:function (data) {
                window.location.reload();
            }

        });

    }

}

$(function () {

    function decimalAdjust(type, value, exp) {
        // Если степень не определена, либо равна нулю...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // Если значение не является числом, либо степень не является целым числом...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Сдвиг разрядов
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Обратный сдвиг
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    if (!Math.round10) {
        Math.round10 = function(value, exp) {
            return decimalAdjust('round', value, exp);
        };
    }

    $('#fileupload').fileupload({
        dropZone: $('#dropzone'),
        dataType: 'json',
        done: function (e, data) {

            //alert(data);

            var rcnt=0;

            $.each(data.result.files, function (index, file) {

                ++rcnt;

                ++allfiles;
                allfilesArr[allfiles]=file;

                //$('<p/>').text(file.name).appendTo(document.body);
                if(file.thumbnailUrl != '' && file.thumbnailUrl != undefined){
                    fstyle=" style='background-image:url("+file.thumbnailUrl+"); background-size: 100%;'";
                    fname='';
                }else{
                    fstyle='';
                    //fname=file.name;

                    //разрешение файла
                    var a = file.name.split('.');
                    var ext = a[a.length-1];


                    //размер файла
                    var size = file.size;

                    if(size < 1024)
                    {
                        size = size+'Б';
                    }else
                    if(size < 1024*1024)
                    {
                        size = size/1024;
                        size = Math.round10(size, -1);
                        size = size+'Кб';
                    }else
                    {
                        size = size / (1024*1024);
                        size = Math.round10(size, -1);
                        size = size+'Мб';
                    }

                    filename=file.name;
                    fl=filename.length;
                    if(fl > 10)
                    {
                        filename=filename.substr(0,10) + '...';
                    }

                    fname='<div class="nullblock">' +
                        '<div class="filedoc"></div>' +
                        '<div class="filedocext"><p class="upper">'+ext+'</p><p>'+size+'</p></div>' +
                        '<div class="filename">'+filename+'</div>' +
                        '</div>';
                }
                var filetext='<div id="fileBlock'+allfiles+'" class="addedFile" '+fstyle+'>' +
                    '<div class="nullblock"><div class="fileDel" onclick="deleteFile('+allfiles+');"><img src="/img/bluecross.png" width="12"' +
                    ' height="12"></div></div>'
                    +fname+'</div>';
                $('#filepreviews').append(filetext);
            });

            if(rcnt == 0)
            {
                alert('Ошибка при загрузке файла - проверьте его формат и размер');
            }

        }

    });

    $('#fileupload').bind('fileuploadstart', function (e, data) {
        $('#progress').show();
    });
    $('#fileupload').bind('fileuploadstop', function (e, data) {
        $('#progress').hide();
    });
    $('#fileupload').bind('fileuploadchunkfail', function (e, data) {
        alert(data.errorThrown);
    });
    $('#fileupload').bind('fileuploadfail', function (e, data) {
        alert(data.errorThrown);
    });


});

$(document).bind('dragover', function (e) {
    var dropZone = $('#dropzone'),
        timeout = window.dropZoneTimeout;
    if (!timeout) {
        dropZone.addClass('in');
    } else {
        clearTimeout(timeout);
    }
    var found = false,
        node = e.target;
    do {
        if (node === dropZone[0]) {
            found = true;
            break;
        }
        node = node.parentNode;
    } while (node != null);
    if (found) {
        dropZone.addClass('hover');
    } else {
        dropZone.removeClass('hover');
    }
    window.dropZoneTimeout = setTimeout(function () {
        window.dropZoneTimeout = null;
        dropZone.removeClass('in hover');
    }, 100);
});
