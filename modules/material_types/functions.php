<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


//функции работы с типами материалов

//селекторы шаблонов
function TemplateSelectorOptions(\iSite $site, $atemplate, $materiallist=1)
{
    $options='';

    $query = 'SELECT * FROM "standart_shablons" WHERE "materiallist" = $1 ORDER BY "name" ASC';

    $res = $site->dbquery($query, array($materiallist));

    if ( ! empty($res))
    {
        foreach($res as $row)
        {
            if($row['id'] == $atemplate)
            {
                $selected='SELECTED';
            }
            else
            {
                $selected='';
            }

            $options.="<option value='$row[id]' $selected>$row[name]</options>";
        }
    }

    return $options;
}

/**
 * @param \iSite $site
 * @param $id
 * @return array
 */
function deleteMaterialType(\iSite $site, $id)
{
    $message_success = ' - тип материала удален';

    //проверяем наличие переданного типа материала
    $query = 'SELECT count("id") cid FROM "material_types" WHERE "id" = $1';
    $res = $site->dbquery($query, array($id));

    if ($res[0]['cid'] == 0) {
        return array(true, $message_success);
    }

    $hook_result = $site->callHook('materialtypedeletable', array(
        'id' => $id
    ));

    if ( ! empty($hook_result->cancel)) {
        return array(
            false,
            ' - ошибка: '.(! empty($hook_result->message) ? $hook_result->message : 'удаление типа материалов невозможно')
        );
    }

    $site->callHook('beforematerialtypedelete', array(
        'id' => $id
    ));

    //проверяем наличие связанных материалов

    $query = 'DELETE FROM "material_types" WHERE "id" = $1';
    $res = $site->dbquery($query, array($id));

    if (empty($res)) {
        return array(false, ' - ошибка: не удалось удалить тип материала');
    }

    $site->callHook('materialtypedelete', array(
        'id' => $id
    ));

    return array(true, $message_success);
}
