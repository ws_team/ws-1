<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


//класс файлов
//(перешел из модуля материалов)
//- сильно отличается от класса iFile
//- файлы привязываются к материалам
//- функции отображения файлов обрабатываются в самом модуле материалов
//- Класс iFile возможно будет выпилен, чтобы не плодить неиспользуемый функционал
class MFile
{
    /**
     * @var \iSite
     */
    public $siteObj;

    /**
     * @var int|null
     */
    public $fileid;

    /**
     * @var array|null
     */
    public $fileinfo;

    public function CreateFileCash()
    {
        $success = false;

        $query = 'SELECT * FROM "files" WHERE "id" = $1';
        $q_params = array($this->fileid);

        $res = $this->siteObj->dbquery($query, $q_params);

        if (!empty($res)) {
            $dbfile = $res[0];
            $filename = $this->siteObj->settings->path . '/photos/' . $dbfile['id'];

            if ($this->siteObj->settings->files->saveindb) {
                $success = file_put_contents($filename, $dbfile['filedata']);
            } else {
                $success = true;
            }

            $filedata_sv = isset($_FILES['filedata_sv']) ? $_FILES['filedata_sv'] : array();

            if($success && $filedata_sv['size'] > 0 && $filedata_sv['tmp_name'] != '')
            {
                $filename_sv=$filename.'_sv';
                file_put_contents($filename_sv, file_get_contents($filedata_sv['tmp_name']));
            }

        }

        return $success;
    }

    public function DoEditFile()
    {
        if (($this->fileid > 0) && ($this->siteObj->values->file_name != '')) {
            $name = str_replace('"', '&quot;', $this->siteObj->values->file_name);
            $name_en = str_replace('"', '&quot;', $this->siteObj->values->file_name_en);
            $content = str_replace('"', '&quot;', $this->siteObj->values->file_content);
            $date_event = $this->siteObj->values->date_event;
            $author = $this->siteObj->values->author;
            $ordernum = $this->siteObj->values->ordernum;
            if($ordernum ==='')
            {
                $ordernum='null';
            }
            else
            {
                $ordernum = (int)$this->siteObj->values->ordernum;
            }

            $query = <<<EOS
UPDATE "files"
SET
    "name" = $1,
    "name_en" = $2,
    "content" = $3,
    "date_event" = TO_DATE($4, 'DD.MM.YYYY HH24:MI'),
    "author" = $5,
    "ordernum" = $ordernum
WHERE "id" = $6
EOS;
            $q_params = array(
                $name,
                $name_en,
                $content,
                $date_event,
                $author,
                $this->siteObj->values->file_id
            );

            if ($this->siteObj->dbquery($query, $q_params)) {
                if ($this->siteObj->values->id != '') {

                    $filedata_sv = isset($_FILES['filedata_sv']) ? $_FILES['filedata_sv'] : array();

                    $filename = $this->siteObj->settings->path . '/photos/' . $this->siteObj->values->file_id;

                    if($filedata_sv['size'] > 0 && $filedata_sv['tmp_name'] != '')
                    {
                        $filename_sv=$filename.'_sv';
                        file_put_contents($filename_sv, file_get_contents($filedata_sv['tmp_name']));
                    }


                    $query = <<<EOS
DELETE FROM "file_pars_connections"
WHERE "file_id" = $1 AND "material_id" = $2
EOS;
                    $q_params = array($this->siteObj->values->file_id, $this->siteObj->values->id);

                    if ($this->siteObj->dbquery($query, $q_params)) {
                        if ($this->siteObj->values->file_par != '') {
                            $query = <<<EOS
INSERT INTO "file_pars_connections" ("file_id","par_id","material_id")
VALUES($1, $2, $3)
EOS;
                            $q_params = array(
                                $this->siteObj->values->file_id,
                                $this->siteObj->values->file_par,
                                $this->siteObj->values->id
                            );

                            $this->siteObj->dbquery($query, $q_params);
                        }
                    }
                }

                $this->GetFileInfo();
                return $this->fileid;
            }
        }

        return false;
    }

    public function MakeImageSizeName($width = 0, $height = 0, $watermark = 0)
    {
        if ((($width == 0) && ($height == 0)) || ($this->fileid == 0)) {
            return false;
        } else {
            if ($width == 0) {
                $imgname = $this->fileid . '_y' . $height;
            } elseif ($height == 0) {
                $imgname = $this->fileid . '_x' . $width;
            } else {
                $imgname = $this->fileid . '_xy' . $width . 'x' . $height;
            }

            if ($watermark > 0) {
                $imgname .= '_w';
            }

            $imgname .= '.jpg';

            return ($imgname);
        }
    }

    public function GetFileInfo()
    {
        $this->fileinfo = Array();
        $this->fileinfo['id'] = '';
        $this->fileinfo['name'] = '';
        $this->fileinfo['name_en'] = '';
        $this->fileinfo['type_id'] = '';
        $this->fileinfo['size'] = '';
        $this->fileinfo['extension'] = '';
        $this->fileinfo['width'] = '';
        $this->fileinfo['height'] = '';
        $this->fileinfo['content'] = '';
        $this->fileinfo['filedata'] = '';
        $this->fileinfo['date_event'] = '';
        $this->fileinfo['author'] = '';
        $this->fileinfo['ordernum'] = '';
        $this->fileinfo['cecutient'] = false;

        if ( ! empty($this->fileid)) {
            $query = 'SELECT * FROM "files" WHERE "id" = $1';
            $res = $this->siteObj->dbquery($query, array($this->fileid));
            if ( ! empty($res)) {
                $this->fileinfo = $res[0];
            }

            $filename_sv = $this->siteObj->settings->path . '/photos/' . $this->fileinfo['id'] .'_sv';

            if(file_exists($filename_sv))
            {
                $this->fileinfo['cecutient'] = true;
            }

        }
    }

    public function __construct(\iSite $siteObj, $fileid = 0)
    {
        $this->siteObj = $siteObj;
        $this->fileid = $fileid;

        if ($this->fileid == 0) {
            if (isset($this->siteObj->values->file_id)) {
                $this->fileid = $this->siteObj->values->file_id;
            }
        }

        $this->GetFileInfo();
    }

    public function clearCaches()
    {
        if ($this->fileinfo['type_id'] == 1) {
            //получаем размеры
            $query = 'SELECT * FROM "files_imagesizes"';
            $res = $this->siteObj->dbquery($query);
            if (!empty($res)) {
                foreach ($res as $row) {
                    $imgname = $this->MakeImageSizeName($row['width'], $row['height']);
                    $fullname = $this->siteObj->settings->path . '/photos/' . $imgname;
                    if (file_exists($fullname))
                        unlink($fullname);
                }
            }

        }
    }

    public function clearFileOriginal()
    {
        $fullname = $this->siteObj->settings->path . '/photos/' . $this->fileid;
        if (file_exists($fullname))
            unlink($fullname);
    }

    public function doDeleteFile()
    {
        //проверяем на колличество связей
        $ccnt = 0;

        $query = 'SELECT count("material_id") "ccnt" FROM "file_connections" WHERE "file_id" = $1';
        $q_params = array($this->fileid);
        $res = $this->siteObj->dbquery($query, $q_params);

        if ( ! empty($res)) {
            $ccnt = $res[0]['ccnt'];
        }

        $query = 'DELETE FROM "file_connections" WHERE "file_id" = $1 AND "material_id" = $2';
        $q_params = array($this->fileid, $this->siteObj->values->id);
        $res = $this->siteObj->dbquery($query, $q_params);

        if ($ccnt < 2) {
            $query = 'DELETE FROM "files" WHERE "id" = $1';
            $q_params = array($this->fileid);
            $res = $this->siteObj->dbquery($query, $q_params);

            //удаляем кэши изображений
            $this->clearCaches();

            //удаляем сырец файла
            $this->clearFileOriginal();
        }
    }

    public function DoSaveFile()
    {
        $this->logWrite(LOG_DEBUG, __METHOD__, 'file_name='.$this->siteObj->values->file_name.';mid='.$this->siteObj->values->id);
        $filedata = isset($_FILES['filedata']) ? $_FILES['filedata'] : array();


        return $this->saveWithParams(
            $filedata,
            $this->siteObj->values->file_name,
            $this->siteObj->values->file_name_en,
            $this->siteObj->values->file_type_id,
            $this->siteObj->values->file_content,
            $this->siteObj->values->date_event,
            $this->siteObj->values->author,
            $this->siteObj->values->ordernum,
            $this->siteObj->values->file_par,
            $this->siteObj->values->id,
            true
        );
    }

    protected function saveImage($err, $errtext)
    {
        $this->logWrite(LOG_DEBUG, __METHOD__, __LINE__);

        //создаем кэши картинок на стороне сайта
        if ( ! $this->CreateImageCash()) {
            $err = true;
            $errtext .= " - ошибка: не удалось сохранить кэши фотографий (переданный файл больше допустимого размера либо имеет неверный формат)";
        }

        return array($err, $errtext);
    }

    protected function saveVideo($err, $errtext, $filedata, $file_content)
    {
        if (isset($filedata) && is_uploaded_file($filedata['tmp_name'])) {
            if ( ! $this->CreateFileCash()) {
                $err = true;
                $errtext .= " - ошибка: не удалось сохранить кэши загружаемого файла (переданный файл больше допустимого размера либо имеет неверный формат)";
            } else {
                //если файл-видео загружен, пробуем сформировать его картинку

                //вырезаем картинку у файлы
                $video_url = $this->siteObj->settings->path . '/photos/' . $this->fileid;
                $image_filename = $this->siteObj->settings->path . '/photos/' . $this->fileid . '_thumb.png';

                //echo ($video_url.' - '.$image_filename);
                //die();

	            //$video_url=str_replace('/home/habkrai/data/www/html.3/photos','/var/www/html/photos',$video_url);
	            //$image_filename=str_replace('/home/habkrai/data/www/html.3/photos','/var/www/html/photos',$image_filename);

	            //echo ($video_url.' - '.$image_filename);
	            //die();

                system('ffmpeg -i ' . $video_url . ' -ss 00:00:5 -vframes 1 ' . $image_filename);
            }
        } else {
            //проверяем URL на наличие
            if ($file_content == '') {
                $err = true;
                $errtext .= " - ошибка: отсутствует ссылка на внешний ресурс";
            }
        }

        return array($err, $errtext);
    }

    protected function saveGenericFile($err, $errtext)
    {
        //создаем кэш файла на стороне сайта
        if ( ! $this->CreateFileCash()) {
            $err = true;
            $errtext .= " - ошибка: не удалось сохранить кэши загружаемого файла (переданный файл больше допустимого размера либо имеет неверный формат)";
        }

        return array($err, $errtext);
    }

    protected function logWrite($level, $category, $message)
    {
        $this->siteObj->logWrite($level, $category, $message);
    }

    /**
     * @param $filedata
     * @param $file_name
     * @param $file_type_id
     * @param $file_content
     * @param $file_par
     * @param $connect_mid
     * @param bool $publish
     * @return array(bool success, string errtext)
     */
    public function saveWithParams($filedata, $file_name, $file_name_en, $file_type_id, $file_content, $date_event, $author, $ordernum, $file_par, $connect_mid, $publish = true)
    {
        $err     = false;
        $errtext = '';
        $fileid  = '';

        $this->logWrite(LOG_DEBUG, __METHOD__, __LINE__);
        
        if ($file_name == '' && isset($filedata)
            && $filedata['name'] != ''
        ) {
            $file_parts = pathinfo($filedata['name']);

            if ( ! empty($file_parts['filename'])) {
                $file_name = $file_parts['filename'];
            }
            else{
                // функция pathinfo плохо работает с русскими названиями файлов (при не настроенной локали), поэтому вот такой костыль
                $file_name = stristr($filedata['name'], '.', true);
            }
        }

        if ( ! empty($file_name) && isset($filedata)
            || $file_type_id == FILETYPE_VIDEO && ! empty($file_content)) {

            list($err, $errtext, $fileid) = $this->JustMakeFileSaved(
                $err,
                $errtext,
                $file_type_id,
                $filedata,
                $file_content,
                $file_name,
                $file_name_en,
                $date_event,
                $author,
                $ordernum
            );

            $this->logWrite(LOG_DEBUG, __METHOD__, __LINE__.' fileid='.$fileid);
        } else {
            $err = true;
            $errtext .= " - ошибка: не переданы обязательные поля для добавления файла";
            echo $errtext;
        }

        //если  все норм - связываем файл с материалом
        if ( ! $err) {
            list($err, $errtext) = $this->afterSaveFile(
                $err,
                $errtext,
                $fileid,
                $file_type_id,
                $connect_mid,
                $publish,
                $file_par
            );
        }

        return Array( ! $err, $errtext, $fileid);
    }

    protected function JustMakeFileSaved($err, $errtext, $file_type_id, $filedata, $file_content, $file_name, $file_name_en, $date_event, $author, $ordernum)
    {
        $fileid = null;

        $this->logWrite(LOG_DEBUG, __METHOD__, __LINE__);

        $fileid = $this->SimpleSaveFile(
            $filedata,
            $file_name,
            $file_name_en,
            $file_content,
            $file_type_id,
            $date_event,
            $author,
            $ordernum
        );

        if ($fileid) {
            $this->fileid = $fileid;

            //определение типа файла
            switch ($file_type_id) {
                //файл фото - загружаем файл, создаём кэш картинок разных размеров
                case FILETYPE_IMAGE:
                    list($err, $errtext) = $this->saveImage($err, $errtext);
                    break;
                //файл видео - сохраняем ссылку youtube
                case FILETYPE_VIDEO:

                    list($err, $errtext) = $this->saveVideo($err, $errtext, $filedata, $file_content);
                    break;

                /*case FILETYPE_AUDIO:
                    break;*/

                // файл видео трансляции - сохраняем ссылку
                case FILETYPE_BROADCAST:
                    //ничего не делаем
                    break;

                //просто сохраняем файл, создаём кэш
                default:

                    list($err, $errtext) = $this->saveGenericFile($err, $errtext);
                    break;
            }
        } else {
            $err = true;
            $errtext .= " - ошибка: не удалось сохранить файл";
        }

        return array($err, $errtext, $fileid);
    }

    function connectMaterial($mid, $err = false, $errtext = '')
    {
        $query = <<<EOS
INSERT INTO "file_connections" ("file_id","material_id")
VALUES($1, $2)
EOS;
        $q_params = array($this->fileid, $mid);
        $res = $this->siteObj->dbquery($query, $q_params);

        $err = $err || empty($res);

        if ($res === false) {
            $errtext .= ' - ошибка: не удалось связать файл с материалом';
        } else {
            $errtext .= ' - файл успешно добавлен';
        }

        return array($err, $errtext);
    }

    function afterSaveFile($err, $errtext, $fileid, $file_type_id, $connect_mid, $publish, $file_par)
    {
        $this->logWrite(LOG_DEBUG, __METHOD__, __LINE__.' connect_mid='.$connect_mid);

        if ($connect_mid) {
            list($err, $errtext) = $this->connectMaterial($connect_mid, $err, $errtext);
        }

        if ( ! $err) {
            if ($file_type_id == FILETYPE_DOCUMENT && $publish) {
                $this->publish($this->fileid);
            }
        }

        //если надо - добавляем раздел файла

        if ( ! empty($file_par) && $connect_mid) {
            //связываем с разделом
            $query = <<<EOS
INSERT INTO "file_pars_connections" ("file_id", "par_id", "material_id")
VALUES($1, $2, $3)
EOS;
            $q_params = array(
                $fileid,
                $file_par,
                $connect_mid
            );

            $this->siteObj->dbquery($query, $q_params);
        }

        return array($err, $errtext);
    }

    protected function publish($fileid)
    {
        Solr_AddFile($this->siteObj, $fileid);
    }

    public function CreateImageCash($rewrite = true)
    {
        $this->siteObj->logWrite(LOG_DEBUG, __METHOD__, __LINE__.' '.$rewrite);
        $succes = false;

        //получаем все данные картинки
        $query = 'SELECT * FROM "files" WHERE "id" = $1';
        $res = $this->siteObj->dbquery($query, array($this->fileid));

        if ( ! empty($res)) {
            $dbfile = $res[0];

            //echo 'test1';

            //преобразуем в жпегъ
            $filename = $this->siteObj->settings->path . '/photos/' . $dbfile['id'];

            $this->siteObj->logWrite(LOG_DEBUG, __METHOD__, __LINE__.' filename='.$filename);

            $savecache = true;

            /*if ($this->siteObj->settings->files->saveindb) {
                $savecache = file_put_contents($filename, $dbfile['filedata']);
            } else {
                $savecache = true;
            }*/

            if ($savecache) {

                //echo 'test2';

                //проверяем файл на картинку
                $smallsizes = getimagesize($filename);
                $typecode = -1;
                switch ($smallsizes[2]) {
                    case '2':
                        //jpg
                        $typecode = 2;
                        $extension = 'jpg';
                        break;
                    case '3':
                        //png
                        $typecode = 3;
                        $extension = 'png';
                        break;
                    case '1':
                        //gif
                        $typecode = 1;
                        $extension = 'gif';
                        break;
                    case '6':
                        //bmp
                        $typecode = 6;
                        $extension = 'bmp';
                        break;
                }

                if ($smallsizes[2] == $typecode) {

                    //echo 'test3';

                    //создаем источник картинки из файла

                    $this->siteObj->logWrite(LOG_DEBUG, __METHOD__, __LINE__.' typecode='.$typecode);

                    $width = $smallsizes[0];
                    $height = $smallsizes[1];

                    //размеры
                    /*$xarr=Array(546,200,600,950);
                    $yarr=Array();
                    $xyarr=Array(Array(64,64),Array(142,109),Array(272,199),Array(58,49),Array(98,76),Array(116,100));*/

                    $xarr = Array();
                    $yarr = Array();
                    $xyarr = Array();

                    //формируем массивы размеров
                    $query = 'SELECT * FROM "files_imagesizes"';
                    if ($res = $this->siteObj->dbquery($query)) {

                        $this->siteObj->logWrite(LOG_DEBUG, __METHOD__, __LINE__);

                        if (count($res) > 0) {

                            //echo 'test4';

                            foreach ($res as $row) {
                                if ($row['height'] == 0) {
                                    $xarr[] = $row['width'];
                                } elseif ($row['width'] == 0) {
                                    $yarr[] = $row['height'];
                                } else {
                                    $xyarr[] = Array($row['width'], $row['height']);
                                }
                            }
                        }
                    }


                    //делаем кэши картинок по x
                    if (count($xarr) > 0)
                        $this->siteObj->logWrite(LOG_DEBUG, __METHOD__, __LINE__.' '.implode(',', $xarr));

                        foreach ($xarr as $x) {
                            $newx = $x;
                            $newy = round($height * $newx / $width);

                            $to = $this->siteObj->settings->path . '/photos/' . $dbfile['id'] . '_x' . $x . '.jpg';
                            if ($rewrite || !file_exists($to)) {
                                $this->SaveImageSize($filename, $to, $newx, $newy, $extension);
                            }
                        }

                    //делаем кэши картинок по y
                    if (count($yarr) > 0) {
                        $this->siteObj->logWrite(LOG_DEBUG, __METHOD__, __LINE__.' '.implode(',', $yarr));

                        foreach ($yarr as $y) {

                            $newy = $y;
                            $newx = round($width * $newy / $height);

                            $to = $this->siteObj->settings->path . '/photos/' . $dbfile['id'] . '_y' . $y . '.jpg';
                            if ($rewrite || !file_exists($to)) {
                                $this->SaveImageSize($filename, $to, $newx, $newy, $extension);
                            }
                        }
                    }

                    //делаем кэши картинок с обрезанием
                    if (count($xyarr) > 0) {

                        //echo 'test5';

                        $this->siteObj->logWrite(LOG_DEBUG, __METHOD__, __LINE__.' '.implode(',', array_map(function ($xy){return implode('+', $xy);}, $xyarr)));

                        foreach ($xyarr as $xy) {
                            $newx = $xy[0];
                            $newy = $xy[1];

                            $to = $this->siteObj->settings->path . '/photos/' . $dbfile['id'] . '_xy' . $newx . 'x' . $newy . '.jpg';
                            if ($rewrite || !file_exists($to)) {
                                $this->SaveImageSize($filename, $to, $newx, $newy, $extension, 1);
                            }
                        }
                    }

                    $succes = true;
                }
            }
        }

        return $succes;
    }

    public function SaveImageSize($from, $to, $newx, $newy, $extension, $crop = 0)
    {
        //echo $from;
        switch ($extension) {
            case 'jpg':
                $im = imagecreatefromjpeg($from);
                $im1 = imagecreatetruecolor($newx, $newy);
                break;
            case 'jpeg':
                $im = imagecreatefromjpeg($from);
                $im1 = imagecreatetruecolor($newx, $newy);
                break;
            case 'png':
                $im = imagecreatefrompng($from);

                $im1 = imagecreatetruecolor($newx, $newy);
                imagealphablending($im1, false);
                imagesavealpha($im1, true);
                break;
            case 'bmp':
                $im = imagecreatefrombmp2($from);
                $im1 = imagecreatetruecolor($newx, $newy);
                break;
            case 'gif':
                $im = imagecreatefromgif($from);
                $im1 = imagecreatetruecolor($newx, $newy);
                break;
        }

        if ($crop == 0) {
            imagecopyresampled($im1, $im, 0, 0, 0, 0, $newx, $newy, imagesx($im), imagesy($im));
        } else {

            //просчитываем с какой стороны обрезать (формируем переменные для обрезания)
            $sootn1 = $newx / $newy;
            $sootn2 = imagesx($im) / imagesy($im);

            //режим по x
            if ($sootn1 >= $sootn2) {
                $ix = imagesx($im);
                $iy = round($newy * imagesx($im) / $newx);
            } else {

                $iy = imagesy($im);
                $ix = round($newx * imagesy($im) / $newy);

            }

            //die($ix.' - '.$iy.' --- '.imagesx($im).' - '.imagesy($im));

            //смещения
            $startx = (int)((imagesx($im) - $ix) / 2);
            //$starty=(int)((imagesy($im)-$iy)/2);
            $starty = 0;

            imagecopyresampled($im1, $im, 0, 0, $startx, $starty, $newx, $newy, $ix, $iy);

        }

        imagejpeg($im1, $to, 100);

    }

    protected function SimpleSaveFile($filedata, $file_name, $file_name_en, $content, $file_type_id, $date_event, $author, $ordernum)
    {
        $this->logWrite(LOG_DEBUG, __METHOD__, __LINE__);

        $fileid = false;

        //определяем id файла
        $query = 'SELECT MAX("id") "fid" FROM "files"';// @todo use sequence

        $res = $this->siteObj->dbquery($query);

        if ( ! empty($res)) {
            if (count($res) > 0) {
                $fileid = $res[0]['fid'];
            } else {
                $fileid = 1;
            }

            if ($fileid == '') {
                $fileid = 1;
            }

            ++$fileid;

            $filesize = 0;
            if (isset($filedata['size'])) {
                $filesize = $filedata['size'];
            }

            $ext = '';
            if (isset($filedata['name'])) {
                $ext = GetFileExtension($filedata['name']);
            }

            $name = str_replace('"', '&quot;', $file_name);
            $name_en = str_replace('"', '&quot;', $file_name_en);
            $type_id = $file_type_id;
            if (empty($type_id))
                $type_id = FILETYPE_IMAGE;

            if ($date_event == '') {
                $date_event = date('d.m.Y H:i');
            }

            $query = <<<EOS
INSERT INTO "files"(
    "id",
    "name",
    "name_en",
    "type_id",
    "size",
    "extension",
    "width",
    "height",
    "content",
    "date_event",
    "author",
    "ordernum"
)
VALUES(
    $1,
    $2,
    $3,
    $4,
    $5,
    $6,
    $7,
    $8,
    $9,
    TO_DATE($10,'DD.MM.YYYY HH24:MI'),
    $11,
    $12)
EOS;
            $q_params = array(
                $fileid,
                $name,
                $name_en,
                $type_id,
                $filesize,
                $ext,
                0,
                0,
                $content,
                $date_event,
                $author,
                $ordernum,
            );

            $q_blobs = array();

            if ( ! empty($filedata['tmp_name'])) {

                $this->logWrite(LOG_DEBUG, __METHOD__, __LINE__);

                //сохраняем файлу на сервер с помощью ftp
                if ($this->siteObj->settings->DB->type == 'postgresql') {
                    $this->siteObj->settings->files->saveindb = false;

                    // функция загрузки файлов по ftp, которая не имела никакого смысла
                    /*
                    $ftp = Array();
                    $ftp['host'] = '172.18.209.12';
                    $ftp['user'] = 'ftpuser';
                    $ftp['pass'] = 'P@$$W0rd123';
                    putFtpFile($ftp,
                        $fileid,
                        $this->siteObj->settings->DB->login,
                        $filedata['tmp_name']
                    );
                    */
                }

                if ($this->siteObj->settings->files->saveindb) {
                    $str = file_get_contents($filedata['tmp_name']);
                    $q_blobs[] = $str;
                } else {
                    $filename = $this->siteObj->settings->path . '/photos/' . $fileid;
                    file_put_contents($filename, file_get_contents($filedata['tmp_name']));

                    //проверяем размер файла
                    $size=filesize($filename);
                    if($size == 0)
                    {
                        return false;
                    }

                }
            }

            $res = $this->siteObj->dbquery($query, $q_params, Array(), $q_blobs);

            if ( ! $res) {
                $fileid = false;
            }
        }

        return $fileid;
    }
}