<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\auth;

require_once('ManagerInterface.php');

class Manager implements ManagerInterface {
    private $rules;

    /**
     * @var \wpf\utils\Logger
     */
    private $logger;

    public function __construct(array $rules, \wpf\utils\LoggerInterface $logger)
    {
        $this->rules = $rules;
        $this->logger = $logger;
    }

    protected static function getAllRoles(\wpf\auth\IdentityInterface $userIdentity)
    {
        $ret = array();
        $names = array('admin', 'cmanager', 'registered', 'guest');
        foreach ($names as $name)
            if ($userIdentity->hasRole($name))
                $ret[] = $name;

        return $ret;
    }

    public function checkPermission($permission, IdentityInterface $userIdentity)
    {
        $uid = $userIdentity->getId();
        $username = $userIdentity->getAttribute('login_name');
        $roleNames = array_map('strval', self::getAllRoles($userIdentity));

        $siteId = getenv('HTTP_HOST');
        $this->logger->write(LOG_DEBUG, __METHOD__,
            sprintf('%s %% user #%d "%s" {%s}',
                $permission,
                $uid,
                $username,
                implode(',', $roleNames)
            )
        );

        $isWildcardPerm = strpos($permission, '*') !== false;

        foreach ($this->rules as $iRule => $rule) {
            $action = $rule[0];

            if ( ! in_array($action, array('allow', 'deny')))
                continue;

            if ( ! empty($rule[1])) {
                if ($rule[0] != 'allow' && $isWildcardPerm)
                    continue;
                if ( ! $this->matchPermission($permission, (array)$rule[1]))
                    continue;
            }

            if ( ! empty($rule['role']) && ! $userIdentity->hasRole($rule['role'])) {
                continue;
            }

            if ( ! empty($rule['user']) && ! in_array($rule['user'], array($uid, $username))) {
                continue;
            }

            if (isset($rule['callback'])) {
                if ( ! is_callable($rule['callback']))
                    continue;
                $params = isset($rule['callback_params']) ? $rule['callback_params'] : array();
                if ( ! call_user_func($rule['callback'], $permission, $userIdentity, $params))
                    continue;
            }

            $this->logger->write(LOG_DEBUG, __METHOD__, "$permission % $uid: $action (rule {$rule['id']})");

            return $action == 'allow';
        }

        return false;
    }

    /**
     * @param string $permission
     * @param string|array $permList
     * @return bool
     */
    protected function matchPermission($permission, $permList)
    {
        if (strpos($permission, '*') !== false) {
            foreach ((array)$permList as $matchPerm) {
                if (fnmatch($permission, $matchPerm))
                    return true;
            }
        } else {
            foreach ((array)$permList as $matchPerm)
                if (strncmp($matchPerm, $permission, strlen($matchPerm)) == 0)
                    return true;
        }
        return false;
    }
}