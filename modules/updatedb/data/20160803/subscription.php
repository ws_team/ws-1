<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$query_list[] = <<<EOS
CREATE TABLE IF NOT EXISTS subscriptionlists(
    id SERIAL PRIMARY KEY,
    name CHARACTER VARYING(40) NOT NULL,
    description TEXT,
    params TEXT,
    "priority" SMALLINT NOT NULL DEFAULT 1000
)
EOS;

$query_list[] = <<<EOS
CREATE INDEX i_subscriptionlists_name ON subscriptionlists(name)
EOS;

$query_list[] = <<<EOS
CREATE TABLE IF NOT EXISTS list_subscribers(
    list_id INTEGER NOT NULL,
    subscriber_id INTEGER NOT NULL,
    PRIMARY KEY(list_id, subscriber_id)
)
EOS;

$query_list[] = <<<EOS
ALTER TABLE subscription_sendqueue ADD COLUMN list_id INTEGER
EOS;

foreach ($query_list as $query) {
    $this->dbquery($query);
    $this->resetError();
}



