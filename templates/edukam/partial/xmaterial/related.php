<?php
/**
 * @var \iSite $this
 * @var array $material
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


//выводим материалы по теме
$this->values->id = $material['id'];
$conmats = vidjet_material_connections($this);

if (!empty($conmats)) {
    ?>
    <div class="content-related">
        <h2>Материалы по теме:</h2>
        <?=$conmats?>
    </div><?php
}