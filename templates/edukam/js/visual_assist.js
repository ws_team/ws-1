/**
 * Поддержка версии для слабовидящих.
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


$(function () {
    var KEY_LEFT = 37,
        KEY_RIGHT = 39,
        KEY_UP = 38,
        KEY_DOWN = 40,
        KEY_TAB = 9,
        KEY_RETURN = 13,
        KEY_ESCAPE = 27,
        KEY_QMARK = 191;

    var vmenu = $('#x-vmenu'),
        crumbs = $('#breadcrumbs_container'),
        mainMenu = $('.mainmenu'),
        secMenu = $('.secmenu'),
        contentLinks = $('.content-index'),
        pagination = $('#pagination'),
        hint = $('#assist_hint');

    var vmenuStickAt;
    var crumbsStickAt;

    if (vmenu.length)
        vmenuStickAt = vmenu.offset().top;

    vmenuStickAt = crumbsStickAt =
        parseInt(mainMenu.height() || 0, 10) +
        parseInt(secMenu.height() || 0, 10);

    var menus = {};
    var deepestLevel = 0;
    var activeMenu = 0;// level

    window.console&&console.log('[assist] enter vmenuStickAt: ', vmenuStickAt, vmenu);

    if (!$(document.body).hasClass('xassist'))
        return false;

    function bindControls() {
        window.console&&console.log('[assist] bindControls');
        $(document).on('keyup', function (evt) {
            if (['INPUT', 'SELECT', 'TEXTAREA'].indexOf(evt.target.tagName.toUpperCase()) >= 0)
                return;

            if (evt.which == KEY_TAB) {
                window.console&&console.log('[assist] controls %s', 'TAB' + (evt.shiftKey ? ' + SHIFT' : ''));
                evt.preventDefault();

                if (evt.shiftKey)
                    focusPrevMenu(activeMenu);
                else
                    focusNextMenu(activeMenu);
                return;
            }

            if (evt.which == KEY_RETURN) {
                window.console&&console.log('[assist] controls RETURN');
                if (followNav()) {
                    evt.preventDefault();
                    evt.stopPropagation();
                    return false;
                }

                return;
            }

            if (evt.which == KEY_ESCAPE) {
                window.console&&console.log('[assist] controls ESC');
                if (activeMenu) {
                    deactivateMenu(activeMenu);
                    activeMenu = 0;
                }
            }

            if (evt.which == KEY_QMARK && evt.shiftKey) {
                toggleAssistHint();
            }

            if (activeMenu && menus[activeMenu].keys.indexOf(evt.which) >= 0) {
                handleMenuNav(evt.which)
            }
        }).on('keydown', function (evt) {
            if (activeMenu && menus[activeMenu].keys.indexOf(evt.which) >= 0
                || evt.which == KEY_TAB) {
                evt.preventDefault();
                evt.stopPropagation();
            }
        });
    }

    function deactivateMenu(menu) {
        if (menu) {
            menus[menu].container.find('.smenu').hide();
            menus[menu].container.find('.submenu_container').hide();
            menus[menu].container.find('.submenu_container_long').hide();
            menus[menu].container.removeClass('focus').parents().andSelf().removeClass('x-focus-path');
        }
    }

    function followNav() {
        if (!activeMenu)
            return false;

        var menu = menus[activeMenu];
        var activeLink = menu.container.find(menu.linkSelector).filter('.focus');

        if (activeLink.length) {
            if (activeLink.attr('onclick')) {
                activeLink.click();
            } else {
                document.location = activeLink[0].href;
            }
        }
        return true;
    }

    function handleMenuNav(keyCode) {

        if (!activeMenu)
            return false;

        window.console&&console.log('[assist] handleMenuNav');

        var menu = menus[activeMenu];
        var dir = menu.keys.indexOf(keyCode);

        if (dir < 0)
            return false;

        dir = dir == 0 ? -1 : 1;

        var links = menu.container.find(menu.linkSelector);
        var prevFocused;
        var link = links.filter('.focus');

        if (!link.length)
            link = links.filter('.active');

        if (!link.length)
            link = links.eq(0);

        var prevFocused = link;

        link = links.eq((links.index(link) + dir + links.length) % links.length);

        links.removeClass('focus');
        link.addClass('focus');
        if (link.length && link[0].tagName.toUpperCase() == 'A')
            link.focus()

        var linkCtr = link.closest('.smenu');
        if (linkCtr.length) {
            var focusAnc = linkCtr.prev(menu.linkSelector);
            focusAnc.addClass('x-focus-anc').siblings(menu.linkSelector).removeClass('x-focus-anc');
            focusAnc.parent('.x-anc').addClass('x-focus-anc').siblings().removeClass('x-focus-anc');
            linkCtr.closest()
        } else {
            links.removeClass('x-focus-anc');
        }

        prevFocused.parents('.submenu_container_long').hide();
        prevFocused.parents('.submenu_container').hide();
        prevFocused.parents('.smenu').hide();

        link.parents('.submenu_container_long').show();
        link.parents('.submenu_container').show();
        linkCtr.show();

        return true;
    }

    function assistizeNav(container, level, linkSelector, vertical) {
        window.console&&console.log('[assist] assistizeNav #%d', level, container, linkSelector, vertical);

        container = $(container);
        if (!container.length)
            return;

        var keys = vertical ?
            [KEY_UP, KEY_DOWN] :
            [KEY_LEFT, KEY_RIGHT];

        if (!level)
            level = deepestLevel + 1;

        linkSelector = linkSelector || 'a.option';

        menus[level] = {
            container: container,
            keys: keys,
            linkSelector: linkSelector
        };

        if (level > deepestLevel)
            deepestLevel = level;
    }

    function focusPrevMenu(startLevel) {
        focusSiblingMenu(startLevel, -1);
    }

    function focusNextMenu(startLevel) {
        focusSiblingMenu(startLevel, +1);
    }

    function focusSiblingMenu(startLevel, direction) {
        var levelIndex = startLevel - 1;

        for (var counter = deepestLevel - 1; counter > 0; counter--) {
            levelIndex = (levelIndex + direction + deepestLevel) % (deepestLevel);

            if (menus[levelIndex + 1]) {
                if (focusMenu(levelIndex + 1))
                    break;
            }
        }
    }

    function focusMenu(level) {
        window.console&console.log('[assist] focusMenu #%d (%d)', level, activeMenu, menus[level]);

        var menu = menus[level];

        if (!menu.container.filter(':visible').length)
            return false;

        menu.container.addClass('focus').parents().andSelf().addClass('x-focus-path');

        var links = menu.container.find(menu.linkSelector);
        var link = links.filter('.focus');
        if (!link.length)
            link = links.filter('.active');
        if (!link.length)
            link = links.eq(0);

        links.removeClass('focus');
        link.addClass('focus');
        if (link.length && link[0].tagName.toUpperCase() == 'A')
            link.focus();

        if (Element.prototype.scrollIntoView)
            menu.container[0].scrollIntoView();

        if (activeMenu) {
            deactivateMenu(activeMenu);
            menu = menus[activeMenu];
            menu.container.removeClass('focus');
        }

        activeMenu = level;

        return true;
    }

    function hideAssistHint() {
        toggleAssistHint(false);
    }

    function toggleAssistHint(show) {
        if (show === undefined)
            show = ! $(document.body).hasClass('assist-showhint');

        $(document.body).toggleClass('assist-showhint');

        if (show) {
            hint.show();
        } else {
            hint.hide();
        }

        $.ajax('/?menu=main&special_showhint=' + (show ? '1' : ''), {
            method: 'POST'
        });
    }

    window.console && console.log('[assist] binding menus');

    assistizeNav(mainMenu, 1);
    assistizeNav(secMenu, 2);
    assistizeNav(crumbs, 3, 'a');
    assistizeNav(vmenu, 4, '.gover_option', true);
    assistizeNav($('#assist_hint'), null, 'a');

    contentLinks.each(function () {
        assistizeNav($(this), null, 'a.item-link', true);
    });

    $('.links-group').each(function () {
        assistizeNav($(this), null, 'a.item-link', true);
    });

    assistizeNav(pagination, null, 'a');

    crumbs.find('a').last().addClass('active');

    bindControls();

    mainMenu.addClass('fixed').parentsUntil('body').andSelf().addClass('x-fixed-path');
    vmenu.addClass('x-vmenu');
    vmenu.parent().addClass('x-vmenu-container');

    if (vmenuStickAt !== undefined) {
        $(window).scroll(function () {
            if (vmenuStickAt !== undefined) {
                if (window.scrollY > vmenuStickAt) {
                    window.console&&console.log('[assist] scroll > %d: vmenu stick', vmenuStickAt);
                    vmenu.addClass('fixed');
                    hint.addClass('fixed');
                    $(document.body).addClass('assist-hint-fixed');
                } else {
                    window.console&&console.log('[assist] scroll <= %d: vmenu unstick', vmenuStickAt);
                    vmenu.removeClass('fixed');
                    $(document.body).removeClass('assist-hint-fixed');
                    hint.removeClass('fixed');
                }
            }
            if (crumbsStickAt !== undefined) {
                if (window.scrollY > crumbsStickAt) {
                    crumbs.addClass('fixed');
                } else {
                    crumbs.removeClass('fixed');
                }
            }
        });
    }

    $('#assist_hint_toggle').click(function(evt) {
        window.console&&console.log('[assist] hint toggle clicked');

        toggleAssistHint();
    });

    $('#assist_hint_hide').click(function (evt) {
        hideAssistHint();
    });

    $('#assist_toggle_sidebar').on('click', 'a', function (evt) {
        evt.preventDefault();
        toggleOption('sidebar', 'hidesidebar', 'nohidesidebar');
    });

    $('#assist_toggle_media').on('click', 'a', function (evt) {
        evt.preventDefault();
        toggleOption('media', 'hidemedia', 'nohidemedia');
    });

    function toggleOption(name, classOn, classOff) {
        var node = $(document.body);
        node.toggleClass(classOn);
        var hide = node.hasClass(classOn);
        if (hide)
            node.removeClass(classOff);
        else
            node.addClass(classOff);

        $.ajax('?menu=main&special_toggle_' + name + '=' + (hide ? '1' : ''), {
            method: 'POST'
        });
    }
});
