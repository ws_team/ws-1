<?php
/**
 * @var \iSite $this
 */


defined('_WPF_') or die();

$this->data->iH1 = 'Обновление базы данных ОИВ';

if ($this->getUser()->isAdmin()) {

    include_once($this->settings->path.$this->settings->templateurl.'/f_header.php');
    include_once($this->locateTemplate('f_header'));

    ?>
    <div class="container container--admin-title">
        <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
    </div>
    <div class="contentblock basemargin">
        <?php

        if (isset($output))
            echo $output;

        ?>
    </div>
    <?php

    include_once($this->locateTemplate('f_footer'));
}