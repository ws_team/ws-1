<?php

defined('_WPF_') or die();

if (!empty($this->values->action)){

    include_once(dirname(__FILE__).'/functions.php');

    $controller_script = dirname(__FILE__).'/action/'.
        str_replace(array('.', '/', '~', '*', '?'), '', $this->values->action).'.php';

    if (file_exists($controller_script)) {

        include($controller_script);

    } else {
        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
        exit;
    }
}

$this->data->errortext = '';

$this->data->iH1 = 'Статистика авторских материалов';