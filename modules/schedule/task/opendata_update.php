<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

defined('_WPF_') or die();

$this->wpf_include('modules/opendata/functions.php');

$this->getPostValues('stage');

$this->logWrite(__LINE__, __FILE__);

// @todo login as daemon user

switch ($this->values->stage) {
    case 'seed':
        if ( ! opendata_import_is_running($this))
            opendata_import_seed($this);
        break;
    case 'proceed':
        if (opendata_import_is_running($this))
            opendata_import_proceed($this);
        break;
}
