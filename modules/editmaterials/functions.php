<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


//клонирование материала
function DoCloneMaterial($site,$id='')
{
    if($id == '')
    {
        $id=$site->values->clonematerial;
    }

    if($id != '')
    {

        //получаем все основные значения клонируемого материала
        $cMat = MaterialGet($site,$id);

        //формируем характеристики для добавления копии материала
        foreach($cMat as $key=>$param)
        {
            if((!is_array($param))&&($key != 'id')&&($key != 'id_char'))
            {
                if($key == 'name')
                {
                    $param.=time();
                }
                elseif($key == 'content')
                {
                    $param=str_replace("'",'"',$param);
                }
                elseif($key == 'status_id')
                {
                    $param='1';
                }

                $site->values->$key=$param;
            }
        }

        //создаем материал
        $site->values->id='';
        $site->values->type_id=$site->values->imenu;
        $nMat=createMaterial($site);

        if($nMat[0])
        {
            //сохраняем связи материалов
            if(isset($cMat['connected_materials']))
            {
                if(count($cMat['connected_materials']) > 0)
                {
                    foreach($cMat['connected_materials'] as $comat)
                    {
                        $site->values->matforconnect = $comat['id'];
                        $site->values->id = $nMat[0];

                        DoConnectMaterial($site);
                    }
                }
            }

            //сохраняем дополнительные значения
            $query='INSERT INTO "extra_materials"
            (SELECT "valuename","valuetext","valuedate",'.$nMat[0].',"type_id","selector_id","valuemat"
            FROM "extra_materials" WHERE "material_id"='.$cMat['id'].')';
            $site->dbquery($query);


            //сохраняем связанные файлы
            $query='SELECT * FROM "file_connections" WHERE "material_id" = '.$cMat['id'];
            if($res=$site->dbquery($query))
            {
                if(count($res) > 0)
                {
                    foreach($res as $row)
                    {

                        $file_id=$row['file_id'];
                        $material_id=$nMat[0];

                        $queryi='INSERT INTO "file_connections"("file_id","material_id")
                        VALUES('.$file_id.','.$material_id.')';

                        $site->dbquery($queryi);
                    }
                }
            }

            //сохраняем разделы вложений
            $query          = 'SELECT * FROM "file_pars" WHERE "material_id" = '.$cMat['id'];
            $newParsIdArr   = Array();
            if($res=$site->dbquery($query))
            {
                if(count($res) > 0)
                {
                    foreach($res as $row)
                    {
                        $query  = 'SELECT max(id) FROM file_pars';
                        $res    = $site->dbquery($query);
                        if (empty($res)) {
                            $nextParsId = 0;
                        }else{
                            $nextParsId = $res[0]['max']+1;
                        }

                        $newParsIdArr[$row['id']] = $nextParsId;

                        $material_id = $nMat[0];
                        $name        = $row['name'];
                        $orderBy     = $row['orderby'];

                        $queryi="INSERT INTO \"file_pars\"(\"id\",\"material_id\",\"name\",\"orderby\")
                        VALUES(".$nextParsId.",".$material_id.",'".$name."',".$orderBy.")";

                        $site->dbquery($queryi);
                    }
                }
            }

            //сохраняем связь между вложениями и разделами
            $query='SELECT * FROM "file_pars_connections" WHERE "material_id" = '.$cMat['id'];
            if($res=$site->dbquery($query))
            {
                if(count($res) > 0)
                {
                    foreach($res as $row)
                    {

                        $file_id     = $row['file_id'];
                        $par_id      = $row['par_id'];
                        $material_id = $nMat[0];

                        $queryi = 'INSERT INTO "file_pars_connections"("file_id","material_id","par_id")
                        VALUES('.$file_id.','.$material_id.','.$newParsIdArr[$par_id].')';

                        $site->dbquery($queryi);
                    }
                }
            }

            delMatCache($site, $cMat['id']);

            //переадресаци на изменение
            header('Location: /?menu=editmaterials&imenu='.$site->values->imenu.'&id='.$nMat[0].'&action=edit');
            die();

        }else{
            print('Ошибка при клонировании материала');
        }
    }

}

//получение данных типа материала
function GetMattypeValues(\iSite $site,$id)
{
    $mattype=Array();

    if ($id) {
        $query = 'SELECT * FROM "material_types" WHERE "id" = $1';
        $res = $site->dbquery($query, array($id));
        if ( ! empty($res)) {
            $mattype = $res[0];
        }
    }

    return($mattype);
}

//связка уже существующего файла с материалом
function DoConnectFile($site)
{
    if(!isset($site->values->fileforconnect))
    {
        $site->values->fileforconnect='';
    }

    if(($site->values->fileforconnect != '')&&($site->values->id != ''))
    {
        $query='INSERT INTO "file_connections" ("file_id", "material_id")
        VALUES('.$site->values->fileforconnect.', '.$site->values->id.')';

        $site->dbquery($query);

        delMatCache($site, $site->values->id);
    }
}

//отвязываем материал
function doUnconnectMat(\iSite $site, $mat)
{
    $mid = $site->values->id;

    $bound_mats = $site->getUser()->getAttribute('bound_materials');
    if (in_array($mat, (array)$bound_mats))
        return;

    $query = <<<EOS
DELETE FROM "material_connections"
WHERE
    "material_parent" = $1 AND "material_child" = $2
    OR "material_parent" = $2 AND "material_child" = $1
EOS;
    $q_params = array($mat, $mid);

    $resd=$site->dbquery($query, $q_params);

    delMatCache($site,$mid);
    delMatCache($site,$mat);

    return($resd);
}

//сохраняем селекторы
function DoAddSelector($site, $selectors, $material)
{
    foreach ($selectors as $selector) {
        $extratypes = GetExtraTypes($site, $selector['mattype_id']);

        //получаем материалы
        $query='SELECT id FROM "materials" WHERE "type_id" = '.$selector['mattype_id'];
        if ($res=$site->dbquery($query)) {
            if (count($res) > 0) {
                foreach ($res as $row) {
                    DoAddExtraMaterials($site, $extratypes, $material, $row['id']);
                }
            }
        }
    }
}

//сохраняем доп. характеристики
function DoAddExtraMaterials(\iSite $site, $extratypes, $material_id, $selector='')
{
    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'id='.$material_id.' #'.count($extratypes));

    $is_changed = false;

    $etypesArr = explode(',', $extratypes[0]);

    if ( ! empty($material_id) && ! empty($etypesArr)) {

        //удаляем предыдущие значения
        if (!empty($selector)) {
            $query='DELETE FROM "extra_materials"
            WHERE "material_id" = $1 AND "type_id" IN ($2)
                AND "selector_id" = $3';
            $q_params = array($material_id, $etypesArr, $selector);
        } else {
            $query = 'DELETE FROM "extra_materials"
            WHERE "material_id" = $1 AND "type_id" IN ($2)
                AND "selector_id" IS NULL';
            $q_params = array($material_id, $etypesArr);
        }

        list($query, $q_params) = $site->dbqueryExpand($query, $q_params);

        $res = $site->dbquery($query, $q_params);

        if ($res) {

            if (pg_affected_rows($res))
                $is_changed = true;

            delMatCache($site,$material_id);

            //сохраняем значения
            foreach ($extratypes[1] as $type) {
                //получаем значение
                $paramname = 'extravalue_'.$type['id'];

                if (!empty($selector) && $selector != 'NULL') {
                    $paramname.='_'.$selector;
                }

                $site->GetPostValues($paramname, 1);

                //сохраняем значение
                if (!empty($site->values->$paramname)) {

                    $value=$site->values->$paramname;

                    if($selector == '')
                        $selector = null;

                    if ($material_id === '')
                        $material_id = null;

                    switch ($type['valuetype']) {
                        case 'valuetext':

                            $query = <<<EOS
                                INSERT INTO "extra_materials"(
                                    "valuename",
                                    "valuetext",
                                    "valuedate",
                                    "material_id",
                                    "type_id",
                                    "selector_id"
                                    )
                                VALUES(
                                    \$1,
                                    \$2,
                                    \$3,
                                    \$4,
                                    \$5,
                                    \$6
                                )
EOS;

                            $q_params = array(null, $value, null, $material_id, $type['id'], $selector);

                            break;

                        case 'valuedate':
                            $query = <<<EOS
                                INSERT INTO "extra_materials"(
                                    "valuename",
                                    "valuetext",
                                    "valuedate",
                                    "material_id",
                                    "type_id",
                                    "selector_id"
                                )
                                VALUES(
                                    \$1,
                                    \$2,
                                    TO_DATE(\$3,'DD.MM.YYYY HH24:MI'),
                                    \$4,
                                    \$5,
                                    \$6
                                )
EOS;

                            $q_params = array(null, null, $value, $material_id, $type['id'], $selector);

                            break;

                        case 'valuemat':
                            $query = <<<EOS
                                INSERT INTO "extra_materials"(
                                    "valuename",
                                    "valuetext",
                                    "valuedate",
                                    "material_id",
                                    "type_id",
                                    "selector_id",
                                    "valuemat"
                                )
                                VALUES(
                                    \$1,
                                    \$2,
                                    \$3,
                                    \$4,
                                    \$5,
                                    \$6,
                                    \$7
                                )
EOS;

                            $q_params = array(null, null, null, $material_id, $type['id'], $selector, $value);

                            break;

                        default:

                            $query = <<<EOS
                                INSERT INTO "extra_materials"(
                                    "valuename",
                                    "valuetext",
                                    "valuedate",
                                    "material_id",
                                    "type_id",
                                    "selector_id"
                                )
                                VALUES(
                                    \$1,
                                    \$2,
                                    \$3,
                                    \$4,
                                    \$5,
                                    \$6
                                )
EOS;

                            $q_params = array($value, null, null, $material_id, $type['id'], $selector);

                            break;
                    }

                    $res = $site->dbquery($query, $q_params);
                    if ($res !== false && pg_affected_rows($res))
                        $is_changed = true;
                }
            }
        }
    }

    if ($is_changed) {
        $site->callHook('materialextraupdate', array(
            'id' => $material_id,// @todo
        ));
    }
}

//печатаем селектовы
//PrintSelectors($this, $selectors, $this->values->id);
function PrintSelectors($site, $selectors, $material = '')
{

    $out='';

    foreach($selectors as $selector)
    {

        $extratypes=GetExtraTypes($site, $selector['mattype_id']);

        $out.='<tr>
            <th colspan="2"><big>'.$selector['mattypename'].'</big></th>
        </tr>';

        //список материалов селектора
        $query='SELECT * FROM "materials" WHERE "type_id" = '.$selector['mattype_id'];
        //print($query);
        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                foreach($res as $row)
                {

                    $out.='<tr>
                    <th colspan="2">'.$row['name'].'</th>
                    </tr>';

                    //получаем характеристики
                    //$params=GetExtraParams($site, $extratypes, $material, $row['id']);

                    //печатаем характеристики
                    $out.=PrintExtraParams($site,$extratypes,$material,$row['id']);

                }
            }
        }

    }

    return $out;

}

//печатаем характеристики в таблицу изменеия/добавления материала
function PrintExtraParams($site, $extratypes, $material = '', $selector='')
{

    $out='';
    $outpice='';

    $params=GetExtraParams($site, $extratypes, $material, $selector);

    //print_r($params);

    foreach($extratypes[1] as $type)
    {

        $value='';

        switch($type['valuetype'])
        {




            case 'valuetext':

                if($selector != '')
                {
                    if(isset($params[$type['id']]['selector'][$selector]))
                    {
                        $value=$params[$type['id']]['selector'][$selector]['valuetext'];
                    }
                }
                elseif(isset($params[$type['id']]))
                {
                    $value=$params[$type['id']]['valuetext'];
                }

                $vname="extravalue_".$type['id'];

                if($selector != '')
                {
                    $vname.='_'.$selector;
                }

                $valueinput="<textarea class='form-control' data-role=\"saveDataInput\" style='max-height: 100px; height: 100px;' name=\"$vname\">$value</textarea>";

                break;

            case 'valuedate':

                if($selector != '')
                {
                    if(isset($params[$type['id']]['selector'][$selector]))
                    {
                        $value=$params[$type['id']]['selector'][$selector]['valuedaten'];
                    }
                }
                elseif(isset($params[$type['id']]))
                {
                    $value=$params[$type['id']]['valuedaten'];
                }

                $vname="extravalue_".$type['id'];

                if($selector != '')
                {
                    $vname.='_'.$selector;
                }

                $valueinput=" <input type='text' name='$vname' data-role=\"saveDataInput\" class='form-control dateinput' value='$value' />";

                break;

            case 'valuemat':

                //GetCityOptions($site,$acity='',$citymattype=12, $parent_id='NULL', $level='')

                if($selector != '')
                {
                    if(isset($params[$type['id']]['selector'][$selector]))
                    {
                        $value=$params[$type['id']]['selector'][$selector]['valuemat'];
                    }
                }
                elseif(isset($params[$type['id']]))
                {
                    $value=$params[$type['id']]['valuemat'];
                }

                //print_r($type);

                $vname="extravalue_".$type['id'];

                if($selector != '')
                {
                    $vname.='_'.$selector;
                }

                $valueinput="<select name='$vname' data-role=\"saveDataInput\" class='styler'><option value=''>---</option>";

                $valueinput.=GetCityOptions($site, $value, $type['valuemattype']);

                $valueinput.="</select>";

                break;

            default:

                if($selector != '')
                {
                    if(isset($params[$type['id']]['selector'][$selector]))
                    {
                        $value=$params[$type['id']]['selector'][$selector]['valuename'];
                    }
                }
                elseif(isset($params[$type['id']]))
                {
                    $value=$params[$type['id']]['valuename'];
                }

                $vname="extravalue_".$type['id'];

                if($selector != '')
                {
                    $vname.='_'.$selector;
                }

                $value = str_replace(
                    array("'", '&amp;quot;'),
                    array('&apos;', '&quot;'),
                    htmlspecialchars($value)
                );

                $valueinput="<input type='text' name='$vname' data-role=\"saveDataInput\" class='form-control' value='$value' />";
                if((($type['id'] == '13')&&($site->settings->DB->name == 'habkrai')) || ($type['name'] == 'Заработная плата'))
                {
                    if($value == '')
                    {
                        $valueinput.='<font color="red"> - ВНИМАНИЕ! Поле не заполнено! Пока поле не будет заполнено, материал не отобразится на сайте!</font>';
                    }
                }

                break;
        }

        $needed='';
        if((($type['id'] == '13')&&($site->settings->DB->name == 'habkrai')) || ($type['name'] == 'Заработная плата'))
        {
            $needed='<sup>*</sup>';
        }

        /*$out.="<tr>
            <th>$needed $type[name]:</th>
            <td>$valueinput</td>
        </tr>";*/

        $outpice.="<div class=\"form-group\">
        <label for=\"$vname\" class=\"col-sm-2 control-label\">$type[name]:</label>
        <div class=\"col-sm-9\">$valueinput</div>
        </div>";

    }


    return($outpice);

}

//получаем значения характеристик и выводим поля для изменения
function GetExtraParams($site, $extratypes, $material = '', $selector='')
{
    $params=Array();

    //print('ffbg'.$material);

    if($material != '')
    {



        $material=(int)$material;

        if($extratypes[0] != '')
        {
            $query='SELECT "m".*, TO_CHAR("m"."valuedate",'."'DD.MM.YYYY HH24:MI'".') "valuedaten" FROM "extra_materials" "m"
        WHERE "m"."type_id" IN ('.$extratypes['0'].')
        AND "m"."material_id" = '.$material;


            if($selector != '')
            {
                $query.=' AND "selector_id" = '.$selector;
            }

            //print($query);

            if($res=$site->dbquery($query))
            {
                if(count($res) > 0)
                {
                    foreach($res as $row)
                    {
                        $params[$row['type_id']]=$row;
                        if($selector != '')
                        {
                            $params[$row['type_id']]['selector'][$selector]=$row;
                        }
                    }
                }
            }

        }

    }

    return($params);
}

//получаем поля характеристик типа материала
function GetExtraTypes($site, $type)
{
    $ids='';
    $types=Array();

    if($type != '')
    {
        $type=(int)$type;

        $query='SELECT *
        FROM "extra_mattypes"
        WHERE "type_id" = '.$type;

        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                foreach($res as $row)
                {
                    if($ids != '')
                    {
                        $ids.=',';
                    }

                    $ids.=$row['id'];

                }

                $types=$res;

            }
        }

    }

    return(Array($ids,$types));
}

function GetSelectors($site, $type)
{

    $selectors = array();

    if ($type) {
        $query = 'SELECT
            "s".*,
            (SELECT "n"."name"
                FROM "material_types" "n" WHERE "n"."id" = "s"."mattype_id") "mattypename"
        FROM "extra_selectors" "s"
        WHERE "s"."type_id" = $1';

        if (($res=$site->dbquery($query, array($type))) !== false && count($res)) {
            $selectors = $res;
        }
    }

    return($selectors);

}

//функция добавления расписания в контакт
function DoAddShed($site){
    $err=false;
    $errtext='';

    if($site->values->sname != '')
    {
        $cid='';
        //получаем id
        $query='SELECT MAX("id")+1 "cid" FROM "schedules"';
        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                $cid=$res[0]['cid'];
            }
        }

        if($cid == '')
        {
            $cid = 1;
        }

        //заносим контакт
        $query='INSERT INTO "schedules" ("id", "contact_id", "name", "content", "material_id")
        VALUES ('.$cid.', '."'".$site->values->doaddshed."'".', '."'".$site->values->sname."'".','."'".$site->values->scontent."'".', NULL)';


        if($res=$site->dbquery($query))
        {
            $err=false;
            $errtext=' - расписание успешно сохранено!';

        }
    }
    else
    {
        $err=true;
        $errtext=' - ошибка: не передано название расписания';
    }

    return Array(!$err,$errtext);
}

//функция изменения контакта
function DoEditCont($site)
{
    $err=false;
    $errtext='';
    $material=$site->values->id;

    if(($site->values->addr != '')&&($site->values->acont != ''))
    {
        $query='UPDATE "contacts" SET "addr" = '."'".$site->values->addr."'".', "tel" = '."'".$site->values->tel."'".',
        "email" = '."'".$site->values->email."'".', "fax" = '."'".$site->values->fax."'".', "building" = '."'".$site->values->building."'".',
        "office" = '."'".$site->values->office."'".' WHERE "id" = '.$site->values->acont;

        if($res=$site->dbquery($query))
        {
            $err=false;
            $errtext=' - контакт успешно сохранен!';

            delMatCache($site,$material);
        }
        else{
            $err=true;
            $errtext=' - ошибка сохранения контакта!';
        }
    }

    return Array(!$err,$errtext);
}

//функция добавления контакта
function DoAddCont($site){

    $err=false;
    $errtext='';

    if($site->values->addr != '')
    {

        $cid='';
        //получаем id
        $query='SELECT MAX("id")+1 "cid" FROM "contacts"';
        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                $cid=$res[0]['cid'];
            }
        }

        if($cid == '')
        {
            $cid = 1;
        }


        if(!isset($site->values->city))
        {
            $site->values->city=0;
        }

        $site->values->city=(int)$site->values->city;

        //заносим контакт
        $query='INSERT INTO "contacts" ("id","material_id","addr","tel","email","fax","building","office","city","latitude","longitude")
        VALUES('.$cid.', '.$site->values->id.', '."'".$site->values->addr."'".','."'".$site->values->tel."'".',
        '."'".$site->values->email."'".','."'".$site->values->fax."'".',
        '."'".$site->values->building."'".','."'".$site->values->office."'".','.$site->values->city.',
        '."'".$site->values->latitude."'".','."'".$site->values->longitude."'".')';



        if($res=$site->dbquery($query))
        {
            $err=false;
            $errtext=' - контакт успешно сохранен!';

            delMatCache($site,$site->values->id);
        }
    }
    else
    {
        $err=true;
        $errtext=' - ошибка: не заполнен адрес контакта';
    }

    return Array(!$err,$errtext);

}

function GetTypeName($site, $type_id)
{
    $type_name = '';

    if (empty($type_id))
        $type_id = null;
    $query = 'SELECT "id", "name", "parent_id" FROM "material_types" WHERE "id" = $1';
    if ($res=$site->dbquery($query, array($type_id))) {
        if($res[0]['parent_id'] != '') {
            $type_name=GetTypeName($site, $res[0]['parent_id']).' - ';
        }
        $type_name.=$res[0]['name'];
    }

    return($type_name);
}