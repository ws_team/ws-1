$(document).ready(function() {

    $('#mainSectionCarousel').slick({
        arrows: false,
        draggable: false,
        responsive: [
            {
                breakpoint: 560,
                settings: {
                    dots: true,
                    draggable: true
                }
            }
        ]
    });

    $('#mainSectionAdditionalCarousel').slick({
        draggable: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '#mainSectionCarousel',
        prevArrow: '#carouselNavArrowPrev',
        nextArrow: '#carouselNavArrowNext' ,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 560,
                settings: {
                    arrows: false
                }
            }
        ]
    });

    $('#usefulLinksCarousel').slick({
        slidesToShow: 6,
        slidesToScroll: 6,
        autoplay: true,
        autoplaySpeed: 5000,
        prevArrow: '#usefulLinksCarouselNavArrowPrev',
        nextArrow: '#usefulLinksCarouselNavArrowNext',
        responsive: [
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5
                }
            },
            {
                breakpoint: 980,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4

                }
            },
            {
                breakpoint: 740,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 560,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

});
