function showError(err) {
    if (typeof err == 'undefined') {
        err = 'Неизвестная ошибка. Пожалуйста свяжитесь с нами и опишите последовательность действий которые привели к данному сообщению.';
    }

    return showModal('#error_popup', err); // Пробрасываем возвращаемый Deferred наружу, чтобы можно было вешать коллбэк цепочкой от вызова этой функции    
}

function clallbackform(){
    // $.colorbox({inline:true, href:'#callbackform'});
    return showModal('#callbackform'); // Пробрасываем возвращаемый Deferred наружу, чтобы можно было вешать коллбэк цепочкой от вызова этой функции
}

function openSubscriptionPopup(modal_id, list_id) {
    var form = $(modal_id).find('form');
    var listIdNode = form.find('input[name="list_id"]');
    listIdNode.val(list_id || '');

    showModal(modal_id);
}

// Показывает модальное окно расположенное в контейнере #modal-overlay, в качестве аргумента передаётся ID окна
function showModal(modal_id, desc) {
    window.console&&console.log('[vidgets] showModal(%s)', modal_id, desc);
    // Если ID передан без символа '#', то добавляем его
    if (!modal_id.match(/\#/)) {
        modal_id = '#'+modal_id;
    }

    // Если элемент с переданным ID отсутствует на странице то возвращаем false.
    if (!$(modal_id).size()) {
        throw('Wrong modal window identificator, window ' + modal_id + ' not found!');
        return false;
    }

    // Создаём Deferred-объект (аналог promise)
    var def = jQuery.Deferred();

    var modals = $('#modal-overlay').find('.centerer').children();
    // Прячем все окна кроме нужного
    modals.not(modal_id).hide();
    // Нужное делаем отображаемым
    modals.filter(modal_id).show();

    if (typeof desc != 'undefined') {
        modals.filter(modal_id).find('.desc:first').empty().text(desc);
    }

    // Плавно показываем overlay с окном
    $('#modal-overlay').stop().fadeIn(function() {
        // По завершению анимации показа нашего окна резолвим наш Deferred-объект, таким образом на это место можно легко повесить callback
        def.resolve();
    });

    // Возвращаем Deferred-объект (В момент вызова функции callback вешается как showModal(...).then(function() { ...callback... });)
    return def;
}

if (typeof DateDialogAPI != 'undefined') {
    DateDialogAPI.prototype.showModal = showModal;
}

function SpecialSetFontsize(size)
{
    var sizeNumval;

    switch (size) {
        case 'small':
            sizeNumval = 0;
            break;
        case 'normal':
            sizeNumval = 1;
            break;
        case 'big':
            sizeNumval = 2;
            break;
        default:
            window.console&&console.log('[vidgets] unsupported fontsize keyword "%s"', size);
    }

    var fontSizes = ['small', 'normal', 'big'];
    var node = $(document.body);

    for (var i=0; i<fontSizes.length; i++) {
        node.removeClass('fontsize-' + fontSizes[i]);
    }
    node.addClass('fontsize-' + size);

    SelectSpecialFontSize(sizeNumval, size);
}

function SelectSpecialFontSize(size, sizeKwd)
{
    //меняем переменную
    $.ajax({
        type: 'POST',
        url: '/',
        async: true,
        data: 'menu=main&fontSize=' + size + '&special_fontsize=' + encodeURIComponent(sizeKwd)
    });
}

function SpecialSetColor(color)
{
    var colors = ['normal', 'inverted', 'calm'];
    if (colors.indexOf(color) < 0) {
        window.console&&console.log('[vidgets] unsupported color "%s"', color);
        return;
    }

    var node = $(document.body);
    for (var i = 0; i < colors.length; i++) {
        node.removeClass('color-' + colors[i]);
    }

    node.addClass('color-' + color);

    $.ajax({
        type: 'POST',
        url: '/',
        async: true,
        data: 'menu=main&special_color=' + encodeURIComponent(color)
    });
}

(function($) {
    if ($.fn.style) {
        return;
    }

    // Escape regex chars with \
    var escape = function (text) {
        return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
    };

    // For those who need them (< IE 9), add support for CSS functions
    var isStyleFuncSupported = !!CSSStyleDeclaration.prototype.getPropertyValue;
    if (!isStyleFuncSupported) {
        CSSStyleDeclaration.prototype.getPropertyValue = function (a) {
            return this.getAttribute(a);
        };
        CSSStyleDeclaration.prototype.setProperty = function (styleName, value, priority) {
            this.setAttribute(styleName, value);
            var priority = typeof priority != 'undefined' ? priority : '';
            if (priority != '') {
                // Add priority manually
                var rule = new RegExp(escape(styleName) + '\\s*:\\s*' + escape(value) +
                    '(\\s*;)?', 'gmi');
                this.cssText =
                    this.cssText.replace(rule, styleName + ': ' + value + ' !' + priority + ';');
            }
        };
        CSSStyleDeclaration.prototype.removeProperty = function (a) {
            return this.removeAttribute(a);
        };
        CSSStyleDeclaration.prototype.getPropertyPriority = function (styleName) {
            var rule = new RegExp(escape(styleName) + '\\s*:\\s*[^\\s]*\\s*!important(\\s*;)?',
                'gmi');
            return rule.test(this.cssText) ? 'important' : '';
        }
    }

    // The style function
    $.fn.style = function (styleName, value, priority) {
        // DOM node
        var node = this.get(0);
        // Ensure we have a DOM node
        if (typeof node == 'undefined') {
            return this;
        }
        // CSSStyleDeclaration
        var style = this.get(0).style;
        // Getter/Setter
        if (typeof styleName != 'undefined') {
            if (typeof value != 'undefined') {
                // Set style property
                priority = typeof priority != 'undefined' ? priority : '';
                style.setProperty(styleName, value, priority);
                return this;
            } else {
                // Get style property
                return style.getPropertyValue(styleName);
            }
        } else {
            // Get CSSStyleDeclaration
            return style;
        }
    };
}(jQuery));

(function($) {
    $(document).ready(function() {
        $('.open-news-gallery').on('click', function(e) {

            $('a.colorbox.image-'+$(this).attr('data-img')).click();
            e.preventDefault();
            return false;
        });
                
        if ($('#search-form').size()) {
            $('#search-form').find('.radios').on('click', 'input[type="radio"]', function() {

                $('#search-form').find('.radios').find('label.active').removeClass('active');
                $(this).siblings('label').addClass('active');
            });
        }

        var secMenu = $('.secmenu');
        var mainMenu = $('.mainmenu');

        if (secMenu.size()) {
            SecMenuTop = secMenu.offset().top;
            SecMenuTop -= mainMenu.height();

            window.console&&console.log('[vidgets] secMenuTop: %d y', SecMenuTop);

            $(window).on('scroll.secmenu', function() {
                if ($(window).scrollTop() >= SecMenuTop && $(window).width() > 724) {
                    secMenu.addClass('fixed');
                } else {
                    secMenu.removeClass('fixed');
                }
            });
        }

        $(window).on('scroll.searchResults', function() {
            // Если страница результатов поиска
            if ($('.contentblock.search-results').size()) {
                // Если прокрутили ниже чем заголовок
                if ($(window).scrollTop() >= $('.contentblock.search-results').offset().top + 50) {
                    // Прибиваем заголовок
                    $('.contentblock.search-results').css('padding-top', $('.search-query-title').height() + 60 + 30);
                    $('.search-query-title').addClass('fixed');

                    // Вычисляем максимально допустимую позицию заголовка от верха страницы при которой он будет не ниже последнего результата поиска на странице
                    var lastRowTopOffset = $('.contentblock.search-results').offset().top + $('.contentblock.search-results').height() - $('.contentblock.search-results').find('.row.last').height() + 50;

                    // Если прибитый заголовок пытается налезть на последний результат поиска
                    if ($(window).scrollTop() >= lastRowTopOffset) {
                        // Меняем ему св-во top, чтобы не наезжал на последний результат
                        $('.search-query-title').css('top', lastRowTopOffset - $(window).scrollTop());
                    } else {
                        // Иначе, сбрасываем приоритетное значение св-ва top
                        $('.search-query-title').css('top', '');
                    }
                } else {
                    $('.contentblock.search-results').css('padding-top', 0);
                    $('.search-query-title').removeClass('fixed');                    
                }
            }            
        });

        $(window).on('scroll.toTopButton', function(e) {
            if ($(window).scrollTop() >= 200) {
                if (!window.onTopShowing) { 
                    window.onTopShowing = 1;
                    $('.on-top').stop().fadeIn(400, function() { 
                        delete(window.onTopShowing);
                    }); 
                }
            } else {
                if (!window.onTopShowing) { 
                    window.onTopShowing = 1;
                    $('.on-top').stop().fadeOut(400, function() { 
                        delete(window.onTopShowing);
                    }); 
                }
            }
        });

        $(window).trigger('scroll.toTopButton');

        $(document).on('keyup', '.search-query-title', function(e) {
            // Если таймаут уже отсчитывается и юзер повторно нажал кнопку, то перезапустить таймаут 
            if (typeof LiveSearchTimeout != 'undefined') {
                clearTimeout(LiveSearchTimeout);
            }

            fitSearchFontSize();

            var $searchField = $(this);

            // Если был нажат Enter то стартуем сразу, иначе через секунду
            var delay = (e.keyCode == 13) ? 1 : 1000;

            // Функция в таймауте отсылающая AJAX-запрос
            LiveSearchTimeout = setTimeout(function() {

            var searchURL = document.location.href.replace(/searchtext\=[^&]*\&/, 'searchtext='+$searchField.val()+'&');

                // $.ajax({
                //     method: 'GET',
                //     url: searchURL,
                //     timeout: 30000,
                //     success: function(data) {
                //         updateMainContent(data);
                //         if (window.hasOwnProperty('history')) {
                //             window.history.pushState(null, 'Поиск: '+$searchField.val(), searchURL);
                //         }

                //         if (typeof DateDialog != 'undefined') {
                //             DateDialog.getFields();
                //         }
                //     },
                //     error: function(data) {
                //         throw('Can't update Live Search');
                //         throw(data);
                //     }
                // });

                $('.search-results-list-wrapper').load(searchURL.replace(/\s/g, '%20')+ ' .search-results-list' , function() {
                    if (window.hasOwnProperty('history')) {
                        window.history.pushState(null, 'Поиск: '+$searchField.val(), searchURL);
                    } 
                    if (typeof DateDialog != 'undefined') {
                        DateDialog.getFields();
                    }

                    $('a').each(function() {
                        if ($(this).attr('href').match(/searchtext\=[^\&]*/)) {
                            var modifiedURL = $(this).attr('href').replace(/searchtext\=[^\&]*/, 'searchtext='+$searchField.val().replace(/\s/g, '%20'));
                            $(this).attr('href', modifiedURL);
                        }
                    });
                });


                delete LiveSearchTimeout;

            }, delay);
        });

        fitSearchFontSize();

        // прикрепляем jquery-ui на поисковые поля ввода
        $(document).on('keydown.autocomplete', '.search-query-title, .searchtext-field', function(e) {
            $(this).autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: '/',
                        dataType: 'json',
                        data : {
                            menu: 'ajax',
                            imenu: 'GetPopularSearchPhrases',
                            phrase: request.term,
                            count: '5'
                        },
                        success : function(data) {
                            response(data);
                        }
                    });
                },
                minLength: 2
            });
        });

        $('.search-query-title, .searchtext-field').attr('autocomplete', 'on');


        // Обработчик клика кнопки закрытия всплывашки о том, что сайт в тестовом режиме
        // KHABKRAI-477 
        $('.custom-closer').on('click', function(event) {
            $modal = $('#modal-overlay').find('.centerer').children().filter(':visible');
            $('#modal-overlay').stop().fadeOut(400, function() {
                $modal.stop().css('margin-top', 0);
                $('body').css('overflow', '');
            });   
            $modal.stop().animate({
                marginTop: -900
            }, 400);
            event.preventDefault();
        });

        // Функция для окна обратной связи, добавляет новое поле загрузки скриншота при заполнении предыдущего
        $('#callbackform').on('change', 'input.file.autoclone', function(e) {
            $fieldsWrapper = $('#callbackform').find('.screenfields');            
            fieldsCount = $fieldsWrapper.find('.screen-field').size();
            // Значение предыдущего изменённого поля
            var prevValue = e.currentTarget.value;
            // Если последнее изменённое поле не стало пустым
            if (prevValue != '') {
                // Создаём новое
                $fieldsWrapper.append(
                    '<div class="screen-field" data-screen-number="' + fieldsCount +'">' +
                        '<input type="file" name="screen['+fieldsCount+']" class="file autoclone">' +
                    '</div>'
                );
            // Иначе...
            } else {
                if (fieldsCount > 1) {
                    jQuery(e.currentTarget).parent().remove();
                    // Пересчитываем нумерацию для оставшихся
                    $fieldsWrapper.find('.screen-field').each(function(idx) {
                        $(this).attr('data-screen-number', idx);
                        $(this).find('input.file.autoclone').attr('name', 'screen['+idx+']');
                    });
                }
                // Удаляем пустое поле
            }
        });
        // Функция для окна обратной связи, добавляет новое поле загрузки скриншота при заполнении предыдущего
        $('#accreditation_form').on('change', 'input.file.autoclone', function(e) {
            $fieldsWrapper = $('#accreditation_form').find('.screenfields');            
            fieldsCount = $fieldsWrapper.find('.screen-field').size();
            // Значение предыдущего изменённого поля
            var prevValue = e.currentTarget.value;
            // Если последнее изменённое поле не стало пустым
            if (prevValue != '') {
                // Создаём новое
                $fieldsWrapper.append('<div class="screen-field"' +
                    'data-screen-number="' + fieldsCount + '">' +
                    '<input type="file" name="screen['+fieldsCount+']" class="file autoclone">' +
                    '</div>'
                );
            // Иначе...
            } else {
                if (fieldsCount > 1) {
                    jQuery(e.currentTarget).parent().remove();
                    // Пересчитываем нумерацию для оставшихся
                    $fieldsWrapper.find('.screen-field').each(function(idx) {
                        $(this).attr('data-screen-number', idx);
                        $(this).find('input.file.autoclone').attr('name', 'screen['+idx+']');
                    });
                }
                // Удаляем пустое поле
            }
        });

        $('#callbackform').find('.screenshot-first').on('click', function() {
            $('#callbackform').find('.screen-field').slideDown();
        });

        $('#accreditation_form').find('.screenshot-first').on('click', function() {
            $('#accreditation_form').find('.screen-field').slideDown();
        });

        jQuery('body').on('click', '.pseudo-radio', function() {

            jQuery(this).siblings('input.custom[type=radio]').click();

            var customRadios = jQuery('input.custom[type=radio]');

            customRadios.each(function() {
                if (jQuery(this).is(':checked')) {
                    jQuery(this).siblings('.pseudo-radio').addClass('checked');
                } else {
                    jQuery(this).siblings('.pseudo-radio').removeClass('checked');
                }

                // jQuery(this).removeAttr('checked');
            });         

        });

        jQuery('body').on('click', 'label', function() {
            if (!jQuery(this).siblings('.pseudo-radio').size()) {
                return;
            }   

            jQuery(this).siblings('input.custom[type=radio]').click();

            var customRadios = jQuery('input.custom[type=radio]');

            customRadios.each(function() {
                if (jQuery(this).is(':checked')) {
                    jQuery(this).siblings('.pseudo-radio').addClass('checked');
                } else {
                    jQuery(this).siblings('.pseudo-radio').removeClass('checked');
                }
                // jQuery(this).removeAttr('checked');
            });         

        });

        jQuery('body').on('click', '.pseudo-checkbox', function() {

            var $realCheckbox = jQuery(this).siblings('input.custom[type=checkbox]')

            $realCheckbox.click();

            if ($realCheckbox.is(':checked')) {
                jQuery(this).addClass('checked');
            } else {
                jQuery(this).removeClass('checked');
            }       

        });

        jQuery('body').on('click', 'label', function() {
            if (!jQuery(this).siblings('.pseudo-checkbox').size()) {
                return;
            }

            if (!jQuery(this).siblings('input.custom[type=checkbox]').is(':checked')) {
                jQuery(this).siblings('.pseudo-checkbox').addClass('checked');
            } else {
                jQuery(this).siblings('.pseudo-checkbox').removeClass('checked');
            }

            //return false;     
        });

        // Задаём radio-переключатели
        var customRadios = $('#search-form').find('input[type="radio"]').not('.custom');
        var customCheckboxes = $('#accreditation_form').find('input[type="checkbox"]').not('.custom');

        // $.merge(customRadios, $('.block-voting').find('input[type='radio']'));

        if (typeof customRadios != 'undefined') {

            // Добавляем классы и вставляем 
            customRadios
                .addClass('custom')
                .after('<span class="pseudo-radio"></span>');

            customRadios.each(function() {
                if (jQuery(this).is(':checked')) {
                    jQuery(this).siblings('.pseudo-radio').addClass('checked');
                }   

                if (jQuery(this).hasClass('ajax-processed')) {
                    jQuery(this).removeClass('ajax-processed');
                }       
            });

            // jQuery('.pseudo-radio').off('click');

        }


        // Добавляем классы и вставляем 
        if (typeof customCheckboxes != 'undefined') {

            customCheckboxes
            .addClass('custom')
            .after('<span class="pseudo-checkbox"></span>');

            customCheckboxes.each(function() {
                if (jQuery(this).is(':checked')) {
                    jQuery(this).siblings('.pseudo-checkbox').addClass('checked');
                }

                if (jQuery(this).hasClass('ajax-processed')) {
                    jQuery(this).removeClass('ajax-processed');
                }           
            });

            // jQuery('.pseudo-checkbox').off('click');

        }        

        $('.topmenu1-icon.search').on('click', function(event) {
            $('#search-form').stop().slideToggle();
            $(this).toggleClass('active');
            event.preventDefault();
            event.stopPropagation();
        });

        $(document).on('click', function(e) {
            if ($('.topmenu1-icon.search').hasClass('active') && !$(e.target).closest('form#search').length
                && !$(e.target).closest('ul.ui-autocomplete').length) {
                $('.topmenu1-icon.search').click();
            }
        });

        $('.search-closer').on('click', function(event) {
            if ($('.topmenu1-icon.search').hasClass('active')) {
                $('.topmenu1-icon.search').click();
            }
            event.preventDefault();
        });

        $('#search-form').find('.search-example').find('a').on('click', function(event) {
            $('.searchtext-field').val($(this).text());
            event.preventDefault();
        });

        $('#search-form').find('input[name="searchtype"]').on('change', function() {
            if ($(this).val() == 'documents') {
                $('#search-form').find('.dropdown').hide();
                $('.searchtext-field').addClass('wide');
            } else {
                $('.searchtext-field').removeClass('wide');
                $('#search-form').find('.dropdown').show();
            }
        });
window.console&&console.log('jCarouselLite init');
        $('.footertop_scroll').jCarouselLite({
            btnNext: '.footertop_scroll_next',
            btnPrev: '.footertop_scroll_prev',
            visible: 4,
            scroll: 2,
            circular: true,
            auto: 3000
        });

        // валидатор формы обратки
        $('form#callback').find('input[type="submit"]').on('click', function() {
            var validate = (($('form#callback').find('input[name="err_fio"]').val() != '')
                && ($('form#callback').find('input[name="err_email"]').val().match(/\S*\@\S*\.\S{2,6}/))
                && ($('form#callback').find('textarea[name="err_content"]').val() != ''
                && $('form#callback').find('textarea[name="err_content"]').val().length >= 10));

            if (!validate) {
                // Выдать ошибку и навесить на её закрытие показ некорректно заполненой формы обратной связи
                showError('Не верно заполнены некоторые поля формы! (Описание ошибки - не менее 10 символов)').then(function() {
                    $('#error_popup').find('.popup-closer').on('click.error', function() {
                        showModal('#callbackform');
                    });
                });
                return false;
            }
        });

        $('#date_dialog').find('.clear-range').on('click', function(e) {
            if (typeof DateDialog != 'undefined') {
                DateDialog.reset();
            }

            e.preventDefault();
        });

        if ($('.search-results').size()) {
            $('#date_dialog').find('.apply-filter').on('click', function(e) {
                console.log('DateDialog: apply');

                $('#search_date_filter').ajaxSubmit({
                    success: function(result) {
                        updateMainContent(result);
                        DateDialog.getFields();
                    }
                });

                var searchURL = document.location.origin + document.location.pathname + '?' + $('#search_date_filter').formSerialize();
                if (window.hasOwnProperty('history')) {
                    window.history.pushState(null, 'Поиск по дате: ' + $(DateDialog.dateStartField).val() + ' - ' + $(DateDialog.dateEndField).val(), searchURL);
                }

                $('#date_dialog').find('.custom-closer').click();

                e.preventDefault();
            });
        }

        $('#search_date_filter').on('submit', function() {
            var searchURL = document.location.origin + document.location.pathname + '?' + $('#search_date_filter').formSerialize();
            if (window.hasOwnProperty('history')) {
                window.history.pushState(null, 'Поиск: по дате:'+ + $(DateDialog.dateStartField).val() + ' - ' + $(DateDialog.dateEndField).val(), searchURL);
            }

            $(this).ajaxSubmit({
                success: function(result) {
                    updateMainContent(result);
                    DateDialog.getFields();
                }
            });
        });

        $('a.callback').on('click', function() {
            showModal('callbackform');
        });

        // Автовертушка для новостей на главной
        if ($('.content.main').size() && $('.main_news').size()) {

            setInterval(function() {
                $items = $('.main_news').find('.option');

                if ($('.main_news').find('.option:hover').size()) { return; }

                var itemsCount = $items.size();
                var currentItem = $items.index($items.filter('.active'));
                var nextItem = (currentItem < itemsCount - 1) ?  currentItem + 1 : 0;
                $items.eq(nextItem).trigger('mouseover');
            }, 5000);

        }

        // Автовертушка для новостей на главной
        if ($('.content.main').size() && $('.main_groups_pages').size()) {

            setInterval(function() {
                $pages = $('.main_groups_pages').find('.main_groups_page');
                var pagesCount = $pages.size();
                var currentPage = $pages.index($pages.filter('.active'));
                var nextPage = (currentPage < pagesCount - 1) ?  currentPage + 1 : 0;
                // var pageNum = $pages.eq(nextPage).attr('id').match(/^main_groups_page_(\d{1,})$/);
                ChangeMainGProgPage(nextPage + 1);
            }, 5000);

        }

        if ($('.content.main').size() && $('.guber_groups_pages').size()) {

            setInterval(function() {
                $pages = $('.guber_groups_pages').find('.main_groups_page');
                var pagesCount = $pages.size();
                var currentPage = $pages.index($pages.filter('.active'));
                var nextPage = (currentPage < pagesCount - 1) ?  currentPage + 1 : 0;
                // var pageNum = $pages.eq(nextPage).attr('id').match(/^main_groups_page_(\d{1,})$/);
                ChangeGuberProgPage(nextPage + 1);
            }, 5000);

        }


        if ($('.main_groups_arrows').size()) {
            $('.main_groups_arrows').find('.next,.prev').on('click', function(e) {
                console.log(255);
                $pages = $('.main_groups_pages').find('.main_groups_page');
                var pagesCount = $pages.size();
                var currentPage = $pages.index($pages.filter('.active'));

                if ($(this).hasClass('prev')) { 
                    var prevPage = (currentPage > 0) ?  currentPage - 1 : pagesCount - 1;
                    ChangeMainGProgPage(prevPage + 1); 
                }

                if ($(this).hasClass('next')) { 
                    var nextPage = (currentPage < pagesCount - 1) ?  currentPage + 1 : 0;
                    ChangeMainGProgPage(nextPage + 1); 
                }

                e.preventDefault();
            });

        }

        $('.filtered-materials-list,.material_page').on('click', '.share-link', function(e) {
            var link = e.target;

            if ($(link).hasClass('active')) {
                $('.activity-buttons').find('.share-modal').stop().fadeOut(200);
            } else {
                $('.activity-buttons').find('.share-modal').stop().fadeIn(200);
            }

            $(link).toggleClass('active');
            e.preventDefault();
        }); 
        
        $('a.colorbox').colorbox({ rel:'gal' });

        // Если страница новостей 
        // if ($('.news_wrapper').size() || $('.omsu_news_wrapper').size() || $('.omsu_all_news_wrapper').size() || $('.omsu_vacancies_wrapper').size()) {
        if ($('.filtered-materials-list').size()) {

            $('#date_dialog').find('.apply-filter').on('click', function(e) { 
                var datestart = $.proxy(ListDateFilter.getDates, ListDateFilter)()[0];
                var dateend = $.proxy(ListDateFilter.getDates, ListDateFilter)()[1];
                $.proxy(ListDateFilter.update, ListDateFilter, datestart, dateend)();
                e.preventDefault();
            });

            // $('.news_wrapper,.omsu_news_wrapper,.omsu_all_news_wrapper,.omsu_vacancies_wrapper').on('click', '.keywords-expander-link', function(e) {
            $('.filtered-materials-list').on('click', '.keywords-expander-link', function(e) {
                $link = $(e.target);
                $keywordsContainer = $link.parent().parent();

                $keywordsContainer.find('.hidden,.showed').toggleClass('showed').toggleClass('hidden');

                var linkText = (!$link.hasClass('expanded')) ? 'скрыть' : 'показать все';

                $link.text(linkText);

                $link.toggleClass('expanded');

                e.preventDefault();
            });

            console.log('Linear call');
            console.log(BlocksAPI);

            setTimeout(function() {

                console.log('BlocksAPI.afterExecute call');

                Filters = {}

                if (typeof RegionsFilter != 'undefined') { Filters['region-filter'] = RegionsFilter; }
                if (typeof KeywordsFilter != 'undefined') { Filters['keywords-filter'] = KeywordsFilter; }
                if (typeof SingleDateFilter != 'undefined') { Filters['single-date-filter'] = SingleDateFilter; }
                if (typeof ListDateFilter != 'undefined') { Filters['list-date-filter'] = ListDateFilter; }
                if (typeof OIVsFilter != 'undefined') { Filters['oiv-filter'] = OIVsFilter; }
                if (typeof VacancyTypes != 'undefined') { Filters['vacancy-type-filter'] = VacancyTypes; }
                if (typeof DocumentTypes != 'undefined') { Filters['document-type-filter'] = DocumentTypes; }
                if (typeof SubTypes != 'undefined') { Filters['sub-type-filter'] = SubTypes; }
                if (typeof StatusesFilter != 'undefined') { Filters['status-filter'] = StatusesFilter; }

                // Для API каждого фильтра
                for(var filterClass in Filters) { 

                    // Если в нём есть метод подписки на событие апдейта фильтра
                    if (typeof Filters[filterClass].addUpdateCallback == 'function') {
                        // Пишем туда функцию, которая позволит нарисовать ссылку-значение в шапке списка новостей
                        Filters[filterClass].addUpdateCallback(function(value, value2) {
                            $pageField = $('form#filters').find('input[name="page"]');
                            $pageField.val(1);
                            $('form#filters').submit();
                            return false;
                        });
                    } else {
                        throw('No "addUpdateCallback" method in "' + filterClass + '" API!');
                    }                
                }

            }, 500);                

            // Обработчик ссылки сброса параметров фильтра
            // $('.news_wrapper,.omsu_news_wrapper,.omsu_all_news_wrapper,.omsu_vacancies_wrapper').on('click', '.reset-filter', function(e) {
            $('.filtered-materials-list').on('click', '.reset-filter', function(e) {
                var $link = $(e.target);
                var $parent = $link.parent();
                          
                // Проверяем по классам контейнера API какого фильтра нам нужно      
                for(var filterClass in Filters) { 
                    if ($parent.hasClass(filterClass)) {
                        // И если в нём есть метод reset - вызываем его
                        if (typeof Filters[filterClass].reset == 'function') {
                            Filters[filterClass].reset();
                        } else {
                            throw('No "reset" method in "' + filterClass + '" API!');
                            return false;
                        }
                    }
                } 

                $parent.remove();               

                e.preventDefault();
            });

            // Обработчик ссылки сброса параметров фильтра
            // $('.news_wrapper,.omsu_news_wrapper,.omsu_all_news_wrapper,.omsu_vacancies_wrapper').on('click', '.remove-filter-value', function(e) {
            $('.filtered-materials-list').on('click', '.remove-filter-value', function(e) {
                var $link = $(e.target);
                var $parent = $link.parent();
                var value = $parent.find('span.value').text();
                          
                // Проверяем по классам контейнера API какого фильтра нам нужно      
                for(var filterClass in Filters) { 
                    if ($parent.hasClass(filterClass)) {
                        // Если это последнее из множественных значений фильтра
                        if ($('.filter-values').find('.remove-filter-value.'+filterClass).size() == 1) {
                            // То в нём есть метод reset - вызываем его
                            if (typeof Filters[filterClass].reset == 'function') {
                                Filters[filterClass].reset();
                            // Если reset не предусмотрен,
                            } else {
                                // Пробуем ломиться в метод deleteValue
                                if (typeof Filters[filterClass].deleteValue == 'function') {
                                    Filters[filterClass].deleteValue(value);
                                // Если и его нет - говорим, что разраб гусь, а API гавно!((
                                } else {
                                    throw('No "reset" and "deleteValue" method in "' + filterClass +
                                        '" API!');
                                    return false;
                                }
                            }
                        // Если не последнее
                        } else {
                            // Пробуем ломиться в метод deleteValue
                            if (typeof Filters[filterClass].deleteValue == 'function') {
                                Filters[filterClass].deleteValue(value);
                            // Если нет метода
                            } else {
                                // Печалька...
                                throw('No "deleteValue" method in "'+filterClass+'" API!');
                                return false;
                            }
                        }
                    }
                } 

                $parent.remove();               

                e.preventDefault();
            });

            // Обработчик ссылки установки значений фильтра
            // $('.news_wrapper,.omsu_news_wrapper,.omsu_all_news_wrapper,.omsu_vacancies_wrapper').on('click', '.filter-link', function(e) {
            $('.filtered-materials-list').on('click', '.filter-link', function(e) {
                var link = e.target;
                var value = $(link).text();

                // Проверяем по классам контейнера API какого фильтра нам нужно      
                for(var filterClass in Filters) { 
                    if ($(link).hasClass(filterClass)) {

                        // Если ссылка задаёт значение одному из фильтров,
                        // который в перспективе рискует стать множественным
                        if (['region-filter','keywords-filter','oiv-filter','vacancy-type-filter','status-filter','document-type-filter','sub-type-filter'].indexOf(filterClass) >= 0) {
                            // Если это всё-же для фильтра регионов - то там заплёт в том,
                            // что истинное значение не в тексте а в аттрибуте
                            if (filterClass == 'region-filter') {
                                value = $(link).attr('data-region');
                            }
                            if (filterClass == 'oiv-filter') {
                                value = $(link).attr('data-oiv');
                            }
                            if (filterClass == 'vacancy-type-filter') {
                                value = $(link).attr('data-vacancy-type');
                            }
                            if (filterClass == 'document-type-filter') {
                                value = $(link).attr('data-document-type');
                            }
                            if (filterClass == 'sub-type-filter') {
                                value = $(link).attr('data-sub-type');
                            }
                            if (filterClass == 'status-filter') {
                                value = $(link).attr('data-status');
                            }
                            // И если в нём есть метод update
                            if (typeof Filters[filterClass].update == 'function') {
                                // Передаём в него напрямую массив с одним значением,
                                // потому-как сейчас нам нужен single-value режим
                                Filters[filterClass].update([value]);
                                e.preventDefault();
                                return;
                            } else {
                                // Если метода update по какой-то неведомой причине нет - ну тут уж ничо не поделаешь, дерьмо какое-то...
                                throw('No "update" method in "' + filterClass + '" API!');
                                e.preventDefault();
                                return false;
                            }
                        }

                        // Если фильтр одиночной даты, то тоже заплёт с аттрибутом ссылки
                        if (filterClass == 'single-date-filter') {
                            value = $(link).attr('data-dmY-date');
                        }

                        // если есть метод update - вызываем его
                        if (typeof Filters[filterClass].update == 'function') {
                            Filters[filterClass].update(value);
                        } else {
                            throw('No "update" method in "'+filterClass+'" API!');
                        }
                    }
                }

                $(link).addClass('active');                

                e.preventDefault();
                return false;
            });

            // $('.omsu_news_wrapper,.omsu_all_news_wrapper,.omsu_vacancies_wrapper').on('click', '.share-link', function(e) {

            FilterAPI = {};

            FilterAPI.form = $('form#filters');
            FilterAPI.fields = $('form#filters').find('input, select');


        /*
         *  ATTENTION! ATTENZIONE! ACHTUNG!
         *
         *  Механизм AJAX предзагрузки страниц,
         *  универсален, элегантен, лёгок в применении
         *  ЦЕПЛЯЕТСЯ САМ К ССЫЛКАМ С КЛАССОМ 'preloadable'
         *  Контейнер в который грузить полученные данные нужно указать в data-ajax-container
         *
         */

        function EmptyPreloader() {
            this.executor = null;
            this.container = null; 
            this.url = null;
            this.timer = null;
            this.state = null;
            this.afterLoad = function() {};
        }

        Preloader = new EmptyPreloader();

        $('body').on('mouseover', 'a.preloadable', function(e) {
            var link = e.target;

            if (typeof Preloader == 'undefined') {
                throw('Preloader object is not initialized!');
                return false;
            }

            if (Preloader.timer != null) {
                clearTimeout(Preloader.timer);
                Preloader = new EmptyPreloader();
            }


            Preloader.state = 'timer started';
            Preloader.executor = link;
            Preloader.container = (typeof Preloader.container == 'undefined' || Preloader.container == null) ? '.content.main' : $(link).attr('data-ajax-container');
            Preloader.url = $(link).attr('href');
            console.log(link);
            console.log('Preloader -> '+Preloader.state+'('+Preloader.url+')');
            Preloader.timer = setTimeout(function() {
                Preloader.state = 'sending AJAX request';
                console.log('Preloader -> '+Preloader.state);

                $.ajax({
                    url: Preloader.url,
                    success: function(data) {
                        Preloader.state = 'loaded';
                        Preloader.responseData = data;
                        console.log('Preloader -> '+Preloader.state);
                        Preloader.afterLoad();
                    },
                    error: function(errmsg) {
                        Preloader = new EmptyPreloader();
                        throw(errmsg);
                    }
                });
            }, 50);

            console.log('Preloader -> '+Preloader.state);
        });

        $('body').on('mouseout', 'a.preloadable', function(e) {
            if (Preloader.timer != null) {
                clearTimeout(Preloader.timer);
                Preloader = new EmptyPreloader();
            }
        });

        // Универсальный обработчик на ссылки с предзагрузкой
        $('body').on('click', 'a.preloadable', function(e) {
            var link = e.target;

            if (Preloader.responseData != null && Preloader.state == 'loaded') {
                if ($(link).attr('href') == Preloader.url) {
                    if ($(Preloader.container).size()) {
                        // $(Preloader.container).empty().html(111);
                        $(Preloader.container).empty().html(Preloader.responseData);
                        if (window.hasOwnProperty('history')) {
                            window.history.pushState({ updatedElement: Preloader.container, data: Preloader.responseData }, 'Link loaded', Preloader.url);
                        }                        
                        Preloader = new EmptyPreloader();
                    } else {
                        throw('AJAX preload container not exists! ("'+Preloader.container+'")');
                        return;
                    }
                }
            } else {
                console.log('Preloader -> link is not ready now, set afterLoad() callback');
                Preloader.afterLoad = function() {
                    $(link).trigger('click');
                    console.log('Preloader -> afterLoad() runned');
                }
            }

            window.addEventListener('popstate', function(e) {
                document.location.href = window.location.href;
            });            

            Preloader.state = 'updating page';
            console.log('Preloader -> '+Preloader.state);            

            e.preventDefault();
        });

        }

        if ($('.f_materials_4_162_208').size()) {
            $('.show-on-map').on('click', function(e) {
                var map_id = $(this).attr('data-map');
                $('#'+map_id).show();
                
                if ($(this).hasClass('map-created')) {
                    e.preventDefault();
                    return false;
                }

                $(this).addClass('map-created');


                var coords, map = null;
                var myGeocoder = ymaps.geocode($(this).attr('title'));
                myGeocoder.then(
                    function (res) {
                        coords = res.geoObjects.get(0).geometry.getCoordinates();
                        map = new ymaps.Map(map_id, {
                            center: coords,
                            // center: [27.525773, 53.89079],
                            zoom:17,
                            controls: ['zoomControl']
                        });

                        var myPlacemark = new ymaps.Placemark(coords);
                        map.geoObjects.add(myPlacemark);

                        console.log(coords);
                        console.log(map);                        
                        console.log(res);                        
                    },
                    function (err) {
                        throw('Не удалось получить координаты геообъекта `'+$(this).attr('title')+'`');
                    }
                );

                console.log($(this).attr('title'));
                console.log(myGeocoder);

                e.preventDefault();
            });

            $('.map-closer').on('click', function(e) {
                $(this).parent().hide();
                e.preventDefault();
            });
        }

        if ($('.photos_wrapper').size()) {
            $('.small-photo').on('click', function(e) {
                var url = $(this).parent().parent().parent().attr('href') + '#' + $(this).attr('data-anchor');

                document.location.href = url;

                e.stopPropagation();
                e.preventDefault();
            });
        }

        if ($('.filtered-materials-list').size() && $('.filtered-materials-list').find('.date-splitter.attachable').size()) {
            $(window).on('scroll', function() {

                // return;

                var y = $(window).scrollTop();
                var offsetY = $('.secmenu_wrapper').height();
                var fixedElement = null;

                $('.filtered-materials-list').find('.date-splitter.attachable').each(function(i) {

                    if (typeof $(this).attr('data-y') == 'undefined' && !$(this).hasClass('fixed')) {
                    // if (!$(this).hasClass('fixed')) {
                        elementOffsetTop = $(this).offset().top;
                        $(this).attr('data-y', elementOffsetTop);
                    }

                    var elementY = $(this).attr('data-y') - y - offsetY;
                    if (elementY < 0) {
                        fixedElement = $(this);
                    } else {
                        if ($(this).hasClass('fixed')) {
                            $(this).removeClass('fixed');
                            if (!$('.filtered-materials-list').find('.date-splitter.attachable.fixed').size()) {
                                $(this).parent().css('padding-top', 0);
                            }
                        }
                    }
                });

                $('.filtered-materials-list').find('.date-splitter.attachable.fixed').removeClass('fixed');

                if (fixedElement != null) {
                    fixedElement.addClass('fixed');
                    fixedElement.parent().css('padding-top', 48);                    
                }

            });
        }

        if ($('.news_material').size()) {
            var toggleY = $('.news_material').find('h1').offset().top;

            $(window).on('scroll', function() {
                if ($(window).scrollTop() >= toggleY) {
                    $('.news_material').find('h1,.activity-buttons').addClass('fixed');
                    $('.news_material').css('padding-top', $('.news_material').find('h1').height());
                } else {
                    $('.news_material').find('h1,.activity-buttons').removeClass('fixed');
                    $('.news_material').css('padding-top', 0);
                }
            });

            $(window).trigger('scroll');

            $('a.news-video').colorbox({
                iframe: true,
                width: 800,
                height: 600
            });
        }

        if ($('#tabs').size()) {
            $('#tabs').tabs({
                active: $('.tab-link.preselected').index('.tab-link'),
                activate: function(event, ui) {
                    if ($('#tabs').hasClass('main')) {
                        var tabNum = $('#tabs').find('.tab-link').index(ui.newTab);

                        switch (tabNum) {
                            case 0: var filterFormAction = '/events/news'; break;
                            case 1: var filterFormAction = '/events/announces'; break;
                            case 2: var filterFormAction = '/events/important'; break;
                            case 3: var filterFormAction = '/events/documentation'; break;
                        }

                        $('#filters').attr('action', filterFormAction);

                    } else {
                        var tabURL = document.location.pathname + '?letter=' + ui.newTab.children().text().toLowerCase();
                        window.history.pushState(null, 'Переход по вкладкам', tabURL);
                        $('.print-link').attr('href', tabURL + '&media=print');
                        $('#book-search-field').val('');
                    }
                }
            });
        }

        console.log('Активный таб', $('.tab-link.preselected').index('.tab-link'));

        if ($('#book-search-field').size()) {
            $('#book-search-field').autocomplete({
              source: function( request, response ) {
                $.ajax({
                  url: '/',
                  dataType: 'json',
                  data: {
                    menu: 'ajax',
                    imenu: 'memorybook',
                    q: request.term
                  },
                  success: function( data ) {
                    console.log(data);
                    response( data );
                  },
                  error: function(err) {
                    console.log('err');
                    console.log(err);
                  } 
                });
              },
              minLength: 3,  
              select: function( event, ui ) {
                var surname = ui.item.label;
                var letter = surname[0].toLowerCase();
                document.location.href = '?letter='+letter+'&surname='+surname;
              }
            });      

            $('#book-search-form').on('submit', function(e) {
                var surname = $('#book-search-field').val();
                var letter = surname[0].toLowerCase();                
                document.location.href = '?letter='+letter+'&surname='+surname;

                e.preventDefault();
                return false;
            });      

            $('.tab-link').find('a').on('click', function(e) {
                var id = $(this).attr('href');

                if ($(id).size()) {
                    if ($(id).find('.no-results').size()) {
                        var letter = $(this).text().toLowerCase();                
                        document.location.href = '?letter='+letter;
                    }
                } else {
                    throw('Wrong href parameter on clicked tab link "'+$(this).text()+'"');
                }

                // e.preventDefault();
            });
        }

        $('.accreditation-link').on('click', function(e) {

            var event_id = $(this).attr('data-event-id');

            if ($('#accreditation_form').find('select').find('option').filter('[value="'+event_id+'"]').size()) {
                $('#accreditation_form').find('select').val(event_id);
                $('#accreditation_form').find('select').trigger('change');
            } else {
                throw('Link provide a wrong event ID or event is missing in form select');
            }

            showModal('accreditation_form');

            e.preventDefault();
        });

        $('#accreditation_form').find('.cbox').on('click', function() {
            if ($(this).is(':checked')) {
                $('.car-info').stop().animate({
                    height: 84
                });
            } else {
                $('.car-info').stop().animate({
                    height: 22
                }, 400, function() {
                    $('.car-info').find('input.textfield').val('');            
                });    

            }
        });

        $('.get-accreditation').find('.get-button').on('click', function(e) {

            var event_id = $(this).attr('data-event-id');

            if ($('#accreditation_form').find('select').find('option').filter('[value="'+event_id+'"]').size()) {
                $('#accreditation_form').find('select').val(event_id);
                $('#accreditation_form').find('select').trigger('change');
            } else {
                throw('Link provide a wrong event ID or event is missing in form select');
            }

            showModal('accreditation_form');            

            e.preventDefault();
        });

    });
})(jQuery);

function updateMainContent(data) {
    $('.content.main').html(data);
    $(document).trigger('ready');
    $(window).scrollTop(0);
    $(window).trigger('scroll');
}

function showFullContact(id)
{
    $.colorbox({width:'984px', inline:true, href:'#fullcontact_'+id});
}

function showShareBlock(){
    $('.sharetext').hide(100);
    $('.shareblock').show(100);
}

//поиск по сайту
function doSearch()
{
    var sstr=$('#globalsearch').val();
    document.location.href='/search&search='+sstr;
}

function searchcont()
{
    var documenttype=$('#documenttype').val();
    var contsearch=$('#contsearch').val();

    document.location.href='/contacts/contacts-governments/&documenttype='+documenttype+'&contsearch='+contsearch;
}

//скролл влево
function jk_vjt_photo_lscroll(mwidth,step)
{
    var mscroll=mwidth-290;
    var oldmargin=$('#jk_vjt_photo_listscroll').css('left');

    if(oldmargin == 'auto')
    {
        oldmargin='0px';
    }

    oldmargin=oldmargin.replace('px','');




    var x=oldmargin*1+step;

    //alert(x);

    if(x > 0)
    {
        x=0;
    }

    //if(x < 0)
    //{
    var xstr=x+'px';

    //alert(xstr);
    $('#jk_vjt_photo_listscroll').animate({
        left: xstr
    },300);
    //}
}

//скрол_вправо
function jk_vjt_photo_rscroll(mwidth,step)
{
    var mscroll=mwidth-290;
    var oldmargin=$('#jk_vjt_photo_listscroll').css('left');

    if(oldmargin == 'auto')
    {
        oldmargin='0px';
    }

    oldmargin=oldmargin.replace('px','');




    var x=oldmargin*1-step;

    //alert(x);

    var check=0 - mscroll + 65;
    //alert(x+' - '+check);

    if(x < check)
    {
        x=check;
    }
        var xstr=x+'px';

        //alert(xstr);
        $('#jk_vjt_photo_listscroll').animate({
            left: xstr
        },300);
    //}



}

function ShowYoutubeVideo(id)
{
    var data = '<div style="width: 800px; height: 600px; overflow: hidden;">' +
        '<iframe width="800" height="600" src="http://www.youtube.com/embed/' + id + '"' +
        ' frameborder="0" allowfullscreen></iframe></div>';

    $.colorbox({width:'800px', height:'660px', html:data, onComplete:function(){}});
}

//смена страницы губернаторских программ
function ChangeGuberProgPage(page)
{
    $('.main_groups_page.active').removeClass('active');
    $('#main_groups_page_'+page).addClass('active');

    // var left=(page-1)*($('.m1_gprogkar').find('.governor-programm:first').width() + 10 + 27)*3;
    var left=(page-1)*984;

    //alert('test!');

    $('.main_gprogs_scroll').animate({left:'-'+left+'px'}, 300);
}

function ChangeMainGProgPage(page)
{
    $('.main_groups_page.active').removeClass('active');
    $('#main_groups_page_'+page).addClass('active');

    // var left=(page-1)*($('.m1_gprogkar').find('.governor-programm:first').width() + 10 + 27)*3;
    var left=($('.main_gprogs_scroll').children(':first').width() + 37) * (page-1);



    //alert('test!');

    $('.main_gprogs_scroll').animate({left:'-'+left+'px'}, 300);
}

function ChangeFooterLinksPage(page)
{
    $('.main_groups_page.active').removeClass('active');
    $('#footer_links_page_'+page).addClass('active');

    var left=(page-1)*1000;
    $('.footertop_scroll').animate({left:'-'+left+'px'}, 300);
}

//выбор большой фотографии
function selectPhoto(i)
{

    if ($('.standart_shablons__photo').size()) {
        var photoname='/photos/'+photos[i]+'_x922.jpg';
    } else {
        var photoname='/photos/'+photos[i]+'_xy289x196.jpg';
    }


    //обновляем фото
    aphoto=i;

    var authortext=authors[i];

    if(authortext != '')
    {
        authortext='Автор: '+authortext;
    }

    $('#photo_big').stop().animate({ opacity: 0 }, 600 ,function(){
        $('#photo_big').attr('src',photoname);

        $('#photo_big').on('load', function() {
            $('#photo_big').stop().animate({ opacity: 1 }, 600);
            $('#photo_big_author').html(authortext);
            $('#photo_big').parent('a').attr('data-img', i);
        });
    });


}

function nextPhoto()
{
    var j=aphoto+1;

    //alert(j);

    if(photos[j] == undefined)
    {
        j=1;
    }

    selectPhoto(j);
}

function prevPhoto()
{

    var j=aphoto-1;
    if(photos[j] == undefined)
    {
        j=(photos.length)-1;
        //alert(j);
    }

    selectPhoto(j);

}

//вывод детализации события на большом календаре
function showBigCalendEvent(id)
{
    var divid='#event_'+id;
    $.colorbox({inline:true, href:divid});
}

//вывод детализации анонса
function ShowAnons(id)
{

//получаем данные ajax
    $.ajax({
        type: 'POST',
        url: '/',
        async: true,
        data: 'menu=ajax&imenu=ShowAnons&id='+id,
        success: function(data){
            if(data == '-1')
            {
                $.colorbox({width:'425px', maxWidth:'95%', maxHeight:'95%', html:'Произошла ошибка, попробуйте повторить позже.'});
            }
            else
            {
                $.colorbox({
                    width:'978px',
                    height:'500px',
                    maxWidth:'95%',
                    maxHeight:'95%',
                    html:data,
                    onComplete:function () {
                    }
                });
            }
        }
    });

}

function vidget_selectSmiDate()
{
    var month=$('#monthselector').val();
    var year=$('#yearselector').val();

    document.location.href='/events/for-mass-media&year='+year+'&month='+month;
}

//выбрать месяц анонса
function vidget_selectAnonsDate(url)
{
    var month=$('#monthselector').val();
    var year=$('#yearselector').val();

    document.location.href=url+'?year='+year+'&month='+month;

}

//переключатель виджета видео/фото/online
function vidget_selectOnline(){

    $('#vidget_photo').removeClass('active');
    $('#vidget_video').removeClass('active');
    $('#vidget_online').addClass('active');

    $('#vidget_photo_content').hide(100);
    $('#vidget_video_content').hide(100);
    $('#vidget_online_content').show(100);

}
//переключатель виджета видео/фото
function vidget_selectVideo(){

    $('#vidget_photo').removeClass('active');
    $('#vidget_online').removeClass('active');
    $('#vidget_video').addClass('active');

    $('#vidget_photo_content').hide(100);
    $('#vidget_online_content').hide(100);
    $('#vidget_video_content').show(100);

}
function vidget_selectPhoto(){

    $('#vidget_video').removeClass('active');
    $('#vidget_online').removeClass('active');
    $('#vidget_photo').addClass('active');

    $('#vidget_video_content').hide(100);
    $('#vidget_online_content').hide(100);
    $('#vidget_photo_content').show(100);
}

function SelectVacansyForm(){

    if($('#onlyactual').is(':checked'))
    {
        var onlyactual=1;
    }
    else{
        var onlyactual=0;
    }

    var oiv=$('#oivselector').val();
    var region=$('#regionselector').val();

    var gsflag=$('#gsflag').val();
    var msflag=$('#msflag').val();

    document.location.href='/civil-service/competitions-vacancy?region='+region+'&onlyactual='+onlyactual+'&oiv='+oiv+'&gsflag='+gsflag+'&msflag='+msflag;
}

function vidget_MainSelectNews(){
    $('#vidget_MainAnons').removeClass('active');
    $('#vidget_MainNews').addClass('active');

    $('#vidget_MainAnons_content').hide(100);
    $('#vidget_MainNews_content').show(100);
}

function vidget_MainSelectAnons(){
    $('#vidget_MainNews').removeClass('active');
    $('#vidget_MainAnons').addClass('active');

    $('#vidget_MainNews_content').hide(100);
    $('#vidget_MainAnons_content').show(100);
}

//отображение всплывашки с видео
function vidget_showVideoVidget()
{}

//пепеключаем типы документов
 function vidget_changeFileType(id,url)
 {
     document.location.href=url+'/&documenttype='+id;
 }

//переключаем фото на главной
function vidget_changeMainPhoto(photo,url)
{
    var oldsrc=$('#main_photo a img').attr('src');

    if(oldsrc != '/photos/'+photo+'_xy317x280.jpg')
    {

        $('#main_photo').html('<a href="'+url+'">' +
            '<img src="/photos/' + photo + '_xy317x280.jpg" width="318" height="280" border="0"></a>'
        );

        $('.main_news .option').removeClass('active');
        $('#option_mainPhoto_'+photo).addClass('active');
    }
}

function showAutoOptions()
{

    if($('#auto').prop('checked'))
    {
        $('#automark').show();
        $('#autonumber').show();
    }
    else{
        $('#automark').hide();
        $('#autonumber').hide();
    }
}

function fitSearchFontSize() {

    if (!$('.search-query-title').size()) { return; }

    var sqLength = $('.search-query-title').val().length;

    console.log(sqLength);

    var fontSize = '46pt';

    switch (true) {
        case (sqLength >= 22 && sqLength < 30) : fontSize = '35pt'; break;
        case (sqLength >= 30 && sqLength < 42) : fontSize = '25pt'; break;
        case (sqLength >= 42) : fontSize = '15pt'; break;
    }

    $('.search-query-title').css('font-size', fontSize);
}

