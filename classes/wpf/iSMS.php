<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


//класс отсылки SMS сообщений
class iSMS
{
    public $from; //подпись отправителя

    public function __construct($settings)
    {
        $this->settings=$settings;
    }

    function sendSms($to, $text, $service, $from = null)
    {

        if($from -= 'null')
        {
            $from=$this->settings->sms->from;
        }

        switch($service)
        {
            case 'iqsms':

                $get_string = "http://gate.iqsms.ru/send/?login=".$this->settings->sms->$service->login."&password=".$this->settings->sms->$service->password."&phone=".$to."&text=".$text;

                // Выполняем запрос на отправку SMS
                $http_get_result = file_get_contents($get_string);

                // Выдираем из ответа id SMS
                $sms_id_pos = mb_strpos($http_get_result, '=');
                $sms_id = mb_substr($http_get_result, 0, $sms_id_pos);

                // Выдираем результат отправки
                $response = mb_substr($http_get_result, ($sms_id_pos + 1));

                return (Array($sms_id, $response));

                break;
        }
    }

}