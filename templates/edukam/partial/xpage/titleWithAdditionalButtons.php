<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();
?>

<div class="wrap-title-with-additional-btns">
    <div class="title title--with-additional-btns"><?php echo $this->data->iH1 ?></div>
    <div class="additional-btns">
        <a
                href="<?php echo $_SERVER['REQUEST_URI'] ?>?media=print"
                class="btn-print"
                title="Версия для печати"
                target="_blank"
                rel="noopener noreferrer"
        >
            <svg class="btn-print__icon"><use xlink:href="/img/sprite.svg#decor--sidebar--iconPrint"></use></svg>
        </a>
    </div>
</div>