<?php
/**
 * @var \iSite $khabkrai
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


//версии (дляслабовидящих и обычная)

$khabkrai->GetPostValue('version');

if ($khabkrai->values->version == 'special') {
    $_SESSION['special'] = 1;
    $_SESSION['fontSize'] = 1;
} elseif ($khabkrai->values->version == 'normal') {
    $_SESSION['special'] = 0;
}

if (!isset($_SESSION['special'])) {
    $_SESSION['special'] = 0;
    $_SESSION['fontSize'] = 1;
}

if ($_SESSION['special'] == 1) {
    //меняем размер шрифта
    if ($khabkrai->values->fontSize != '') {
        switch ($khabkrai->values->fontSize) {
            case '0':
                $_SESSION['fontSize'] = 0;
                break;
            case '2':
                $_SESSION['fontSize'] = 2;
                break;
            default:
                $_SESSION['fontSize'] = 1;
                break;
        }
    }
}

$khabkrai->data->special = $_SESSION['special'];

$specialver_options = array(
    'toggle_media',
    'toggle_sidebar',
    'showhint',
    'fontsize',
    'color',
);

foreach ($specialver_options as $opt) {
    $key = 'special_'.$opt;
    if (isset($_REQUEST[$key])) {
        $_SESSION[$key] = $_REQUEST[$key];
    }
}
