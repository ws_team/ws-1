<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if ($this->values->id != '')
{
    $query = 'SELECT * FROM "material_types" WHERE "id" = $1';
    $res = $this->dbquery($query, array($this->values->id));
    if ( ! empty($res))
    {
        $this->data->amattype = $res[0];

        //получаем связанные характеристики

        $query = <<<EOS
SELECT
    "t".*,
    mt.name valuemattypename
FROM
    "extra_mattypes" "t"
    LEFT JOIN material_types mt ON mt.id = t.valuemattype
WHERE "t"."type_id" = $1
EOS;

        $q_params = array($this->values->id);
        $res = $this->dbquery($query, $q_params);
        if ( ! empty($res)) {
            $this->data->extratypes = $res;
        }

        //получаем свуязанные селекторы
        $this->data->extraselectors = Array();
        $query = <<<EOS
SELECT
    "s".*,
    "t"."name" "mattypename"
FROM
    "extra_selectors" "s"
    LEFT JOIN material_types "t" ON "t"."id" = "s"."mattype_id"
WHERE "s"."type_id" = $1 
EOS;

        $q_params = array($this->values->id);
        $res = $this->dbquery($query, $q_params);

        if ( ! empty($res)) {
            $this->data->extraselectors = $res;
        }
    }
}