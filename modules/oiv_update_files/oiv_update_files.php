<?php

require_once(WPF_CLASSROOT.'/utils.php');

/**
 * @param iSite $site
 * @param string $oivcat  Путь к папке с ОИВами отн. корня движка
 * @param string $masteroiv  Имя папки ОИВа, с которого идет обновление
 * @param array $oivlist  Список имен папок ОИВов для обновления
 */
function sync_oivs($site, $oivcat, $masteroiv, $oivlist)
{
    // Именя файлов/папок для исключений
    $oiv_update_exceptions = array(
        '/settings.php',
        '/defines-oiv',
        '/defines-local',
        '/photos',
        '/old',
        '/min',
        '/classes',
        '/caches',
        '/exportMIN.dmp',
        '/tinymce',
        '/img',
        '/css/main.css',
        '/templates/khabkrai/css/main.css',
        '/templates/khabkrai/img/flag2.jpg',
    );
    $oiv_exceptions_per_site = array(
        'bdd' => array('/templates/khabkrai/standart_shablons/main.php'),
    );

    $oivcat = $site->settings->pathglobal . ltrim($oivcat, DIRECTORY_SEPARATOR);

    // код для работы с оивами
    chmod($oivcat, 0777);

    foreach ($oivlist as $oiv_machine_name) {
        $site->data->oiv_update_log .= '<a href="#'.$oiv_machine_name.'">'.$oiv_machine_name.'</a><br>';
    }

    $site->data->oiv_update_log .= '<br><br>';
    $site->data->oiv_update_log .= '<table>';
    $site->data->oiv_update_log .= '<tr>';
    $site->data->oiv_update_log .= '<th>Источник</th>';
    $site->data->oiv_update_log .= '<th>Назначение</th>';
    $site->data->oiv_update_log .= '<th>Статус</th>';
    $site->data->oiv_update_log .= '</tr>';

    $prevCwd = getcwd();
    if (chdir($oivcat)) {
        foreach ($oivlist as $oiv_machine_name) {
            if ($oiv_machine_name != $masteroiv) {

                $site->data->oiv_update_log .= '<tr>';
                $site->data->oiv_update_log .=
                    '<td colspan="3" style="background-color: #bbb;"><h2 id="'.$oiv_machine_name.'">' .
                        $oiv_machine_name . '</h2></td>';
                $site->data->oiv_update_log .= '</tr>';

                $exceptions = $oiv_update_exceptions;
                if (isset($oiv_exceptions_per_site[$oiv_machine_name]))
                    $exceptions = array_merge($exceptions, $oiv_exceptions_per_site[$oiv_machine_name]);

                recurse_copy($oivcat . $masteroiv, $oivcat . $oiv_machine_name, $exceptions);
            }
        }
    }

    $site->data->oiv_update_log .= '</table>';
    chdir($prevCwd);
}

function recurse_copy($src, $dst, $exclude = array())
{
    $dir = opendir($src);
    @mkdir($dst);
    $output = '';
    while(false !== ( $file = readdir($dir)) ) {
        if ($file == '.' || $file == '..')
            continue;
        $srcPath = $src.DIRECTORY_SEPARATOR.$file;
        if (wpf\utils\in_pathset($srcPath, $exclude))
            continue;
        if ( is_dir($src . '/' . $file) ) {

            $output .= '<tr>';
            $output .= '<td colspan="3" style="background-color: #eee;"><h3>' . $src .
                '/' . $file . '</h3></td>';
            $output .= '</tr>';

            recurse_copy($srcPath, $dst . '/' . $file, $exclude);
        }
        else
        {
            $status = copy($srcPath, $dst . '/' . $file);

            if ($status) {
                $status = '<span style="color: green;">скопирован</span>';
                $chmod_status = chmod($dst . '/' . $file, 0775);
                if ($chmod_status) {
                    $status .= ', запись разрешена';
                } else {
                    $status .= ', <span style="color: orange;">нет доступа</span>';
                }
            } else {
                $errors= error_get_last();
                $er = "COPY ERROR: ".$errors['type'];
                $er.=  "<br />\n".$errors['message'];

                $status = '<span style="color: red;">не скопирован ('.$er.')</span>';
            }

            $output .= '<tr>';
            $output .= '<td>' .$src . '/' . $file . '</td>';
            $output .= '<td>'.$dst . '/' . $file.' ('.$file.')</td>';
            $output .= '<td>'.$status.'</td>';
            $output .= '</tr>';
        }
    }
    closedir($dir);
    return $output;
}

if(($this->autorize->autorized == 1)&&($this->autorize->userrole > 0))
{
    // домашняя папка со всеми папками ОИВов
    $oivcat = '/oiv/';

    ini_set('max_execution_time', 3600);
    ini_set('memory_limit', 5205987900);
    $this->data->oiv_update_log = '';

    $this->GetPostValues('select');

    if (strncmp($_SERVER['SERVER_NAME'], 'khabkrai.', 9) == 0) {
        $update_what = array('oiv', 'omsu');
    } else {
        $oiv = basename($_SERVER['DOCUMENT_ROOT']);
        if (strncmp($oiv, 'omsu_', 5) == 0)
            $update_what = array('omsu');
        else
            $update_what = array('oiv');
    }
    
    if (in_array('oiv', $update_what)) {
        // главный ОИВ, файлы которого будут раскопированы
        $masteroiv = 'uprzags';

        // Список ОИВов для копирования
        $oivlist = array(
            'bdd',
            'gku',
            'komcit',
            'komgrz',
            'komgzk',
            'kominv',
            'kompimk',
            'komsud',
            'komtek',
            'komtrud',
            'minec',
            'minfin',
            'minio',
            'minit',
            'minjkh',
            'minkult',
            'minobr',
            'minpt',
            'minsh',
            'minsport',
            'minstr',
            'minszn',
            'minzdrav',
            'mpr',
            'ogs',
            'predstav',
            'uprarch',
            'uprles',
            'uprovr',
            'uprvet',
            'uprzags',
            'test_oiv'
        );
        if (!empty($this->values->select))
            $oivlist = \wpf\utils\filter_oivlist($oivlist, $this->values->select);
        sync_oivs($this, $oivcat, $masteroiv, $oivlist);
    }

    if (in_array('omsu', $update_what)) {
        $master_omsu = 'omsu_raionkms';
        $omsu_list = array();
        foreach (scandir(rtrim($this->settings->pathglobal, DIRECTORY_SEPARATOR).$oivcat) as $item) {
            if (strncmp($item, 'omsu_', 5) == 0)
                $omsu_list[] = $item;
        }
        $exclude_omsu = array($master_omsu, 'omsu_khb');

        foreach ($exclude_omsu as $ex)
            if (($p = array_search($ex, $omsu_list)) !== false)
                $omsu_list[$p] = null;
        $omsu_list = array_filter($omsu_list);
        if (!empty($this->values->select))
            $omsu_list = oivlist_filter($omsu_list, $this->values->select);

        sync_oivs($this, $oivcat, $master_omsu, $omsu_list);
    }
}