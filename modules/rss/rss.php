<?php

namespace wpf\modules\rss;

$this->data->html='';

ob_start();
header('Content-Type: text/xml; charset=utf-8');// application/rss+xml
$this->headers = ob_get_contents();
ob_end_clean();

ob_start();

function output_feed($site)
{
    //получаем разделы новостей
    $type_id = null;
    $query = 'SELECT "id" FROM "material_types" WHERE id_char=\'news\' OR id_char=\'Novosti\'';
    $r = $site->dbquery($query);
    if ($r !== false && count($r))
        $type_id = $r[0]['id'];

    $batch_size = 100;
    $page = 1;
    $last_time = 0;
    $base_url = 'http://'.$_SERVER['HTTP_HOST'];

    ob_start();
    do {
        $materials = MaterialList($site, $type_id, $page, $batch_size, STATUS_ACTIVE);
        foreach ($materials as $mat) {
            $mat_time = intval($mat['unixtime']);
            if ($mat_time > $last_time)
                $last_time = $mat_time;
            $item_url = $base_url.$mat['url'];
            $date = date(DATE_RSS, $mat_time);
            $anons = strip_tags(str_replace('&nbsp;','',$mat['anons']));
            ?>
    <item>
            <ID><?=htmlspecialchars($mat['id'], ENT_XML1)?></ID>
            <title><?=htmlspecialchars($mat['name'], ENT_XML1)?></title>
            <link><?=htmlspecialchars($item_url, ENT_XML1)?></link>
            <description><?=htmlspecialchars($anons, ENT_XML1)?></description>
            <pubDate><?=$date?></pubDate>
    </item><?php
        }
        ++$page;
    } while (count($materials) == $batch_size);
    $materials_data = ob_get_clean();

    if (empty($last_time))
        $last_time = time();

    $site->data->html = '<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0">
    <channel>
    <title>'.htmlspecialchars($site->data->title, ENT_XML1).'</title>
    <link>'.htmlspecialchars($base_url, ENT_XML1).'</link>
    <description>'.htmlspecialchars($site->data->title, ENT_XML1).'</description>
    <language>ru</language>
    <lastBuildDate>'.date(DATE_RSS, $last_time).'</lastBuildDate>'.
    $materials_data.
    '
    </channel>
</rss>';
}

output_feed($this);
