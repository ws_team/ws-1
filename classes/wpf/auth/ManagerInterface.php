<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\auth;

interface ManagerInterface {
     function checkPermission($permission, IdentityInterface $userIdentity);
}