<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


return array(
    'admkhb' => array(
        'pass' => 'Fbh4wsS',
        'omsu' => '771',
        'reg' => '41',
        'db' => 'omsu_raionkhb',
    ),
    'admulch' => array(
        'pass' => 'Gbd5sFg',
        'omsu' => '770',
        'reg' => '40',
        'db' => 'omsu_ulchi',
    ),
    'admtug' => array(
        'pass' => 'dSdsFg5',
        'omsu' => '768',
        'reg' => '39',
        'db' => 'omsu_chumikan',
    ),
    'admoh' => array(
        'pass' => 'dSHs447',
        'omsu' => '767',
        'reg' => '35',
        'db' => 'omsu_ohotsk',
    ),
    'admnan' => array(
        'pass' => 'Grs4Gw3',
        'omsu' => '766',
        'reg' => '33',
        'db' => 'omsu_nanraion',
    ),
    'admsol' => array(
        'pass' => 'HdfvDw3',
        'omsu' => '765',
        'reg' => '38',
        'db' => 'omsu_solnechniy',
    ),

    'admosi' => array(
        'pass' => 'Hr6AcYg7',
        'omsu' => '764',
        'reg' => '36',
        'db' => 'omsu_osipenko',
    ),

    'admlaz' => array(
        'pass' => 'D2F4bd',
        'omsu' => '763',
        'reg' => '32',
        'db' => 'omsu_lazo',
    ),

    'admkom' => array(
        'pass' => 'Jer45h',
        'omsu' => '762',
        'reg' => '31',
        'db' => 'omsu_raionkms',
    ),

    'admvya' => array(
        'pass' => 'Jdr5se',
        'omsu' => '761',
        'reg' => '30',
        'db' => 'omsu_vyazemskiy',
    ),

    'admverh' => array(
        'pass' => 'Mfd57e',
        'omsu' => '758',
        'reg' => '29',
        'db' => 'omsu_vbr',
    ),

    'admvan' => array(
        'pass' => 'Jdfg46',
        'omsu' => '757',
        'reg' => '28',
        'db' => 'omsu_vanino',
    ),

    'admaya' => array(
        'pass' => 'Sbn79r',
        'omsu' => '743',
        'reg' => '26',
        'db' => 'omsu_ayan',
    ),

    'admbik' => array(
        'pass' => 'Nr3o5u',
        'omsu' => '742',
        'reg' => '27',
        'db' => 'omsu_bikin',
    ),

    'admamu' => array(
        'pass' => 'Fgb45h',
        'omsu' => '739',
        'reg' => '25',
        'db' => 'omsu_amursk',
    ),

    'admnik' => array(
        'pass' => 'jwF63w',
        'omsu' => '738',
        'reg' => '34',
        'db' => 'omsu_nikol',
    ),

    'admsg' => array(
        'pass' => 'HJs5sS',
        'omsu' => '734',
        'reg' => '37',
        'db' => 'omsu_sovgav',
    ),

    'admgkms' => array(
        'pass' => 'GHdt3d',
        'omsu' => '733',
        'reg' => '24',
        'db' => 'omsu_kms',
    ),

    'admgkhb' => array(
        'pass' => 'Ndt5wc',
        'omsu' => '732',
        'reg' => '23',
        'db' => 'omsu_khb',
    ),
);