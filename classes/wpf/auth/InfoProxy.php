<?php
/**
 * Временный костыль для перехода от прямой манипуляции данными авторизации к работе через \wpf\auth\User
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

namespace wpf\auth;

class InfoProxy {
    private $props = array();

    public function __construct($data = null)
    {
        if (is_object($data))
            $this->props = get_object_vars($data);
        elseif (is_array($data))
            $this->props = $data;
    }

    public function __get($prop)
    {
        if (isset($this->props[$prop]))
            return $this->props[$prop];
    }

    public function __set($prop, $val)
    {
        $this->props[$prop] = $val;
    }

    public function __isset($prop)
    {
        return isset($this->props[$prop]);
    }

    public function __unset($prop)
    {
        unset($this->props[$prop]);
    }
}