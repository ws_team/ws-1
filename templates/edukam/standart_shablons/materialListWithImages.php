<?php
/**
 * @var \iSite $this
 */

defined('_WPF_') or die();

$materials = MaterialList($this, $this->data->atype, 1, $this->values->perpage, 2);

if(specialver_is_active($this)){
    $cssClassMatList = 'news-list';
    $printMaterialItemFunctionName = 'printMaterialListItem';
}
else{
    $cssClassMatList = 'materials-list-with-images';
    $printMaterialItemFunctionName = 'printMaterialListItemWithImage';
}

?>

<?php include($this->partial('/xpage/breadcrumbs'));  ?>


<div class="container">
    <div class="wrap-content">

        <?php include($this->partial('/xpage/titleWithAdditionalButtons')); ?>

        <div id="ajaxPaginatorDestinationHtml" class="<?php echo $cssClassMatList ?>">

            <?php

            foreach ($materials as $materialsItem){
                $printMaterialItemFunctionName($this, $materialsItem);
            }

            ?>

        </div>
    </div>
</div>
<div class="container can-be-refreshed">

    <?php

    $allMaterials   = MaterialList($this, $this->data->atype, 1, 10000, 2);
    $materialsCount = count($allMaterials);

    print $this->data->blocks->render(
        'ajaxUniversalPaginator',
        'ajaxGetMaterialListWithImages',
        $this->data->atype,
        $this->values->perpage,
        1,
        $materialsCount
    );
    ?>

    <div class="preloader"><img src="/img/preloader.svg" class="preloader__image"></div>

</div>
