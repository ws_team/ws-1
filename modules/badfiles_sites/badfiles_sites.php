<?php
/**
 * @var \iSite $this
 */

unlink('/var/www/html/css/filedata.html');

error_reporting(E_ALL);
ini_set('display_errors', 1);

ini_set("upload_max_filesize", $max_uploaded_size);
ini_set("post_max_size", $max_uploaded_size);
ini_set('max_execution_time', 3000);

defined('_WPF_') or die();

//if ( ! $this->requireAuthUser()->can('admin.sysoptions')) {
//	$this->endRequest(403);
//}

//die('step1');

$this->data->title = 'Битые файлы';
$this->data->iH1 = 'Битые файлы';
$this->data->keywords = '';
$this->data->description = 'Битые файлы';
$this->data->breadcrumbsmore = '';
$this->data->errortext = '';

//получаем список сайтов
global $sites, $stypes, $files, $wrongfiles, $outarr, $days, $sitesFiles;
$sites=Array();
$sitesFiles=Array();
$files=Array();
$wrongfiles=Array();

$outarr=Array();
$days=Array();


$imagecaches=array();

//получаем все кэши изображений
$query='SELECT * FROM files_imagesizes';
if($res=$this->dbquery($query))
{
    if(count($res) > 0)
    {
        $imagecaches=$res;
    }
}

//Все файлы событий
$query='SELECT f.file_id FROM file_connections f WHERE f.material_id IN 
	(
	SELECT m.id FROM materials m WHERE m.type_id IN 
		(
		SELECT t.id FROM material_types t WHERE t.parent_id = 1
		)
	)';

$size=0;

if($res=$this->dbquery($query))
{
    if(count($res) > 0)
    {
        foreach ($res as $row)
        {
            $mainfile='/var/www/html/photos/'.$row['file_id'];
            if(file_exists($mainfile))
            {
                $size+=filesize($mainfile);

                //размеры кэшей
                foreach ($imagecaches as $ic)
                {
                    $cfile=$mainfile;

                    if ($ic['width'] == 0) {
                        $cfile .= '_y' . $ic['height'];
                    } elseif ($ic['height'] == 0) {
                        $cfile .= '_x' . $ic['width'];
                    } else {
                        $cfile .= '_xy' . $ic['width'] . 'x' . $ic['height'];
                    }

                    $cfile.='.jpg';
                    if(file_exists($cfile))
                    {
                        $size+=filesize($cfile);
                    }
                }
            }
        }
    }
}

//получаем список папок и время создания photos
//$oivs=scandir('/var/www/html/oiv/');

//krumo($oivs);
/*
$files=Array();
$years=Array();
$ym=Array();

$oivs[]='khabkrai';

//файлы ОИВ
foreach ($oivs as $oiv)
{
    if($oiv != '.' && $oiv != '..')
    {

        $pdir='/var/www/html/oiv/'.$oiv.'/photos';

        if($oiv == 'khabkrai')
        {
            $pdir='/var/www/html/photos';
        }

        if(file_exists($pdir)){

            $oivfiles=scandir($pdir);
            foreach ($oivfiles as $oivfilen)
            {
                $oivfile=$pdir.'/'.$oivfilen;

                if(file_exists($oivfile) && is_file($oivfile))
                {
                    //$filedate=date("d.m.Y", filemtime($oivfile));
                    $fileyear=date("Y", filemtime($oivfile));
                    $filem=date("m", filemtime($oivfile));
                    $filesize=filesize($oivfile);

                    if(!in_array($fileyear,$years))
                    {
                        $years[]=$fileyear;
                    }

                    if(!in_array($filem,$ym[$fileyear]))
                    {
                        $ym[$fileyear][]=$filem;
                    }

                    if(!isset($files[$oiv][$fileyear][$filem]))
                    {
                        $files[$oiv][$fileyear][$filem]=$filesize;
                    }
                    else
                    {
                        $files[$oiv][$fileyear][$filem]+=$filesize;
                    }
                }
            }
        }
    }
}

$out='';

$out.='<table>';

$out.='<tr>
        <th>Директория</th>';

asort($years);

foreach ($years as $year)
    {
        asort($ym[$year]);

        foreach ($ym[$year] as $mo)
        {
            $me=$mo.'.'.$year;
            $out.='<th>'.$me.'</th>';
        }
    }

$out.='</tr>';

foreach ($oivs as $oiv)
{
    if($oiv != '.' && $oiv != '..')
    {
        $out.='<tr>
        <th>'.$oiv.'</th>';

        foreach ($years as $year)
        {
            foreach ($ym[$year] as $mo)
            {
                $filesize=round(($files[$oiv][$year][$mo]/(1024*1024)),2);

                $out.='<td>'.$filesize.'</td>';
            }
        }

        $out.='</tr>';
    }
}

$out.='</table>';

file_put_contents('/var/www/html/css/filedata.html',$out);

//$site=$sitesFiles;
*/

file_put_contents('/var/www/html/css/filedata.html',$size);

die($size);

die('step12');