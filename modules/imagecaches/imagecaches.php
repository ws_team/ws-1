<?php

if($this->autorize->userrole == 1){
//добьявляем переменные
$this->data->errortext='';

$this->GetPostValues(Array('width','height'));

//выполняем действия
switch($this->values->action)
{
    case 'add':

        $this->data->errortext='<p style="color: #ff0000;">Ошибка при добавлении размера!</p>';

        $width=(int)$this->values->width;
        $height=(int)$this->values->height;

        if(($width > 0)||($height > 0))
        {
            $query='INSERT INTO "files_imagesizes" ("width", "height", "watermark")
            VALUES('.$width.','.$height.',0)';

            if($res=$this->dbquery($query))
            {
                $this->data->errortext='<p style="color: green;">Размер добавлен - не забудьте обновить кэши!</p>';
            }
        }

        break;
    case 'delete':

        $this->data->errortext='<p style="color: #ff0000;">Ошибка при удалении размера!</p>';

        $width=(int)$this->values->width;
        $height=(int)$this->values->height;

        if(($width > 0)||($height > 0))
        {
            $query='DELETE FROM "files_imagesizes" WHERE "width" = '.$width.' AND "height" = '.$height.' AND "watermark" = 0';
            if($res=$this->dbquery($query))
            {
                $this->data->errortext='<p style="color: green;">Размер удален!</p>';

            }
        }

        break;
    case 'refresh':

        /*ini_set('max_execution_time', 3000);
        ini_set('memory_limit', 5205987900);

        //пробегаемся по всем файлам и пересоздаем их кэши
        $query='SELECT "id" FROM "files" WHERE "type_id" = 1 ORDER BY "id" DESC';
        if($res=$this->dbquery($query))
        {

            if(count($res) > 0)
            {
                foreach($res as $row)
                {
                    //echo ' - '.$row['id'];
                    $cimage=new MFile($this,$row['id']);
                    $cimage->CreateImageCash(false);
                    unset($cimage);
                }

                $this->data->errortext='<p style="color: green;">кэши изображений перерисованы!</p>';
            }
        }*/

        break;

    case 'refreshfiles':

        /*ini_set('max_execution_time', 3000);
        $query='SELECT * FROM "files" WHERE "type_id" = 4';
        if($res=$this->dbquery($query))
        {
            if(count($res) > 0)
            {
                $this->data->errortext='<small>файлы:';
                foreach($res as $row)
                {
                    //$cimage=new MFile($this,$row['id']);
                    //$cimage->CreateImageCash();

                    $this->data->errortext.=$row['name'].';<br>';
                    //сохраняем
                    $filename=$this->settings->path.'/photos/'.$row['id'];

                    $savecache=file_put_contents($filename,$row['filedata']);


                    //unset($cimage);
                }

                $this->data->errortext.='</small><p style="color: green;">Кэши файлов развернуты!</p>';
            }
        }*/

        break;
}

    $this->data->iH1='Кэши изображений';

    //получаем список кэшей
    $imagesizes=Array();
    $query='SELECT * FROM "files_imagesizes" ORDER BY "width" ASC, "height" ASC';
    if($res=$this->dbquery($query))
    {
        if(count($res) > 0)
        {
            $imagesizes=$res;
        }
    }


}else{
    header('Location: /404');
    die();
}