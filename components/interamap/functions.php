<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


function iteramap_mark_input_data(\iSite $site)
{
    $site->getPostValues(array(
        'name',
        'anons',
        'coords',
        'event_date_date',
        'event_date_time',
    ));

    if ( ! $site->validateCsrfToken())
        $site->endRequest(400);

    if ($site->isSpamPost())
        $site->endRequest(400);

    if (empty($site->values->coords) || empty($site->values->name)) {
        $site->endRequest(400);
    }

    $date = $site->values->event_date_date;
    $time = $site->values->event_date_time;

    $now = date('d.m.Y H:i');
    $now = explode(' ', $now);

    $date_st = strptime($date, '%d.%m.%Y');
    $time_st = strptime($time, '%H:%M');

    if ( ! $date_st)
        $date = $now[0];

    if ( ! $time_st)
        $time = $now[1];

    $date = explode('.', $date);
    $time = explode(':', $time);

    $datetime = $date[2].'-'.$date[1].'-'.$date[0].' '.$time[0].':'.$time[1].':00';

    $pm_type_id = $site->data->atype;

    return $data = array(
        'type_id' => $pm_type_id,
        'date_event' => $datetime,
        'name' => $site->values->name,
        'anons' => $site->values->anons,
        'coords' => $site->values->coords,
    );
}

/**
 * @param iSite $site
 * @param array $attributes
 * @param string $coords_param_name
 * @return bool|int
 */
function iteramap_mark_add(\iSite $site, $attributes, $extra_attributes, $coords_param_name = 'Координаты')
{
    $id = null;
    $pm_type_id = $attributes['type_id'];

    if (isset($extra_attributes['coords'])) {
        $coords = $extra_attributes['coords'];
        $site->logWrite(LOG_DEBUG, __FUNCTION__, 'checking existing pm coords="'.$coords.'"');

        $coords_param_id = FindParamIdByName($site, $pm_type_id, $coords_param_name);
        // var_dump(__FILE__,__LINE__,$coords_param_id, '$');exit;

        $id = interamap_find_dub($site, $coords_param_id, $coords);
        if ($id) {
            $site->callHook('interamapdub', array(
                'id' => $id,
                'attributes' => $attributes,
                'extra_attributes' => $extra_attributes
            ));
            $site->getLogger()->write(LOG_DEBUG, __FUNCTION__, 'Found dub: id='.$id);
        }
    }

    if ( ! $id) {
        $id = MaterialNew($site);

        $attributes['user_id'] = $site->getUser()->getId();
        $attributes['id_char'] = MaterialGetNewCharId($site, $pm_type_id, $attributes['name']);
        $attributes['date_add'] = date('Y-m-d H:i:s');
        $attributes['language_id'] = LANG_RU;
        $attributes['status_id'] = STATUS_ACTIVE;

        if ( ! empty($extra_attributes['coords'])) {
            $mp_defs = MaterialGetParamDefs($site, $pm_type_id);

            foreach ($mp_defs as $param_id => $def) {
                if ($param_id == $coords_param_id) {
                    MaterialParamSet($site, $id, $param_id, $def['valuetype'], $extra_attributes['coords']);
                    break;
                }
            }
        }

        $site->callHook('interamapadd', array(
            'id' => $id,
            'attributes' => $attributes,
            'extra_attributes' => $extra_attributes,
        ));

        $site->callHook('materialcreate', array(
            'id' => $id,
            'type_id' => $pm_type_id,
            'attributes' => $attributes,
        ));
    }

    if ( ! isset($attributes['date_edit']))
        $attributes['date_edit'] = date('Y-m-d H:i:s');

    DbUpdate($site, 'materials', $attributes, array('id' => $id));

    interamap_after_mark_save($site, $id, $attributes, $extra_attributes);

    return $id;
}

function interamap_add(\iSite $site, $coords_param_name = 'Координаты')
{
    $data = iteramap_mark_input_data($site, $coords_param_name);
    $params = array();

    if (isset($data['coords'])) {
        $params['coords'] = $data['coords'];
        unset($data['coords']);
    }

    return iteramap_mark_add($site, $data, $params, $coords_param_name);
}

function interamap_after_mark_save(\iSite $site, $id, $attributes, $extra_attributes)
{
    $mattype_id = $attributes['type_id'];

    $site->callHook('interamapsave', array(
        'id' => $id,
        'attributes' => $attributes,
        'extra_attributes' => $extra_attributes,
    ));

    $site->callHook('materialsave', array(
        'id' => $id,
        'type_id' => $mattype_id,
        'attributes' => $attributes,
    ));

    $site->getLogger()->write(LOG_INFO, __FUNCTION__, "id=$id");

    _interamap_subscription_update($site, $id, $mattype_id);
}

function _interamap_subscription_update(\iSite $site, $id, $type_id)
{
    if (defined('SUBSCRIPTION_ENABLED') && constant('SUBSCRIPTION_ENABLED')) {
        $site->wpf_include('modules/subscription/functions.php');
        subscription_queue_item_add($site, $type_id, $id, SUBSCRIPTION_SEND_DAILY);
    }
}

function interamap_check_access(\iSite $site, $permission)
{
    $permission = 'editmaterials.'.$permission;

    $type_chain = GetTypeParents($site, $site->data->atype);
    array_push($type_chain, $site->data->atype, $site->values->id);

    $perm = $permission.'.'.implode('.', $type_chain);

    if ( ! $site->getUser()->can($perm)) {
        $site->endRequest(403);
    }
}

function interamap_find_dub(\iSite $site, $coords_param_id, $coords)
{
    $query = <<<EOS
SELECT em.material_id id
FROM extra_materials em
    INNER JOIN extra_mattypes emt ON emt.id = em.type_id
    INNER JOIN materials m ON m.id = em.material_id
WHERE
    emt.type_id = $1
    AND em.valuename = $2
    AND m.status_id <> $3
EOS;

    $q_params = array($site->data->atype, $coords, STATUS_DELETED);
    $res = $site->dbquery($query, $q_params);
    if ( ! empty($res)) {
        return $res[0]['id'];
    }
}

function interamap_run_action(\iSite $site, $accident)
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && ! $site->validateCsrfToken())
        $site->badRequest();

    $site->getPostValue('action');
    $action = $site->values->action;
    $valid_actions = array('resolve');

    if ( ! in_array($action, $valid_actions))
        $site->badRequest();

    $permission = 'materials.accident.'.$action.':'.$site->data->atype.':'.$accident['id'];
    if ( ! $site->getUser()->can($permission))
        $site->forbidden();

    $action_handler = 'interamap_action_'.$action;
    if ( ! is_callable($action_handler))
        $site->notFound();

    $resp = call_user_func($action_handler, $site, $accident);

    $site->setResponseHeader('content-type', 'application/json');
    $site->sendResponse(json_encode($resp));
}

/**
 * @param iSite $site
 * @param $accident
 */
function interamap_action_resolve(\iSite $site, $accident)
{
    $resolved_param = null;
    foreach ($accident['params'] as $param) {
        if (mb_strtolower($param['name']) == 'работы завершены') {
            $is_resolved = ! empty($param['valuename']) && mb_strtolower($param['valuename']) == 'да';
            if ( ! $is_resolved) {
                $mattype_cat = FindTypeIdByChar($site, 'cat-yesno');
                $matval_yes = FindMatIdByChar($site, 'yes', $mattype_cat);
                MaterialParamSet($site, $accident['id'], $param['id'], 'valuemat', $matval_yes);
                _interamap_after_resolve($site, $accident);
            }

            return array('success' => true);
        }
    }

    $site->endRequest(500);
}

function _interamap_after_resolve(\iSite $site, $accident)
{
    $site->callHook('materialchange', array(
        'id' => $accident['id'],
        'type_id' => $accident['type_id'],
        'attributes' => $accident,
    ));

    $site->getLogger()->write(LOG_INFO, __FUNCTION__, "id={$accident['id']}");

    _interamap_subscription_update($site, $accident['id'], $site->data->atype);
}