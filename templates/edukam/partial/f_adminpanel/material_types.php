<?php
/**
 * @var \iSite $site
 * @var array $materials
 * @var int $level
 * @var string $action
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

?>
<ul class="dropdown-menu">
    <?php

    $user = $site->getUser();

    foreach ($materials as $material) {
        if (mb_strlen($material['name'],'utf8') > 60) {
            $material['name'] = mb_substr($material['name'],0,60,'utf8').'...';
        }

        if(is_array($material['childs']) && count($material['childs']) > 0)
        {
            $subclass=' class="dropdown-submenu"';
        }
        else
        {
            $subclass='';
        }

        if ($user->can('editmaterials:'.$material['id'])) {
            //рисуем тип

            ?>
            <li <?php echo $subclass; ?>><a href="<?= htmlspecialchars(
                '/?menu=editmaterials&imenu='.$material['id'].'&action='.$action
            )?>"><?=
                $material['name'] ?></a>
            <?php
        } else {
            ?><li <?php echo $subclass; ?>><a style='color: #d0aaac;'><?= $material['name'] ?></a><?php
        }

        //проверяем его дочерний тип
        if ( ! empty($material['childs'])) {
            $materials = $material['childs'];
            include($site->partial('/f_adminpanel/material_types'));
        }
        ?>
        </li>
        <?php
    }

    ?>
</ul>