<?php
/**
 * @var \iSite $this
 */


defined('_WPF_') or die();

if ( ! $this->requireAuthUser()->can('admin.oiv_update_alldb'))
	$this->endRequest(403);

header('Content-type: text/plain');

ob_start();

ini_set('max_execution_time', 0);

require_once(__DIR__.'/functions.php');

$this->getPostValues(array('updatedb_tables'));

$this->wpf_include('modules/remotecontrol/functions.php');

$oivlist = null;

if (is_null($oivlist) && file_exists(__DIR__.'/data/oivlist-local.php'))
	$oivlist = oivupdatealldb_expand_oivlist((array)include(__DIR__.'/data/oivlist-local.php'));

if (is_null($oivlist) && defined('WPF_ENV') && file_exists(__DIR__.'/data/oivlist-'.constant('WPF_ENV').'.php'))
    $oivlist = oivupdatealldb_expand_oivlist((array)include(__DIR__.'/data/oivlist-'.constant('WPF_ENV').'.php'));

if (is_null($oivlist) && file_exists(__DIR__.'/data/oivlist.php'))
	$oivlist = oivupdatealldb_expand_oivlist((array)include(__DIR__.'/data/oivlist.php'));

$module_params_forward = oivupdatealldb_get_forwarded_params($_REQUEST, 'oivupdatedb_');

foreach ($oivlist as $oiv => $oivinfo) {
	$remote_url = $oivinfo['url'];
	$rc_params = isset($oivinfo['rc_params']) ? $oivinfo['rc_params'] : array();
	$call_module = 'oiv_update_db';
	$module_params = isset($oivinfo['module_params']) ? $oivinfo['module_params'] : array();
    $module_params = array_merge($module_params_forward, $module_params);

	echo "OIV $oiv $remote_url..";

	$resp = remotecontrol_call($remote_url, $rc_params, $call_module, $module_params);

	if ($resp === false) {
		echo "fail\n";
	} else {
		echo "ok\n";
	}
	?>
    <pre>
    <?php

    if (is_array($resp)) {// json
	    print_r($resp);
    } else {
        echo $resp;
    }
    ?>
    </pre>
    <?php

//    break;
}

$this->html = ob_get_clean();
echo $this->html;
exit;
