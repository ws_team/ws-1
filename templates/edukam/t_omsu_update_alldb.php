<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

global $migration;

if ($this->user->isAdmin()) {
    include_once($this->locateTemplate('f_header'));

    if ( ! empty($this->data->iH1)) { ?>
        <div class="container container--admin-title">
            <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
        </div>
    <?php } ?>
    <div class="contentblock basemargin">
        <?= $migration->getOutput(); ?>
    </div>
    <?php
    include_once($this->locateTemplate('f_footer'));
}