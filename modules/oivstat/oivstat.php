<?php
//добьявляем переменные
$this->data->errortext='';

$this->data->iH1='Статистика заполнения конента';

$this->GetPostValues(Array('startdate','enddate', 'period', 'oiv','mode'));


if($this->values->period == '')
{
    $this->values->period = 'month';
}

if(($this->values->startdate == ''))
{
    $this->values->startdate='01.'.date('m.Y');
    $this->values->enddate=date('d.m.Y');
}

$starttime=MakeTimeStampFromStr($this->values->startdate);
$endtime=MakeTimeStampFromStr($this->values->enddate) + 24*60*60;

if($this->values->mode=='accesslogs')
{

    $query='SELECT * FROM userlogs WHERE actionname NOT IN ('."'Изменение материала', 'Добавление материала', 'Удаление материала'".')
    AND actiondate between '.$starttime.' AND '.$endtime;
    if ($this->values->oiv != '') {
        $query .= ' AND "site" = \''.$this->values->oiv.'\' ';
    }
    $query .= ' ORDER BY actiondate DESC';

    if($res=$this->dbquery($query))
    {
        $this->data->stat=$res;

        //krumo($res);
    }

}
else
{
    //получаем список изменений, добавлений материалов пользователем за выбранный период
    $query='SELECT "site", "username", "userlogin", "userid", "actionname", count("objid") "ocnt"
FROM "userlogs" WHERE "actiondate" between '.$starttime.' AND '.$endtime;

    if ($this->values->oiv != '') {
        $query .= ' AND "site" = \''.$this->values->oiv.'\' ';
    }

    $query.='GROUP BY "site", "username", "userlogin", "userid", "actionname"
ORDER BY "site" ASC, "actionname" ASC, "username" ASC';

//krumo($query);

    $this->data->stat=Array();

    if($res=$this->dbquery($query))
    {
        $this->data->stat=$res;
    }
}

$this->data->jsdocready = "


        $('#startdate').datetimepicker({
            lang:'ru',
            timepicker:true,
            format:'d.m.Y'
        });

        $('#enddate').datetimepicker({
            lang:'ru',
            timepicker:true,
            format:'d.m.Y'
        });

        ";