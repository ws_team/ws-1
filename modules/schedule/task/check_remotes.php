<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$remotes = array(
    'https://gosmonitor.ru',
);

foreach ($remotes as $rem) {
    echo "Checking remote '$rem'.. ";
    $status = $this->isRemoteAvailable($rem, true);
    echo ($status ? 'available' : 'not available')."\n";
}
