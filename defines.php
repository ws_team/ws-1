<?php

if (!defined('MATTYPE_AUTHORITY_PARAM_TYPE_ID'))
    define('MATTYPE_AUTHORITY_PARAM_TYPE_ID', 59);
if (!defined('MATTYPE_AUTHORITY_TYPE'))
    define('MATTYPE_AUTHORITY_TYPE', 271);

if (!defined('MATTYPE_EVENT_IMPORTANT'))
    define('MATTYPE_EVENT_IMPORTANT', 20);

if (!defined('MATTYPE_SIGNEVENT_PARAM_TYPE'))
    define('MATTYPE_SIGNEVENT_PARAM_TYPE', 60);
if (!defined('MATTYPE_SIGNEVENT_PARAM_TYPE_EACHYEAR'))
    define('MATTYPE_SIGNEVENT_PARAM_TYPE_EACHYEAR', 150756);
if (!defined('MATTYPE_SIGNEVENT_PARAM_TYPE_ONLYYEAR'))
    define('MATTYPE_SIGNEVENT_PARAM_TYPE_ONLYYEAR', 150757);

if (!defined('MATTYPE_KHABKRAI'))
    define('MATTYPE_KHABKRAI', 4);
if (!defined('MATTYPE_GOVERNOR'))
    define('MATTYPE_GOVERNOR', 5);
if (!defined('MATTYPE_CORRUPTION'))
    define('MATTYPE_CORRUPTION', 191);
if (!defined('MATTYPE_PERSON'))
    define('MATTYPE_PERSON', 119);
if (!defined('MATTYPE_GOVERNCONGRATS'))
    define('MATTYPE_GOVERNCONGRATS', 26);
if (!defined('MATTYPE_GOVERNAPPEAL'))
    define('MATTYPE_GOVERNAPPEAL', 37);
if (!defined('MATTYPE_GOVERNPUB'))
    define('MATTYPE_GOVERNPUB', 28);
if (!defined('MATTYPE_GOVERNBIO'))
    define('MATTYPE_GOVERNBIO', 27);
if (!defined('MATTYPE_TERRORISM'))
    define('MATTYPE_TERRORISM', 333);

if (!defined('MATTYPE_ANNOUNCE'))
    define('MATTYPE_ANNOUNCE', 3);
if (!defined('MATTYPE_VIDEO'))
    define('MATTYPE_VIDEO', 12);

if (!defined('MATTYPE_NEWS'))
    define('MATTYPE_NEWS', 2);
if (!defined('MATTYPE_REGION'))
    define('MATTYPE_REGION', 21);

if (!defined('MATTYPE_AUTHORITY'))
    define('MATTYPE_AUTHORITY', 6);

if (!defined('MATTYPE_ACCREDFORM'))
    define('MATTYPE_ACCREDFORM', 15);

if (!defined('MATTYPE_SIGNEVENT'))
    define('MATTYPE_SIGNEVENT', 66);

if ( ! defined('MATTYPE_BANNER'))
    define('MATTYPE_BANNER', 56);

