<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\mail;

require_once('SenderInterface.php');

class StubSender implements SenderInterface
{
    private $filePath;

    public function __construct($settings)
    {
        $this->filePath = $settings->path.'/runtime/email/'.date('Ymd').'.txt';
    }

    public function send($to, $subject, $body, $attachments = array())
    {
        $delimiterOpen = '--MSG-'.sprintf('%.6f', microtime(true));
        $delimiterClose = $delimiterOpen.'--';

        $contents = array($delimiterOpen);
        foreach ((array)$to as $tos)
            $contents[] = "To: $tos";

        $contents[] = '';
        $contents[] = 'Subject: '.$subject;
        if (!empty($attachments))
            $contents[] = 'Has-attachments';

        $contents[] = '';
        $contents[] = $body;
        $contents[] = $delimiterClose;
        $contents[] = '';

        file_put_contents($this->filePath, implode("\n", $contents), FILE_APPEND);
    }
}
