<?php
//функции модуля материалов=========================================================

//клонирование материала
function DoCloneMaterial($site,$id='')
{
    if($id == '')
    {
        $id=$site->values->clonematerial;
    }

    if($id != '')
    {
        //получаем все основные значения клонируемого материала
        $cMat=MaterialGet($site,$id);

        //формируем характеристики для добавления копии материала
        foreach($cMat as $key=>$param)
        {
            if((!is_array($param))&&($key != 'id')&&($key != 'id_char'))
            {
                if($key == 'name')
                {
                    $param.=time();
                }
                elseif($key == 'content')
                {
                    $param=str_replace("'",'"',$param);
                }
                elseif($key == 'status_id')
                {
                    $param='1';
                }

                $site->values->$key=$param;
            }
        }

        //создаем материал
        $site->values->id='';
        $site->values->type_id=$site->values->imenu;
        $nMat=createMaterial($site);

        //print_r($nMat);
        //    die();

        if($nMat[0])
        {
            //сохраняем связи материалов
            if(isset($cMat['connected_materials']))
            {
                if(count($cMat['connected_materials']) > 0)
                {
                    foreach($cMat['connected_materials'] as $comat)
                    {

                        $site->values->matforconnect = $comat['id'];
                        $site->values->id = $nMat[0];

                        DoConnectMaterial($site);

                    }
                }
            }

            //сохраняем дополнительные значения
            $query='SELECT * FROM "extra_materials" WHERE "material_id" = '.$cMat['id'];
            if($res=$site->dbquery($query))
            {
                if(count($res))
                {
                    foreach($res as $row)
                    {

                        //вставляем дубликуат записи в копируемый материал

                        $valuename=$row['valuename'];
                        $valuetext=$row['valuetext'];
                        $valuedate=$row['valuedate'];
                        $material_id=$nMat[0];
                        $type_id=$row['type_id'];
                        $selector_id=$row['selector_id'];
                        $valuemat=$row['valuemat'];

                        if($valuename == '')
                        {
                            $valuename=NULL;
                        }
                        else
                        {
                            $valuename="'$valuename'";
                        }

                        if($valuetext == '')
                        {
                            $valuetext=NULL;
                        }
                        else
                        {
                            $valuetext="'$valuetext'";
                        }

                        if($valuedate == '')
                        {
                            $valuedate=NULL;
                        }
                        else
                        {
                            $valuedate="'$valuedate'";
                        }

                        if($material_id == '')
                        {
                            $material_id=NULL;
                        }


                        if($type_id == '')
                        {
                            $type_id=NULL;
                        }


                        if($selector_id == '')
                        {
                            $selector_id=NULL;
                        }



                        if($valuemat == '')
                        {
                            $valuemat=NULL;
                        }



                        $queryi='INSERT INTO "extra_materials" ("valuename","valuetext","valuedate","material_id","type_id","selector_id","valuemat")
                        VALUES('.$valuename.','.$valuetext.','.$valuedate.','.$material_id.','.$type_id.','.$selector_id.','.$valuemat.')';
                        $site->dbquery($queryi);

                    }
                }
            }


            //сохраняем связанные файлы
            $query='SELECT * FROM "file_connections" WHERE "material_id" = '.$cMat['id'];
            if($res=$site->dbquery($query))
            {
                if(count($res) > 0)
                {
                    foreach($res as $row)
                    {

                        $file_id=$row['file_id'];
                        $material_id=$nMat[0];

                        $queryi='INSERT INTO "file_connections"("file_id","material_id")
                        VALUES('.$file_id.','.$material_id.')';

                        $site->dbquery($queryi);
                    }
                }
            }

            //переадресаци на изменение
            header('Location: /?menu=editmaterials&imenu='.$site->values->imenu.'&id='.$nMat[0].'&action=edit');
            die();

        }else{
            print('косяк');
        }
    }

}

//получение данных типа материала
function GetMattypeValues($site,$id)
{
    $mattype=Array();

    $query='SELECT * FROM "material_types" WHERE "id" = '.$id;
    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            $mattype=$res[0];
        }
    }

    return($mattype);
}

//связка уже существующего файла с материалом
function DoConnectFile($site)
{
    if(($site->values->fileforconnect != '')&&($site->values->id != ''))
    {
        $query='INSERT INTO "file_connections" ("file_id", "material_id")
        VALUES('.$site->values->fileforconnect.', '.$site->values->id.')';

        $site->dbquery($query);
    }
}

//отвязываем материал
function doUnconnectMat($site, $mat)
{
    //$mat=$this->values->mat;
    $mid=$site->values->id;
    $query='DELETE FROM "material_connections" WHERE ("material_parent" = '.$mat.' AND "material_child" = '.$mid.')
            OR ("material_parent" = '.$mid.' AND "material_child" = '.$mat.')';
    $resd=$site->dbquery($query);

    return($resd);
}

//удаляем файл
function doDeleteFile($site, $file)
{

    //$file=$this->values->dfile;


    //проверяем на колличество связей
    $ccnt=0;
    $query='SELECT count("material_id") "ccnt" FROM "file_connections" WHERE "file_id" = '.$file;
    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            $ccnt=$res[0]['ccnt'];
        }
    }

    $query='DELETE FROM "file_connections" WHERE "file_id" = '.$file.' AND "material_id" = '.$site->values->id;
    $res=$site->dbquery($query);

    if($ccnt < 2)
    {
        $query='DELETE FROM "files" WHERE "id" = '.$file;
        $res=$site->dbquery($query);
    }

}

//сохраняем селекторы
function DoAddSelector($site, $selectors, $material)
{

    foreach($selectors as $selector)
    {
        $extratypes=GetExtraTypes($site, $selector['mattype_id']);

        //получаем материалы
        $query='SELECT * FROM "materials" WHERE "type_id" = '.$selector['mattype_id'];
        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                foreach($res as $row)
                {
                    DoAddExtraMaterials($site, $extratypes, $material, $row['id']);
                }
            }
        }
    }

}

//сохраняем доп. характеристики
function DoAddExtraMaterials($site, $extratypes, $material, $selector='')
{

    //

    if(($material != '')&&($extratypes[0] != ''))
    {



        //$params=GetExtraParams($site, $extratypes, $material);

        //удаляем предыдущие значения
        $query='DELETE FROM "extra_materials" WHERE "material_id" = '.$material.' AND "type_id" IN ('.$extratypes[0].')
         AND "selector_id" IS NULL';

        if($selector != '')
        {
            $query='DELETE FROM "extra_materials" WHERE "material_id" = '.$material.' AND "type_id" IN ('.$extratypes[0].')
         AND "selector_id" = '.$selector;
        }

        if($res=$site->dbquery($query))
        {

            //сохраняем значения
            foreach($extratypes[1] as $type)
            {
                //получаем значение
                $paramname='extravalue_'.$type['id'];

                if(($selector != '')&&($selector != 'NULL'))
                {
                    $paramname.='_'.$selector;
                }

                //print('<br>'.$paramname);

                $site->GetPostValues($paramname,1);



                //сохраняем значение
                if($site->values->$paramname != '')
                {
                    $value=$site->values->$paramname;

                    if($selector == '')
                    {
                        $selector='NULL';
                    }

                    switch($type['valuetype'])
                    {
                        case 'valuetext':

                            $query='INSERT INTO "extra_materials" ("valuename", "valuetext", "valuedate", "material_id", "type_id", "selector_id")
                        values(NULL, '."'$value'".', NULL, '.$material.', '.$type['id'].', '.$selector.')';

                            //print($query);

                            break;

                        case 'valuedate':

                            $query='INSERT INTO "extra_materials" ("valuename", "valuetext", "valuedate", "material_id", "type_id", "selector_id")
                        values(NULL, NULL, TO_DATE('."'$value'".','."'DD.MM.YYYY HH24:MI'".'), '.$material.', '.$type['id'].', '.$selector.')';

                            //print($query);

                            break;

                        case 'valuemat':

                            $query='INSERT INTO "extra_materials" ("valuename", "valuetext", "valuedate", "material_id", "type_id", "selector_id", "valuemat")
                        values(NULL, NULL, NULL, '.$material.', '.$type['id'].', '.$selector.', '.$value.')';

                            break;

                        default:

                            $query='INSERT INTO "extra_materials" ("valuename", "valuetext", "valuedate", "material_id", "type_id", "selector_id")
                        values('."'$value'".', NULL, NULL, '.$material.', '.$type['id'].', '.$selector.')';

                            break;
                    }

                    //print('yaaa - '.$site->values->$paramname.'<br>'.$query.'<br>');

                    $res=$site->dbquery($query);


                }


            }

        }
    }


    //die();

}

//печатаем селектовы
//PrintSelectors($this, $selectors, $this->values->id);
function PrintSelectors($site, $selectors, $material = '')
{

    $out='';

    foreach($selectors as $selector)
    {

        $extratypes=GetExtraTypes($site, $selector['mattype_id']);

        $out.='<tr>
            <th colspan="2"><big>'.$selector['mattypename'].'</big></th>
        </tr>';

        //список материалов селектора
        $query='SELECT * FROM "materials" WHERE "type_id" = '.$selector['mattype_id'];
        //print($query);
        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                foreach($res as $row)
                {

                    $out.='<tr>
                    <th colspan="2">'.$row['name'].'</th>
                    </tr>';

                    //получаем характеристики
                    //$params=GetExtraParams($site, $extratypes, $material, $row['id']);

                    //печатаем характеристики
                    $out.=PrintExtraParams($site,$extratypes,$material,$row['id']);

                }
            }
        }

    }

    return $out;

}

//печатаем характеристики в таблицу изменеия/добавления материала
function PrintExtraParams($site, $extratypes, $material = '', $selector='')
{

    $out='';

    $params=GetExtraParams($site, $extratypes, $material, $selector);

    //print_r($params);

    foreach($extratypes[1] as $type)
    {

        $value='';

        switch($type['valuetype'])
        {




            case 'valuetext':

                if($selector != '')
                {
                    if(isset($params[$type['id']]['selector'][$selector]))
                    {
                        $value=$params[$type['id']]['selector'][$selector]['valuetext'];
                    }
                }
                elseif(isset($params[$type['id']]))
                {
                    $value=$params[$type['id']]['valuetext'];
                }

                $vname="extravalue_".$type['id'];

                if($selector != '')
                {
                    $vname.='_'.$selector;
                }

                $valueinput="<textarea class='styler' name=\"$vname\">$value</textarea>";

                break;

            case 'valuedate':

                if($selector != '')
                {
                    if(isset($params[$type['id']]['selector'][$selector]))
                    {
                        $value=$params[$type['id']]['selector'][$selector]['valuedaten'];
                    }
                }
                elseif(isset($params[$type['id']]))
                {
                    $value=$params[$type['id']]['valuedaten'];
                }

                $vname="extravalue_".$type['id'];

                if($selector != '')
                {
                    $vname.='_'.$selector;
                }

                $valueinput="<input type='text' name='$vname' class='styler dateinput' value='$value' />";

                break;

            case 'valuemat':

                //GetCityOptions($site,$acity='',$citymattype=12, $parent_id='NULL', $level='')

                if($selector != '')
                {
                    if(isset($params[$type['id']]['selector'][$selector]))
                    {
                        $value=$params[$type['id']]['selector'][$selector]['valuemat'];
                    }
                }
                elseif(isset($params[$type['id']]))
                {
                    $value=$params[$type['id']]['valuemat'];
                }

                //print_r($type);

                $vname="extravalue_".$type['id'];

                if($selector != '')
                {
                    $vname.='_'.$selector;
                }

                $valueinput="<select name='$vname' class='styler'><option value=''>---</option>";

                $valueinput.=GetCityOptions($site, $value, $type['valuemattype']);

                $valueinput.="</select>";

                break;

            default:

                if($selector != '')
                {
                    if(isset($params[$type['id']]['selector'][$selector]))
                    {
                        $value=$params[$type['id']]['selector'][$selector]['valuename'];
                    }
                }
                elseif(isset($params[$type['id']]))
                {
                    $value=$params[$type['id']]['valuename'];
                }

                $vname="extravalue_".$type['id'];

                if($selector != '')
                {
                    $vname.='_'.$selector;
                }

                $valueinput="<input type='text' name='$vname' class='styler' value='$value' />";

                break;
        }

        $out.="<tr>
            <th>$type[name]:</th>
            <td>$valueinput</td>
        </tr>";
    }

    return($out);

}

//получаем значения характеристик и выводим поля для изменения
function GetExtraParams($site, $extratypes, $material = '', $selector='')
{
    $params=Array();

    //print('ffbg'.$material);

    if($material != '')
    {



        $material=(int)$material;

    if($extratypes[0] != '')
    {
        $query='SELECT "e".*, TO_CHAR("e"."valuedate",'."'DD.MM.YYYY HH24:MI'".') "valuedaten" FROM "extra_materials" "e" WHERE "e"."type_id" IN ('.$extratypes['0'].')
        AND "material_id" = '.$material;


        if($selector != '')
        {
            $query.=' AND "selector_id" = '.$selector;
        }

        //print($query);

        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                foreach($res as $row)
                {
                    $params[$row['type_id']]=$row;
                    if($selector != '')
                    {
                        $params[$row['type_id']]['selector'][$selector]=$row;
                    }
                }
            }
        }

    }

    }

    return($params);
}

//получаем поля характеристик типа материала
function GetExtraTypes($site, $type)
{
    $ids='';
    $types=Array();

    if($type != '')
    {
        $type=(int)$type;

        $query='SELECT *
        FROM "extra_mattypes"
        WHERE "type_id" = '.$type;

        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                foreach($res as $row)
                {
                    if($ids != '')
                    {
                        $ids.=',';
                    }

                    $ids.=$row['id'];

                }

                $types=$res;

            }
        }

    }

    return(Array($ids,$types));
}

function GetSelectors($site, $type)
{

    $selectors=Array();

    $query='SELECT "s".*,
            (SELECT "n"."name" FROM "material_types" "n" WHERE "n"."id" = "s"."mattype_id") "mattypename"
            FROM "extra_selectors" "s" WHERE "s"."type_id" = '.$type;

    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            $selectors=$res;
        }
    }


    return($selectors);

}

//функция выводит типы как опшены в селекте
function PrintTypeOptions($types, $atype, $level=0)
{
    $rows='';

    foreach($types as $mattype)
    {

        //считаем кол-во отступов
        $otst='';
        if($level > 0)
        {
            for($i=0; $i<$level; ++$i)
            {
                $otst.='&nbsp;&nbsp;&nbsp;';
            }
        }

        if($atype == $mattype['id'])
        {
            $selected='SELECTED';
        }
        else
        {
            $selected='';
        }


        if(mb_strlen($mattype['name'],"utf8") > 75)
        {
            $mattype['name']=mb_substr($mattype['name'],0,75,"utf8").'...';
            $mattype['name']=str_replace('�','',$mattype['name']);

        }


        $rows.="<option value='$mattype[id]' $selected>$otst$mattype[name]</option>";

        //проверяем на наличае подтипов
        if(is_array($mattype['childs']))
        {
            $rows.=PrintTypeOptions($mattype['childs'], $atype, ($level+1));
        }



    }

    return($rows);

}

//функция записи файла в БД
function SimpleSaveFile($site)
{

    $fileid=false;

    //определяем id файла
    $query='SELECT MAX("id") "fid" FROM "files"';



    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            $fileid=$res[0]['fid'];
        }
        else
        {
            $fileid=1;
        }

        if($fileid == '')
        {
            $fileid=1;
        }

        //print($fileid.'-');

        ++$fileid;
        //print($fileid.'-');
        //++$fileid;

        //print($fileid);

        $filesize=0;
        if(isset($_FILES['filedata']['size']))
        {
            $filesize = $_FILES['filedata']['size'];
        }

        $ext='';
        if(isset($_FILES['filedata']['name']))
        {
            $ext=GetFileExtension($_FILES['filedata']['name']);
        }



        $name=$site->values->file_name;
        $name_en=$site->values->file_name_en;
        $type_id=$site->values->file_type_id;
        $content=$site->values->file_content;
        $parent_id=$site->values->parent_id;

        $date_event=$site->values->date_event;
        if($date_event == '')
        {
            $date_event=date('d.m.Y H:i');
        }

        $author=$site->values->author;
        $ordernum=(int)$site->values->ordernum;

        if($site->settings->DB->type != 'oracle')
        {
            $query='INSERT INTO "files"("id","name","name_en","type_id","size","extension","width","height","content","filedata","date_event","author","ordernum")
        VALUES('."$fileid, '$name', '$name_en', '$type_id', '$filesize', '$ext', 0, 0, '$content', ?".', TO_DATE('."'$date_event','DD.MM.YYYY HH24:MI'".'), '."'$author'".', '.$ordernum.')';
        }
        else
        {
            $query='INSERT INTO "files"("id","name","name_en","type_id","size","extension","width","height","content","filedata","date_event","author","ordernum")
        VALUES('."$fileid, '$name', '$name_en', '$type_id', '$filesize', '$ext', 0, 0, '$content', EMPTY_BLOB()".', TO_DATE('."'$date_event','DD.MM.YYYY HH24:MI'".'), '."'$author'".', '.$ordernum.')';
        }


        if((isset($_FILES['filedata']['tmp_name']))&&($_FILES['filedata']['tmp_name'] != ''))
        {
            $str=file_get_contents($_FILES['filedata']['tmp_name']);

            if($site->settings->DB->type == 'oracle')
            {
                $query.=' returning "filedata" into :b1';
            }

            $res=$site->dbquery($query,Array(),Array(),Array($str));
            //print($str);
            //print_r($res);
            //die();

        }
        else
        {
            if($site->settings->DB->type != 'oracle'){
                $query=str_replace('?','NULL',$query);
                $res=$site->dbquery($query);
            }
            else{
                $res=$site->dbquery($query);
            }

        }

    if(!$res)
    {
        $fileid=false;

    }


    }

    //

    return($fileid);

}

function SaveImageSize($from,$to,$newx,$newy,$extension,$crop=0)
{
    //echo $from;
    switch($extension)
    {
        case 'jpg':
            $im=imagecreatefromjpeg($from);
            $im1=imagecreatetruecolor($newx,$newy);
            break;
        case 'jpeg':
            $im=imagecreatefromjpeg($from);
            $im1=imagecreatetruecolor($newx,$newy);
            break;
        case 'png':
            $im=imagecreatefrompng($from);

            $im1=imagecreatetruecolor($newx,$newy);
            imagealphablending($im1, false);
            imagesavealpha($im1, true);
            break;
        case 'bmp':
            $im=imagecreatefrombmp2($from);
            $im1=imagecreatetruecolor($newx,$newy);
            break;
        case 'gif':
            $im=imagecreatefromgif($from);
            $im1=imagecreatetruecolor($newx,$newy);
            break;
    }

    if($crop==0)
    {
        imagecopyresampled($im1,$im,0,0,0,0,$newx,$newy,imagesx($im),imagesy($im));
    }
    else
    {

        //просчитываем с какой стороны обрезать (формируем переменные для обрезания)
        $sootn1=$newx/$newy;
        $sootn2=imagesx($im)/imagesy($im);

        //режим по x
        if($sootn1 >= $sootn2)
        {
            $ix=imagesx($im);
            $iy=round($newy*imagesx($im)/$newx);
        }
        else
        {

            $iy=imagesy($im);
            $ix=round($newx*imagesy($im)/$newy);

        }



        imagecopyresampled($im1,$im,0,0,0,0,$newx,$newy,$ix,$iy);

    }

    imagejpeg($im1,$to,90);

}

function CreateFileCash($site, $fileid)
{
    $succes = false;

    $query='SELECT * FROM "files" WHERE "id" = '.$fileid;

    //print($query);
    if($res=$site->dbquery($query))
    {
        $dbfile=$res[0];
        $filename=$site->settings->path.'/photos/'.$dbfile['id'];
        if(file_put_contents($filename,$dbfile['filedata']))
        {
            $succes=true;
        }
    }

    return($succes);
}

function CreateImageCash($site, $fileid)
{
    $succes=false;

    //получаем все данные картинки
    $query='SELECT * FROM "files" WHERE "id" = '.$fileid;
    if($res=$site->dbquery($query))
    {
        $dbfile=$res[0];

        //преобразуем в жпегъ
        $filename=$site->settings->path.'/photos/'.$dbfile['id'];
        if(file_put_contents($filename,$dbfile['filedata']))
        {

            //проверяем файл на картинку
            $smallsizes=getimagesize($filename);
            $typecode=-1;
            switch($smallsizes[2])
            {
                case '2':
                    //jpg
                    $typecode=2;
                    $extension='jpg';
                    break;
                case '3':
                    //png
                    $typecode=3;
                    $extension='png';
                    break;
                case '1':
                    //gif
                    $typecode=1;
                    $extension='gif';
                    break;
                case '6':
                    //bmp
                    $typecode=6;
                    $extension='bmp';
                    break;
            }

            if($smallsizes[2] == $typecode)
            {
                //создаем источник картинки из файла

                $width=$smallsizes[0];
                $height=$smallsizes[1];


                //размеры
                $xarr=Array(100,158,187,200,210,270,290,370,546,600,922);
                $yarr=Array(71,35);
                $xyarr=Array(Array(64,64),Array(142,109),Array(289,196),Array(58,49),Array(98,76),Array(116,100),Array(370,296),Array(447,296),Array(246,245),Array(270,120), Array(688,120));

                //делаем кэши картинок по x
                foreach($xarr as $x)
                {

                    $newx=$x;
                    $newy=round($height*$newx/$width);

                    $to=$site->settings->path.'/photos/'.$dbfile['id'].'_x'.$x.'.jpg';
                    SaveImageSize($filename,$to,$newx,$newy,$extension);
                }

                //делаем кэши картинок по y
                foreach($yarr as $y)
                {

                    $newy=$y;
                    $newx=round($width*$newy/$height);

                    $to=$site->settings->path.'/photos/'.$dbfile['id'].'_y'.$y.'.jpg';
                    SaveImageSize($filename,$to,$newx,$newy,$extension);
                }

                //делаем кэши картинок с обрезанием
                foreach($xyarr as $xy)
                {
                    $newx=$xy[0];
                    $newy=$xy[1];

                    $to=$site->settings->path.'/photos/'.$dbfile['id'].'_xy'.$newx.'x'.$newy.'.jpg';
                    SaveImageSize($filename,$to,$newx,$newy,$extension,1);

                }

                $succes=true;

            }

        }

    }


    return($succes);
}

//функция добавления расписания в контакт
function DoAddShed($site){
    $err=false;
    $errtext='';

    if($site->values->sname != '')
    {
        $cid='';
        //получаем id
        $query='SELECT MAX("id")+1 "cid" FROM "schedules"';
        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                $cid=$res[0]['cid'];
            }
        }

        if($cid == '')
        {
            $cid = 1;
        }

        //заносим контакт
        $query='INSERT INTO "schedules" ("id", "contact_id", "name", "content", "material_id")
        VALUES ('.$cid.', '."'".$site->values->doaddshed."'".', '."'".$site->values->sname."'".','."'".$site->values->scontent."'".', NULL)';


        if($res=$site->dbquery($query))
        {
            $err=false;
            $errtext=' - расписание успешно сохранено!';
        }
    }
    else
    {
        $err=true;
        $errtext=' - ошибка: не передано название расписания';
    }

    return Array(!$err,$errtext);
}

//функция добавления контакта
function DoAddCont($site){

    $err=false;
    $errtext='';

    if($site->values->addr != '')
    {

        $cid='';
        //получаем id
        $query='SELECT MAX("id")+1 "cid" FROM "contacts"';
        if($res=$site->dbquery($query))
        {
         if(count($res) > 0)
         {
            $cid=$res[0]['cid'];
         }
        }

        if($cid == '')
        {
            $cid = 1;
        }

        //заносим контакт
        $query='INSERT INTO "contacts" ("id","material_id","addr","tel","email","fax","building","office","city","latitude","longitude")
        VALUES('.$cid.', '.$site->values->id.', '."'".$site->values->addr."'".','."'".$site->values->tel."'".',
        '."'".$site->values->email."'".','."'".$site->values->fax."'".',
        '."'".$site->values->building."'".','."'".$site->values->office."'".','."'".$site->values->city."'".',
        '."'".$site->values->latitude."'".','."'".$site->values->longitude."'".')';


        if($res=$site->dbquery($query))
        {
            $err=false;
            $errtext=' - контакт успешно сохранен!';
        }


    }
    else
    {
        $err=true;
        $errtext=' - ошибка: не заполнен адресс контакта';
    }

    return Array(!$err,$errtext);

}

//функция связки материала
function DoConnectMaterial($site)
{
    $err=false;
    $errtext='';

    if(($site->values->matforconnect != '')&&($site->values->id != ''))
    {
        //пробуем связать материалы
        $query='INSERT INTO "material_connections" ("material_parent", "material_child")
        VALUES('.$site->values->id.','.$site->values->matforconnect.')';

        if($res=$site->dbquery($query))
        {

        }
        else
        {
            $err=true;
            $errtext=' - ошибка: не получилось записать в БД';
        }

    }
    else
    {
        $err=true;
        $errtext=' - ошибка: не передан материал для связки';
    }

    return Array(!$err,$errtext);

}

//функция изменения файла
function DoEditFile($site)
{
    if(($site->values->file_id != '')&&($site->values->file_name != ''))
    {

        $name=$site->values->file_name;
        $name_en=$site->values->file_name_en;
        $content=$site->values->file_content;
        $date_event=$site->values->date_event;
        $author=$site->values->author;
        $ordernum=(int)$site->values->ordernum;

        $query='UPDATE "files" SET "name" = '."'$name'".', "name_en" = '."'$name_en'".', "content" = '."'$content'".',
        "date_event" = TO_DATE('."'$date_event'".','."'DD.MM.YYYY HH24:MI'".'), "author" = '."'$author'".', "ordernum" = '.$ordernum.'
        WHERE "id" = '.$site->values->file_id;

        $site->dbquery($query);


    }
}

//функция сохранения файла с разбором типов и сохранением различных кэшей
function DoSaveFile($site)
{
    $err=false;
    $errtext='';


    if(($site->values->file_name != '')&&(isset($_FILES['filedata'])||(($site->values->file_type_id == 2)&&($site->values->file_content != ''))))
    {

        if($fileid=SimpleSaveFile($site))
        {

        //определение типа файла
        switch($site->values->file_type_id)
        {
            //файл фото - загружаем файл, создаём кэш картинок разных размеров
            case '1':

                //создаем кэши картинок на стороне сайта
                if(!CreateImageCash($site, $fileid))
                {
                    $err=true;
                    $errtext.=" - ошибка: не удалось сохранить кэши фотографий (переданный файл больше допустимого размера либо имеет неверный формат)";
                }

                break;

            //файл видео - сохраняем ссылку youtube
            case '2':

                //ничего не делаем


                break;

            /*case '3':
                break;*/

            //файл видео трансляции - сохраняем ссылку
            case '5':
                //ничего не делаем


                break;

            //просто сохраняем файл, создаём кэш
            default:

                //создаем кэш файла на стороне сайта
                if(!CreateFileCash($site, $fileid))
                {
                    $err=true;
                    $errtext.=" - ошибка: не удалось сохранить кэши загружаемого файла (переданный файл больше допустимого размера либо имеет неверный формат)";
                }

                break;
        }
        }
        else
        {
            $err=true;
            $errtext.=" - ошибка: не удалось сохранить файл";
        }


    }
    else
    {
        $err=true;
        $errtext.=" - ошибка: не переданы обязательные поля для добавления файла";
    }


    //если  все норм - связываем файл с материалом
    if(!$err)
    {
        $query='INSERT INTO "file_connections" ("file_id","material_id") VALUES('.$fileid.','.$site->values->id.')';
        if($res=$site->dbquery($query))
        {
            $errtext=' - файл успешно добавлен';
        }
        else
        {
            $err=true;
            $errtext=' - ошибка: не удалось связать файл с материалом';
        }
    }


    return Array(!$err,$errtext);

}

//сохраняем файл
function savefile($site,$mid)
{
    //пробуем добавить файл (если он передан)
    //пробуем сохранить файл
    $file = new iImgFile($site);
    $file->Save('imagefile', null, null, null, 3, $mid);

    //print_r($file);

    $file->Resize(true, 'jpg', 800);

    if($small=$file->Resize(false, false, 200))
    {
        unset($file);
        //$files = new iImgFile($this, $small);
        $file = new iImgFile($site, $small);
    }


    unset($file);
}

function createMaterial($site)
{
    $err=false;
    $errtext='';

    //получаем переданные данные материала
    //$site->GetPostValues(Array('id','id_char','type_id','language_id','name','anons','content','date_add','date_edit','date_event','status_id','extra_number','extra_number2','extra_text1','extra_text2'));


    //если есть нужные переменные
    if(($site->values->type_id != '')&&($site->values->name != ''))
    {
        //обработка остальных переменных

        //id
        if($site->values->id == '')
        {
            $id = 'NULL';
        }
        else
        {
            $id=$site->values->id;
        }

        //name
        $name=$site->values->name;

        //id_char
        $id_char=$site->values->id_char;
        if($id_char == '')
        {
            $id_char=mb_transliterate($name);
            $id_char=str_replace(' ','_',$id_char);
        }
        //проверяем id_char на валидность
        if($id == 'NULL')
        {
            $query='SELECT count("id") "cid" FROM "materials" WHERE "id_char" = '."'".$id_char."'";
        }
        else
        {
            $query='SELECT count("id") "cid" FROM "materials" WHERE "id_char" = '."'".$id_char."'".' AND "id" <> '.$id;
        }
        if($res=$site->dbquery($query))
        {
            if($res[0]['cid'] > 0)
            {
                $id_char.=time();
            }
        }
        else
        {
            $err=true;
            $errtext.=" - ошибка: Не получилось проверить буквенный идентификатор";
        }


        //type_id
        $type_id = $site->values->type_id;

        //language_id
        $language_id = $site->values->language_id;
        if($language_id == '')
        {
            $language_id=1;
        }

        //anons
        $anons=$site->values->anons;

        //content
        $content=$site->values->content;
        $content=str_replace('`',"'",$content);

        //date_add
        if($id == 'NULL')
        {
            $date_add='SYSDATE';
        }

        //date_edit
        $date_edit='SYSDATE';

        //date_event
        if($site->values->date_event == '')
        {
            $date_event='SYSDATE';
        }
        else
        {
            $date_event="TO_DATE('".$site->values->date_event."','DD.MM.YYYY HH24:MI')";
        }

        //user_id
        $user_id = $site->autorize->userid;

        //status_id
        if($site->values->status_id == '')
        {
            $status_id = 1;
        }
        else
        {
            $status_id = $site->values->status_id;
        }

        //extra_number
        if($site->values->extra_number == '')
        {
            $extra_number = 'NULL';
        }
        else
        {
            $extra_number = $site->values->extra_number;
        }

        //extra_number2
        if($site->values->extra_number2 == '')
        {
            $extra_number2 = 'NULL';
        }
        else
        {
            $extra_number2 = $site->values->extra_number2;
        }

        //extra_text1
        if($site->values->extra_text1 == '')
        {
            $extra_text1 = '';
        }
        else
        {
            $extra_text1 = $site->values->extra_text1;
        }

        //extra_text2
        if($site->values->extra_text2 == '')
        {
            $extra_text2 = '';
        }
        else
        {
            $extra_text2 = $site->values->extra_text2;
        }

        if($site->values->parent_id != '')
        {
            $parent_id=$site->values->parent_id;
        }
        else
        {
            $parent_id='NULL';
        }

        //пробуем добавить/сохранить
        if(!$err)
        {
            if($id == 'NULL')
            {



                //получаем ногвый ID
                $nid='';
                $query='SELECT MAX("id")+1 "nid" FROM "materials"';
                if($res=$site->dbquery($query))
                {
                       if(count($res) > 0)
                       {
                           $id = $res[0]['nid'];
                       }
                }

                if($id == '')
                {
                    $id = 1;
                }



                if($site->settings->DB->type != 'oracle')
                {
                    $query='INSERT INTO "materials" ("id","id_char","type_id","language_id","name","anons","content","date_add","date_edit","date_event","user_id","status_id","extra_number","extra_number2","extra_text1","extra_text2","parent_id")
                '."VALUES($id, '$id_char', '$type_id', '$language_id', '$name', '$anons', '$content', $date_add, $date_edit, $date_event, $user_id, $status_id, $extra_number, $extra_number2, '$extra_text1', '$extra_text2','$parent_id')
                ".'';
                }
                else
                {
                    $query='INSERT INTO "materials" ("id","id_char","type_id","language_id","name","anons","content","date_add","date_edit","date_event","user_id","status_id","extra_number","extra_number2","extra_text1","extra_text2","parent_id")
                '."VALUES($id, '$id_char', '$type_id', '$language_id', '$name', '$anons', EMPTY_CLOB(), $date_add, $date_edit, $date_event, $user_id, $status_id, $extra_number, $extra_number2, '$extra_text1', '$extra_text2',$parent_id)
                ".'RETURNING "content" INTO :c1';
                }

                //print($query);
                //die();
            }
            else
            {
                if($site->settings->DB->type != 'oracle')
                {
                    $query='UPDATE "materials" SET "id_char" = '."'$id_char'".', "type_id" = '."'$type_id'".',
                "language_id" = '."'$language_id'".', "name" = '."'$name'".', "anons" = '."'$anons'".',
                "content" = '."'".$content."'".', "date_edit" = SYSDATE,
                "date_event" = '."$date_event".', "user_id" = '."'$user_id'".', "status_id" = '."'$status_id'".',
                "extra_number" = '.$extra_number.', "extra_number2" = '.$extra_number2.', "extra_text1" = '."'$extra_text1'".', "extra_text2" = '."'$extra_text2'".', "parent_id" = '.$parent_id.'
                WHERE "id" = '.$id.' ';
                }
                else
                {
                    $query='UPDATE "materials" SET "id_char" = '."'$id_char'".', "type_id" = '."'$type_id'".',
                "language_id" = '."'$language_id'".', "name" = '."'$name'".', "anons" = '."'$anons'".',
                "content" = EMPTY_CLOB(), "date_edit" = SYSDATE,
                "date_event" = '."$date_event".', "user_id" = '."'$user_id'".', "status_id" = '."'$status_id'".',
                "extra_number" = '.$extra_number.', "extra_number2" = '.$extra_number2.', "extra_text1" = '."'$extra_text1'".', "extra_text2" = '."'$extra_text2'".', "parent_id" = '.$parent_id.'
                WHERE "id" = '.$id.' RETURNING "content" INTO :c1';
                }
            }

            if($site->settings->DB->type != 'oracle')
            {
                if($res=$site->dbquery($query))
                {
                    $errtext=' - Выполнено успешно: материал сохранен.';
                }
            }
            else{
            if($res=$site->dbquery($query,Array(),Array($content)))
            {
                $errtext=' - Выполнено успешно: материал сохранен.';
            }}

        }

    }
    else
    {
        $err=true;
        $errtext.=' - ошибка: Пустое наименование либо тип материала!';
        //print_r($site->values);
    }


    if($err)
    {
        return array(!$err, $errtext);
    }
    else
    {
        return array($id, $errtext);
    }

}

//обьявляем переменные
$this->data->errortext='';

//тип материала
$fulltypename='';
if(isset($this->values->imenu))
{
    $type_id = $this->values->imenu;
}

function GetTypeName($site, $type_id)
{
    $type_name='';

    $query='SELECT "id", "name", "parent_id" FROM "material_types" WHERE "id" = '.$type_id;
    if($res=$site->dbquery($query))
    {
        if($res[0]['parent_id'] != '')
        {
            $type_name=GetTypeName($site, $res[0]['parent_id']).' - ';
        }
        $type_name.=$res[0]['name'];
    }

    return($type_name);
}

$fulltypename=GetTypeName($this, $type_id);



$this->GetPostValues(Array('id','id_char','type_id','language_id','name','anons','content','date_add','date_edit','date_event',
    'status_id','extra_number','extra_number2','extra_text1','extra_text2','file_name','file_name_en','file_type_id','file_content',
    'doaddfile','doconnectmaterial','matforconnect','doaddcont','addr','tel','email','fax','building','office','doaddshed','sname',
    'scontent','mat','dfile','dcont','parent_id','city','latitude','longitude','orderby','ordersort','search','page','ordernum','author'
    ,'file_id','doeditfile','deletefilegroup','deleteconmatgroup','fileforconnect','clonematerial'),1);



//рисуем заголовки
switch($this->values->action)
{
    case 'add':
        $this->data->iH1='Добавление материала (шаг 1 из 2)';


        //получаем связанные характеристики
        $extratypes=GetExtraTypes($this, $this->values->imenu);


        //получаем селекторы
        $selectors=GetSelectors($this, $this->values->imenu);

        //обработка добавления
        if($this->values->name != '')
        {
            $resarr=createMaterial($this);
            $this->data->errortext=$resarr[1];

            if($resarr[0])
            {
                $this->values->action='edit';
                $this->values->id=$resarr[0];
                $this->data->iH1='Изменение материала';

                //формируем массив материала
                $material=MaterialGet($this, $this->values->id);


                //связка материала
                if(($this->values->doconnectmaterial == '1')&&($this->values->matforconnect != ''))
                {
                    DoConnectMaterial($this);
                    //формируем массив материала
                    $material=MaterialGet($this, $this->values->id);
                }

                //добавляем значения характеристик
                if($extratypes[0] != '')
                {
                    DoAddExtraMaterials($this, $extratypes, $this->values->id);
                }

                if(count($selectors) > 0)
                {
                    DoAddSelector($this, $selectors, $this->values->id);
                }

                $temptext= ob_get_contents();
                //ob_end_clean();

                header("Location: /?menu=editmaterials&imenu=".$this->values->imenu."&id=".$this->values->id."&action=edit");
                die();

                ob_start();



                $this->headers = ob_get_contents();
                ob_end_clean();

                ob_start();
                echo $temptext;


            }

        }

        break;
    case 'edit':
        $this->data->iH1='Изменение материала';

        $this->data->mattype=GetMattypeValues($this,$this->values->imenu);

        if($this->values->clonematerial != '')
        {
            DoCloneMaterial($this);
        }

        //получаем связанные характеристики
        $extratypes=GetExtraTypes($this, $this->values->imenu);

        //получаем селекторы
        $selectors=GetSelectors($this, $this->values->imenu);

        //обработка изменения
        if($this->values->name != '')
        {
            $resarr=createMaterial($this);
            $this->data->errortext=$resarr[1];

            //добавляем значения характеристик
            if($extratypes[0] != '')
            {
                DoAddExtraMaterials($this, $extratypes, $this->values->id);
            }

            if(count($selectors) > 0)
            {
                DoAddSelector($this, $selectors, $this->values->id);
            }

        }

        //удаление связки материалов
        if($this->values->mat != '')
        {

            doUnconnectMat($this, $this->values->mat);

        }




        //удаление файлов
        if($this->values->dfile != '')
        {
            doDeleteFile($this,$this->values->dfile);
        }


        //удаление контакта
        if($this->values->dcont != '')
        {
            $dcont=$this->values->dcont;
            //удаление расписаний контакта
            $query='DELETE FROM "schedules" WHERE "contact_id" = '.$dcont;
            if($resd=$this->dbquery($query))
            {
                //удаление самого контакта
                $query='DELETE FROM "contacts" WHERE "id" = '.$dcont;
                $resd=$this->dbquery($query);
            }



        }


        //формируем массив материала
        $material=MaterialGet($this, $this->values->id);

        $this->values->status_id = $material['status_id'];
        $this->values->language_id = $material['language_id'];


        //удаление группы связанных материалов
        if($this->values->deleteconmatgroup == 1)
        {
            if(isset($material['connected_materials'][0]))
            {
                foreach($material['connected_materials'] as $cmat)
                {
                    $param='cmat_'.$cmat['id'];
                    $this->GetPostValues(Array($param));

                    if($this->values->$param == 1)
                    {
                        doUnconnectMat($this,$cmat['id']);
                    }
                }
            }
        }

        //удаление группы файлов
        if($this->values->deletefilegroup == 1)
        {
            //получаем переданные файлы
            foreach($material['allfiles'] as $file)
            {
                $paramname='dfile_'.$file['id'];
                $this->GetPostValues(Array($paramname));

                if($this->values->$paramname == 1)
                {
                    //удаляем/отвязываем
                    doDeleteFile($this,$file['id']);
                }

            }

            $material=MaterialGet($this, $this->values->id);

        }


        //добавление файла
        if($this->values->doaddfile == '1')
        {
            $resarr=DoSaveFile($this);
            //print_r($resarr);
            //формируем массив материала
            $material=MaterialGet($this, $this->values->id);

            $this->data->errortext.=$resarr[1];
        }

        //связка файла
        if($this->values->fileforconnect != '')
        {
            DoConnectFile($this);
            $material=MaterialGet($this, $this->values->id);
        }

        //изменение файла
        if($this->values->doeditfile == '1')
        {
            DoEditFile($this);
            //формируем массив материала
            $material=MaterialGet($this, $this->values->id);
        }

        //связка материала
        if(($this->values->doconnectmaterial == '1')&&($this->values->matforconnect != ''))
        {
            DoConnectMaterial($this);
            //формируем массив материала
            $material=MaterialGet($this, $this->values->id);
        }




        //добавление контакты
        if($this->values->doaddcont == '1')
        {
            DoAddCont($this);
            //формируем массив материала
            $material=MaterialGet($this, $this->values->id);
        }

        //добавление расписания контакта
        if($this->values->doaddshed != '')
        {
            DoAddShed($this);
            //формируем массив материала
            $material=MaterialGet($this, $this->values->id);
        }

        break;
    case 'list':
        $this->data->iH1='Администрирование материалов';

        if($this->values->imenu != '')
        {
            $query='SELECT "name" FROM "material_types" WHERE "id" = '.$this->values->imenu;
            if($res=$this->dbquery($query))
            {
                $this->data->iH1.=' "'.$res[0]['name'].'"';
            }
        }

        if($this->values->page == '')
        {
            $this->values->page=1;
        }

        $this->values->perpage=1000;

        //получаем список материалов
        $this->data->materials=MaterialList($this, $this->values->imenu, $this->values->page, $this->values->perpage, $this->values->status_id);

        break;
}



//селектор языка
$language_selector='';
$query='SELECT * FROM "languages" ORDER BY "id" ASC';
if($res=$this->dbquery($query))
{
    foreach($res as $row)
    {

        if((isset($this->values->language_id))&&($this->values->language_id == $row['id']))
        {
            $selected='SELECTED';
        }
        else
        {
            $selected='';
        }

        $language_selector.="<option value='$row[id]' $selected>$row[name]</option>";
    }
}

//селектор статуса
$status_selector='';
$query='SELECT * FROM "material_statuses" ORDER BY "id" ASC';
if($res=$this->dbquery($query))
{
    foreach($res as $row)
    {

        if((isset($this->values->status_id))&&($this->values->status_id == $row['id']))
        {
            $selected='SELECTED';
        }
        else
        {
            $selected='';
        }

        $status_selector.="<option value='$row[id]' $selected>$row[name]</option>";
    }
}


//селектор типов файлов
$file_type_selector='';
$query='SELECT * FROM "file_types" ORDER BY "id" ASC';
if($res=$this->dbquery($query))
{
    foreach($res as $row)
    {
        if((isset($this->values->file_type_id))&&($this->values->file_type_id == $row['id']))
        {
            $selected='SELECTED';
        }
        else
        {
            $selected='';
        }
        $file_type_selector.="<option value='$row[id]' $selected>$row[name]</option>";
    }
}


//подключаем tinymce
$this->data->morejs = "<script type=\"text/javascript\" src=\"".$this->settings->templateurl."/js/tinymce/tinymce.js\"></script>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"".$this->settings->templateurl."/css/pickmeup.css\">
        <script type=\"text/javascript\" src=\"".$this->settings->templateurl."/js/jquery.pickmeup.min.js\"></script>
        ";

$this->data->jsdocready = "
        $('#date_event').datetimepicker({
            lang:'ru',
            timepicker:true,
            format:'d.m.Y H:i'
        });

        $('.dateinput').datetimepicker({
            lang:'ru',
            timepicker:true,
            format:'d.m.Y H:i'
        });

        tinymce.PluginManager.load('moxiecut', '".$this->settings->templateurl."/js/tinymce/plugins/moxiecut/plugin.min.js');
        tinymce.init({
					selector: \"textarea:not(.notiny)\",
					language : 'ru',
					plugins: [
						\"advlist autolink lists link charmap print preview anchor\",
						\"searchreplace visualblocks code fullscreen\",
						\"insertdatetime media table contextmenu paste moxiecut\"
					],
					toolbar: \"undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image\",
					autosave_ask_before_unload: false,
					height : 200,
					relative_urls: false,
					valid_styles: {
					    '*': 'border,text-decoration,font-size,color'
					},
                    images_upload_url: '/?menu=ajax&imenu=saveUploadedFileTinyMCE'
				});
        ";

//формируем хлебные крошки
$this->data->breadcrumbs.='&rarr;<a href="/?menu=admin">Администрирование</a>';

$this->values->typename='';
//определяем выбранный тип материала
if($this->values->imenu != '')
{
    $query='SELECT "name" FROM "material_types" WHERE "id" = '.$this->values->imenu;
    if($res=$this->dbquery($query))
    {
        if(count($res) > 0)
        {
            $this->values->typename=$res[0]['name'];
            if($this->values->action == 'list')
            {
                $this->data->breadcrumbs.='&rarr;<b>'.$res[0]['name'].'</b>';
            }
            else
            {
                $this->data->breadcrumbs.='&rarr;<a href="/?menu=editmaterials&action=list&imenu='.$this->values->imenu.'">'.$res[0]['name'].'</a>';
            }
        }
    }
}

if($this->values->action == 'add')
{
    $this->data->breadcrumbs.='&rarr;<b>Добавление материала (шаг 1)</b>';
}
elseif(($this->values->action == 'edit')&&($this->values->id != ''))
{
    $this->data->breadcrumbs.='&rarr;<b>'.$material['name'].' (изменение)</b>';
}