<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


function MaterialTypesResetActive(\iSite $site)
{
    global $normaltypes;

    $site->logWrite(LOG_DEBUG, __FUNCTION__, '_');

    //перекэшируем массив типов материалов с активными материалами
    $normaltypes = Array();

    $query = <<<EOS
WITH RECURSIVE types(id) AS(
    SELECT DISTINCT "type_id" FROM "materials" WHERE "status_id" = $1
    UNION
    SELECT mt.parent_id
    FROM
        "material_types" "mt",
        types
    WHERE
        mt.parent_id IS NOT NULL
        AND mt.parent_id != 0
        AND "mt"."id" IN(types.id)
)
SELECT id FROM "types"
EOS;

    $res = $site->dbquery($query, array(STATUS_ACTIVE), true);

    $site->logWrite(LOG_DEBUG, __FUNCTION__, __LINE__);

    if ( ! empty($res)) {
        foreach ($res as $item)
            $normaltypes[] = $item['id'];
    }

    $site->getLogger()->write(LOG_INFO, __FUNCTION__.':'.__LINE__, "nt={".implode(',', $normaltypes).'}');

    ArrCacheWrite($site, $normaltypes, 'normal_material_types');
}

function MaterialTypeExtraAdd(\iSite $site, $type_id, $param_name, $valuetype, $valuemattype)
{
    //$query = 'SELECT nextval(\'extra_mattypes_id_seq\') "id"';
    //$res = $res = $site->dbquery($query);


    $query="SELECT MAX(id) mid FROM extra_mattypes";
    if($res=$site->dbquery($query))
    {
        if(is_array($res) && count($res) > 0)
        {
            $id = (int)$res[0]['mid'];

            $id += 1;

        }
        else
        {
            return array(
                false,
                '- ошибка: сбой базы данных('.$site->getLogger()->write(LOG_ERR, __FUNCTION__, __LINE__).')',
            );
        }
    }
    else
    {
        return array(
            false,
            '- ошибка: сбой базы данных('.$site->getLogger()->write(LOG_ERR, __FUNCTION__, __LINE__).')',
        );
    }

    //формируем id
    /*if (empty($res)) {
        return array(
            false,
            '- ошибка: сбой базы данных('.$site->getLogger()->write(LOG_ERR, __FUNCTION__, __LINE__).')',
        );
    }*/

    //$id = $res[0]['id'];

    if (empty($valuemattype))
        $valuemattype = null;

    $res = DbInsert($site, 'extra_mattypes', array(
        'id' => $id,
        'type_id' => $type_id,
        'name' => $param_name,
        'valuetype' => $valuetype,
        'valuemattype' => $valuemattype
    ));

    if ( ! empty($res)) {
        $site->callHook('materialtypeextraadd', array(
            'id' => $id,
            'type_id' => $type_id,
            'name' => $param_name,
            'valuetype' => $valuetype,
            'valuemattype' => $valuemattype,
        ));

        return array(
            true,
            '- доп. характеристика добавлена'
        );
    }

    return array(
        false,
        '- ошибка: сбой базы данных('.$site->getLogger()->write(LOG_ERR, __FUNCTION__, pg_last_error($site->getDb())).')',
    );
}

function MaterialTypeExtraSelectorAdd(\iSite $site, $type_id, $selector_type_id)
{
    /*$query = 'SELECT nextval(\'extra_selectors_id_seq\') "id"';
    $res = $site->dbquery($query);

    if (empty($res)) {
        return array(
            false,
            ' - ошибка: сбой базы данных('.$site->getLogger()->write(LOG_ERR, __FUNCTION__, __LINE__).')',
        );
    }*/


    $query="SELECT MAX(id) mid FROM extra_selectors";
    if($res=$site->dbquery($query))
    {
        if(is_array($res) && count($res) > 0)
        {
            $mid = (int)$res[0]['mid'];

            $mid += 1;

        }
        else
        {
            return array(
                false,
                '- ошибка: сбой базы данных('.$site->getLogger()->write(LOG_ERR, __FUNCTION__, __LINE__).')',
            );
        }
    }
    else
    {
        return array(
            false,
            '- ошибка: сбой базы данных('.$site->getLogger()->write(LOG_ERR, __FUNCTION__, __LINE__).')',
        );
    }



    //$mid = $res[0]['id'];

    $res = DbInsert($site, 'extra_selectors', array(
        'id' => $mid,
        'type_id' => $type_id,
        'mattype_id' => $selector_type_id,
    ));

    if ( ! empty($res)) {
        $site->callHook('materialtypeextraselectoradd', array(
            'id' => $mid,
            'type_id' => $type_id,
            'mattype_id' => $selector_type_id,
        ));

        return array(
            true,
            ' - тип материала характеристик добавлен',
        );
    }

    return array(
        false,
        ' - ошибка: сбой базы даныых('.$site->getLogger()->write(
            LOG_ERR, __FUNCTION__, pg_last_error($site->getDb())).')',
    );
}

function MaterialTypeExtraDelete(\iSite $site, $param_id)
{
    //удаляем значения
    if (false === DbDelete($site, 'extra_materials', array('type_id' => $param_id))) {
        $log_id = $site->getLogger()->write(LOG_ERR, __FUNCTION__, __LINE__.'/ '.pg_last_error($site->getDb()));
        // @todo собрать удаленные значения типа valuemat и вызвать хук
        return array(
            false,
            'Ошибка - не удалось удалить значения характеристики ('.$log_id.')',
        );
    }

    if (false !== DbDelete($site, 'extra_mattypes', array('id' => $param_id))) {
        $site->callHook('materialtypeextradelete', array(
            'id' => $param_id
        ));

        return array(
            true,
            'Характеристика удалена!',
        );
    }

    $log_id = $site->getLogger()->write(LOG_ERR, __FUNCTION__, __LINE__.'/ '.pg_last_error($site->getDb()));

    return array(
        false,
        'Ошибка - не удалось удалить характеристику ('.$log_id.')',
    );
}

function MaterialTypeExtraSelectorDelete(\iSite $site, $selector_id)
{
    if (DbDelete($site, 'extra_selectors', array('id' => $selector_id))) {
        $site->callHook('materialtypeextraselectordelete', array(
            'id' => $selector_id,
            //'type_id' => $site->values->type_id,
        ));

        return array(
            true,
            'Тип материала характеристик удален',
        );
    }

    $log_id = $site->getLogger()->write(LOG_ERR, __FUNCTION__, pg_last_error($site->getDb()));

    return array(
        false,
        'Ошибка при удалении типа материала характеристик ('.$log_id.')',
    );
}

function MaterialTypeExtraUpdate(\iSite $site, $param_id, $name, $valuetype, $valuemattype)
{
    if ($valuemattype == '' || $valuetype != 'valuemat')
        $valuemattype = null;

    $columns = array(
        'name' => $name,
        'valuetype' => $valuetype,
        'valuemattype' => $valuemattype,
    );

    $res = DbUpdate($site, 'extra_mattypes', $columns, array('id' => $param_id));

    if ($res === false) {
        $log_id = $site->getLogger()->write(LOG_ERR, __FUNCTION__, pg_last_error($site->getDb()));

        return array(
            false,
            '- ошибка при соханении доп. характеристики: сбой базы данных('.$log_id.')',
        );
    }

    if ($res) {
        $site->callHook('materialtypeextraupdate', array(
            'id' => $param_id,
            'attributes' => $columns,
        ));
    }


    return array(
        true,
        'Доп. характеристика изменена',
    );
}

function getMaterialTypeIdByCharId(\iSite $site, $charId){
    $query = <<<EOS
SELECT "id" FROM "material_types" WHERE "id_char" = $1
EOS;

    $q_params = array($charId);

    return $site->dbquery($query, $q_params);
}

function getMaterialTypeCharIdById(\iSite $site, $id){
    $query = <<<EOS
SELECT "id_char" FROM "material_types" WHERE "id" = $1
EOS;

    $q_params   = array($id);
    $resArray   = $site->dbquery($query, $q_params);

    return $resArray ? $resArray[0] : false;
}