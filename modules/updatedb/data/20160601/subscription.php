<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$query_list = array();

$query_list[] = <<<EOS
CREATE TABLE IF NOT EXISTS subscribers(
	id SERIAL PRIMARY KEY,
	email CHARACTER VARYING(255) NOT NULL,
	token_unsubscribe CHARACTER VARYING(255),
	time TIMESTAMP NOT NULL DEFAULT NOW(),
	CONSTRAINT c_email UNIQUE (email)
);
EOS;

$query_list[] = <<<EOS
CREATE TABLE IF NOT EXISTS subscription_sendqueue(
	subscriber_id INTEGER NOT NULL,
	material_id BIGINT NOT NULL,
	schedule_time TIMESTAMP NOT NULL,
	PRIMARY KEY (subscriber_id,material_id)
);
EOS;

foreach ($query_list as $query) {
    $this->resetError();
    try {
        $this->dbquery($query);
    } catch (\Exception $ex) {
        printf("(%d) %s\n", $ex->getCode(), $ex->getMessage());
    }
}
