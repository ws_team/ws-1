<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\base;

trait Hookable {
    private $hooks = array();
    private $priorityCounters = array();

    public function hook($name, $handler, $priority = 1000)
    {
        if ( ! isset($this->hooks[$name])) {
            $this->hooks[$name] = array();
        }

        if (in_array($handler, $this->hooks[$name]))
            return;

        $priority_key = $name.$priority;

        $priority_counter = isset($this->priorityCounters[$priority_key]) ?
            $this->priorityCounters[$priority_key] :
            0;

        $key = strval($priority).'.'.$priority_counter;
        $this->hooks[$name][$key] = $handler;
        ksort($this->hooks[$name], SORT_NUMERIC);

        $this->priorityCounters[$priority_key] = $priority_counter + 1;
    }

    public function unhook($name, $handler)
    {
        if ( ! isset($this->hooks[$name]))
            return;
        $key = array_search($handler, $this->hooks[$name]);
        if ($key !== false) {
            unset($this->hooks[$name][$key]);
        }
    }

    /**
     * @param string $name
     * @param array|object $params
     * @return object  hook state
     */
    public function callHook($name, $params = array())
    {
        $hookState = (object)$params;

        if ( ! empty($this->hooks[$name])) {
            foreach ($this->hooks[$name] as $handler) {
                call_user_func($handler, $hookState);
                if ( ! empty($hookState->cancel))
                    break;
            }
        }

        return $hookState;
    }
}