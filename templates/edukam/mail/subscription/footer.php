<?php
/**
 * @var \iSite $this
 * @var string $unsubscribe_url
 * @var string $subscriptionTitle
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if (empty($subscriptionTitle))
    $subscriptionTitle = 'электронную рассылку портала Хабаровского края.';

?>
Вы подписаны на <?= $subscriptionTitle ?><br />
Чтобы отписаться от рассылки, пройдите по ссылке <a href="<?= htmlspecialchars($unsubscribeUrl) ?>"><?=
    htmlspecialchars($unsubscribeUrl)?></a>