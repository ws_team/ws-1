<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


if (!defined('MATTYPE_USEFULRESRC'))
    define('MATTYPE_USEFULRESRC', 35);

if (!defined('MATTYPE_VIDEO'))
    define('MATTYPE_VIDEO', 30);
if (!defined('MATTYPE_PHOTO'))
    define('MATTYPE_PHOTO', 31);

if (!defined('MATTYPE_GOVERNOR'))
    define('MATTYPE_GOVERNOR', 7);
if (!defined('MATTYPE_HEAD'))
    define('MATTYPE_HEAD', 7);
if (!defined('MATTYPE_HEADSPEECH'))
    define('MATTYPE_HEADSPEECH', 16);
if (!defined('MATTYPE_DIRECTION'))
    define('MATTYPE_DIRECTION', 9);

if (!defined('MATTYPE_SIGHT'))
    define('MATTYPE_SIGHT', 5);

if (!defined('MATTYPE_SIGNEVENT'))
    define('MATTYPE_SIGNEVENT', 55);

// @todo drop:
if (!defined('MATTYPE_SIGNEVENT_PARAM_TYPE'))
    define('MATTYPE_SIGNEVENT_PARAM_TYPE', 14);
if (!defined('MATTYPE_SIGNEVENT_PARAM_TYPE_EACHYEAR'))
    define('MATTYPE_SIGNEVENT_PARAM_TYPE_EACHYEAR', 46);
if (!defined('MATTYPE_SIGNEVENT_PARAM_TYPE_ONLYYEAR'))
    define('MATTYPE_SIGNEVENT_PARAM_TYPE_ONLYYEAR', 47);

if (!defined('MATTYPE_ACCREDFORM'))
    define('MATTYPE_ACCREDFORM', 0);

if (!defined('MATTYPE_ANNOUNCE'))
    define('MATTYPE_ANNOUNCE', 29);
if (!defined('MATTYPE_ANNOUNCE'))
    define('MATTYPE_ANNOUNCE', 28);
if (!defined('MATTYPE_EVENT'))
    define('MATTYPE_EVENTS', 27);
if (!defined('MATTYPE_EVENTS'))
    define('MATTYPE_EVENTS', 27);

if ( ! defined('MATTYPE_NEWS'))
    define('MATTYPE_NEWS', 28);

