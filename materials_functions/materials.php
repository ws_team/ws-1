<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


use wpf\exception\db\QueryFailed;

function DeleteMaterial(\iSite $site, $id)
{
    $deleted = DbUpdate($site, 'materials', array('status_id' => STATUS_DELETED), array('id' => $id));

    if ($deleted) {
        $site->callhook('materialdelete', array(
            'id' => $id,
            'physical' => false,
        ));
    }

    return (bool)$deleted;
}


// 2018-10-24 Denis T. Remove next number calculation by db sequence. All calculations met with common material add function. 
function MaterialNew(\iSite $site)
{
    $query = 'SELECT max(id) id FROM materials';
    $res = $site->dbquery($query);
    if(empty($res)){
	return false;
    }else{
	$id = $res[0]['id'] + 1;
    } 


    //$query = 'SELECT nextval(\'materials_id_seq\') nextid';
    //$res = $site->dbquery($query);
    //if (empty($res))
    //    return false;

    //$id = intval($res[0]['nextid']);
    
    // $site->getLogger()->write(LOG_INFO, __FUNCTION__, 'New material ID = '.$id.'.');
    // echo $id;

    $success = DbInsert($site, 'materials', array(
        'id' => $id,
        'status_id' => STATUS_HIDDEN
    ));

    return $success ? $id : false;
}


function MaterialGetNewCharId(\iSite $site, $type_id, $name)
{
    $id_char = mb_transliterate($name);
    $id_char = str_replace(' ','_',$id_char);

    $query = <<<EOS
SELECT count("id") "cid" FROM "materials" WHERE "id_char" = $1 AND type_id = $2
EOS;
    $q_params = array($id_char, $type_id);

    $res = $site->dbquery($query, $q_params);
    if ( ! empty($res)) {
        if($res[0]['cid'] > 0)
            $id_char .= time();
    }

    return $id_char;
}

function DbUpsert(\iSite $site, $table, $columns, $pk)
{
    $q_insfields = array();
    $q_insvals = array();
    $q_updvals = array();
    $q_params = array_values($columns);

    $cur_param = 1;
    foreach ($columns as $col => $val) {
        $q_updvals[] = '"'.$col.'" = $'.$cur_param;
        $q_insfields[] = '"'.$col.'"';
        $q_insvals[] = '$'.$cur_param;

        ++$cur_param;
    }
    $q_updvals = implode(',', $q_updvals);

    $q_pk = array();
    foreach ($pk as $col => $val) {
        $val_ph = '$'.$cur_param;
        $q_pk[] = '"'.$col.'" = '.$val_ph;
        $q_params[] = $val;
        $q_insfields[] = '"'.$col.'"';
        $q_insvals[] = $val_ph;

        $cur_param++;
    }
    $q_pk = implode(' AND ', $q_pk);
    $q_insvals = implode(',', $q_insvals);
    $q_insfields = implode(',', $q_insfields);

    $q_update = <<<EOS
UPDATE "$table"
SET
    $q_updvals
WHERE
    $q_pk
RETURNING *
EOS;

    $query = <<<EOS
WITH upsert AS($q_update)
INSERT INTO "$table"($q_insfields)
SELECT $q_insvals
WHERE NOT EXISTS(
    SELECT 1 FROM upsert
)
EOS;

    $res = $site->dbquery($query, $q_params);
    if ($res === false)
        return false;

    return pg_affected_rows($res);
}

/**
 * @param iSite $site
 * @param string $table
 * @param array $columns
 * @param array $condition
 * @return int  Число затронутых записей
 */
function DbUpdate(\iSite $site, $table, $columns, $condition)
{
    list($query, $q_params) = DbBuildUpdateQuery($table, $columns, $condition);
    $res = $site->dbquery($query, $q_params);
    return $res === false ? false : pg_affected_rows($res);
}

/**
 * @param \iSite $site
 * @param string $table
 * @param array $columns
 * @return bool
 */
function DbInsert(\iSite $site, $table, $columns)
{
    $pg = $site->getDb();
    //if ( ! )
    return pg_insert($pg, $table, $columns);
    /*
    list($query, $q_params) = DbBuildInsertQuery($table, $columns);
    return $site->dbquery($query, $q_params) !== false;
    */
}

/*function DbInsertAutoid(\iSite $site, $table, $columns)
{
    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'table, {}#'.count($columns));
    if (empty($columns['id'])) {
        $id_seq = $table.'_id_seq';
        $query = <<<EOS
        SELECT nextval('$id_seq'::regclass) "id"
EOS;
        $res = $site->dbquery($query);
        if (empty($res)) {
            $site->logWrite(LOG_ERR, __FUNCTION__, '..failed to fetch next rowid');
            return false;
        }

        $columns['id'] = $res[0]['id'];
    }

    $res = DbInsert($site, $table, $columns);
    if ($res === false) {
        $site->logWrite(LOG_ERR, __FUNCTION__, '..failed to insert');
        return false;
    }

    $site->logWrite(LOG_DEBUG, __FUNCTION__, '..inserted');
    return $columns['id'];
}*/

function DbInsertAutoid(\iSite $site, $table, $columns)
{
    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'table, {}#'.count($columns));
    if (empty($columns['id'])) {
        $id_seq = $table.'_id_seq';
        /*$query = <<<EOS
        SELECT nextval('$id_seq'::regclass) "id"
EOS;*/

        $newid=0;

        $query='SELECT max(id)+1 mid FROM '.$table;

        //echo ' - '.$query;

        $res = $site->dbquery($query);

        if($res)
        {
            if(count($res) > 0)
            {
                $newid = $res[0]['mid'];
            }
        }



        /*if (empty($res)) {
            $site->logWrite(LOG_ERR, __FUNCTION__, '..failed to fetch next rowid');
            return false;
        }*/

        //$columns['id'] = $res[0]['id'];

        $columns['id'] = $newid;
    }

    $res = DbInsert($site, $table, $columns);
    if ($res === false) {
        $site->logWrite(LOG_ERR, __FUNCTION__, '..failed to insert');
        return false;
    }

    $site->logWrite(LOG_DEBUG, __FUNCTION__, '..inserted as '.$columns['id']);
    return $columns['id'];
}

/**
 * @param iSite $site
 * @param string $table
 * @param array $condition
 * @return int
 */
function DbDelete(\iSite $site, $table, $condition)
{
    list($query, $q_params) = DbBuildDeleteQuery($table, $condition);
    $res = $site->dbquery($query, $q_params);
    return $res === false ? false : pg_affected_rows($res);
}

function DbBuildDeleteQuery($table, $condition)
{
    list($q_cond, $q_params) = DbBuildQueryCondition($condition);
    $query = 'DELETE FROM "'.$table.'"'.( ! empty($q_cond) ? ' WHERE '.$q_cond : '');
    return array($query, $q_params);
}

/**
 * @param string $table
 * @param array $columns
 * @param array|string $condition
 * @return array (string query, array params)
 */
function DbBuildUpdateQuery($table, $columns, $condition)
{
    $q_params = array();
    $q_set = array();

    foreach ($columns as $col => $val) {
        $assign = '"'.$col.'" = ';
        if (is_array($val) && $val[0] == '!') {
            $q_set[] = $assign.$val[1];
        } else {
            $q_params[] = $val;
            $q_set[] = $assign.'$'.count($q_params);
        }
    }

    list($q_cond, $q_params) = DbBuildQueryCondition($condition, $q_params);

    if (empty($q_set))
        return array('SELECT 1', array());

    $query = 'UPDATE "'.$table.'" SET '.implode(',', $q_set);
    if ( ! empty($q_cond))
        $query .= ' WHERE '.$q_cond;

    return array($query, $q_params);
}

function DbBuildQueryCondition($condition, $q_params = array())
{
    $q_cond = array();
    foreach ($condition as $column => $value) {
        $q_params[] = $value;
        $q_cond[] = '"'.$column.'" = $'.count($q_params);
    }
    $q_cond = implode(' AND ', $q_cond);
    return array($q_cond, $q_params);
}

function DbBuildInsertQuery($table, $columns)
{
    $q_params = array();
    $q_fields = array();
    $q_vals = array();
    foreach ($columns as $col => $val) {
        $q_fields[] = '"'.$col.'"';
        if (is_array($val) && $val[0] == '!') {
            $q_vals[]= $val[1];
        } else {
            $q_params[] = $val;
            $q_vals[] = '$'.count($q_params);
        }
    }

    $query = 'INSERT INTO "'.$table.'"('.implode(',', $q_fields).') VALUES('.
        implode(',', $q_vals).')';

    return array($query, $q_params);
}

function MaterialInsertParam(\iSite $site, $mat_id, $param_id, $param_type, $value)
{
    return MaterialParamSet($site, $mat_id, $param_id, $param_type, $value) !== false;
}

function MaterialParamSet(\iSite $site, $mat_id, $param_id, $param_type, $value)
{
    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'mat='.$mat_id.';param='.$param_id.';ptype='.$param_type.';value='.$value);
    $valuename = $valuedate = $valuemat = $valuetext = null;

    switch ($param_type) {
        case 'valuemat':
            $valuemat = $value;
            break;
        default:
        case 'valuename':
            $valuename = $value;
            break;
        case 'valuedate':
            $valuedate = $value;
            break;
    }

    $query = <<<EOS
WITH upsert(material_id, type_id, valuename, valuedate, valuemat, valuetext, selector_id) AS (
    UPDATE extra_materials
    SET
        valuename = $3,
        valuedate = $4,
        valuemat = $5,
        valuetext = $6
    WHERE
        material_id = $1
        AND type_id = $2
    RETURNING
        material_id, type_id, valuename, valuedate, valuemat, valuetext, NULL
)
INSERT INTO extra_materials(material_id, type_id, valuename, valuedate, valuemat, valuetext, selector_id)
SELECT $1, $2, $3, $4, $5, $6, NULL
WHERE NOT EXISTS (SELECT 1 FROM upsert)
EOS;

    $q_params = array(
        $mat_id,
        $param_id,
        $valuename,
        $valuedate,
        $valuemat,
        $valuetext,
    );

    $res = $site->dbquery($query, $q_params);
    if ($res === false) {
        $site->logWrite(LOG_ERR, __FUNCTION__,
            'query failed q=<<---'.$query.'--->>;params='.serialize($q_params).';error='.pg_last_error());
        return false;
    }

    return pg_affected_rows($res);
}

function MaterialListNames(\iSite $site, $type_id, $lang_id = LANG_RU)
{
    $name_field = $lang_id == LANG_RU ? 'name' : 'en_name name';
    $q = <<<EOS
SELECT id, $name_field FROM materials WHERE type_id = $1 AND status_id = $2
ORDER BY name ASC
EOS;

    $q_params = array($type_id, STATUS_ACTIVE);
    return $site->dbquery($q, $q_params);
}

function MaterialGetParamDefs($site, $type_id)
{
    return MaterialParamDefGetall($site, $type_id);
}

function MaterialParamDefGetall(\iSite $site, $type_id)
{
    $q = 'SELECT id, "name", valuetype, valuemattype FROM extra_mattypes WHERE type_id = $1 ORDER BY "name"';
    $res = $site->dbquery($q, array($type_id));

    $ret = array();
    if ( ! empty($res)) {
        foreach ($res as $row) {
            $ret[$row['id']] = $row;
        }
    }

    return $ret;
}

function getMaterialMaxFileOrderNum(\iSite $site, $materialId){
    $query = <<<end
SELECT MAX(files.ordernum)
FROM files
INNER JOIN file_connections
ON files.id = file_connections.file_id and file_connections.material_id = $1
end;

    $params = array($materialId);

    $res = $site->dbquery($query, $params);

    return $res ? $res[0] : null;
}
