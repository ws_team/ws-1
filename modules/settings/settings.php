<?php
/**
 * Настройки сайта, редактируемые через админку.
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


if ($this->getUser()->can('admin.settings')) {

    require_once(dirname(__FILE__).'/functions.php');

    $this->getPostValues(array('action'));
    $action = !empty($this->values->action) ? $this->values->action : 'list';
    $valid_actions = array('list', 'edit', 'add', 'delete');
    if (!in_array($action, $valid_actions)) {
        $this->error->flag = 1;
        $this->error->text = 'Действие не поддерживается';
        $action = $valid_actions[0];
    }

    $this->values->action = $action;

    require('action/'.$action.'.php');
}
