<?php


if (file_exists($_SERVER['DOCUMENT_ROOT'].'/defines-local.php'))
    require_once($_SERVER['DOCUMENT_ROOT'].'/defines-local.php');

if (file_exists($_SERVER['DOCUMENT_ROOT'].'/defines.php'))
    require_once($_SERVER['DOCUMENT_ROOT'].'/defines.php');

if (defined('WPF_OIV') && constant('WPF_OIV') && file_exists(WPF_ROOT.'/defines-oiv.php'))
    require_once(WPF_ROOT.'/defines-oiv.php');

if (defined('WPF_OMSU') && constant('WPF_OMSU') && file_exists(WPF_ROOT.'/defines-omsu.php'))
    require_once(WPF_ROOT.'/defines-omsu.php');

if (file_exists(WPF_ROOT.'/defines-any-local.php'))
    require_once(WPF_ROOT.'/defines-any-local.php');

if (file_exists(WPF_ROOT.'/defines-any.php'))
    require_once(WPF_ROOT.'/defines-any.php');
