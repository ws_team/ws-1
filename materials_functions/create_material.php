<?php
/**
 * @var \iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


use wpf\exception\db\QueryFailed;

require_once(WPF_ROOT.'/classes/wpf/exception/db/QueryFailed.php');

function materialIdCharCheckUnique(\iSite $site, $id_char, $id = null)
{
    //проверяем id_char на валидность
    $q_params = array($id_char);

    if (is_null($id)) {
        $query = 'SELECT count("id") "cid" FROM "materials" WHERE "id_char" = $1';
    } else {
        $query = 'SELECT count("id") "cid" FROM "materials" WHERE "id_char" = $1 AND "id" <> $2';
        $q_params[] = $id;
    }

    $res = $site->dbquery($query, $q_params);

    if (empty($res)) {
        throw new QueryFailed($query, $q_params);
    }

    return $res[0]['cid'] == 0;
}

/**
 * @param iSite $site
 * @param ?string $id
 * @return array
 */
function createMaterialGetAttributes(\iSite $site, $id)
{
    $columns = array();

    $date_add = $date_edit = $date_event = null;

    $name = $site->values->name;
    // $name = str_replace('"', '&quot;', $name);

    if (isset($site->values->id_char)) {
        $id_char = $site->values->id_char;
    } else {
        $id_char = '';
    }

    if ($id_char == '') {
        $id_char = mb_transliterate($name);
        $id_char = str_replace('"', '-quot-', $id_char);
        $id_char = trim(preg_replace('/-{2,}/', '-', $id_char), '-');
        $id_char = str_replace(' ','_',$id_char);
    }
    if ( ! materialIdCharCheckUnique($site, $id_char, $id)) {
        $id_char .= time();
    }

    $type_id = $site->values->type_id;

    if(isset($site->values->language_id)) {
        $language_id = $site->values->language_id;
    } else {
        $language_id='';
    }

    if (empty($language_id)) {
        $language_id = LANG_RU;
    }

    $anons = $site->values->anons;

    $content = $site->values->content;

    $content = strip_tags(
        $content,
        '<p>'.
        '<a>'.
        '<li><ul><ol>'.
        '<div><span>'.
        '<table><tbody><tr><td><th>'.
        '<i><em><strong><b><sup><sub>'.
        '<small><big>'.
        '<h2><h3><h4><h5><h6>'.
        '<form><font><hr><br><img><u><strike><s>'.
        '<ins><del>'.
        '<blockquote><q><cite>'.
        '<pre>'.
        '<iframe>'.
        '<figure><figcaption>'
    );

    if (empty($id)) {
        $date_add = 'SYSDATE';
    }

    $date_edit = 'SYSDATE';

    if( ! isset($site->values->date_event)) {
        $site->values->date_event = '';
    }

    //date_event
    if ($site->values->date_event == '') {
        $date_event = 'SYSDATE';
    } else {
        $tz = new \DateTimeZone(DB_TIMEZONE);
        $date = \DateTime::createFromFormat('d.m.Y H:i', $site->values->date_event, $tz);
        if ( ! $date)
            $date = new \DateTime('now', $tz);
        $date_event = $date->format('Y-m-d H:i:s');
    }

    if (empty($site->values->author_id)) {
        $author_id = null;
    } else {
        $author_id = $site->values->author_id;
    }

    if (empty($site->values->status_id)) {
        $status_id = STATUS_HIDDEN;
    } else {
        $status_id = $site->values->status_id;
    }

    if ( ! isset($site->values->extra_number)) {
        $site->values->extra_number='';
    }

    if (empty($site->values->extra_number)) {
        $extra_number = null;
    } else {
        $extra_number = $site->values->extra_number;
    }

    if ( ! isset($site->values->extra_number2)) {
        $site->values->extra_number2 = '';
    }

    if (empty($site->values->extra_number2)) {
        $extra_number2 = null;
    } else {
        $extra_number2 = $site->values->extra_number2;
    }

    if (!isset($site->values->extra_text1)) {
        $site->values->extra_text1 = '';
    }
    if (empty($site->values->extra_text1)) {
        $extra_text1 = '';
    } else {
        $extra_text1 = $site->values->extra_text1;
    }

    if ( ! isset($site->values->extra_text2))
    {
        $site->values->extra_text2 = '';
    }
    if (empty($site->values->extra_text2)) {
        $extra_text2 = '';
    } else {
        $extra_text2 = $site->values->extra_text2;
    }

    if (!isset($site->values->parent_id)) {
        $site->values->parent_id = '';
    }
    if(!empty($site->values->parent_id)){
        $parent_id = $site->values->parent_id;
    } else {
        $parent_id = null;
    }

    $sysdate = new \DateTime('now', new \DateTimeZone(DB_TIMEZONE));
    $sysdate = $sysdate->format('Y-m-d H:i:s');

    if ($date_add == 'SYSDATE')
        $date_add = $sysdate;
    if ($date_edit == 'SYSDATE')
        $date_edit = $sysdate;
    if ($date_event == 'SYSDATE')
        $date_event = $sysdate;

    $columns['name'] = $name;
    $columns['id_char'] = $id_char;
    $columns['anons'] = $anons;
    $columns['content'] = $content;
    $columns['type_id'] = $type_id;
    $columns['user_id'] = $site->getUser()->getId();
    $columns['language_id'] = $language_id;
    $columns['status_id'] = $status_id;
    $columns['parent_id'] = $parent_id;
    $columns['extra_number'] = $extra_number;
    $columns['extra_number2'] = $extra_number2;
    $columns['extra_text1'] = $extra_text1;
    $columns['extra_text2'] = $extra_text2;
    $columns['author_id'] = $author_id;
    $columns['last_redactor_email'] = isset($_SESSION['autorize']['email']) ? $_SESSION['autorize']['email'] : '';
    if ( ! empty($date_add))
        $columns['date_add'] = $date_add;
    $columns['date_event'] = $date_event;
    if ( ! empty($date_edit))
        $columns['date_edit'] = $date_edit;

    return $columns;
}

/**
 * @param iSite $site
 * @return array(bool|int, string)
 */
function createMaterial(\iSite $site)
{
    $id = empty($site->values->id) ? null : $site->values->id;

    $is_new = empty($id);

    $site->logWrite(LOG_DEBUG, __LINE__.' is_new='.$is_new, __METHOD__);

    if (empty($site->values->type_id) || empty($site->values->name)) {
        return array(
            false,
            ' - ошибка: Пустое наименование либо тип материала!'
        );
    }

    $columns = createMaterialGetAttributes($site, $id);

    $site->logWrite(LOG_DEBUG, __LINE__.' columns=#'.count($columns), __METHOD__);

    //пробуем добавить/сохранить
    if ($is_new) {
        //$query = 'SELECT nextval(\'materials_id_seq\') id';

        $query= 'SELECT max(id) id FROM materials';

        $res = $site->dbquery($query);
        if (empty($res)) {
            $id=0;//throw new QueryFailed($query);
        }else{
            $id = $res[0]['id']+1;
        }

        $columns['id'] = $id;

        $err = false === DbInsert($site, 'materials', $columns);
        $logaction = 'Добавление материала';
    } else {
        $logaction = 'Изменение материала';
        $err = false === DbUpdate($site, 'materials', $columns, array('id' => $id));
    }

    if ($err) {
        return array(
            false,
            ' - ошибка: не выполнился скрипт записи материала! - ',
        );
    }

    SaveLog($site, $logaction, $id);

    $site->logWrite(LOG_DEBUG, __LINE__, __METHOD__);

    if ($is_new) {
        $site->callHook('materialcreate', array(
            'id' => $id,
            'type_id' => $columns['type_id'],
            'attributes' => $columns,
        ));
    } else {
        if (isset($columns['status_id']) && $columns['status_id'] == STATUS_DELETED) {
            $site->callHook('materialdelete', array(
                'id' => $id,
                'physical' => false,
            ));
        } else {
            $site->callHook('materialchange', array(
                'id' => $id,
                'type_id' => $columns['type_id'],
                'attributes' => $columns,
            ));
        }
    }

    $site->callHook('materialsave', array(
        'id' => $id,
        'type_id' => $columns['type_id'],
        'attributes' => $columns,
    ));

    return array($id, ' - Выполнено успешно: материал сохранен.');
}

function MaterialBind(\iSite $site, $id)
{
    $bound_mats = $site->getUser()->getAttribute('bound_materials');

    if ( ! empty($bound_mats)) {
        foreach ((array)$bound_mats as $mat_id)
            MaterialAddConnection($site, $id, $mat_id);
    }
}

function MaterialAddConnection(\iSite $site, $parent_id, $child_id)
{
    $query = <<<EOS
INSERT INTO "material_connections" ("material_parent", "material_child")
    SELECT $1, $2
    WHERE NOT EXISTS(
        SELECT 1 FROM material_connections
        WHERE material_parent = $1 AND material_child = $2
    )
EOS;

    $q_params = array($parent_id, $child_id);
    $res = $site->dbquery($query, $q_params);

    if ( ! empty($res) && pg_affected_rows($res)) {
        $site->callHook('materialconnect', array(
            'parent_id' => $parent_id,
            'child_id' => $child_id,
        ));
    }
}

function MaterialsAreConnected(\iSite $site, $matid1, $matid2)
{
    $query = <<<EOS
SELECT count("id") "cnt"
FROM (
    SELECT "mp"."material_parent" "id"
    FROM "material_connections" "mp"
    WHERE "mp"."material_child" = $1 AND "mp"."material_parent" = $2

    UNION ALL

    SELECT "mc"."material_child" "id"
    FROM "material_connections" "mc"
    WHERE "mc"."material_parent" = $1 AND "mc"."material_child" = $2
) matconn
EOS;

    $cnt = 0;
    $res = $site->dbquery($query, array($matid1, $matid2));

    if ( ! empty($res)) {
        $cnt = intval($res[0]['cnt']);
    }

    return $cnt > 0;
}
