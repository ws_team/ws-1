<?php


//переназначаем некоторые настройки сайта=============================================
$this->data->title=''.$this->data->title;
$this->data->iH1='';
$this->data->keywords='тут, ключевые, слова';
$this->data->description='тут описание раздела';
$this->data->breadcrumbsmore='';

$khabkrai = $this;
include(WPF_ROOT . '/specialver.php');

// выполняем нужные бизнесс-процессы===================================================

$this->data->content='';
$this->data->name='';

include_once($this->settings->path.'/components/materials/material_types_array.php');

// Задаем максимальную длинну анонсов и названий
$maxLength = 100;

// Подгружаем новости
$newsList = MaterialList($this, 10, 1, 5, STATUS_ACTIVE);
$bigNews = 0;
$shortNews = array();

uasort($newsList, 'sortNewsByDate');

if (count($newsList[0]) > 0) {
    $ids='';
    foreach($newsList as $nl)
    {
        if($ids != '')
        {
            $ids.=',';
        }
        $ids.=$nl['id'];
    }

    $carr=Array();
    $query='SELECT "t"."cid" FROM (SELECT "material_child" "cid", "material_parent" "pid" FROM "material_connections" WHERE "material_parent" IN ('.$ids.')
    UNION ALL
    SELECT "material_parent" "cid", "material_child" "pid" FROM "material_connections" WHERE "material_child" IN ('.$ids.')) t,
    "materials" "m" WHERE "m"."type_id" = 1 AND "m"."id" = "t"."cid"';
    if($res=$this->dbquery($query))
    {
        if(count($res) > 0)
        {
            foreach($res as $row)
            {
                $carr[]=$row['cid'];
            }
        }
    }

    foreach($newsList as $news)
    {
        $fullNews = $news;

        $fullNews['name'] = cutString($news['name'], $maxLength);
        $fullNews['anons'] = cutString($news['anons'], $maxLength);
        //$fullNews['isConnectedWithJK'] = isConnectedWithJK($news);

        if(in_array($news['id'],$carr))
        {
            $fullNews['isConnectedWithJK']=true;
        }
        else
        {
            $fullNews['isConnectedWithJK']=false;
        }


        if($bigNews == 0 && $news['extra_number'] == 1)
            {$bigNews = $fullNews;
                //$bigNews['isConnectedWithJK']=
            }
        else array_push($shortNews, $fullNews);
    }
}

// Подгружаем объявления
$adsList = MaterialList($this, 11, 1, 2, STATUS_ACTIVE);

if (count($adsList[0]) > 0) {
    foreach($adsList as $ads) {
        $ads['name'] = cutString($ads['name'], $maxLength);
    }
}
// Подгружаем строящиеся объекты для карусели
$tmpBuildingsList = MaterialList($this, 1, 1, 100, 2);
$buildingsList = array();

if (count($tmpBuildingsList[0]) > 0) {
    foreach ($tmpBuildingsList as $building) {
        // Выбираем из БД дату сдачи ЖК
        //$query = 'SELECT MAX("valuedate") FROM "extra_materials" WHERE "selector_id" = 10 AND "material_id" IN (SELECT "material_parent" FROM "material_connections" WHERE "material_child" = '.$building['id'].' UNION ALL (SELECT "material_child" FROM "material_connections" WHERE "material_parent" = '.$building['id'].'))';

        $tmp=$building;

        //дата сдачи

        $query="SELECT max(t.valuedate) enddate FROM (
    SELECT e.valuedate FROM extra_materials e, material_connections c
    WHERE e.selector_id IS NOT NULL AND e.type_id IN (6,8) AND e.material_id = c.material_parent AND c.material_child = ".$building['id']."
    
    UNION ALL
    
    SELECT e.valuedate FROM extra_materials e, material_connections c
    WHERE e.selector_id IS NOT NULL AND e.type_id IN (6,8) AND e.material_id = c.material_child AND c.material_parent = ".$building['id'].") t";

        if($res=$this->dbquery($query))
        {
            if(count($res) > 0)
            {
                $tmp['endDate'] = $res[0]["enddate"];
            }
        }

        $tmp['contacts']=Array();
        //аддресс
        $query='SELECT "c"."addr", "c"."building", "m"."name" "cityname"
        FROM "contacts" "c", "materials" "m"
        WHERE "m"."id" = "c"."city" AND "c"."material_id" = '.$building['id'];
        if($res=$this->dbquery($query))
        {
            if(count($res) > 0)
            {
                $tmp['contacts']=$res;
            }
        }

        array_push($buildingsList, $tmp);
    }
}

uasort($buildingsList, 'sortJKByEndDate');

$this->data->morejs="<script type=\"text/javascript\" src=\"".$this->settings->templateurl."/js/carousel.js\"></script>";
$this->data->jsdocready = "initCarousel(".count($buildingsList).");";

function sortNewsByDate($a, $b)
{
    $date1 = $a['unixtime'];
    $date2 = $b['unixtime'];

    if ($date1 == $date2) {
        return 0;
    }
    return ($date1 > $date2) ? -1 : 1;
}

function sortJKByEndDate($a, $b)
{
    if($a['endDate'] != "")
    {
        $str1 = mb_substr($a['endDate'], 0, mb_strpos($a['endDate'], ' '));
        $tdate1 = DateTime::createFromFormat('Y-m-d', $str1, new DateTimeZone('Asia/Vladivostok'));
        $date1 = $tdate1->getTimestamp();
    }
    else
    {
        $date1 = PHP_INT_MAX;
    }

    if($b['endDate'] != "")
    {
        $str2 = mb_substr($b['endDate'], 0, mb_strpos($b['endDate'], ' '));
        $tdate2 = DateTime::createFromFormat('Y-m-d', $str2, new DateTimeZone('Asia/Vladivostok'));
        $date2 = $tdate2->getTimestamp();
    }
    else
    {
        $date2 = PHP_INT_MAX;
    }

    if ($date1 == $date2) {
        return 0;
    }
    return ($date1 < $date2) ? -1 : 1;
}
