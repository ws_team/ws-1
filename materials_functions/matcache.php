<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


//функция удаления кэша материала
function delMatCache(\iSite $site, $id)
{
    //очищаем кэш материала
    $uncache = array($id);

    //очищаем кэши связанных материалов
    $query = <<<EOS
SELECT "material_parent" "id"
FROM "material_connections"
WHERE "material_child" = $1
UNION ALL
SELECT "material_child" "id"
FROM "material_connections"
WHERE "material_parent" = $1
EOS;

    $q_params = array($id);
    $res = $site->dbquery($query, $q_params);

    if ( ! empty($res)) {
        foreach($res as $row) {
            $uncache[] = $row['id'];
        }
    }

    //очищаем кэши материалов, у которых значения характеристик - очищаемый материал
    $query = 'SELECT "material_id" "id" FROM "extra_materials" WHERE "valuemat" = $1';
    $res = $site->dbquery($query, array($id));

    if ( ! empty($res)) {
        foreach ($res as $row) {
            $uncache[] = $row['id'];
        }
    }

    foreach ($uncache as $id) {
        $site->getCache()->delete('material:'.$id);
    }

    unset($redis);
}

/**
 * Сброс кэша материала, и всех непосредственных связанных материалов. @see delMatCache
 *
 * @param iSite $site
 * @param $id
 */
function delMatCacheNeighbors(\iSite $site, $id)
{
    delMatCache($site, $id);
    //у связанных материалов
    $querycache = <<<EOS
SELECT "material_parent" "id"
FROM "material_connections"
WHERE "material_child" = $1

UNION ALL

SELECT "material_child" "id"
FROM "material_connections"
WHERE "material_parent" = $1
EOS;

    $rescache = $site->dbquery($querycache, array($id));

    if ( ! empty($rescache)) {
        foreach($rescache as $mc) {
            delMatCache($site, $mc['id']);
        }
    }
}