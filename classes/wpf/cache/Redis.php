<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

namespace wpf\cache;

require_once('CacheInterface.php');

class Redis implements CacheInterface {
    private $redis;
    private $connParams;
    private $keyPrefix;

    /**
     * @param array= $connectionParams
     * @param string= $keyPrefix
     */
    public function __construct($connectionParams = array(), $keyPrefix = '')
    {
        $this->connParams = $connectionParams;
        $this->keyPrefix = $keyPrefix;
    }

    public function get($key)
    {
        return $this->getConnection()->get($this->keyPrefix.$key);
    }

    public function has($key)
    {
        return $this->getConnection()->has($this->keyPrefix.$key);
    }

    public function set($key, $value, $expire = null)
    {
        $this->getConnection()->set($this->keyPrefix.$key, $value, $expire);
    }

    public function delete($key)
    {
        $this->getConnection()->del($this->keyPrefix.$key);
    }

    protected function getConnection()
    {
        if (is_null($this->redis)) {
            global $redis;
            if (is_null($redis)) {
                if ($this->connParams instanceof \Yampee_Redis_Client) {
                    $redis = $this->connParams;
                } else {
                    $host = 'localhost';
                    $port = 6379;
                    if ( ! empty($this->connParams)) {
                        if ( ! empty($this->connParams['host']))
                            $host = $this->connParams['host'];
                        if ( ! empty($this->connParams['port']))
                            $port = $this->connParams['port'];
                    }
                    $redis = new \Yampee_Redis_Client($host, $port);
                }
            }
            $this->redis = $redis;
        }
        return $this->redis;
    }

    public function hset($hname, $hkey, $value, $expire = null)
    {
        $redis = $this->getConnection();
        $redis->hashSet($this->keyPrefix.$hname, $hkey, $value);
        $redis->set($this->getHashKeyPrefix($hname).$hkey, $value, $expire);
    }
    
    public function hget($hname, $hkey = null)
    {
        $ret = null;
        $redis = $this->getConnection();
        $hnameFull = $this->keyPrefix.$hname;
        $hKeyPrefix = $this->getHashKeyPrefix($hname);

        if (is_null($hkey)) {
            $ret = array();
            $hash = $redis->send('hgetall', array($hnameFull));
            if ( ! empty($hash)) {
                $key = null;
                foreach ($hash as $hashItem) {
                    if (is_null($key)) {
                        $key = $hashItem;
                        $keyFull = $hKeyPrefix.$key;
                        if ($redis->has($keyFull)) {
                            $ret[$key] = $redis->get($keyFull);
                        }
                    } else {
                        $key = null;
                    }
                }
            }
        } else {
            $hkeyFull = $hKeyPrefix.$hkey;
            if ($redis->has($hkeyFull))
                $ret = $redis->get($hkeyFull);
        }

        return $ret;
    }

    public function hdel($hname, $hkey = null)
    {
        $redis = $this->getConnection();
        $hnameFull = $this->keyPrefix.$hname;
        $hashKeyPrefix = $this->getHashKeyPrefix($hname);

        if (is_null($hkey)) {
            $keys = $redis->send('hkeys', array($hnameFull));
            if ( ! empty($keys)) {
                $args = $keys;
                array_unshift($args, $hnameFull);
                $redis->send('hdel', $args);

                foreach ($keys as $key) {
                    $redis->del($hashKeyPrefix.$key);
                }
            }
        } else {
            $redis->hashDelete($hnameFull, $hkey);
            $redis->del($hashKeyPrefix.$hkey);
        }
    }

    protected function getHashKeyPrefix($hname)
    {
        return $this->keyPrefix.$hname.'#';
    }

    function getGlobal($key)
    {
        $ret = null;
        $redis = $this->getConnection();
        if ($redis->has($key)) {
            $ret = $redis->get($key);
        }

        return $ret;
    }

    function hasGlobal($key)
    {
        return $this->getConnection()->has($key);
    }

    function setGlobal($key, $val, $lifetime = null)
    {
        $this->getConnection()->set($key, $val, $lifetime);
    }

    function deleteByPrefix($prefix)
    {
        $keyMask = $this->keyPrefix.$prefix.'*';
        $redis = $this->getConnection();
        $keys = $redis->findKeys($keyMask);
        foreach ((array)$keys as $key) {
            $redis->del($key);
        }
    }

    function countByPrefix($prefix)
    {
        $keyMask = $this->keyPrefix.$prefix.'*';
        return count($this->getConnection()->findKeys($keyMask));
    }
}