<?php

$statData = getAuthorsStat($this);

$this->data->stat = Array();

if($statData) {
    $this->data->stat = $statData;
}

$this->data->jsdocready = "

        $('#startdate').datetimepicker({
            lang:'ru',
            timepicker:true,
            format:'d.m.Y'
        });

        $('#enddate').datetimepicker({
            lang:'ru',
            timepicker:true,
            format:'d.m.Y'
        });

        ";