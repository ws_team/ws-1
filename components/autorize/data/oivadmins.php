<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

return array(
    'oivadmin' => array(
        'pass' => 'FGfj34aV',
    ),
    'zadmin' => array(// тестовый
        'pass' => 'afjiJE0w12XNwq',
        'materialtypes' => array(21, 170),
        'role' => ROLE_CMANAGER,
    ),
    'qadmin' => array(// тестовый
        'pass' => '0920nhowWM8x',
        'role' => ROLE_CMANAGER,
        'bound_materials' => array(23),// region khabarovsk city
    ),
    'system.update' => array(
        'id' => 1000000001,
        'pass' => 'j8npyl1Ob0qT92cuxeCAlG77YzwsbvGhgOWHpn5hTEbVngLO',
        'role' => ROLE_ADMIN,
        'roles' => array('admin', 'cmanager', 'dev'),
        'nologin' => true,// запрещен обычный вход (через UI)
    ),
    'system.daemon' => array(
        'id' => 1000000002,
        'pass' => 'w/c3LH76G1huUYs7BZXIbB+X',
        'role' => ROLE_ADMIN,
    ),
);