<?php
/**
 * @var \iSite $this
 * @var array $material
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


if (!empty($material['anons'])) {
    $announceCutLen = 240;
    $anons = strip_tags($material['anons']);
    $anons = str_replace('"', '', $anons);
    if (mb_strlen($anons) > 240) {
        $anons = mb_substr($anons, 0, 238).'...';
    }
    $this->setPageMeta('description', $anons);
}

if (!empty($material['photos'])) {
    $urlPrefix = 'http://'.$_SERVER['HTTP_HOST'];
    $imageUrl = $urlPrefix.'/photos/'.$material['photos'][0]['id'].'_x600.jpg';
    $this->setPageMeta('image', $imageUrl);
}
