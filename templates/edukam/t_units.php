<?php


global $units;


include_once($this->locateTemplate('f_header'));
?>

<div class="container container--admin-title">
    <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
</div>

<div class="contentblock basemargin contentblock_page-list-editor">

    <p class="errortext"><?php echo $this->data->errortext; ?></p>

    <?php

    if (isset($units) && count($units) > 0)
    { ?>

        <div class="titles-form-inputs">Название подразделения</div>

        <?php

        foreach($units as $unit)
        {
            echo "<form class='form form_page-list-editor' method='post'>

                        <div class='form__inputs'>
                            <input type='hidden' name='action' value='edit'>
                            <input type='hidden' name='id' value='$unit[id]'>
                            <input type='text' name='name' value='$unit[name]' class=\"form-control\" placeholder='Название подразделения'>
                        </div>
              
                        <div class='form__buttons'>
                        
                            <button type=\"submit\" class=\"btn btn-default showtooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Сохранить изменения\">
                                <span class=\"glyphicon glyphicon-pencil\"></span>
                            </button>
                            
                            <a class='link_delete-item' href='/?menu=units&action=delete&id=$unit[id]'>
                                <button type=\"button\" class=\"btn btn-danger showtooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"\" data-original-title=\"Удалить подразделение\">
                                    <span class=\"glyphicon glyphicon-remove-circle\"></span>
                                </button>
                            </a> 
                            
                        </div>
                    
                    </form>";
        } ?>

        <?php
    } ?>

    <h2 class="adminSubtitle contentblock_title_add-new">Добавить подразделение:</h2>
    <div class="titles-form-inputs">
        <div class="titles-form-inputs__item">Название подразделения</div>
    </div>

    <?php

    echo "<form class='form form_page-list-editor' method='post'>

                <div class='form__inputs'>
                    <input type='text' name='name' class=\"form-control\" placeholder='Название подразделения'>
                </div>
      
                <input type='hidden' name='action' value='add'>
                
                <input type='submit' class='styler styler_btn-add-new' value='Добавить'>
            
          </form>";

    ?>

</div>

<?php

include_once($this->locateTemplate('f_footer'));
