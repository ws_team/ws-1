<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

return array(
    array(
        'deny',
        'editmaterials',
        'callback' => '\wpf\components\autorize\editmaterials_restrict_subdivision',
    ),
    array(
        'allow',
        'editmaterials',
        'role' => 'cmanager',
    ),
    array(
        'allow',
        'role' => 'admin',
    ),
    array(
        'allow',
        'usersettings',
        'role' => 'cmanager',
    ),
    array(
        'deny',
    ),
);
