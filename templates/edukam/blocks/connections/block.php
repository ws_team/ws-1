<?php

namespace connections { // имя namespace должно соответствовать типу блока (названию папки)

    // Функция которая возвращает
    function block_arguments() {
        return array(
            1 => '(array) Материал, для которого необходимо вывести все связи'
            );
    }

    // Функция, которая генерирует контент блока и возвращает его при вставке на страницу через Blocks API
    function block_content($material) {
        global $khabkrai;

        ob_start();

        if (array_key_exists('connected_materials', $material)
            && count($material['connected_materials'])) {
            ?><div class="connected-materials"><?php

            usort($material['connected_materials'], "connections\cmp_materials_by_typeid");

            $items = array();

            foreach ($material['connected_materials'] as $num => $cmaterial) {
                if (in_array((int)$cmaterial['type_id'], array(162, 46, 19, 21))) {

                    if ($cmaterial['type_id'] == 21) {
                        $cmaterial = GetRegionOMSU($khabkrai, $cmaterial['id']);
                    }

                    $items['villages'][] = $cmaterial;
                } else {
                    $items[$cmaterial['type_id']][] = $cmaterial;
                }
            }

            foreach ($items as $tid => $group) {
                ?><div class="material material-type-<?= $tid ?>">
                    <span class="title">
                        <?php
                        foreach ($group as $num => $grouped_cmaterial) {

                        	if($grouped_cmaterial['url'] == '/governor/1222'){
		                        $grouped_cmaterial['url'] = '/governor/governer-news';
	                        }


                            ?><a href="<?=
                                htmlspecialchars($grouped_cmaterial['url'])
                                ?>"><?= $grouped_cmaterial['name'] ?></a><?php
                        }
                        ?>
                    </span>
                </div>
                <?php
            }

            if (!is_array($material['extra_text1']) && $material['extra_text1'] != '') {
                $material['extra_text1'] = explode(',', $material['extra_text1']);
                foreach ($material['extra_text1'] as $num => $keyword) {
                    $material['extra_text1'][$num] = trim($keyword);
                }
            }

            if (is_array($material['extra_text1'])
                && count($material['extra_text1'])
                && $material['extra_text1'][0] != '') {

                $urlPrefix = $urlPrefix = GetMaterialTypeUrl(
                    $khabkrai,
                    $khabkrai->data->atype
                );
                $urlPrefix .= strpos($urlPrefix, '?') !== false ? '&' : '?';
                $junction = '';

                ob_start();

                foreach ($material['extra_text1'] as $num => $keyword) {
                    $keyword = trim($keyword);
                    ?><?= $junction ?><a href="<?=
                    htmlspecialchars($urlPrefix.'keywords='.urlencode($keyword)) ?>"><?=
                        $keyword ?></a><?php
                    // $junction = ', ';
                }

                $title = ob_get_clean();

                ?>
                <div class="material material-type-keywords">
                    <span class="title"><?= $title ?></span>
                </div>
                <?php
            }

            ?></div><?php
        }

        return ob_get_clean();
    }

    function cmp_materials_by_typeid($a, $b)
    {

        if (intval($a['type_id']) == intval($b['type_id'])) {
            return 0;
        }
        return (intval($a['type_id']) < intval($b['type_id'])) ? -1 : 1;
    }

}