<?php

global $localize;

if ($this->data->language != LANG_RU) {
    header("Location: /khabarovsk-krai/Istoriya-kraya/Simvolika-kraya/");
    die();
}

if (!isset($localize))
    $localize = array();

$progs = MaterialList($this, 67, 1, 100, STATUS_ACTIVE);
$pcnt = count($progs);
$pwidth = $pcnt*246;
$pages = (int)($pcnt/4);

if($pages*4 < $pcnt)
{
    ++$pages;
}

?>
<div class="doubleblock" style="padding:0; margin: 0 auto;"></div>
<div class="contentblock m5_carus" style="height: 245px;">
    <div class="main_gprogs">
        <div class="nullblock">
            <div class="main_gprogs_scroll" style="width: <?php echo $pwidth; ?>px;">
                <?php
                foreach($progs as $prog) {
                    $imageInfo = FileGetInfo($this, $prog['photo']);
                    ?>
                    <div onclick="document.location.href='<?=
                        htmlspecialchars($prog['url']) ?>';"
                        class="governor-programm" style="float: left; position: relative; z-index: 0; cursor: pointer;">
                        <div class="xmedia mt-image s246x245">
                            <img style="position: absolute; z-index: -1;" src="/photos/<?= $prog['photo'] ?>_xy246x245.jpg"
                                 class="media-content">
                            <span class="media-textview">Изображение: <?=
                                htmlspecialchars($imageInfo['name']) ?></span>
                        </div>
                        <div style="z-index: 1; line-height: 245px;">
                            <div style="display: inline-block; vertical-align: middle; line-height: 1.2;">
                                <div style="color: #fff; font-size: 18px; width: 246px;"></div>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="nullblock">
        <div class="guber_groups_pages" style="text-align: center; width: 984px; left: 0px; top: -26px; position: absolute;">
            <?php
            for ($i=1; $i<=$pages; ++$i) {
                $astyle='';
                if ($i == 1)
                {
                    $astyle='active';
                }
                ?>
                <div class='normalblock main_groups_page <?= $astyle ?>' id='main_groups_page_<?= $i ?>'
                     onclick='ChangeGuberProgPage(<?= $i ?>);'></div>
                <?php
            }

            ?>
        </div>
        <div style="position: absolute; padding: 20px 10px; margin: 0 8px; background: rgba(0,0,0,0.4); border-radius: 10px; width: 340px; height: 35px; top: -160px; left: 310px;  color: #ffffff; text-align: center; font-size: 20px;">
            <?=
            localize('Чудеса Хабаровского края',$this->data->language,$localize)
            ?>
        </div>
    </div>
</div>


<div class="contentblock" style="min-height: 560px;">
    <div class="nullblock">
        <div class="main_region_news"></div>
        <div class="main_regions_someline"></div>
        <div class="main_regions">
            <p style="font-size: 17px; padding-left: 30px; padding-top: 10px;"><?=
                localize('География края',$this->data->language,$localize)
                ?></p>
            <?php
            //получаем список регионов края
            $query = <<<EOS
                SELECT * FROM "materials"
                WHERE "status_id" = $2
                    AND "type_id" = $1
                    AND "language_id" = $3
                ORDER BY "id" ASC
EOS;
            $q_params = array(21, STATUS_ACTIVE, $this->data->language);

            if($res=$this->dbquery($query, $q_params))
            {
                if(count($res) > 0)
                {
                      $javaarr='var backarr = {';
                      $contentarr='var contentarr = {';

                      $ji=0;
                      foreach($res as $row)
                      {
                          ++$ji;
                          $row['anons']=str_replace('<p>','',$row['anons']);
                          $row['anons']=str_replace('</p>','',$row['anons']);

                          ?>
                          <div class='option' onmouseout="map_colorRegion('<?= $row['anons'] ?>', '#b4b4b4'); map_hideRegionInfo();"
                               onclick="document.location.href='/events/news/?region=<?= $row['id'] ?>';"
                               onmouseover="map_moveRegionInfo(100,200); map_colorRegion('<?= $row['anons'] ?>', '#0071aa'); map_changeContent('<?= $row['anons'] ?>'); map_showRegionInfo();"
                               id='option_region_<?= $row['id'] ?>'><?= $row['name'] ?></div>
                          <?php

                          //формируем обратный массив соответстввий javascript
                          if($ji != 1)
                          {
                              $javaarr.=', ';
                          }

                          $javaarr.="'$row[anons]': $row[id]";

                          //получаем последнюю новость по региону
                          $queryreg = <<<EOS
                                SELECT "m".*
                                FROM "materials" "m"
                                WHERE "m"."id" = (
                                    SELECT MAX("i"."id")
                                    FROM "materials" "i"
                                    WHERE "i"."type_id" = $1
                                        AND "i"."status_id" = $2
                                        AND "i"."date_event" >= (CURRENT_TIMESTAMP - INTERVAL '30 SECOND')
                                        AND "i"."id" IN (
                                            SELECT "material_parent"
                                            FROM "material_connections"
                                            WHERE "material_child" = $3
                                            UNION ALL
                                            SELECT "material_child"
                                            FROM "material_connections"
                                            WHERE "material_parent" = $3
                                        )
                                    )
EOS;

                          $regnew = Array();
                          $q_params = array(2, STATUS_ACTIVE, $row['id']);

                          if ($resreg = $this->dbquery($queryreg, $q_params)) {
                              if((count($resreg) > 0))
                              {
                                  $regnew = $resreg;
                              }
                          }

                          if ($ji != 1)
                          {
                              $contentarr.=', ';
                          }

                          //рисуем последнюю новость региона
                          if ((count($regnew) > 0)&&(isset($regnew['name']))) {
                              $newcontent='<div class="main_smallnews_content">'.$regnew['name'].'</div>';
                              $contentarr.="'$row[anons]': '$newcontent'";
                          } else {//рисуем логотип региона
                              $newcontent='<div class="main_smallnews_content"><img src="/templates/khabkrai/img/regions/'.$row['id'].'.png" width="123" height="150" align="left"><strong>'.$row['name'].'</strong></div>';
                              $contentarr.="'$row[anons]': '$newcontent'";
                          }
                      }
                    $javaarr.='};';
                    $contentarr.='};';

                    print("<script>$javaarr
                    $contentarr</script>");
                }
            }

            ?>
        </div>
        <div class="main_map">
            <div class="SmallRegionInfo" id="SmallRegionInfo">&nbsp;</div>
            <object data="/templates/khabkrai/img/map.svg" type="image/svg+xml" width="400" height="558" id="map"></object>
        </div>
    </div>
    <div class="main_important_header" style="background-color:#07a467; width: 338px">&nbsp;&nbsp;&nbsp;&nbsp;<?=
        localize('Новости', $this->data->language, $localize)
        ?></div>
    <div class="main_region_news">
        <?php

        $mquery='SELECT "c"."material_parent" "mid" FROM "material_connections" "c"
        WHERE "c"."material_child" IN
            (SELECT "m"."id" FROM "materials" "m" WHERE "m"."type_id" = 21)
            AND "c"."material_parent" IN
            (SELECT "mz"."id" FROM "materials" "mz" WHERE "mz"."type_id" IN (164,2))


            UNION ALL

            SELECT "co"."material_child" "mid" FROM "material_connections" "co"
        WHERE "co"."material_parent" IN
            (SELECT "ma"."id" FROM "materials" "ma" WHERE "ma"."type_id" = 21)
            AND "co"."material_child" IN
            (SELECT "mzo"."id" FROM "materials" "mzo" WHERE "mzo"."type_id" IN (164,2))';

        $ids='yeap';

        if ($ids != '') {
            //получаем список новостей регионов
            $mnarr = MaterialList($this, '', 1, 5, STATUS_ACTIVE, '', '', $mquery);

            $mi=0;

            $mainimp='';

            foreach($mnarr as $material)
            {
                ++$mi;

                $stylewidth='';

                ?>
                <div class='main_material_list' style='width: 315px; height: auto;'><?php

                    $material['date_event']=russian_datetime_from_timestamp($material['unixtime']);

                    //определяем регион новости
                    $cmats = MaterialList($this, 21, 1, 2, STATUS_ACTIVE, $material['cids'], 21);

                    if (count($cmats) > 0) {
                        $regionname=', '.$cmats[0]['name'];
                    } else {
                        $regionname='';
                    }

                    ?>
                    <div class='main_material_list_content' onclick="document.location.href='<?=
                        htmlspecialchars($material['url']) ?>'">
                        <p class='material_list_date'><?= $material['date_event'].$regionname ?></p>
                        <p class='material_list_name' style='font-size: 13px; padding: 0px;'><?= $material['name'] ?></p>
                    </div>
                </div>
                <?php
            }
        }

        ?>
        <div class="widget-news-link" style="margin: 25px 0 30px 20px;">
            <a href="/events/news/"><?=
                localize('Смотреть все', $this->data->language, $localize)
                ?></a>
        </div>
    </div>
</div>
<div class="SmallRegionInfo" id="SmallRegionInfo">&nbsp;</div>
