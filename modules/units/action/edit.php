<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if ($this->values->id != '') {
    $subquery = '';

    if ($this->values->name != ''){
        $subquery.=' name = '."'".$this->values->name."'";
    }

    $query = 'UPDATE units SET '.$subquery.' WHERE id = '.$this->values->id;

    $res = $this->dbquery($query);

    if ($res) {
        $this->data->errortext.='<p style="color: green;">Изменения сохранены!</p>';
    }
    else{
        $this->data->errortext.='<p style="color: #ff0000;">Ошибка изменения подразделелния (не удалось записать изменения в БД).</p>';
    }
}